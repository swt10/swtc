#include <cstring>
#include <fstream>
#include <iostream>
#include <string>

#include "ASMBuilder.h"
#include "AST.h"
#include "FlexLexer.h"
#include "IRModule.h"
#include "PassManager.h"
#include "config.h"
#include "parser.hpp"
#include "preParse.h"
#include "regAlloc.h"


#define COLOR_RESET "\033[0m"
#define COLOR_BLACK "\033[30m"              /* Black */
#define COLOR_RED "\033[31m"                /* Red */
#define COLOR_GREEN "\033[32m"              /* Green */
#define COLOR_YELLOW "\033[33m"             /* Yellow */
#define COLOR_BLUE "\033[34m"               /* Blue */
#define COLOR_MAGENTA "\033[35m"            /* Magenta */
#define COLOR_CYAN "\033[36m"               /* Cyan */
#define COLOR_WHITE "\033[37m"              /* White */
#define COLOR_BOLDBLACK "\033[1m\033[30m"   /* Bold Black */
#define COLOR_BOLDRED "\033[1m\033[31m"     /* Bold Red */
#define COLOR_BOLDGREEN "\033[1m\033[32m"   /* Bold Green */
#define COLOR_BOLDYELLOW "\033[1m\033[33m"  /* Bold Yellow */
#define COLOR_BOLDBLUE "\033[1m\033[34m"    /* Bold Blue */
#define COLOR_BOLDMAGENTA "\033[1m\033[35m" /* Bold Magenta */
#define COLOR_BOLDCYAN "\033[1m\033[36m"    /* Bold Cyan */
#define COLOR_BOLDWHITE "\033[1m\033[37m"   /* Bold White */

extern yyFlexLexer lexer;
extern int yyparse();

// #define   ir_file  std::cerr

#define COLOR_MAIN " ======== "

#define ADD_ARG(str, var, val)           \
    {                                    \
        if (strcmp(argv[i], str) == 0) { \
            var = val;                   \
            continue;                    \
        }                                \
    }

int main(int argc, char **argv) {
    bool nr = false;
    bool isO1 = false;
    bool isO2 = false;
    isNoRunPass = false;

    for (int i = 1; i < argc; i++) {
        if (argv[i][0] != '-') {
            // it's input_file path;
            if (isInputFilePathGot == false) {
                isInputFilePathGot = true;
                input_file_path = argv[i];
            } else {
                logError("more than one input file path indicated");
                return 1;
            }
        } else {
            //  indicate output file path
            if (strcmp(argv[i], "-o") == 0) {
                if (isOutputFilePathGot) {
                    logError("more than one output file path indicated");
                    return 1;
                }

                i++;
                if (i >= argc) {
                    logError("missing output file path");
                    return 1;
                }

                output_file_path = argv[i];
                isOutputFilePathGot = true;
                continue;
            }

            // print ast
            if (strcmp(argv[i], "-a") == 0) {
                isPrintAst = true;
                ast_file.open(ast_file_path,
                              std::ios::trunc |
                                  std::ios::in |
                                  std::ios::out);
                continue;
            }

            // print IR to ir.ll
            if (strcmp(argv[i], "-r") == 0) {
                isPrintIR = true;
                ir_file.open(IR_file_path,
                             std::ios::trunc |
                                 std::ios::in |
                                 std::ios::out);
                continue;
            }

            // do not genIR
            ADD_ARG("-nr", nr, true)
            // print inst list in ir.ll
            ADD_ARG("-ri", isPrintIRInst, true)
            // print bb list in ir.ll
            ADD_ARG("-rb", isPrintIRBB, true)
            // print pred and succ in ir.ll
            ADD_ARG("-rp", isPrintPredSucc, true)
            // run pass
            ADD_ARG("-np", isNoRunPass, true)
            // mask global arrays
            ADD_ARG("-ng", isRead, true)

            ADD_ARG("-g", isCodeGen, true)

            ADD_ARG("-S", isCodeGen, true)

            ADD_ARG("-O1", isNoRunPass, false)

            ADD_ARG("-mem2reg", isMem2Reg, true)

            ADD_ARG("-dr", isDebugRegAlloc, true)

            ADD_ARG("-O2", isO2, true)

            // unknown arguments
            logError(std::string("unknown arguments ") + std::string(argv[i]));
            return 1;
        }
    }

    if (!isInputFilePathGot) {
        logError(std::string("none input file path indicated"));
        return 1;
    }

    char tmpPath[] = "tmp.cpp";
    preParse(input_file_path.c_str(), tmpPath);

    // got input file;
    // start parse;
    freopen(tmpPath, "r", stdin);

    lexer.yyset_lineno(1);
    std::cerr << COLOR_BOLDBLUE;
    yyparse();
    std::cerr << COLOR_RESET;
    // print AST;

    if (root == nullptr) {
        std::cerr << COLOR_MAIN << " root null \n ";
    } else
        std::cerr << COLOR_MAIN << " root not null \n";

    if (isPrintAst) {
        if (ast_file) {
            std::cerr << COLOR_MAIN << "start to print AST\n";
            root->print();
        } else {
            logError("illegal AST file path: ");
            logError(ast_file_path);
            return 1;
        }
    }

    if (!nr) {
        root->genIR();
    }

    //Pass
    SWTC::run_passes(mo, true);

    // if (isDebugRegAlloc) {
    //     runRegAlloc(mo);
    // }

    if (isPrintIR) {
        if (ir_file) {
            std::cerr << COLOR_MAIN << "start to print IR\n"
                      << (mo == nullptr ? "but mo == null" : "");
            ir_file << mo;
            std::cerr << COLOR_MAIN << "end start to print IR\n";
        } else {
            logError("illegal ir file path: ");
            logError(IR_file_path);
            return 1;
        }
    }

    if (isCodeGen) {
        asm_file.open(
            output_file_path,
            std::ios::trunc |
                std::ios::in |
                std::ios::out

        );
        if (asm_file) {
            std::cerr << COLOR_MAIN << "Start to generate target assembly.\n";
            ASMBuilder asm_builder(mo);
            asm_builder.build();
            std::cerr << COLOR_MAIN << "Start to generate target assembly.\n";
            asm_file << std::string(asm_builder);
            std::cerr << COLOR_MAIN << "Codegen finished, exit.\n";
            asm_file.close();
        }
    }

    delete root;
    // delete mo;
    return 0;
}