#include "GVN.h"

extern std::map<Value *, std::list<MemoryAccess *>> VtoM;

bool anyof(Instruction *inst) {
    BasicBlock *p = inst->getParent();
    for (int i = 0; i < inst->getNumOperands(); i++) {
        if (auto I = dyn_cast<Instruction>(inst->getOperand(i))) {
            if (I->getParent() != p) return false;
        } else {
            return false;
        }
    }
    return true;
}

bool GVNimpl::isCritical(Instruction *inst, BasicBlock *bb) {
    auto br = dyn_cast<BranchInst>(inst);
    if (!br || br->getNumSuccessors() == 1) return false;
    bb->pred_init();
    return (bb->pred.size()) > 1;
}

BasicBlock *GVNimpl::splitCriticalEdge(Instruction *inst, int i) {
    BranchInst *br = dyn_cast<BranchInst>(inst);
    if (!br) return nullptr;
    if (!isCritical(br, br->getSuccessor(i))) return nullptr;
    BasicBlock *succ = br->getSuccessor(i);
    BasicBlock *pred = br->getParent();

    //TODO need loopinfo

    BasicBlock *nbb = new BasicBlock(pred->getName() + "." + succ->getName() + "_crit_edge");
    BranchInst *nbi = new BranchInst(succ, nbb);
    Function *f = pred->getParent();
    nbb->setParent(f);
    f->getBasicBlockList().insertAfter(nbb, pred);
    br->setSuccessor(i, nbb);
    //TODO cfg修改
    //如果succ里有phi节点
    int bidx = 0;
    for (auto it = succ->getInstList().head; it; it = it->next) {
        if (PHINode *pn = dyn_cast<PHINode>(it)) {
            if (pn->getIncomingBlock(bidx) != pred)
                bidx = pn->getBasicBlockIndex(pred);
            pn->setIncomingBlock(bidx, nbb);
        }
    }
    //TODO 重边考虑
    return nbb;
}

BasicBlock *GVNimpl::findcriticaledge(BasicBlock *pred, BasicBlock *succ) {
    Instruction *t = pred->getTerminator();
    for (int i = 0;; i++) {
        assert(i != 3 && "findcriticaledge error!");
        if (auto br = dyn_cast<BranchInst>(t)) {
            if (br->getSuccessor(i) == succ) {
                return splitCriticalEdge(br, i);
            }
        }
    }
    return nullptr;
}
//递推求出所有deadblock
void GVNimpl::adddeadblock(BasicBlock *bb) {
    std::vector<BasicBlock *> ndead;
    std::set<BasicBlock *> df;
    ndead.push_back(bb);
    while (!ndead.empty()) {
        BasicBlock *tmp = ndead.back();
        ndead.pop_back();
        if (deadblock.find(tmp) != deadblock.end()) continue;

        deadblock.insert(tmp->dominates.begin(), tmp->dominates.end());
        for (auto x : tmp->dominates) {
            x->succ_init();
            for (auto y : x->succ) {
                if (deadblock.count(y))
                    continue;
                bool allpreddead = true;
                y->pred_init();
                for (auto z : y->pred) {
                    if (deadblock.find(z) == deadblock.end()) {
                        allpreddead = false;
                        break;
                    }
                }
                if (!allpreddead) {
                    df.insert(y);
                } else {
                    ndead.push_back(y);
                }
            }
        }
    }
    for (auto it = df.begin(); it != df.end(); it++) {
        BasicBlock *tmp = *it;
        if (!deadblock.count(tmp)) continue;
        tmp->pred_init();
        std::vector<BasicBlock *> p(tmp->pred);
        for (auto *x : p) {
            if (!deadblock.count(x)) continue;
            bool has = false;
            x->succ_init();
            for (auto q : x->succ) {
                if (q == tmp) {
                    has = true;
                    break;
                }
            }
            if (has && isCritical(x->getTerminator(), tmp)) {
                if (BasicBlock *nb = findcriticaledge(x, tmp))
                    deadblock.insert(x = nb);
            }
        }
        tmp->pred_init();
        for (auto *x : tmp->pred) {
            if (!deadblock.count(x)) continue;
            for (auto i = tmp->getInstList().head; i; i = i->next) {
                if (PHINode *pn = dyn_cast<PHINode>(i)) {
                    UndefValue *ud = UndefValue::get(pn->getType());
                    for (int op = 0; op < pn->getNumOperands(); op++) {
                        if (pn->getIncomingBlock(op) == x) {
                            pn->setIncomingValue(op, ud);
                        }
                    }
                }
            }
        }
    }
}

bool GVNimpl::edgedomuse(std::pair<BasicBlock *, BasicBlock *> &edge, Use *u) {
    Instruction *inst = dyn_cast<Instruction>(u->getUser());
    PHINode *pn = dyn_cast<PHINode>(inst);
    if (pn && pn->getParent() == edge.second && pn->getIncomingBlock(u) == edge.first)
        return true;

    BasicBlock *bb = nullptr;
    if (pn)
        bb = pn->getIncomingBlock(u);
    else
        bb = inst->getParent();
    return edgedominate(edge, bb);
}

bool GVNimpl::edgedominate(std::pair<BasicBlock *, BasicBlock *> &edge, BasicBlock *bb) {
    BasicBlock *st = edge.first;
    BasicBlock *ed = edge.second;
    if (!dt->b1Domb2(ed, bb)) return false;
    ed->pred_init();
    if (ed->pred.size() == 1) return true;
    //x dom d = x dom (b1,b2,b3,...)(all of b1,b2,b3,... dom d)
    for (auto x : ed->pred) {
        if (x == st) {
            continue;
        }
        if (!dt->b1Domb2(ed, x)) return false;
    }
    return true;
}

GVNimpl::Expression GVNimpl::createExpr(Instruction *inst) {
    Expression n;
    n.type = inst->getType();
    //if(inst->getType()->isPointerTy())n.type=nullptr;
    n.opcode = inst->getOpcode();
    for (int i = 0; i < inst->getNumOperands(); i++) {
        auto arg = inst->getOperand(i);
        n.varargs.push_back(findoraddvn(arg));
    }
    if (inst->isCommutative(inst->getOpcode())) {
        if (n.varargs[0] > n.varargs[1]) std::swap(n.varargs[0], n.varargs[1]);
        n.commutative = true;
    }
    if (auto *com = dyn_cast<ICmpInst>(inst)) {
        ICmpInst::Predicate pre = com->getPredicate();
        if (n.varargs[0] > n.varargs[1]) {
            std::swap(n.varargs[0], n.varargs[1]);
            pre = ICmpInst::getSwappedPredicate(pre);
        }
        n.opcode = (com->getOpcode() << 8) | pre;  //icmp保留符号信息
        n.commutative = true;
    }
    return n;
}

int GVNimpl::findoraddcall(CallInst *c) {
    //
    if (c->doesNotAccessMemory()) {
        Expression exp = createExpr(c);
        int num = addexpvn(exp).first;
        vn[c] = num;
        return num;
    } else if (!c->mayWriteToMemory()) {
        Expression exp = createExpr(c);
        auto valnum = addexpvn(exp);
        if (valnum.second) {
            vn[c] = valnum.first;
            return valnum.first;
        }

        //TODO这里要寻找def下面的相同的call来比较
    }
    vn[c] = nextvaluenumber;
    return nextvaluenumber++;
}
//first->valuenumber second->is newly created
std::pair<int, bool> GVNimpl::addexpvn(GVNimpl::Expression exp) {
    int e = en[exp];
    bool isnew = !e;
    if (isnew) {
        exps.push_back(exp);
        if (expidx.size() < nextvaluenumber + 1)
            expidx.resize(nextvaluenumber * 2);
        en[exp] = e = nextvaluenumber;
        expidx[nextvaluenumber++] = nextexpnumber++;
    }
    return {e, isnew};
}

int GVNimpl::findoraddvn(Value *v) {
    if (vn.find(v) != vn.end()) return vn.find(v)->second;
    if (!isa<Instruction>(v)) {
        vn[v] = nextvaluenumber;
        return nextvaluenumber++;
    }
    Instruction *inst = dyn_cast<Instruction>(v);
    Expression exp;
    switch (inst->getOpcode()) {
        case Instruction::Call:
            return findoraddcall(dyn_cast<CallInst>(inst));

        case Instruction::Add:
        case Instruction::Sub:
        case Instruction::Mul:
        case Instruction::UDiv:
        case Instruction::SDiv:
        case Instruction::URem:
        case Instruction::SRem:
        case Instruction::Shl:
        case Instruction::LShr:
        case Instruction::AShr:
        case Instruction::And:
        case Instruction::Or:
        case Instruction::Xor:
        case Instruction::ICmp:
        case Instruction::ZExt:
        case Instruction::GetElementPtr:
            exp = createExpr(inst);
            break;
        case Instruction::PHI:
            vn[v] = nextvaluenumber;
            nphi[nextvaluenumber] = dyn_cast<PHINode>(v);
            return nextvaluenumber++;
        default:
            vn[v] = nextvaluenumber;
            return nextvaluenumber++;
    }
    int num = addexpvn(exp).first;
    vn[v] = num;
    return num;
}

int GVNimpl::replacedomuse(Value *from, Value *to, std::pair<BasicBlock *, BasicBlock *> &edge) {
    int cnt = 0;
    for (auto it = from->use_begin(); it; it = it->next) {
        if (!edgedomuse(edge, it)) continue;
        it->set(to);
        cnt++;
    }
    return cnt;
}
GVNimpl::Expression GVNimpl::createCmpExpr(unsigned Opcode, ICmpInst::Predicate Predicate, Value *LHS, Value *RHS) {
    Expression exp;
    exp.type = Module::getInt1Ty();
    exp.varargs.push_back(findoraddvn(LHS));
    exp.varargs.push_back(findoraddvn(RHS));

    if (exp.varargs[0] > exp.varargs[1]) {
        std::swap(exp.varargs[0], exp.varargs[1]);
        Predicate = ICmpInst::getSwappedPredicate(Predicate);
    }
    exp.opcode = (Opcode << 8) | Predicate;
    exp.commutative = true;
    return exp;
}
//常量传播
bool GVNimpl::propagateEq(Value *l, Value *r, std::pair<BasicBlock *, BasicBlock *> &edge) {
    std::vector<std::pair<Value *, Value *>> worklist;
    worklist.push_back(std::make_pair(l, r));
    bool ischange = false;
    bool edgedomed = edgedominate(edge, edge.second);
    while (!worklist.empty()) {
        std::pair<Value *, Value *> tmp = worklist.back();
        worklist.pop_back();
        l = tmp.first;
        r = tmp.second;
        if (l == r) continue;
        if (isa<Constant>(l) && isa<Constant>(r)) continue;
        if (isa<Constant>(l) || (isa<Argument>(l) && !isa<Constant>(r))) std::swap(l, r);

        int lnum = findoraddvn(l);
        if ((isa<Argument>(l) && isa<Argument>(r)) ||
            (isa<Instruction>(l) && isa<Instruction>(r))) {
            int rnum = findoraddvn(r);
            if (lnum < rnum) {
                std::swap(l, r);
                lnum = rnum;
            }
        }
        if (edgedomed && !isa<Instruction>(r))
            addlt(lnum, r, edge.second);
        if ((l->getUseList().size() != 1)) {
            int numreplace = replacedomuse(l, r, edge);
            ischange |= numreplace > 0;
        }
        //l=(a!=b) r=false means a==b
        if ((r->getType() != Module::getInt1Ty())) continue;
        ConstantInt *ci = dyn_cast<ConstantInt>(r);
        if (!ci) continue;
        bool istrue = (ci->getValue() == 1);
        bool isfalse = !istrue;
        Value *a = nullptr, *b = nullptr;
        if ((istrue && touchinst(l, Instruction::And, a, b)) ||
            (isfalse && touchinst(l, Instruction::Or, a, b))) {
            worklist.push_back(std::make_pair(a, r));
            worklist.push_back(std::make_pair(b, r));
            continue;
        }
        // l=(a==b) r=true means a==b
        if (ICmpInst *icmp = dyn_cast<ICmpInst>(l)) {
            Value *op1 = icmp->getOperand(0), *op2 = icmp->getOperand(1);

            if ((istrue && icmp->getPredicate() == ICmpInst::ICMP_EQ) ||
                (isfalse && icmp->getPredicate() == ICmpInst::ICMP_NE)) {
                worklist.push_back(std::make_pair(op1, op2));
            }
            ICmpInst::Predicate notpred = icmp->getInversePredicate();
            ConstantInt *notval = isfalse ? ConstantInt::getTrue() : ConstantInt::getFalse();
            int nexnum = nextvaluenumber;
            Expression exp = createCmpExpr(icmp->getOpcode(), notpred, op1, op2);
            int num = addexpvn(exp).first;
            if (num < nexnum) {
                Value *notcmp = findlt(edge.second, num);
                if (notcmp && isa<Instruction>(notcmp)) {
                    int numreplace = replacedomuse(notcmp, notval, edge);
                    ischange |= numreplace > 0;
                }
            }
            if (edgedomed)
                addlt(num, notval, edge.second);
            continue;
        }
    }
    return ischange;
}

bool GVNimpl::processLoad(LoadInst *l) {
    bool ischange = false;

    if (l->use_empty()) {
        Erase(l);
        insterase.push_back(l);
        return true;
    }
    if (VtoM.empty()) return false;
    MemoryAccess *mu = VtoM[l].back();
    auto def = mu->getDefiningAccess();
    if (auto md = dyn_cast<MemoryDef>(def)) {
        Instruction *definst = md->MemInstruction;
        if (!definst) return false;  //全局
        if (auto Si = dyn_cast<StoreInst>(definst)) {
            if (Si->getParent() == l->getParent()) {
                auto tmp = l->prev;
                while (tmp != nullptr && tmp != Si) {
                    if (isa<LoadInst>(tmp) && tmp->getOperand(0) == l->getOperand(0)) {
                        break;
                    }
                    tmp = tmp->prev;
                }
                if (tmp && tmp != Si) {
                    l->replaceAllUsesWith(tmp);
                    insterase.push_back(l);
                    ischange = true;
                }
            }
        }
    }
    if (auto md = dyn_cast<MemoryPhi>(def)) {
        if (md->Block == l->getParent()) {
            auto tmp = l->prev;
            while (tmp != nullptr) {
                if (isa<LoadInst>(tmp) && tmp->getOperand(0) == l->getOperand(0)) {
                    break;
                }
                tmp = tmp->prev;
            }
            if (tmp) {
                l->replaceAllUsesWith(tmp);
                insterase.push_back(l);
                ischange = true;
            }
        }
    }
    if (auto gep = dyn_cast<GetElementPtrInst>(l->getOperand(0))) {
        for (int i = 1; i < gep->getNumOperands(); i++) {
            if (auto inst = dyn_cast<Instruction>(gep->getOperand(i)))
                ischange |= performScalarPRE(inst);
        }
    }

    return ischange;
}

bool GVNimpl::processInst(Instruction *inst) {
    if (Value *v = SimplifyInst(inst, dt)) {
        bool ischange = false;
        if (!inst->use_empty()) {
#ifdef DEBUG
            std::cerr << "GVN simplifyinst " << inst->getName() << "\n";
#endif
            inst->replaceAllUsesWith(v);
            ischange = true;
        }
        if (isinstructionnormaldead(inst)) {
#ifdef DEBUG
            std::cerr << "GVN deadinst " << inst->getName() << "\n";
#endif
            Erase(inst);
            insterase.push_back(inst);
            ischange = true;
        }
        if (ischange) {
            return true;
        }
    }

    if (LoadInst *load = dyn_cast<LoadInst>(inst)) {
        if (processLoad(load))
            return true;
        int num = findoraddvn(load);
        addlt(num, load, load->getParent());
        return false;
    }

    if (BranchInst *branch = dyn_cast<BranchInst>(inst)) {
        bool ischange = false;
        if (!branch->isConditional()) return false;
        //若br的条件为常量
        if (isa<Constant>(branch->getCondition())) {
            if (branch->getSuccessor(0) == branch->getSuccessor(1)) {
                return false;
            }
            ConstantInt *cond = dyn_cast<ConstantInt>(branch->getCondition());
            if (!cond) return false;
            BasicBlock *dead = cond->getValue() ? branch->getSuccessor(1) : branch->getSuccessor(0);
            if (deadblock.find(dead) != deadblock.end()) return false;
            dead->pred_init();
            if (dead->pred.size() != 1) {
                dead = findcriticaledge(branch->getParent(), dead);
                if (dead) {
                    //TODO update cfg
                }
            }
            adddeadblock(dead);
            return true;
        }

        Value *cond = branch->getCondition();
        BasicBlock *trueblock = branch->getSuccessor(0);
        BasicBlock *falseblock = branch->getSuccessor(1);
        if (trueblock == falseblock) return false;

        BasicBlock *p = branch->getParent();
        Value *t = ConstantInt::getTrue();
        if (cond->getType() == Module::getInt1Ty()) {
            std::pair<BasicBlock *, BasicBlock *> TrueE(p, trueblock);
            ischange |= propagateEq(cond, t, TrueE);

            Value *f = ConstantInt::getFalse();
            std::pair<BasicBlock *, BasicBlock *> FalseE(p, falseblock);
            ischange |= propagateEq(cond, f, FalseE);
        }

        return ischange;
    }
    if (inst->getType()->isVoidTy()) return false;
    int nexnum = nextvaluenumber;
    int num = findoraddvn(inst);

    if (isa<AllocaInst>(inst) || inst->isTerminator() || isa<PHINode>(inst)) {
        addlt(num, inst, inst->getParent());
        return false;
    }
    //新的编号
    if (num >= nexnum) {
        addlt(num, inst, inst->getParent());
        return false;
    }
    Value *v = findlt(inst->getParent(), num);
    if (!v) {
        addlt(num, inst, inst->getParent());
        return false;
    } else if (v == inst) {
        return false;
    }

    inst->replaceAllUsesWith(v);
    Erase(inst);
    insterase.push_back(inst);
    return true;
}

bool GVNimpl::foldSinglePhi(BasicBlock *bb) {
    if (!isa<PHINode>(bb->getInstList().head)) return false;
    while (PHINode *pn = dyn_cast<PHINode>(bb->getInstList().head)) {
        if (pn->getIncomingValue(0) != pn)
            pn->replaceAllUsesWith(pn->getIncomingValue(0));
        else
            pn->replaceAllUsesWith(UndefValue::get(pn->getType()));

        pn->eraseFromParent();
    }
    return true;
}

bool GVNimpl::mergeblockintopred(BasicBlock *bb) {
    //多个pred无法merge
    bb->pred_init();
    if (bb->pred.size() != 1) return false;
    BasicBlock *up = bb->pred[0];
    if (up == bb) return false;
    up->succ_init();
    //up有多个后继
    if (up->succ.size() != 1 || up->succ[0] != bb) return false;
    //up最多两个pred,一个是bb
    BranchInst *up_br;
    BasicBlock *newsucc = nullptr;
    //phi 循环
    for (auto inst = bb->getInstList().head; inst; inst = inst->next) {
        if (PHINode *pn = dyn_cast<PHINode>(inst)) {
            for (int i = 0; i < pn->getNumIncomingValues(); i++) {
                Value *incv = pn->getIncomingValue(i);
                if (incv == pn) return false;
            }
        }
    }
    std::vector<Value *> Incvalues;
    if (isa<PHINode>(bb->getInstList().head)) {
        for (auto inst = bb->getInstList().head; inst; inst = inst->next) {
            if (PHINode *pn = dyn_cast<PHINode>(inst)) {
                if (!isa<PHINode>(pn->getIncomingValue(0)) ||
                    dyn_cast<PHINode>(pn->getIncomingValue(0))->getParent() != bb)
                    Incvalues.push_back(pn->getIncomingValue(0));
            }
        }
        foldSinglePhi(bb);
    }
    bb->succ_init();
    Instruction *tup = up->getTerminator();
    Instruction *tb = bb->getTerminator();
    Instruction *st = bb->getInstList().head;
    if (st == tb) st = tup;
    up->getInstList().splice(tup, bb->getInstList(), bb->getInstList().head, tb);

    bb->replaceAllUsesWith(up);
    tup->eraseFromParent();
    up->getInstList().insertilistAtEnd(bb->getInstList());
    up->resetParent();
    bb->eraseFromParent();
    return true;
}

bool GVNimpl::processBlock(BasicBlock *bb) {
    if (deadblock.count(bb)) return false;
    // dt->calDomInfo(bb->getParent());
    bool ischange = false;
    for (auto it = bb->getInstList().head; it;) {
        dt->calDomInfo(bb->getParent());
        ischange |= processInst(it);
        if (insterase.empty()) {
            it = it->next;
            continue;
        }
        //delete inst
        bool atstart = it == bb->getInstList().head;
        if (!atstart) {
            it = it->prev;
        }
#ifdef DEBUG
        std::cerr << "GVN delete inst num:" << insterase.size() << "\n";
#endif
        for (auto inst : insterase) {
            inst->eraseFromParent();
        }
        insterase.clear();
        if (atstart)
            it = bb->getInstList().head;
        else
            it = it->next;
    }
    return ischange;
}

void GVNimpl::clear() {
    vn.clear();
    expidx.clear();
    en.clear();
    nphi.clear();
    phiTable.clear();
    nextvaluenumber = 1;
    nextexpnumber = 0;
    exps.clear();
    lt.clear();
    bRPOn.clear();
    InvalidbRPOn = true;
}

bool GVNimpl::iterateGVN(Function *func) {
    clear();

    bool ischange = false;
    for (auto bb : reverse_post_order(func->getEntryBlock())) {
        ischange |= processBlock(bb);
        // dt->calDomInfo(func);
    }
    return ischange;
}

int GVNimpl::phiTranslateimpl(BasicBlock *pre, BasicBlock *phibb, int num) {
    if (PHINode *pn = nphi[num]) {
        for (int i = 0; i < pn->getNumIncomingValues(); i++) {
            if (pn->getParent() == phibb && pn->getIncomingBlock(i) == pre)
                if (int Tval = (vn.find(pn->getIncomingValue(i)) != vn.end() ? (vn.find(pn->getIncomingValue(i))->second) : 0))
                    return Tval;
        }
        return num;
    }
    //
    ltentry *t = &lt[num];
    while (t && t->b == phibb)
        t = t->next;
    if (t) return num;
    //
    if (num >= expidx.size() || expidx[num] == 0)
        return num;
    Expression exp = exps[expidx[num]];
    for (int i = 0; i < exp.varargs.size(); i++) {
        exp.varargs[i] = phiTranslate(pre, phibb, exp.varargs[i]);
    }

    if (exp.commutative) {
        if (exp.varargs[0] > exp.varargs[1]) {
            std::swap(exp.varargs[0], exp.varargs[1]);
            int opcode = exp.opcode >> 8;
            if (opcode == Instruction::ICmp) {
                exp.opcode = (opcode << 8) | ICmpInst::getSwappedPredicate(static_cast<ICmpInst::Predicate>(exp.opcode & 255));
            }
        }
    }
    if (int newnum = en[exp]) {
        if (exp.opcode == Instruction::Call && newnum != num) {
            //TODO need memdep and alias
            return num;
        }
        return newnum;
    }
    return num;
}

int GVNimpl::phiTranslate(BasicBlock *pre, BasicBlock *phibb, int num) {
    auto Findres = phiTable.find({num, pre});
    if (Findres != phiTable.end())
        return Findres->second;
    int newnum = phiTranslateimpl(pre, phibb, num);
    phiTable.insert({{num, pre}, newnum});
    return newnum;
}

bool GVNimpl::PREinsertion(Instruction *inst, BasicBlock *pre, BasicBlock *cur, int num) {
    bool success = true;
    for (int i = 0; i < (inst->getNumOperands()); i++) {
        Value *Op = inst->getOperand(i);
        if (isa<Argument>(Op) || isa<Constant>(Op) || isa<GlobalValue>(Op)) {
            continue;
        }
        if (!vn.count(Op)) {
            success = false;
            break;
        }
        int tnum = phiTranslate(pre, cur, vn[Op]);
        if (Value *v = findlt(pre, tnum)) {
            inst->setOperand(i, v);
        } else {
            success = false;
            break;
        }
    }
    if (!success) return false;
    pre->getInstList().insertBefore(inst, pre->getTerminator());
    inst->setParent(pre);
    inst->setName(inst->getName() + ".pre");
    int newnum = findoraddvn(inst);
    vn.insert(std::make_pair(inst, newnum));
    if (PHINode *pn = dyn_cast<PHINode>(inst)) {
        nphi[newnum] = pn;
    }
    addlt(newnum, inst, pre);
    return true;
}
void GVNimpl::Erase(Value *v) {
    int num = vn[v];
    vn.erase(v);
    if (isa<PHINode>(v)) nphi.erase(num);
}

bool GVNimpl::performScalarPRE(Instruction *inst) {
    if (isa<AllocaInst>(inst) || inst->isTerminator() ||
        isa<PHINode>(inst) || inst->getType()->isVoidTy() ||
        inst->mayReadFromMemory() || inst->mayWriteToMemory())
        return false;

    if (isa<CmpInst>(inst) || isa<GetElementPtrInst>(inst)) return false;
    int num = vn[inst];
    int numwith = 0;
    int numwithout = 0;
    BasicBlock *pre = nullptr;
    BasicBlock *cur = inst->getParent();
    if (InvalidbRPOn) {
        Function *func = cur->getParent();
        bRPOn.clear();
        int nexblocknum = 1;
        for (auto bb : reverse_post_order(cur->getParent()->getEntryBlock())) {
            bRPOn[bb] = nexblocknum++;
        }
        InvalidbRPOn = false;
    }

    std::vector<std::pair<Value *, BasicBlock *>> predMap;
    cur->pred_init();
    for (auto p : cur->pred) {
        if (p->dominate_by.find(p->getParent()->getEntryBlock()) == p->dominate_by.end()) {
            numwithout = 2;
            break;
        }
        if (bRPOn[p] >= bRPOn[cur] && anyof(inst)) {
            numwithout = 2;
            break;
        }
        int TvalNo = phiTranslate(p, cur, num);
        Value *predV = findlt(p, TvalNo);

        if (!predV) {
            predMap.push_back(std::make_pair(static_cast<Value *>(nullptr), p));
            pre = p;
            ++numwithout;
        } else if (predV == inst) {
            numwithout = 2;
            break;
        } else {
            predMap.push_back(std::make_pair(predV, p));
            ++numwith;
        }
    }
    if (numwithout > 1 || numwith == 0) return false;
    Instruction *PREinst = nullptr;
    if (numwithout != 0) {
        int succnum;
        for (int i = 0;; i++) {
            if (dyn_cast<BranchInst>(pre->getTerminator())->getSuccessor(i) == cur) {
                succnum = i;
                break;
            }
        }
        if (isCritical(pre->getTerminator(), cur)) {
            toSplit.push_back(std::make_pair(pre->getTerminator(), succnum));
            return false;
        }
        PREinst = inst->clone();
        if (!PREinsertion(PREinst, pre, cur, num)) {
            PREinst->dropAllReferences();
            return false;
        }
    }
    PHINode *pn = new PHINode(inst->getType(), inst->getName() + ".pre-phi", cur->getInstList().head);
    for (int i = 0; i < predMap.size(); i++) {
        if (Value *v = predMap[i].first) {
            pn->addIncoming(v, predMap[i].second);
        } else {
            pn->addIncoming(PREinst, pre);
        }
    }
    vn.insert(std::make_pair(pn, num));
    nphi[num] = pn;
    cur->pred_init();
    for (auto x : cur->pred) {
        phiTable.erase(std::make_pair(num, x));
    }
    addlt(num, pn, cur);
    inst->replaceAllUsesWith(pn);
    Erase(inst);
    removelt(num, inst, cur);
    inst->eraseFromParent();
    //TODO
    return true;
}
bool GVNimpl::splitCriticalEdges() {
    if (toSplit.empty())
        return false;
    bool ischange = false;
    do {
        std::pair<Instruction *, int> Edge = toSplit.back();
        toSplit.pop_back();
        ischange |= (splitCriticalEdge(Edge.first, Edge.second) != nullptr);
    } while (!toSplit.empty());
    if (ischange) {
        InvalidbRPOn = true;
    }
    return ischange;
}

bool GVNimpl::performPRE(Function *func) {
    bool ischange = false;
    auto depfirst = depth_first(func->getEntryBlock());
    for (auto x : depfirst) {
        if (x == func->getEntryBlock()) continue;

        for (auto inst = x->getInstList().head; inst; inst = inst->next) {
            ischange |= performScalarPRE(inst);
        }
    }
    if (splitCriticalEdges()) ischange = true;
    return ischange;
}

bool GVNimpl::run(Function *func) {
    if (dt == nullptr)
        dt = new Dominate(func->getParent());
    dt->calDomInfo();

    bool ischange = false;
    for (auto bb = func->getEntryBlock(); bb; bb = bb->next) {
        bool removeblock = mergeblockintopred(bb);
        ischange |= removeblock;
    }
    if (ischange) dt->calDomInfo();

    bool Conti = true;  //是否继续迭代
    while (Conti) {
        Conti = iterateGVN(func);
        if (Conti) dt->calDomInfo();
        ischange |= Conti;
    }

    //PRE
    bool PREchange = true;
    for (BasicBlock *bb : deadblock) {
        for (Instruction *it = bb->getInstList().head; it; it = it->next) {
            int num = findoraddvn(it);
            addlt(num, it, bb);
        }
    }
    while (PREchange) {
        if (PREchange) dt->calDomInfo();
        PREchange = performPRE(func);
        ischange |= PREchange;
    }

    clear();
    deadblock.clear();
    return ischange;
}
GVNimpl g;
void GVN(Function *func) {
    if (func->isBuildin()) return;
    bool ishchange = g.run(func);
}