#include "Dominate.h"
#include <cstdio>
#include <iostream>
#include <algorithm>
unsigned int i = 0;
Dominate *dt=nullptr;

std::set<BasicBlock *> set1;

void dfsDT(std::vector<BasicBlock*> &dep,BasicBlock*bb)
{
    dep.push_back(bb);
    for(auto x:dt->DTSuccBlocks[bb])
    {
        dfsDT(dep,x);
    }
}

std::vector<BasicBlock*> depth_firstDT(BasicBlock* bb)
{
    std::vector<BasicBlock*> dep;
    dep.push_back(bb);
    for(auto x:dt->DTSuccBlocks[bb])
    {
        dfsDT(dep,x);
    }
    return dep;
}


void dfs(std::vector<BasicBlock*> &dep,std::set<BasicBlock*> &isin,BasicBlock *it)
{
    if(isin.count(it))return;
    isin.insert(it);
    dep.push_back(it);
    it->succ_init();
    for(auto x:it->succ)
    {
        dfs(dep,isin,x);
    }
}

void post_dfs(std::vector<BasicBlock*> &dep,std::set<BasicBlock*> &isin,BasicBlock *it)
{
    if(isin.count(it))return;
    isin.insert(it);
    it->succ_init();
    for(auto x:it->succ)
    {
        dfs(dep,isin,x);
    }
    dep.push_back(it);
}

std::vector<BasicBlock*> reverse_post_order(BasicBlock* bb)
{
    std::vector<BasicBlock*> dep;
    std::set<BasicBlock*> isin;
    bb->succ_init();
    isin.insert(bb);
    for(auto x:bb->succ)
    {
        post_dfs(dep,isin,x);
    }
    dep.push_back(bb);
    std::reverse(dep.begin(),dep.end());
    return dep;
}
std::vector<BasicBlock*> depth_first(BasicBlock* bb)
{
    std::vector<BasicBlock*> dep;
    std::set<BasicBlock*> isin;
    bb->succ_init();
    isin.insert(bb);
    dep.push_back(bb);
    for(auto x:bb->succ)
    {
        dfs(dep,isin,x);
    }
    return dep;
}

bool Dominate::dominates(BasicBlock *a,BasicBlock*b)
{
    if(a==b)return true;
    for(auto x:a->dominates)
    {
        if(x==b)return true;
    }
    return false;
}


bool Dominate::dominates(Instruction* def,BasicBlock* usebb)
{
    
    BasicBlock *defbb=def->getParent();


    if(defbb==usebb)return false;

    return dominates(defbb,usebb);
}
bool Dominate::dominates(Value* defv,Instruction* user)
{
    Instruction *def=dyn_cast<Instruction>(defv);
    if(!def)
    {
        return true;
    }
    BasicBlock *usebb=user->getParent();
    BasicBlock *defbb=def->getParent();
    if(def==user)return false;
    if(isa<PHINode>(user))
    {
        return dominates(def,usebb);
    }
    return def->comesBefore(user);
}

//此时除了entry，没有pred的BB已经被处理了
void Dominate::calDomInfo() {
    for (auto F = M->getFunctionList().head; F; F = F->next) {
        if (F->getEntryBlock() != nullptr) {
            calDomInfo(F);
        }
    }
}

void Dominate::calDomInfo(Function *F){
    if(F->isBuildin()){
        return;
    }
    DF.clear();
    DFST.clear();
    DTSuccBlocks.clear();
    set1.clear();
    for(auto BB = F->getEntryBlock(); BB; BB = BB->next){
        BB->dominates = {BB};
        BB->dominate_by = {BB};
        BB->succ_init();
        BB->pred_init();
        if(BB != F->getEntryBlock() && BB->pred.empty()){
            set1.insert(BB);
        }
    }
    for(auto BB : set1){
        for(auto succ : BB->succ){
            auto it = std::find(succ->pred.begin(), succ->pred.end(), BB);
            succ->pred.erase(it);
        }
    }
    calReversePostOrder(F);
    calIDom(F);
    calDominanceFrontier(F);
    getDomTreeSucc(F);
    getDomsAndDomby(F->getEntryBlock());
}


void Dominate::calReversePostOrder(Function *F){
    RPO.clear();
    dfn.clear();
    i = 0;
    for (auto BB = F->getEntryBlock(); BB; BB = BB->next) {
        BB->isVisit = false;
        i++;
    }
    search(F->getEntryBlock());
    RPO.reverse();
}

void Dominate::search(BasicBlock *BB) {
    BB->isVisit = true;
    BB->succ_init();
    for (auto s : BB->succ) {
        if (!s->isVisit) {
            DFST[BB].insert(s);
            search(s);
        }
    }
    dfn[BB] = i--;
    RPO.push_back(BB);
}

void Dominate::calIDom(Function *F){
    for (auto BB = F->getEntryBlock(); BB; BB = BB->next) {
        BB->idom = nullptr;
    }
    auto entry = F->getEntryBlock();
    entry->idom = entry;
    bool changed = true;
    while (changed) {
        changed = false;
        for(auto BB : RPO){
            if(BB != entry){
                BB->pred_init();
                BasicBlock *new_idom = nullptr;
                for(auto p : BB->pred){
                    if(p->idom){
                        new_idom = p;
                        break;
                    }
                }
                for (auto p : BB->pred) {
                    if (p == new_idom) {
                        continue;
                    }
                    if (p->idom) {
                        new_idom = intersect(p, new_idom);
                    }
                }
                if (BB->idom != new_idom) {
                    BB->idom = new_idom;
                    changed = true;
                }
            }
        }
    }
}

BasicBlock *Dominate::intersect(BasicBlock *b1, BasicBlock *b2) {
    while (b1 != b2) {
        while (dfn[b1] > dfn[b2]) {
            b1 = b1->idom;
        }
        while (dfn[b2] > dfn[b1]) {
            b2 = b2->idom;
        }
    }
    return b1;
}

void Dominate::calDominanceFrontier(Function *F) {
    for (auto BB = F->getEntryBlock(); BB; BB = BB->next) {
        BB->pred_init();
        if (BB->pred.size() >= 2) {
            for (auto p : BB->pred) {
                auto runner = p;
                while (runner != BB->idom) {
                    DF[runner].insert(BB);
                    runner = runner->idom;
                }
            }
        }
    }
}

void Dominate::getDomTreeSucc(Function *F) {
    for (auto BB = F->getEntryBlock(); BB; BB = BB->next) {
        auto idom = BB->idom;
        if (idom != BB) {
            DTSuccBlocks[idom].insert(BB);
        }
    }
}

void Dominate::getDomsAndDomby(BasicBlock *BB) {
    if (!DTSuccBlocks[BB].empty()) {
        for (auto p : DTSuccBlocks[BB]) {
            p->dominate_by.insert(BB->dominate_by.begin(), BB->dominate_by.end());
            getDomsAndDomby(p);
            BB->dominates.insert(BB->dominates.end(), p->dominates.begin(), p->dominates.end());
        }
    }
}

bool Dominate::b1Domb2(BasicBlock *b1, BasicBlock *b2) {
    if(b2==nullptr)return true;
    for (auto p : b1->dominates) {
        if (p == b2) {
            return true;
        }
    }
    return false;
}
