#include "LiveVariables.h"

std::map<BasicBlock*,std::vector<Value*>> PhiVarinfo;
std::map<Value *,Varinfo>Varinfomap;//Varinfo


std::map<Instruction*,int>Distance;//指令离从bb开头到它的距离

bool isclobberforValue(Value *v1,Value *v2)
{
    if(v1==v2)return true;
    Varinfo &a1=Varinfomap[v1];
    Varinfo &a2=Varinfomap[v2];
    auto I1=dyn_cast<Instruction>(v1),I2=dyn_cast<Instruction>(v2);
    if(I1)
    {
        if(a2.isLiveIn(I1->getParent(),v2))return true;
        auto mi=a2.findLast(I1->getParent());
        if(!mi)return false;
        I1->getParent()->renumberInstructions();
        return mi->Order>I1->Order;
    }
    if(I2)
    {
        if(a1.isLiveIn(I2->getParent(),v1))return true;
        auto mi=a1.findLast(I2->getParent());
        if(!mi)return false;
        I1->getParent()->renumberInstructions();
        return mi->Order>I2->Order;
    }
    return true;//如果都不是具体语句，那么一定冲突
}

Varinfo &getVarInfo(Value*v)
{
    if(Varinfomap.find(v)==Varinfomap.end())
    {
        Varinfomap[v]=*(new Varinfo());

    }
    return Varinfomap[v];
}

void markAlive(Varinfo &info,BasicBlock *defbb, BasicBlock *bb,std::vector<BasicBlock*>&worklist)
{
    for(int i=0;i<info.Kills.size();i++)
    {
        if(info.Kills[i]->getParent()==bb)
        {
            info.Kills.erase(info.Kills.begin()+i);
            break;
        }
    }
    if(bb==defbb)return;
    if(info.Alivebbs.count(bb))
    return;
    info.Alivebbs.insert(bb);
    bb->pred_init();
    worklist.insert(worklist.begin(),bb->pred.begin(),bb->pred.end());
}

void markAliveinBlock(Varinfo &info,BasicBlock *defbb,BasicBlock *bb)
{
    std::vector<BasicBlock*>worklist;
    markAlive(info,defbb,bb,worklist);
    while(!worklist.empty())
    {
        BasicBlock *pred=worklist.back();
        worklist.pop_back();
        markAlive(info,defbb,pred,worklist);
    }
}

void handleUse(Value*v,BasicBlock*bb,Instruction*inst)
{
    Varinfo &info=getVarInfo(v);
    if(!info.Kills.empty()&&info.Kills.back()->getParent()==bb)
    {
        info.Kills.back()=inst;
        return ;
    }
    auto def=dyn_cast<Instruction>(v);
    if(def)
    {
        if(bb==def->getParent())return ;
    }
    if(!info.Alivebbs.count(bb))
        info.Kills.push_back(inst);
    bb->pred_init();
    for(auto x:bb->pred)
    {
        markAliveinBlock(info,def?def->getParent():nullptr,x);
    }
}

void handleDef(Value *v,Instruction *inst)
{
    Varinfo &info=getVarInfo(v);
    if(info.Alivebbs.empty())
        info.Kills.push_back(inst);
}

void processInst(Instruction*inst, std::vector<int>&Defs)
{
    int opnum=inst->getNumOperands();
    //phi节点不关注它的use
    if(isa<PHINode>(inst))opnum=0;
    std::vector<Value*>Useops;
    
    for(int i=0;i<opnum;i++)
    {
        Value* op=inst->getOperand(i);
        if(isa<ConstantInt>(op))continue;
        Useops.push_back(op);
    }
    BasicBlock* bb=inst->getParent();
    for(int i=0;i<Useops.size();i++)
    {
        Value *u=Useops[i];
        handleUse(u,bb,inst);
    }
    handleDef(inst,inst);
}

void LiveVarprocessBlock(BasicBlock *bb)
{
    std::vector<int>Defs;
    Distance.clear();
    int Dist=0;
    for(auto inst:bb->getInstList())
    {
        Distance.insert(std::make_pair(inst,Dist++));
        processInst(inst,Defs);
    }

    if(!PhiVarinfo[bb].empty())
    {
        std::vector<Value*> &Phivec=PhiVarinfo[bb];
        for(auto x:Phivec)
        if(!isa<ConstantInt>(x)){
            auto def=dyn_cast<Instruction>(x);
            markAliveinBlock(getVarInfo(x),def?def->getParent():nullptr,bb);
        }
    }

}

void LiveVariables(Function *func)
{
    if(func->isBuildin())return ;
    Varinfomap.clear();
    for(auto bb:func->getBasicBlockList())
        PhiVarinfo[bb]=std::vector<Value*>();

    for(auto bb:func->getBasicBlockList())
        for(auto inst:bb->getInstList())
        {
            if(PHINode *pn=dyn_cast<PHINode>(inst))
            {
                for(int i=0;i<pn->getNumIncomingValues();i++)
                {
                    PhiVarinfo[pn->getIncomingBlock(i)].push_back(pn->getIncomingValue(i));
                }
            }
        }
    for(auto x:depth_first(func->getEntryBlock())){
        LiveVarprocessBlock(x);
    }


    //TODO 删除dead的语句
    // for(auto it:Varinfomap)
    // {
    //     Value *v=it.first;
    //     Varinfo &info=it.second;
    //     for(int i=0;i<info.Kills.size();i++)
    //     {
    //         if(info.Kills[i]==dyn_cast<Instruction>(v))//定义的时候就不活跃了，可以删除这条语句
    //         {

    //         }
    //     }
    // }
    return ;
}