#include "InstSimplify.h"

bool isOverflowing = false;
// template<type>
// bool match()

bool touchinst(Value *inst, Instruction::Ops opcode, Value *&op1, Value *&op2) {
    if (auto x = dyn_cast<BinaryOperator>(inst)) {
        if (x->getOpcode() == opcode) {
            op1 = x->getOperand(0);
            op2 = x->getOperand(1);
            return true;
        }
    }

    return false;
}

//分配率
//a*c+b*c
static Value *expandBinOp(Instruction::Ops opcode, Value *op1, Value *op2, Instruction::Ops extend, int dep, Dominate *dt) {
    auto inst = dyn_cast<BinaryOperator>(op1);
    if (inst == nullptr || inst->getOpcode() != extend)
        return nullptr;

    Value *a = inst->getOperand(0);
    Value *b = inst->getOperand(1);
    if (Value *L = SimplifyBin(opcode, a, op2, dep, dt))
        if (Value *R = SimplifyBin(opcode, b, op2, dep, dt)) {
            if ((L == a && R == b) || (Instruction::isCommutative(extend) && L == b && R == a)) {
                return inst;
            }
            if (Value *m = SimplifyBin(extend, L, R, dep, dt)) {
                return m;
            }
        }
    return nullptr;
}

static Value *expandCommutativeBin(Instruction::Ops opcode, Value *l, Value *r, Instruction::Ops extend, int dep, Dominate *dt) {
    if (!dep--) return nullptr;
    if (Value *ex = expandBinOp(opcode, l, r, extend, dep, dt))
        return ex;
    if (Value *ex = expandBinOp(opcode, r, l, extend, dep, dt))
        return ex;
    return nullptr;
}

static Constant *foldandcanonical(Instruction::Ops opcode, Value *&a, Value *&b) {
    if (auto ca = dyn_cast<ConstantInt>(a)) {
        if (auto cb = dyn_cast<ConstantInt>(b))
            return ConstFoldBin(opcode, a, b);
        if (Instruction::isCommutative(opcode))
            std::swap(a, b);
    }
    return nullptr;
}

bool isneg(Value *a, Value *b) {
    //a = sub 0 b

    if (a->getValueID() == Value::InstructionVal + Instruction::Sub) {
        auto *inst = dyn_cast<BinaryOperator>(a);
        if (auto *op1 = dyn_cast<ConstantInt>(inst->getOperand(0))) {
            if (op1->getValue() == 0 && inst->getOperand(1) == b) {
                return true;
            }
        }
    }
    // b=sub 0 a
    if (b->getValueID() == Value::InstructionVal + Instruction::Sub) {
        auto *inst = dyn_cast<BinaryOperator>(b);
        if (auto *op1 = dyn_cast<ConstantInt>(inst->getOperand(0))) {
            if (op1->getValue() == 0 && inst->getOperand(1) == a) {
                return true;
            }
        }
    }
    //a=sub x y ,b=sub y x
    if (a->getValueID() == Value::InstructionVal + Instruction::Sub && b->getValueID() == Value::InstructionVal + Instruction::Sub) {
        auto *inst1 = dyn_cast<BinaryOperator>(a);
        auto *inst2 = dyn_cast<BinaryOperator>(b);
        if (inst1&&inst2&&inst1->getOperand(0) == inst2->getOperand(1) && inst1->getOperand(1) == inst2->getOperand(0)) {
            return true;
        }
    }
    return false;
}
static Value *SimplifyBin(Instruction::Ops opcode, Value *l, Value *r, int dep, Dominate *dt) {
    switch (opcode) {
        case Instruction::Add:
            return SimplifyAdd(l, r, dep, dt);
        case Instruction::Sub:
            return SimplifySub(l, r, dep, dt);
        case Instruction::Mul:
            return SimplifyMul(l, r, dep, dt);
        case Instruction::SDiv:
            return SimplifySDiv(l, r, dep, dt);
        case Instruction::UDiv:
            return SimplifyUDiv(l, r, dep, dt);
        case Instruction::SRem:
            return SimplifySRem(l, r, dep, dt);
        case Instruction::URem:
            return SimplifyURem(l, r, dep, dt);
        case Instruction::Shl:
            return SimplifyShl(l, r, dep, dt);
        case Instruction::LShr:
            return SimplifyLShr(l, r, dep, dt);
        case Instruction::AShr:
            return SimplifyAShr(l, r, dep, dt);
        case Instruction::And:
            //          return SimplifyAnd(l,r,dep,dt);
        case Instruction::Or:
            //           return SimplifyOr(l,r,dep,dt);
        case Instruction::Xor:
            //           return SimplifyXor(l,r,dep,dt);
        default:
            return nullptr;
    }
}
static Value *SimplifyAssociativeBin(Instruction::Ops opcode, Value *lhs, Value *rhs, int dep, Dominate *dt) {
    if (!dep--) return nullptr;
    BinaryOperator *l = dyn_cast<BinaryOperator>(lhs);
    BinaryOperator *r = dyn_cast<BinaryOperator>(rhs);
    //结合律
    // (a op b) op c -> a op (b op c)
    //lhs=a+b
    if (l && l->getOpcode() == opcode) {
        Value *a = l->getOperand(0);
        Value *b = l->getOperand(1);
        Value *c = rhs;
        if (Value *result = SimplifyBin(opcode, b, c, dep, dt)) {
            if (result == b) return lhs;
            if (Value *result1 = SimplifyBin(opcode, a, result, dep, dt)) {
                return result1;
            }
        }
    }
    // a op (b op c) -> (a op b) op c
    if (r && r->getOpcode() == opcode) {
        Value *a = lhs;
        Value *b = r->getOperand(0);
        Value *c = r->getOperand(1);
        if (Value *result = SimplifyBin(opcode, a, b, dep, dt)) {
            if (result == b) return rhs;
            if (Value *result1 = SimplifyBin(opcode, result, c, dep, dt)) {
                return result1;
            }
        }
    }
    if (!Instruction::isCommutative(opcode)) return nullptr;

    //交换律
    //(a op b) op c -> (c op a) op b
    if (l && l->getOpcode() == opcode) {
        Value *a = l->getOperand(0);
        Value *b = l->getOperand(1);
        Value *c = rhs;
        if (Value *result = SimplifyBin(opcode, c, a, dep, dt)) {
            if (result == a) return lhs;
            if (Value *result1 = SimplifyBin(opcode, result, b, dep, dt)) {
                return result1;
            }
        }
    }
    //a op (b op c) -> b op (c op a)
    if (r && r->getOpcode() == opcode) {
        Value *a = lhs;
        Value *b = r->getOperand(0);
        Value *c = r->getOperand(1);
        if (Value *result = SimplifyBin(opcode, c, a, dep, dt)) {
            if (result == c) return rhs;
            if (Value *result1 = SimplifyBin(opcode, b, result, dep, dt)) {
                return result1;
            }
        }
    }
    return nullptr;
}
static Value *SimplifyAdd(Value *op1, Value *op2, int dep, Dominate *dt) {
    if (Constant *val = foldandcanonical(Instruction::Add, op1, op2))
        return val;
    //x+undef
    if (isa<UndefValue>(op2)) return op2;

    //x+0
    if (auto z = dyn_cast<ConstantInt>(op2)) {
        if (z->getValue() == 0)
            return op1;
    }
    //0+x
    if (auto z = dyn_cast<ConstantInt>(op1)) {
        if (z->getValue() == 0)
            return op2;
    }
    if (isneg(op1, op2)) {
        return ConstantInt::get(0);
    }
    //x+(y-x) (y-x)+x
    if (op1->getValueID() == Value::InstructionVal + Instruction::Sub) {
        auto *inst = dyn_cast<BinaryOperator>(op1);
        if (inst->getOperand(1) == op2) {
            return inst->getOperand(0);
        }
    }
    if (op2->getValueID() == Value::InstructionVal + Instruction::Sub) {
        auto *inst = dyn_cast<BinaryOperator>(op2);
        if (inst->getOperand(1) == op1) {
            return inst->getOperand(0);
        }
    }
    if (Value *V = SimplifyAssociativeBin(Instruction::Add, op1, op2, dep, dt))
        return V;
    return nullptr;
}
static Value *SimplifySub(Value *op1, Value *op2, int dep, Dominate *dt) {
    if (Constant *val = foldandcanonical(Instruction::Sub, op1, op2))
        return val;
    //x+0
    if (auto z = dyn_cast<ConstantInt>(op2)) {
        if (z->getValue() == 0)
            return op1;
    }
    if (op1 == op2) {
        return ConstantInt::get(0);
    }
    if (dep) {
        //(a+b)-c -> a+(b-c)| b+(a-c)
        if (op1->getValueID() == Value::InstructionVal + Instruction::Add) {
            auto *inst = dyn_cast<BinaryOperator>(op1);
            Value *a = inst->getOperand(0), *b = inst->getOperand(1), *c = op2;
            if (Value *d = SimplifyBin(Instruction::Sub, b, c, dep - 1, dt))
                if (Value *e = SimplifyBin(Instruction::Add, a, d, dep - 1, dt)) {
                    return e;
                }
            if (Value *d = SimplifyBin(Instruction::Sub, a, c, dep - 1, dt))
                if (Value *e = SimplifyBin(Instruction::Add, b, d, dep - 1, dt)) {
                    return e;
                }
        }
        //a-(b+c)-> (a-b)-c | (a-c)-b
        if (op2->getValueID() == Value::InstructionVal + Instruction::Add) {
            auto *inst = dyn_cast<BinaryOperator>(op2);
            Value *a = op1, *b = inst->getOperand(0), *c = inst->getOperand(1);
            if (Value *d = SimplifyBin(Instruction::Sub, a, b, dep - 1, dt))
                if (Value *e = SimplifyBin(Instruction::Sub, d, c, dep - 1, dt)) {
                    return e;
                }
            if (Value *d = SimplifyBin(Instruction::Sub, a, c, dep - 1, dt))
                if (Value *e = SimplifyBin(Instruction::Add, d, b, dep - 1, dt)) {
                    return e;
                }
        }
        //a-(b-c) -> (a-b)+c
        if (op2->getValueID() == Value::InstructionVal + Instruction::Sub) {
            auto *inst = dyn_cast<BinaryOperator>(op2);
            Value *a = op1, *b = inst->getOperand(0), *c = inst->getOperand(1);
            if (Value *d = SimplifyBin(Instruction::Sub, a, b, dep - 1, dt))
                if (Value *e = SimplifyBin(Instruction::Add, d, c, dep - 1, dt)) {
                    return e;
                }
        }
    }
    return nullptr;
}
static bool valueDominatesPHI(Value *V, PHINode *P, Dominate *dt) {
    Instruction *I = dyn_cast<Instruction>(V);
    if (!I)
        return true;

    if (!I->getParent() || !P->getParent() || !I->getFunction())
        return false;

    if (dt)
        return dt->dominates(I, P);

    if (I->getParent() == I->getFunction()->getEntryBlock())
        return true;

    return false;
}
static Value *threadBinopOverPhi(Instruction::Ops opcode, Value *lhs, Value *rhs, int dep, Dominate *dt) {
    if (!dep--) return nullptr;
    PHINode *pn = dyn_cast<PHINode>(lhs);
    if (pn) {
        if (!valueDominatesPHI(rhs, pn, dt))
            return nullptr;
    } else {
        pn = dyn_cast<PHINode>(rhs);
        if (!valueDominatesPHI(lhs, pn, dt))
            return nullptr;
    }
    Value *commonValue = nullptr;
    for (int i = 0; i < pn->getNumIncomingValues(); i++) {
        Value *incomming = pn->getIncomingValue(i);
        if (incomming == pn) continue;
        Value *V = (pn == lhs) ? SimplifyBin(opcode, incomming, rhs, dep, dt) : SimplifyBin(opcode, lhs, incomming, dep, dt);
        if (!V || (commonValue && V != commonValue))
            return nullptr;
        commonValue = V;
    }
    return commonValue;
}
static Value *SimplifyMul(Value *op1, Value *op2, int dep, Dominate *dt) {
    if (Constant *val = foldandcanonical(Instruction::Mul, op1, op2))
        return val;
    if (isa<UndefValue>(op2))
        return ConstantInt::get(0);
    //x*0
    if (auto x = dyn_cast<ConstantInt>(op2))
        if (x->getValue() == 0)
            return ConstantInt::get(0);
    //x*1
    if (auto x = dyn_cast<ConstantInt>(op2))
        if (x->getValue() == 1)
            return op1;
    //associate
    if (Value *v = SimplifyAssociativeBin(Instruction::Mul, op1, op2, dep, dt))
        return v;
    // 分配率拆开，为了可能的更多的乘加
    if (Value *v = expandCommutativeBin(Instruction::Mul, op1, op2, Instruction::Add, dep, dt))
        return v;
    if (isa<PHINode>(op1) || isa<PHINode>(op2))
        if (Value *v = threadBinopOverPhi(Instruction::Mul, op1, op2, dep, dt))
            return v;
    return nullptr;
}
static Value *SimplifyDivRem(Value *op1, Value *op2, bool isDiv) {
    //0/x 0%x=0
    if (ConstantInt *c = dyn_cast<ConstantInt>(op1)) {
        if (c->getValue() == 0)
            return ConstantInt::get(0);
    }
    if (op1 == op2)
        return isDiv ? ConstantInt::get(1) : ConstantInt::get(0);
    //x/1 x%1
    if (ConstantInt *c = dyn_cast<ConstantInt>(op2)) {
        if (c->getValue() == 1)
            return isDiv ? op1 : ConstantInt::get(0);
    }
    return nullptr;
}

static bool isICmpTrue(ICmpInst::Predicate Pred, Value *LHS, Value *RHS, int dep, Dominate *dt) {
    Value *V = SimplifyICmp(Pred, LHS, RHS, dep, dt);
    ConstantInt *C = dyn_cast<ConstantInt>(V);
    return (C && C->getValue() == -1);
}

static bool isDivZero(Value *X, Value *Y, int dep, Dominate *dt) {
    if (!dep--)
        return false;
    // |X| / |Y| --> 0
    Type *Ty = X->getType();
    ConstantInt *C = dyn_cast<ConstantInt>(X);
    if (C && (C->getValue() != (1 << 31))) {
        // |Y| > |C| --> Y < -abs(C) or Y > abs(C)
        Constant *PosDividendC = ConstantInt::get(abs(C->getValue()));
        Constant *NegDividendC = ConstantInt::get(-abs(C->getValue()));
        if (isICmpTrue(ICmpInst::ICMP_SLT, Y, NegDividendC, dep, dt) ||
            isICmpTrue(ICmpInst::ICMP_SGT, Y, PosDividendC, dep, dt))
            return true;
    }
    C = dyn_cast<ConstantInt>(Y);
    if (C) {
        if (C->getValue() == (1 << 31))
            return isICmpTrue(ICmpInst::ICMP_NE, X, Y, dep, dt);
        // |X| < |C| --> X > -abs(C) and X < abs(C)
        Constant *PosDivisorC = ConstantInt::get(abs(C->getValue()));
        Constant *NegDivisorC = ConstantInt::get(-abs(C->getValue()));
        if (isICmpTrue(ICmpInst::ICMP_SGT, X, NegDivisorC, dep, dt) &&
            isICmpTrue(ICmpInst::ICMP_SLT, X, PosDivisorC, dep, dt))
            return true;
    }
    return false;
}

static Value *SimplifyDiv(Instruction::Ops opcode, Value *op1, Value *op2, int dep, Dominate *dt) {
    if (Constant *c = foldandcanonical(opcode, op1, op2))
        return c;

    if (Value *v = SimplifyDivRem(op1, op2, true))
        return v;

    //(x*y)/y
    Value *x=nullptr, *y=nullptr;
    if (touchinst(op1, Instruction::Mul, x, y)) {
        if (x == op2) {
            Value *x1, *x2;
            if (touchinst(y, Instruction::SDiv, x1, x2)) {
                if (x2 == op2)
                    return y;
            }
        }
        if (y == op2) {
            Value *x1, *x2;
            if (touchinst(x, Instruction::SDiv, x1, x2)) {
                if (x2 == op2)
                    return x;
            }
        }
    }
    //(x%y)/y =0
    if (touchinst(op1, Instruction::SRem, x, y)) {
        if (y == op2) {
            return ConstantInt::get(0);
        }
    }
    //x/y/z
    if (touchinst(op1, Instruction::SDiv, x, y)) {
        if (ConstantInt *c1 = dyn_cast<ConstantInt>(y))
            if (ConstantInt *c2 = dyn_cast<ConstantInt>(op2)) {
                int64_t tmp = (int64_t)c1->getValue() * (int64_t)c2->getValue();
                if ((int64_t)((int32_t)tmp) != tmp) {
                    return ConstantInt::get(0);
                }
            }
    }

    if (isa<PHINode>(op1) || isa<PHINode>(op2))
        if (Value *V = threadBinopOverPhi(opcode, op1, op2, dep, dt))
            return V;
    if (isDivZero(op1, op2, dep, dt))
        return ConstantInt::get(0);
    return nullptr;
}

static Value *SimplifySDiv(Value *op1, Value *op2, int dep, Dominate *dt) {
    if (isneg(op1, op2))
        return ConstantInt::get(-1);
    return SimplifyDiv(Instruction::SDiv, op1, op2, dep, dt);
}
static Value *SimplifyUDiv(Value *op1, Value *op2, int dep, Dominate *dt) {
    //TODO
    return nullptr;
}

static Value *SimplifyRem(Instruction::Ops opcode, Value *op1, Value *op2, int dep, Dominate *dt) {
    if (Constant *c = foldandcanonical(opcode, op1, op2))
        return c;

    if (Value *v = SimplifyDivRem(op1, op2, false))
        return v;

    Value *x=nullptr, *y=nullptr;
    //(x%y)%y
    if (touchinst(op1, Instruction::SRem, x, y)) {
        if (y == op2) {
            return op1;
        }
    }
    //(x<<y)%x=0
    if (touchinst(op1, Instruction::Shl, x, y)) {
        if (x == op2) {
            return ConstantInt::get(0);
        }
    }
    if (isa<PHINode>(op1) || isa<PHINode>(op2))
        if (Value *V = threadBinopOverPhi(opcode, op1, op2, dep, dt))
            return V;
    if (isDivZero(op1, op2, dep, dt))
        return op1;

    return nullptr;
}

static Value *SimplifySRem(Value *op1, Value *op2, int dep, Dominate *dt) {
    if (isneg(op1, op2))
        return ConstantInt::get(0);
    return SimplifyRem(Instruction::SRem, op1, op2, dep, dt);
}
static Value *SimplifyURem(Value *op1, Value *op2, int dep, Dominate *dt) {
    //TODO
    //return SimplifyRem(Instruction::URem,op1,op2,dep,dt);
    return nullptr;
}
//位运算只会出现在instcombine时，故不用简化
static Value *SimplifyShl(Value *op1, Value *op2, int dep, Dominate *dt) {
    return nullptr;
}
static Value *SimplifyLShr(Value *op1, Value *op2, int dep, Dominate *dt) {
    return nullptr;
}
static Value *SimplifyAShr(Value *op1, Value *op2, int dep, Dominate *dt) {
    return nullptr;
}

static Value *simplifyAndOfICmpsWithSameOperands(ICmpInst *Op0, ICmpInst *Op1) {
    Value *A1=nullptr, *B1=nullptr, *A2=nullptr, *B2=nullptr;
    bool ok = false;
    if (touchinst(Op0, Instruction::ICmp, A1, B1))
        if (touchinst(Op1, Instruction::ICmp, A2, B2)) {
            if (A1 == A2 && B1 == B2) {
                ok = true;
            }
            if (A1 == B2 && B1 == A2 && (Op0->isCommutative() || Op1->isCommutative())) {
                if (Op0->isCommutative())
                    std::swap(A1, B1);
                else
                    std::swap(A2, B2);
                ok = true;
            }
        }
    if (!ok)
        return nullptr;

    // (icmp Pred0, A, B) & (icmp Pred1, A, B).

    if (ICmpInst::isImpliedTrueByMatchingCmp(Op0->getPredicate(), Op1->getPredicate()))
        return Op0;
    ICmpInst::Predicate Pred0 = Op0->getPredicate(), Pred1 = Op1->getPredicate();

    // Check for any combination of predicates that cover the entire range of
    // possibilities.
    if ((Pred0 == ICmpInst::getInversePredicate(Pred1)) ||
        (Pred0 == ICmpInst::ICMP_EQ && ICmpInst::isFalseWhenEqual(Pred1)) ||
        (Pred0 == ICmpInst::ICMP_SLT && Pred1 == ICmpInst::ICMP_SGT) ||
        (Pred0 == ICmpInst::ICMP_ULT && Pred1 == ICmpInst::ICMP_UGT))
        return ConstantInt::getFalse();

    return nullptr;
}

static Value *simplifyOrOfICmpsWithSameOperands(ICmpInst *Op0, ICmpInst *Op1) {
    Value *A1=nullptr, *B1=nullptr, *A2=nullptr, *B2=nullptr;
    bool ok = false;
    if (touchinst(Op0, Instruction::ICmp, A1, B1))
        if (touchinst(Op1, Instruction::ICmp, A2, B2)) {
            if (A1 == A2 && B1 == B2) {
                ok = true;
            }
            if (A1 == B2 && B1 == A2 && (Op0->isCommutative() || Op1->isCommutative())) {
                if (Op0->isCommutative())
                    std::swap(A1, B1);
                else
                    std::swap(A2, B2);
                ok = true;
            }
        }
    if (!ok)
        return nullptr;

    // (icmp Pred0, A, B) | (icmp Pred1, A, B).

    if (ICmpInst::isImpliedTrueByMatchingCmp(Op0->getPredicate(), Op1->getPredicate()))
        return Op1;
    ICmpInst::Predicate Pred0 = Op0->getPredicate(), Pred1 = Op1->getPredicate();

    // Check for any combination of predicates that cover the entire range of
    // possibilities.
    if ((Pred0 == ICmpInst::getInversePredicate(Pred1)) ||
        (Pred0 == ICmpInst::ICMP_NE && ICmpInst::isTrueWhenEqual(Pred1)) ||
        (Pred0 == ICmpInst::ICMP_SLE && Pred1 == ICmpInst::ICMP_SGE) ||
        (Pred0 == ICmpInst::ICMP_ULE && Pred1 == ICmpInst::ICMP_UGE))
        return ConstantInt::getTrue();

    return nullptr;
}

static Value *simplifyAndOrOfICmpsWithConstants(ICmpInst *Cmp0, ICmpInst *Cmp1,
                                                bool IsAnd) {
    //TODO 这里需要写一个区间类，后面再来写
    return nullptr;
}

static Value *simplifyAndOfICmpsWithAdd(ICmpInst *Op0, ICmpInst *Op1) {
    // (icmp (add V, C0), C1) & (icmp V, C0)
    ICmpInst::Predicate Pred0 = Op0->getPredicate(), Pred1 = Op1->getPredicate();
    Value *C0=nullptr, *C1=nullptr;
    Value *V=nullptr, *X=nullptr;
    bool ok = false;
    if (touchinst(Op0, Instruction::ICmp, X, C1)) {
        if (dyn_cast<ConstantInt>(C1)) {
            if (touchinst(X, Instruction::Add, V, C0)) {
                if (dyn_cast<ConstantInt>(C0)) {
                    Value *VV, *CC0;
                    if (touchinst(Op1, Instruction::ICmp, VV, CC0)) {
                        if (dyn_cast<ConstantInt>(CC0)) {
                            if (V == VV && CC0 == C0) {
                                ok = true;
                            }
                        }
                    }
                }
            }
        }
    }
    if (!ok) return nullptr;
    //TODO 溢出判断
    return nullptr;
}

static Value *simplifyOrOfICmpsWithAdd(ICmpInst *Op0, ICmpInst *Op1) {
    // (icmp (add V, C0), C1) | (icmp V, C0)
    //TODO
    ICmpInst::Predicate Pred0 = Op0->getPredicate(), Pred1 = Op1->getPredicate();
    Value *C0, *C1;
    Value *V, *X;
    bool ok = false;
    if (touchinst(Op0, Instruction::ICmp, X, C1)) {
        if (dyn_cast<ConstantInt>(C1)) {
            if (touchinst(X, Instruction::Add, V, C0)) {
                if (dyn_cast<ConstantInt>(C0)) {
                    Value *VV, *CC0;
                    if (touchinst(Op1, Instruction::ICmp, VV, CC0)) {
                        if (dyn_cast<ConstantInt>(CC0)) {
                            if (V == VV && CC0 == C0) {
                                ok = true;
                            }
                        }
                    }
                }
            }
        }
    }
    if (!ok) return nullptr;
    return nullptr;
}

static Value *simplifyAndOfICmps(ICmpInst *Op0, ICmpInst *Op1) {
    if (Value *X = simplifyAndOfICmpsWithSameOperands(Op0, Op1))
        return X;
    if (Value *X = simplifyAndOfICmpsWithSameOperands(Op1, Op0))
        return X;

    if (Value *X = simplifyAndOrOfICmpsWithConstants(Op0, Op1, true))
        return X;

    if (Value *X = simplifyAndOfICmpsWithAdd(Op0, Op1))
        return X;
    if (Value *X = simplifyAndOfICmpsWithAdd(Op1, Op0))
        return X;

    return nullptr;
}

static Value *simplifyOrOfICmps(ICmpInst *Op0, ICmpInst *Op1) {
    if (Value *X = simplifyOrOfICmpsWithSameOperands(Op0, Op1))
        return X;
    if (Value *X = simplifyOrOfICmpsWithSameOperands(Op1, Op0))
        return X;

    if (Value *X = simplifyAndOrOfICmpsWithConstants(Op0, Op1, false))
        return X;

    if (Value *X = simplifyOrOfICmpsWithAdd(Op0, Op1))
        return X;
    if (Value *X = simplifyOrOfICmpsWithAdd(Op1, Op0))
        return X;

    return nullptr;
}

static Value *simplifyAndOrOfICmps(Value *Op0, Value *Op1, bool IsAnd) {
    Value *V = nullptr;
    auto *ICmp0 = dyn_cast<ICmpInst>(Op0);
    auto *ICmp1 = dyn_cast<ICmpInst>(Op1);
    if (ICmp0 && ICmp1)
        V = IsAnd ? simplifyAndOfICmps(ICmp0, ICmp1)
                  : simplifyOrOfICmps(ICmp0, ICmp1);
    if (!V)
        return nullptr;
    if (auto *C = dyn_cast<Constant>(V))
        return C;
    return nullptr;
}
static Value *SimplifyAnd(Value *op1, Value *op2, int dep, Dominate *dt) {
    if (Constant *c = foldandcanonical(Instruction::And, op1, op2))
        return c;
    if (op1 == op2) return op1;
    if (ConstantInt *c = dyn_cast<ConstantInt>(op2))
        if (c->getValue() == 0)
            return ConstantInt::get(0);
    Value *a=nullptr, *b=nullptr;
    if (touchinst(op1, Instruction::Or, a, b)) {
        if (a == op2 || b == op2) return op2;
    }
    if (touchinst(op2, Instruction::Or, a, b)) {
        if (a == op1 || b == op1) return op1;
    }
    if (Value *V = simplifyAndOrOfICmps(op1, op2, true))
        return V;
    if (Value *V = SimplifyAssociativeBin(Instruction::And, op1, op2, dep, dt))
        return V;
    if (Value *V = expandCommutativeBin(Instruction::And, op1, op2, Instruction::Or, dep, dt))
        return V;
    if (isa<PHINode>(op1) || isa<PHINode>(op2))
        if (Value *V = threadBinopOverPhi(Instruction::And, op1, op2, dep, dt))
            return V;
    return nullptr;
}
static Value *SimplifyOr(Value *op1, Value *op2, int dep, Dominate *dt) {
    if (Constant *c = foldandcanonical(Instruction::Or, op1, op2))
        return c;
    if (op1 == op2) return op1;
    if (ConstantInt *c = dyn_cast<ConstantInt>(op2))
        if (c->getValue() == 0)
            return op1;
    Value *a, *b;
    if (touchinst(op1, Instruction::And, a, b)) {
        if (a == op2 || b == op2) return op2;
    }
    if (touchinst(op2, Instruction::And, a, b)) {
        if (a == op1 || b == op1) return op1;
    }
    if (Value *V = simplifyAndOrOfICmps(op1, op2, false))
        return V;
    if (Value *V = SimplifyAssociativeBin(Instruction::Or, op1, op2, dep, dt))
        return V;
    if (Value *V = expandCommutativeBin(Instruction::Or, op1, op2, Instruction::Or, dep, dt))
        return V;
    if (isa<PHINode>(op1) || isa<PHINode>(op2))
        if (Value *V = threadBinopOverPhi(Instruction::Or, op1, op2, dep, dt))
            return V;
    return nullptr;
}
static Value *SimplifyXor(Value *op1, Value *op2, int dep, Dominate *dt) {
    return nullptr;
}
static Value *simplifyICmpWithZero(ICmpInst::Predicate Pred, Value *LHS, Value *RHS) {
    if (ConstantInt *crhs = dyn_cast<ConstantInt>(RHS))
        if (crhs->getValue() != 0)
            return nullptr;

    switch (Pred) {
        default:
            return nullptr;
        case ICmpInst::ICMP_ULT:
            return ConstantInt::getFalse();
        case ICmpInst::ICMP_UGE:
            return ConstantInt::getTrue();
        case ICmpInst::ICMP_EQ:
        case ICmpInst::ICMP_ULE:
            //TODO 非零预测
            if (false)
                return ConstantInt::getFalse();
            break;
        case ICmpInst::ICMP_NE:
        case ICmpInst::ICMP_UGT:
            //TODO 非零预测
            if (false)
                return ConstantInt::getTrue();
            break;
        case ICmpInst::ICMP_SLT: {
            break;
        }
        case ICmpInst::ICMP_SLE: {
            break;
        }
        case ICmpInst::ICMP_SGE: {
            break;
        }
        case ICmpInst::ICMP_SGT: {
            break;
        }
    }

    return nullptr;
}

static bool isKnownNonZero(Value *V, unsigned dep, Dominate *dt) {
    if (ConstantInt *v = dyn_cast<ConstantInt>(V))
        return !(v->isZero());
    if (dep++ >= 10) return false;
    //TODO
    return false;
}

static bool isAddofNonZero(Value *V1, Value *V2, unsigned dep, Dominate *dt) {
    const BinaryOperator *BO = dyn_cast<BinaryOperator>(V1);
    if (!BO || BO->getOpcode() != Instruction::Add)
        return false;
    Value *Op = nullptr;
    if (V2 == BO->getOperand(0))
        Op = BO->getOperand(1);
    else if (V2 == BO->getOperand(1))
        Op = BO->getOperand(0);
    else
        return false;
    return isKnownNonZero(Op, dep + 1, dt);
}

static bool isKnownNonEqual(Value *v1, Value *v2, unsigned dep, Dominate *dt) {
    if (v1 == v2) return false;
    if (dep >= 10) return false;
    auto b1 = dyn_cast<BinaryOperator>(v1);
    auto b2 = dyn_cast<BinaryOperator>(v2);
    if (b1 && b2 && b1->getOpcode() == b2->getOpcode()) {
        switch (b1->getOpcode()) {
            case Instruction::Add:
            case Instruction::Sub:
                if (b1->getOperand(0) == b2->getOperand(0))
                    return isKnownNonEqual(b1->getOperand(1), b2->getOperand(1), dep + 1, dt);
                if (b1->getOperand(1) == b2->getOperand(1))
                    return isKnownNonEqual(b1->getOperand(0), b2->getOperand(0), dep + 1, dt);
                break;
            case Instruction::Mul:
                //a*b==a*b在模2^N意义下
                if (b1->getOperand(1) == b2->getOperand(1) &&
                    isa<ConstantInt>(b1->getOperand(1)) &&
                    !(dyn_cast<ConstantInt>(b1->getOperand(1))->isZero()))
                    return isKnownNonEqual(b1->getOperand(0), b2->getOperand(0), dep + 1, dt);
                break;
            default:
                break;
        }
    }
    if (isAddofNonZero(v1, v2, dep, dt) || isAddofNonZero(v2, v1, dep, dt))
        return true;
    return false;
}

static Value *simplifyICmpWithBinop(ICmpInst::Predicate Pred, Value *LHS, Value *RHS, int dep, Dominate *dt) {
    BinaryOperator *b1 = dyn_cast<BinaryOperator>(LHS);
    BinaryOperator *b2 = dyn_cast<BinaryOperator>(RHS);
    if (dep && (b1 || b2)) {
        Value *a=nullptr, *b=nullptr, *c=nullptr, *d=nullptr;
        if (b1 && b1->getOpcode() == Instruction::Add) {
            a = b1->getOperand(0);
            b = b1->getOperand(1);
        }
        if (b2 && b2->getOpcode() == Instruction::Add) {
            c = b2->getOperand(0);
            d = b2->getOperand(1);
        }
        //icmp (a+b),a
        if ((a == RHS || b == RHS) && !isOverflowing)
            if (Value *v = SimplifyICmp(Pred, a == RHS ? b : a, ConstantInt::getFalse(), dep - 1, dt))
                return v;
        if ((c == LHS || d == LHS) && !isOverflowing)
            if (Value *v = SimplifyICmp(Pred, ConstantInt::getFalse(), c == LHS ? d : c, dep - 1, dt))
                return v;
        //icmp (a+b),(a+c)
        if (a && c && (a == c || a == d || b == c || b == d) && !isOverflowing) {
            Value *y, *z;
            if (a == c) {
                y = b;
                z = d;
            } else if (a == d) {
                y = b;
                z = c;
            } else if (b == c) {
                y = a;
                z = d;
            } else {
                y = a;
                z = c;
            }
            if (Value *v = SimplifyICmp(Pred, y, z, dep - 1, dt))
                return v;
        }
    }

    //TODO

    return nullptr;
}

static Value *threadICmpOverPhi(ICmpInst::Predicate Pred, Value *LHS, Value *RHS, int dep, Dominate *dt) {
    if (!dep--) return nullptr;

    // Make sure the phi is on the LHS.
    if (!isa<PHINode>(LHS)) {
        std::swap(LHS, RHS);
        Pred = ICmpInst::getSwappedPredicate(Pred);
    }
    PHINode *PI = dyn_cast<PHINode>(LHS);

    if (!valueDominatesPHI(RHS, PI, dt))
        return nullptr;

    Value *CommonValue = nullptr;
    for (unsigned u = 0, e = PI->getNumIncomingValues(); u < e; ++u) {
        Value *Incoming = PI->getIncomingValue(u);
        Instruction *InTI = PI->getIncomingBlock(u)->getTerminator();
        if (Incoming == PI) continue;

        Value *V = SimplifyICmp(Pred, Incoming, RHS, dep, dt);

        if (!V || (CommonValue && V != CommonValue))
            return nullptr;
        CommonValue = V;
    }

    return CommonValue;
}

Value *SimplifyICmp(ICmpInst::Predicate Pred, Value *LHS, Value *RHS, int dep, Dominate *dt) {
    if (ConstantInt *clhs = dyn_cast<ConstantInt>(LHS)) {
        if (ConstantInt *crhs = dyn_cast<ConstantInt>(RHS))
            return ConstFoldICmp(Pred, clhs, crhs);
        std::swap(LHS, RHS);
        Pred = ICmpInst::getSwappedPredicate(Pred);
    }
    if (isa<UndefValue>(RHS) && ICmpInst::isEquality(Pred))
        return UndefValue::get(Module::getInt1Ty());
    if (LHS == RHS || isa<UndefValue>(RHS))
        return ConstantInt::get(ICmpInst::isTrueWhenEqual(Pred));
    if (Value *V = simplifyICmpWithZero(Pred, LHS, RHS))
        return V;
    if (ICmpInst::isEquality(Pred) && isKnownNonEqual(LHS, RHS, 0, dt))
        return Pred == ICmpInst::ICMP_NE ? ConstantInt::getTrue() : ConstantInt::getFalse();

    if (Value *V = simplifyICmpWithBinop(Pred, LHS, RHS, dep, dt))
        return V;
    if (isa<PHINode>(LHS) || isa<PHINode>(RHS))
        if (Value *V = threadICmpOverPhi(Pred, LHS, RHS, dep, dt))
            return V;
    return nullptr;
}
Value *SimplifyGEP(Instruction *inst) {
    //TODO
}
static Value *SimplifyPHI(PHINode *pn, Dominate *dt) {
    Value *CommonValue = nullptr;
    bool HasUndefInput = false;
    for(int i=0;i<pn->getNumIncomingValues();i++)
    {
        Value *Incoming=pn->getIncomingValue(i);
        if (Incoming == pn) continue;
        if (isa<UndefValue>(Incoming)) {
            // Remember that we saw an undef value, but otherwise ignore them.
            HasUndefInput = true;
            continue;
        }
        if (CommonValue && Incoming != CommonValue)
            return nullptr;  // Not the same, bail out.
        CommonValue = Incoming;
    }


    if (!CommonValue)
        return UndefValue::get(pn->getType());

    if (HasUndefInput)
        return valueDominatesPHI(CommonValue, pn,dt) ? CommonValue : nullptr;

    return CommonValue;
}

Value *SimplifyInst(Instruction *inst, Dominate *dt) {
    Value *result = nullptr;
    switch (inst->getOpcode()) {
        case Instruction::Add:
            result = SimplifyAdd(inst->getOperand(0), inst->getOperand(1), 3, dt);
            break;
        case Instruction::Sub:
            result = SimplifySub(inst->getOperand(0), inst->getOperand(1), 3, dt);
            break;
        case Instruction::Mul:
            result = SimplifyMul(inst->getOperand(0), inst->getOperand(1), 3, dt);
            break;
        case Instruction::SDiv:
            result = SimplifySDiv(inst->getOperand(0), inst->getOperand(1), 3, dt);
            break;
        case Instruction::UDiv:
            result = SimplifyUDiv(inst->getOperand(0), inst->getOperand(1), 3, dt);
            break;
        case Instruction::SRem:
            result = SimplifySRem(inst->getOperand(0), inst->getOperand(1), 3, dt);
            break;
        case Instruction::URem:
            result = SimplifyURem(inst->getOperand(0), inst->getOperand(1), 3, dt);
            break;
        case Instruction::Shl:
            result = SimplifyShl(inst->getOperand(0), inst->getOperand(1), 3, dt);
            break;
        case Instruction::LShr:
            result = SimplifyLShr(inst->getOperand(0), inst->getOperand(1), 3, dt);
            break;
        case Instruction::AShr:
            result = SimplifyAShr(inst->getOperand(0), inst->getOperand(1), 3, dt);
            break;
        case Instruction::And:
            result = SimplifyAnd(inst->getOperand(0), inst->getOperand(1), 3, dt);
            break;
        case Instruction::Or:
            result = SimplifyOr(inst->getOperand(0), inst->getOperand(1), 3, dt);
            break;
        case Instruction::Xor:
            result = SimplifyXor(inst->getOperand(0), inst->getOperand(1), 3, dt);
            break;
        case Instruction::ICmp:
            result = SimplifyICmp(dyn_cast<ICmpInst>(inst)->getPredicate(), inst->getOperand(0), inst->getOperand(1), 3, dt);
            break;
        case Instruction::GetElementPtr:
            //result=SimplifyGEP(dyn_cast<GetElementPtrInst>(inst)->getSourceElementType(),inst->get);
            //TODO
            break;
        case Instruction::PHI:
            result = SimplifyPHI(dyn_cast<PHINode>(inst), dt);
            break;
        case Instruction::Call:
            //目前只考虑函数内联，不做这个
            //result=SimplifyCall(dyn_cast<CallInst>(inst));
            break;
        default:
            result = Const_Eval(inst);
            break;
    }
    return result;
}