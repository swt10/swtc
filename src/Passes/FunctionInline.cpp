#include "FunctionInline.h"


std::unordered_map<Function *, FCGnode *> FCGmap;
std::unordered_set<CallInst *> inlineableList;
std::unordered_set<Function *> deleteList;
std::unordered_set<Function *> ignoredList;

void findUsedFunction(Function *main){
    FCGmap[main]->isVisited = true;
    for(auto callee : FCGmap[main]->callees){
        if(callee.second > 0){
            if(callee.first != FCGmap[main]){
                findUsedFunction(callee.first->F);
            }
        }
    }
}

void deleteUnusedFunction(Function *main){
    for(auto F = main->getParent()->getFunctionList().head; F; F = F->next){
        FCGmap[F]->isVisited = false;
    }
    findUsedFunction(main);
    for(auto F = main->getParent()->getFunctionList().head; F; F = F->next){
        if(!FCGmap[F]->isVisited){
            deleteList.insert(F);
        }
    }
    for(auto F : deleteList){
        FCGmap.erase(F);
        F->eraseFromParent();
    }
}

void runFunctionInline(Module *M){
    init(M);
    Function *main = nullptr;
    for(auto F = M->getFunctionList().head; F; F = F->next){
        if(F->getName() == "main"){
            main = F;
            inlineFunc(F);
        } else if(1 && FCGmap[F]->isRecursive) {
            inlineFunc(F);
        }
    }
    deleteUnusedFunction(main);
    tailRecursive2Loop(main);
    for(auto F = M->getFunctionList().head; F; F = F->next){
        if(1 && FCGmap[F]->isRecursive && deleteList.find(F) == deleteList.end()) {
            tailRecursive2Loop(F);
        }
    }
    deleteUnusedFunction(main);
}

void init(Module *M){
    FCGmap.clear();
    inlineableList.clear();
    deleteList.clear();
    std::unordered_set<CallInst *> callInstList;
    for(auto F = M->getFunctionList().head; F; F = F->next){
        auto node = new FCGnode(F);
        FCGmap[F] = node;
    }
    for(auto F = M->getFunctionList().head; F; F = F->next){
        if(F->getEntryBlock() != nullptr){
            for(auto BB = F->getEntryBlock(); BB; BB = BB->next){
                for(auto inst = BB->getInstList().head; inst; inst = inst->next){
                    if(auto x = dyn_cast<CallInst>(inst)){
                        if(FCGmap[F]->callees.find(FCGmap[x->getCalledFunction()]) == FCGmap[F]->callees.end()){
                            FCGmap[F]->callees[FCGmap[x->getCalledFunction()]] = 1;
                        } else {
                            FCGmap[F]->callees[FCGmap[x->getCalledFunction()]]++;
                        }
                        if(x->getCalledFunction() == F){
                            FCGmap[F]->isRecursive = true;
                        }
                        callInstList.insert(x);
                    }
                }
            }
        } 
    }
    for(auto call : callInstList){
        if(!FCGmap[call->getCalledFunction()]->isRecursive && !call->getCalledFunction()->isBuildin() && FCGmap[call->getCalledFunction()]->getNumInsts() < 80){
            inlineableList.insert(call);
        }
    }
}

void inlineFunc(Function *F){
    for(auto BB = F->getEntryBlock(); BB; BB = BB->next){
        for(auto inst = BB->getInstList().head; inst; inst = inst->next){
            if(auto x = dyn_cast<CallInst>(inst)){
                if(inlineableList.find(x) != inlineableList.end()){
                    //切分BB
                    splitBB(inst);
                    //复制callee的BB，并做相应处理
                    handlecallee(x, x->getCalledFunction());
                    x->removeFromParent();
                    for(auto callee : FCGmap[x->getCalledFunction()]->callees){
                        if(FCGmap[F]->callees.find(callee.first) == FCGmap[F]->callees.end()){
                            FCGmap[F]->callees[callee.first] = callee.second;
                        } else {
                            FCGmap[F]->callees[callee.first] += callee.second;
                        }
                    }
                    FCGmap[F]->callees[FCGmap[x->getCalledFunction()]]--;
                    break;
                }
            }
        }
    }
}


void splitBB(Instruction *call){
    BasicBlock *newBB = new BasicBlock("SecondHalf", call->getParent()->getParent(), call->getParent() == call->getParent()->getParent()->getBasicBlockList().getLast() ? nullptr : call->getParent()->next);
    auto inst = call->next;
    while(inst){
        auto p = inst->next;
        inst->removeFromParent();
        inst->setParent(newBB);
        newBB->getInstList().insertAtEnd(inst);
        inst = p;
    }
    for(inst = call->getParent()->getInstList().head; inst; inst = inst->next){
        for(auto use : inst->getUseList()){
            if(auto x = dyn_cast<PHINode>(use->getUser())){
                x->replaceUsesOfWith(call->getParent(), newBB);
            }
        }
    }
}

void handlecallee(CallInst *call, Function *callee){
    auto addBB = call->getParent()->next;
    std::map<Value *, Value *> valmap; 

    if(callee->getArgumentList().head != nullptr){
        int i = 0;
        for(auto arg = callee->getArgumentList().head; arg; arg = arg->next){
            valmap[arg] = call->getOperand(++i);
        }
    }    
    for(auto BB = callee->getEntryBlock(); BB; BB = BB->next){
        auto newBB = new BasicBlock(callee->getName(), call->getParent()->getParent(), addBB);
        valmap[BB] = newBB;
        for(auto inst = BB->getInstList().head; inst; inst = inst->next){
            auto newinst = inst->clone();
            newinst->setParent(newBB);
            newBB->getInstList().insertAtEnd(newinst);
            valmap[inst] = newinst;
        }
    }

    for(auto BB = callee->getEntryBlock(); BB; BB = BB->next){
        for(auto inst = BB->getInstList().head; inst; inst = inst->next){
            auto newinst = dyn_cast<Instruction>(valmap[inst]);
            for(int i = 0; i < newinst->getNumOperands(); ++i){
                if(valmap.find(newinst->getOperand(i)) != valmap.end()){
                    newinst->setOperand(i, valmap[newinst->getOperand(i)]);
                }
            }
        }
    }

    new BranchInst(call->getParent()->next, call->getParent());

    std::vector<Instruction *> retinsts;
    for(auto BB = callee->getEntryBlock(); BB; BB = BB->next){
        if(auto inst = dyn_cast<ReturnInst>(BB->getTerminator())){
            retinsts.push_back(dyn_cast<Instruction>(valmap[inst]));
        }
    }
    if(!callee->getReturnType()->isVoidTy()){
        auto alloca = new AllocaInst(call->getType(), "return Value" , call->getParent()->next->getInstList().head);
        for(auto ret : retinsts){
            auto store = new StoreInst(dyn_cast<ReturnInst>(ret)->getOperand(0), alloca, ret);
        }
        auto load = new LoadInst(alloca, "return Value", addBB->getInstList().head);
        call->replaceAllUsesWith(load);
    }
    for(auto ret : retinsts){
        new BranchInst(addBB, ret->getParent());
        ret->eraseFromParent();
    }
}

void fo(CallInst *call, std::unordered_map<CallInst *, int> &map){
    map[call] = 1;
    for(int i = 1; i < call->getNumOperands(); ++i){
        if(auto x = dyn_cast<CallInst>(call->getOperand(i))){
            if(x->getCalledFunction() ==  call->getParent()->getParent()){
                fo(x, map);
                map[call]++;
            } 
        }
    }
}

bool doTailRursive2Loop(CallInst *call, Function *F){    
    std::unordered_map<CallInst *, int> map; //只存调用自己的call/int = 0直接递归/int = 1单次尾递归/int = n嵌套递归，有n-1个参数为递归
    std::vector<Instruction *> list;    // 尾递归
    std::map<Value*, Value *> valmap;
    for(auto p = F->getArgumentList().head; p; p = p->next){
        for(auto inst = F->getEntryBlock()->getInstList().head; inst; inst = inst->next){
            if(auto x = dyn_cast<StoreInst>(inst)){
                if(x->getOperand(0) == p){
                    valmap[p] = x->getOperand(1);
                }
            }
        }
    }
    for(auto BB = F->getEntryBlock(); BB; BB = BB->next){
        for(auto inst = BB->getInstList().head; inst; inst = inst->next){
            if(auto x = dyn_cast<CallInst>(inst)){
                if(x->getCalledFunction() == F){
                    map[x] = 0;
                    if(isa<ReturnInst>(inst->next)){
                        list.push_back(inst);
                    }
                }  
            }
        }
    }
    for(auto inst : list){
        auto x = dyn_cast<CallInst>(inst);
        fo(x, map);
    }
    for(auto it : map){
        if(it.second != 1){
            return false;
        } 
    }
    for(auto call : list){ //无嵌套尾递归
        auto x = dyn_cast<CallInst>(call);
        if(map[x] == 1){
            int i = 0;
            for(auto p = F->getArgumentList().head; p; p = p->next){
                auto store = new StoreInst(x->getOperand(++i), valmap[p], call);
            }
            new BranchInst(F->getEntryBlock()->next, call->getParent());
            call->next->eraseFromParent();
            call->eraseFromParent();
        }
    }
    return true;
}

void tailRecursive2Loop(Function *F){  //函数F里的 callinst 调用的函数(不含F本身)若是尾递归则转换为循环
    deleteList.clear();
    for(auto BB = F->getEntryBlock(); BB; BB = BB->next){
        for(auto inst = BB->getInstList().head; inst; inst = inst->next){
            if(auto x = dyn_cast<CallInst>(inst)){
                if(FCGmap[x->getCalledFunction()]->isRecursive && x->getCalledFunction()!=F){
                    if(doTailRursive2Loop(x, x->getCalledFunction())){
                        splitBB(inst);
                        handlecallee(x, x->getCalledFunction());
                        x->removeFromParent();
                        for(auto callee : FCGmap[x->getCalledFunction()]->callees){
                            if(FCGmap[F]->callees.find(callee.first) == FCGmap[F]->callees.end()){
                                FCGmap[F]->callees[callee.first] = callee.second;
                            } else {
                                FCGmap[F]->callees[callee.first] += callee.second;
                            }
                        }
                        FCGmap[F]->callees[FCGmap[x->getCalledFunction()]]--;
                        break;
                    }
                }
            }
        }
    }
}