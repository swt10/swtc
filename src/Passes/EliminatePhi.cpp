#include "EliminatePhi.h"

extern std::map<Value *,Varinfo>Varinfomap;

std::map<std::pair<BasicBlock*,Value*>,int>PHIusecnt;


void LowerPHInode(PHINode *pn,Instruction *firstnonPhi)
{
    
    int num=pn->getNumIncomingValues();
    bool isallundef=true;
    for(int i=0;i<num;i++)
        if(!isa<UndefValue>(pn->getIncomingValue(i)))
        {
            isallundef=false;
            break;
        }
    if(isallundef)
    {
        pn->replaceAllUsesWith(UndefValue::get(pn->getType()));
        pn->eraseFromParent();
        return ;
    }
    for(int i=0;i<num;i++)
    {
        --PHIusecnt[std::make_pair(pn->getIncomingBlock(i),pn->getIncomingValue(i))];
    }
    auto AI=new AllocaInst(pn->getType(),nullptr,"",pn->getFunciton()->getEntryBlock()->getInstList().head);
    for(int i=num-1;i>=0;i--)
    {
        Value *src=pn->getIncomingValue(i);
        BasicBlock *srcbb=pn->getIncomingBlock(i);
        new StoreInst(src,AI,srcbb->getTerminator());
    }
    auto Li=new LoadInst(AI,"lp",firstnonPhi);
    pn->replaceAllUsesWith(Li);
    pn->eraseFromParent();
}

void EphiprocessBlock(BasicBlock *bb)
{
    Instruction *end=bb->getFirstNonPHI();//因为bb必有一个终结指令，所以一定能找到
    for(auto inst:bb->getInstList().copy())
    {
        if(PHINode *pn=dyn_cast<PHINode>(inst))
        {
            LowerPHInode(pn,end);
        }
    }
}

void EliminatePhi(Function *func)
{
    if(func->isBuildin())return ;
    bool ischange=false;
    //TODO
    // std::map<BasicBlock*,std::vector<Value*>>Livein;
    // for(auto bb:func->getBasicBlockList())
    // Livein[bb]=std::vector<Value*>();
    // for(auto it:Varinfomap)
    // {
    //     Value *v=it.first;
    //     Varinfo &info=it.second;
    //     if(isa<ConstantInt>(v))continue;
    //     for(auto x:info.Alivebbs)
    //     {
    //         Livein[]
    //     }
    // }
    for(auto bb:func->getBasicBlockList())
        for(auto inst:bb->getInstList())
        {
            if(auto pn=dyn_cast<PHINode>(inst))
            for(int i=0;i<pn->getNumIncomingValues();i++)
            {
                PHIusecnt[std::make_pair(pn->getIncomingBlock(i),pn->getIncomingValue(i))]++;
            }
        }
    for(auto bb:func->getBasicBlockList())
         EphiprocessBlock(bb);
}