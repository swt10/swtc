#include "PassManager.h"
#include "config.h"

#include <cstring>
#include <iostream>
#include <variant>

namespace SWTC {

using FunctionPass = void (*)(Function *);
using ModulePass = void (*)(Module *);
// waitting for add
using Pass = std::variant<FunctionPass, ModulePass>;
using PassInfo = std::pair<Pass, const char *>;

#define DEFINE_PASS(p, isRun) { \
    if(isRun){  \
        IRPassOrder.push_back(std::make_pair( p, #p )); \
    }}

void qwer(Function *f) {}

std::vector<PassInfo> IRPassOrder;


static inline void run_pass(PassTarget p, const PassInfo &desc) {
    auto &pass = std::get<0>(desc);
    auto run_pass = std::string("Running Pass ") + std::get<1>(desc);

    std::visit(
        overload{
            [&](Module *M) {
                std::visit(
                    overload{[&](ModulePass pass) {
                                 pass(M);
                             },
                             [&](FunctionPass pass) {
                                 for (auto *f = M->getFunctionList().head; f; f = f->next) {
                                     if (!f->isBuildin()) {
                                     }
                                     pass(f);
                                 }
                             }},
                    pass);
            },
            // Waiting
        },
        p);
}

void run_passes(PassTarget p, bool opt) {

    

    DEFINE_PASS(GlobalOpt, !isNoRunPass)
    DEFINE_PASS(SimplifyCFG,!isNoRunPass)
    // // // DEFINE_PASS(runFunctionInline, !isNoRunPass)
    DEFINE_PASS(mem2Reg, isMem2Reg)
    // DEFINE_PASS(MemSSA, !isNoRunPass)
    DEFINE_PASS(SimplifyCFG,!isNoRunPass)
    DEFINE_PASS(CSE, !isNoRunPass)
    // DEFINE_PASS(MemSSA, !isNoRunPass)
    DEFINE_PASS(GVN, !isNoRunPass)
    DEFINE_PASS(CSE, !isNoRunPass)
    DEFINE_PASS(SimplifyCFG,!isNoRunPass)
    // DEFINE_PASS(MemSSA, !isNoRunPass)
    // // DEFINE_PASS(runLoopInvariantMove, !isNoRunPass)
    DEFINE_PASS(CSE, !isNoRunPass)
    // DEFINE_PASS(MemSSA, !isNoRunPass)
    DEFINE_PASS(SimplifyCFG,!isNoRunPass)
    DEFINE_PASS(GVN, !isNoRunPass)
    DEFINE_PASS(runSplitGep, true)
    // DEFINE_PASS(CSE, !isNoRunPass)
    // DEFINE_PASS(GVN, !isNoRunPass) 
    // DEFINE_PASS(LiveVariables, !isNoRunPass) 
    // DEFINE_PASS(regAlloc,!isNoRunPass)
    DEFINE_PASS(EliminatePhi, true)



    if (std::get_if<Module *>(&p)) {
        for (auto &desc : IRPassOrder) {
            std::cerr << COLOR_BOLDYELLOW << "RUNNING PASS BEGIN: " << desc.second << "\n" << COLOR_RESET;
            run_pass(p, desc);
            std::cerr << COLOR_YELLOW << "RUNNING PASS END: " << desc.second << "\n" << COLOR_RESET;
        }
    }
}

void print_passes() {
    std::cerr << "IR Passes:" << std::endl;
    for (auto &[pass, name] : IRPassOrder) {
        std::cerr << "* " << name << std::endl;
    }
}
}  // namespace SWTC