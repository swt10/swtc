#include "LoopInvariant.h"

extern LoopInfo *li;

std::vector<Instruction *> instsWaitMove;


std::map<BasicBlock *, int> BBmap;
std::map<Instruction * , int> instmap;

void runLoopInvariantMove(Function *F){
    if(F->getEntryBlock() != nullptr){
        int i = 0;
        for(auto BB = F->getEntryBlock(); BB; BB = BB->next){
            BBmap[BB] = i++;
        }
        for(auto BB = F->getEntryBlock(); BB; BB = BB->next){
            i = 0;
            for(auto inst = BB->getInstList().head; inst; inst = inst->next){
                instmap[inst] = i++; 
            }
        }
        if(!li) li = new LoopInfo;
        li->identify_loops(F);
        for(auto loop : li->loops){
            if(loop->subLoops.empty()){
                while(loop){
                    instsWaitMove.clear();
                    findInvariants(loop);
                    if(!instsWaitMove.empty()){
                        InvariantsMoveOut(loop);
                    }
                    loop = loop->parent;
                }
            }
        }
    }
}

void findInvariants(Loop *loop){
    std::unordered_set<Value *> instsInLoop;
    for(auto BB : loop->LoopNodes){
        for(auto inst = BB->getInstList().head; inst; inst = inst->next){
            instsInLoop.insert(inst);
        }
    }
    bool change = true;
    while(change){
        change = false; // 本次执行是否有外提
        for(auto BB : loop->LoopNodes){
            for(auto inst = BB->getInstList().head; inst; inst = inst->next){
                if((isa<GetElementPtrInst>(inst) || isa<LoadInst>(inst) || isa<StoreInst>(inst) || isa<BinaryOperator>(inst)) && instsInLoop.find(inst) != instsInLoop.end()){
                    bool flag = true; //是否可以外提
                    for(int i = 0; i < inst->getNumOperands(); i++){
                        if(instsInLoop.find(inst->getOperand(i)) != instsInLoop.end()){
                            flag = false;
                        }
                    }
                    if(!flag) continue;
                    if(isa<LoadInst>(inst)){
                        auto p1 = dyn_cast<LoadInst>(inst)->getPointerOperand();
                        while(auto x = dyn_cast<GetElementPtrInst>(p1)){
                            p1 = x->getOperand(0);
                        }
                        for(auto it : instsInLoop){
                            if(isa<StoreInst>(it)){
                                auto p2 = dyn_cast<StoreInst>(it)->getOperand(1);{
                                    while(auto x = dyn_cast<GetElementPtrInst>(p2)){
                                        p2 = x->getOperand(0);
                                    }
                                    if(p1 == p2){
                                        flag = false;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if(flag){
                        instsInLoop.erase(inst);
                        instsWaitMove.push_back(inst);
                        change = true;
                    }
                }
            }
        }
    }
}

void InvariantsMoveOut(Loop *loop){
    auto header = loop->head;
    
    //存放之前header->pred
    std::vector<BasicBlock *> preds;
    header->pred_init();
    for(auto pred: header->pred){
        preds.push_back(pred);
    }

    //外提变量放置位置
    BasicBlock *newBB = new BasicBlock("InvariantBB", header->getParent(), header);
    for(auto inst : instsWaitMove){
        std::cerr<< instmap[inst];
        if(isa<GetElementPtrInst>(inst)) std::cerr<<" is GEP  ";
        if(isa<LoadInst>(inst)) std::cerr<<" is Load  ";
        if(isa<StoreInst>(inst)) std::cerr<<" is Store ";
        if(isa<BinaryOperator>(inst)) std::cerr<<" is Binary";
        std::cerr<< "  in BB " << BBmap[inst->getParent()]<<" 被提到BB " <<BBmap[header]<<" 前"<< std::endl;
        inst->removeFromParent();
        inst->setParent(newBB);
        newBB->getInstList().insertAtEnd(inst);
    }
    // auto p = loop->parent;
    // while(p){
    //     auto it = std::find(p->LoopNodes.begin(), p->LoopNodes.end(), header);
    //     p->LoopNodes.insert(it, newBB);
    //     p = p->parent;
    // }

    //处理Branch
    new BranchInst(header, newBB);
    for(auto pred : preds){
        if(!li->isBBInLoop(pred, loop)){
            pred->getInstList().getLast()->replaceUsesOfWith(header, newBB);
        }
    }

    //处理Phi
    for(auto inst = header->getInstList().head; inst; inst = inst->next){
        if(auto x = dyn_cast<PHINode>(inst)){
            //phi结点里来自循环外的BB位置 
            std::vector<BasicBlock *> BBs;
            for(int i = 0; i < x->getNumIncomingValues(); ++i){
                if(!li->isBBInLoop(x->getIncomingBlock(i), loop)){
                    BBs.push_back(x->getIncomingBlock(i));
                }
            }
            if(BBs.size() == 1){
                x->replaceUsesOfWith(BBs[0], newBB);
            } else if(BBs.size() > 1) {
                auto phi = new PHINode(inst->getType(), "newPHI", newBB->getInstList().head);   
                for(int i = 0; i < BBs.size(); ++i){
                    phi->addIncoming(x->getIncomingValue(x->getBasicBlockIndex(BBs[i])), BBs[i]);
                }
                for(int i = 0; i < BBs.size(); ++i){
                    x->removeIncomingValue(BBs[i]);
                }
                if(x->getNumIncomingValues() == 0){
                    x->replaceAllUsesWith(phi);
                    inst->eraseFromParent();
                } else {
                    x->addIncoming(phi, newBB);
                }
            }
        }
    }
}