#include "Loop.h"
#include "Value.h"

LoopInfo *li;

void getLoopNodes(BasicBlock *BB, Loop *loop){
    BB->isVisit = true;
    for(auto x : BB->pred){
        if(!x->isVisit){
            getLoopNodes(x, loop);
        }
    }
    loop->LoopNodes.insert(BB);
}

void setLoopParent(Loop *loop){
    for(auto subLoop : loop->subLoops){
        subLoop->parent = loop;
        setLoopParent(subLoop);
    }
}

void LoopInfo::identify_loops(Function *F){
    if(F->getEntryBlock() != nullptr){
        map.clear();
        loops.clear();
        backEdge.clear();
        BB_deepest_loop.clear();
        
        //构造CFGnode并初始化
        buildCFG(F, map);

        //递归计算每个CFGnode所处的最深循环的循环头
        trav_loops_dfs(map[F->getEntryBlock()], 1);

        //得到回边
        for(auto BB = F->getEntryBlock(); BB; BB = BB->next){
            if(map[BB]->isLoopHeader){
                for(auto x : BB->pred){
                    if(map[x]->iloop_header == map[BB]){
                        backEdge.insert({x, BB});
                    }
                }
            }
        }

        //用回边计算自然循环
        for(auto it : backEdge){
            bool flag = false;
            for(auto BB = F->getEntryBlock(); BB; BB = BB->next){
                BB->isVisit = false;
            }
            for(auto loop : loops){
                if(loop->head == it.second){
                    flag = true;
                    it.second->isVisit = true;
                    loop->LoopNodes.insert(it.second);
                    getLoopNodes(it.first, loop);
                    break;
                }
            }
            if(!flag){
                auto loop = new Loop;
                loop->head = it.second;
                it.second->isVisit = true;        
                loop->LoopNodes.insert(it.second);
                getLoopNodes(it.first, loop);
                loops.insert(loop);
            }
        }


        // std::map<BasicBlock *, int> map1;
        // int i = 0;
        // for(auto BB = F->getEntryBlock(); BB; BB = BB->next){
        //     map1[BB] = i++;
        // }
        // for(auto BB = F->getEntryBlock(); BB; BB = BB->next){
        //     std::cerr<<BB<<" "<<map1[BB]<<" iloopheader ";
        //     if(map[BB]->iloop_header)std::cerr<<map1[map[BB]->iloop_header->BB];
        //     if(map[BB]->isLoopHeader)std::cerr<<"  "<<map1[BB]<<" is head";
        //     std::cerr<<std::endl;
        // }
        // for(auto loop : loops){
        //     std::cerr<<map1[loop->head] << "  head\n";
        //     for(auto node : loop->LoopNodes){
        //         std::cerr <<map1[node]<<" ";
        //     }
        //     std::cerr<<std::endl;
        // }

        //记录每个BB所处的最深循环
        for(auto BB = F->getEntryBlock(); BB; BB = BB->next){
            if(map[BB]->isLoopHeader){
                for(auto loop : loops){
                    if(BB == loop->head){
                        BB_deepest_loop[BB] = loop;
                    }
                }
            } else if(map[BB]->iloop_header){
                for(auto loop : loops){
                    if(!map[BB]->isLoopHeader){
                        if(map[BB]->iloop_header == map[loop->head]){
                            BB_deepest_loop[BB] = loop;
                        }
                    }
                }    
            }
        }
        //计算循环的嵌套关系
        for(auto loop : loops){
            loop->parent = nullptr;
            loop->subLoops.clear();
        }
        for(auto loop :loops){
            for(auto BB : loop->LoopNodes){
                if(BB != loop->head && map[BB]->isLoopHeader){
                    loop->subLoops.insert(BB_deepest_loop[BB]);
                }
            }
        }
        for(auto loop : loops){
            setLoopParent(loop);
        }
    }
}



void LoopInfo::buildCFG(Function *F, std::unordered_map<BasicBlock *, CFGnode *> &map){
    for(auto BB = F->getEntryBlock(); BB; BB = BB->next){
        auto node = new CFGnode(BB);
        map[BB] = node;
        node->isLoopHeader = false;
    }
    for(auto BB = F->getEntryBlock(); BB; BB = BB->next){
        auto node = map[BB];
        BB->succ_init();
        BB->pred_init();
        for(auto succ : BB->succ){
            node->succ.insert(map[succ]);
        }
        for(auto pred : BB->pred){
            node->pred.insert(map[pred]);
        }
    }
}

CFGnode *LoopInfo::trav_loops_dfs(CFGnode *b0, int DFSP_pos){
    b0->traversed = true;
    b0->DFSP_pos = DFSP_pos;
    for(auto b : b0->succ){
        if(!b->traversed){
            auto nh = trav_loops_dfs(b, DFSP_pos + 1);
            tag_lhead(b0, nh);
        } else {
            if(b->DFSP_pos > 0){
                b->isLoopHeader = true;
                tag_lhead(b0, b);
            } else if(b->iloop_header == nullptr){
            } else {
                auto h = b->iloop_header;
                if(h->DFSP_pos > 0){
                    tag_lhead(b0, h);
                }
            }
        }
    }
    b0->DFSP_pos = 0;
    return b0->iloop_header;
}

void LoopInfo::tag_lhead(CFGnode *b, CFGnode *h){
    if(b == h || h == nullptr){return;}
    auto cur1 = b;
    auto cur2 = h;
    while(cur1->iloop_header != nullptr){
        auto ih = cur1->iloop_header;
        if(ih == cur2){return;}
        if(ih->DFSP_pos < cur2->DFSP_pos){
            cur1->iloop_header = cur2;
            cur2 = ih;
        } else{
            cur1 = ih;
        }
    }
    cur1->iloop_header = cur2;
}

unsigned int LoopInfo::getBBDepth(BasicBlock *BB){
    return BB_deepest_loop[BB]->getDepth();
}

Loop *LoopInfo::getLoop(BasicBlock *BB){
    return BB_deepest_loop[BB];
}
bool LoopInfo::isBBInLoop(BasicBlock *BB, Loop *loop){
    for(auto x : loop->LoopNodes){
        if(BB == x){
            return true;
        }
    }
    return false;
}