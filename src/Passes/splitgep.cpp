#include "splitgep.h"
extern std::map<Value *, std::list<MemoryAccess *>> VtoM;  //call指令会有一串访问

extern std::map<BasicBlock *, std::list<MemoryAccess *>> BtoA;  //bb to AccessList
extern std::map<BasicBlock *, std::list<MemoryAccess *>> BtoD;  //bb defList
extern std::map<Function *, std::set<Value *>> funUse;

void runSplitGep(Module *M) {
    BtoA.clear();
    BtoD.clear();
    VtoM.clear();
    funUse.clear();
    for (auto F = M->getFunctionList().head; F; F = F->next) {
        if (F->getEntryBlock() != nullptr) {
            for (auto BB = F->getEntryBlock(); BB; BB = BB->next) {
                splitgep(BB);
            }
        }
    }
}

void splitgep(BasicBlock *BB) {
    for (auto inst = BB->getInstList().head; inst;) {
        if (auto x = dyn_cast<GetElementPtrInst>(inst)) {
            std::vector<int> sizes;
            auto ty = dyn_cast<PointerType>(x->getOperand(0)->getType())->getElementType();
            if (ty->getTypeID() == Type::TypeID::IntegerTyID) {
                sizes.push_back(1);
            } else {
                auto A = dyn_cast<ArrayType>(ty);
                sizes.push_back(A->getNumElements());
                while (A->getElementType()->getTypeID() != Type::TypeID::IntegerTyID) {
                    A = dyn_cast<ArrayType>(A->getElementType());
                    sizes.push_back(A->getNumElements());
                }
                sizes.push_back(1);
            }
            for (int i = 0; i < sizes.size(); i++) {
                for (int j = i + 1; j < sizes.size(); j++) {
                    sizes[i] *= sizes[j];
                }
            }
            auto it = x->getOperand(0);
            for (int i = x->getNumIndices(); i > 0; i--) {
                {
                    // if(auto Ci=dyn_cast<ConstantInt>(x->getOperand(i)))
                    // {
                    //     if(Ci->getValue()==0)continue;
                    // }
                    auto offset = BinaryOperator::create(Instruction::Mul, x->getOperand(i), ConstantInt::get(sizes[i - 1] * 4), "mul", inst);
                    auto addr = BinaryOperator::create(Instruction::Add, it, offset, "add", inst);
                    it = addr;
                }
            }
            inst = inst->next;
            inst->prev->replaceAllUsesWith(inst->prev->prev);
            inst->prev->eraseFromParent();
        } else {
            inst = inst->next;
        }
    }
}