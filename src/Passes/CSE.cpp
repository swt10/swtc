#include "CSE.h"
#include "InstSimplify.h"
#include "GVN.h"
#include "MemSSA.h"

#include <deque>
#include <set>

extern Dominate *dt;
extern GVNimpl g;
int CSEcur;


extern std::map<Value *, std::list<MemoryAccess *>> VtoM;
extern std::map<Function *, std::set<Value *>> funUse;


std::map<Instruction*,Value*>availValue;
std::map<Value*,unsigned>availInvari;
std::map<Value*,std::pair<Instruction*,unsigned>> AvailableLoads;
std::map<Value*,std::pair<Instruction*,unsigned>> AvailableCalls;


bool overrideStore(Instruction *a,Instruction *b)
{
    auto sa=dyn_cast<StoreInst>(a);
    auto sb=dyn_cast<StoreInst>(b);
    if(!sa||!sb)return false;
    if(sa->getPointerOperand()!=sb->getPointerOperand())
        return false;
    return true;
}

static bool canHandle(Instruction *Inst) {
    // This can only handle non-void readnone functions.
    if (CallInst *CI = dyn_cast<CallInst>(Inst))
        return CI->doesNotAccessMemory() && !CI->getType()->isVoidTy();
    return isa<BinaryOperator>(Inst) || isa<GetElementPtrInst>(Inst) || isa<CmpInst>(Inst) ;
}
bool isOponInvariantMem(Instruction *inst,unsigned cur)
{


    Value *memloc=nullptr;
    if(auto si=dyn_cast<StoreInst>(inst))
        memloc=si->getPointerOperand();
    if(auto li=dyn_cast<LoadInst>(inst))
        memloc=li->getPointerOperand();
    if(!availInvari.count(memloc))
    return false;
    return availInvari[memloc]<=cur;
}
bool isSameGeneration(unsigned g1,unsigned g2,Instruction *inst1,Instruction *inst2)
{
    if(g1==g2)return true;
    if(VtoM.empty())return false;
    auto IMA1=VtoM[inst1];
    if(IMA1.empty())return true;
    auto IMA2=VtoM[inst2];
    if(IMA2.empty())return true;
    auto ma=dyn_cast<MemoryAccess>(IMA2.back());
    auto earlyma=dyn_cast<MemoryAccess>(IMA1.back());
    if(ma->getDefiningAccess()==nullptr)return true;
    auto def=ma->getDefiningAccess();
    if(def==earlyma)return  true;
    if(def->Block==nullptr)return true;
    if(def->Block!=earlyma->Block)
        return dt->b1Domb2(def->Block,earlyma->Block);
    if(isa<MemoryPhi>(def))return true;
    def->Block->renumberInstructions();
    return def->MemInstruction->Order<=earlyma->MemInstruction->Order;
}
Value *matchingValue(std::pair<Instruction*,unsigned> &Val,Instruction*inst,unsigned cur)
{
    if(Val.first==nullptr)return nullptr;
    bool isStore=isa<StoreInst>(inst);
    Instruction *match=(isStore?inst:Val.first);
    Instruction *other=(isStore?Val.first:inst);
    Value *result=nullptr;
    auto getCreat=[](Instruction *m){
        if(auto Si=dyn_cast<StoreInst>(m))
            return (Value*)(Si->getOperand(0));
        if(auto Li=dyn_cast<LoadInst>(m))
            return (Value*)Li;
    };
    if(isStore){
        result=getCreat(match);
        if(Val.first!=result)return nullptr;
    }
    if(!isOponInvariantMem(inst,Val.second)&&
        !isSameGeneration(Val.second,cur,Val.first,inst))
        return nullptr;
    if(!result)
        result=getCreat(match);
    return result;
}

bool handleBrcond(Instruction *inst,BranchInst*br,BasicBlock*bb,BasicBlock*pred)
{
    auto isfirst =(br->getSuccessor(0)==bb)?ConstantInt::getTrue():ConstantInt::getFalse();
    auto matchbinop=[](Instruction *inst,unsigned opcode,Value*&l,Value*&r){
        if(touchinst(inst,Instruction::And,l,r)||touchinst(inst,Instruction::Or,l,r))
        return true;
        return false;
    };
    unsigned propagateop=(isfirst->getValue()?Instruction::And:Instruction::Or);
    bool ischange=false;
    std::vector<Instruction*>worklist;
    std::set<Instruction*>vis;
    worklist.push_back(inst);
    while(!worklist.empty()){
        Instruction *cur =worklist.back();
        worklist.pop_back();
        availValue[cur]=isfirst;
        auto p=std::make_pair(pred,bb);
        if(int cnt=g.replacedomuse(cur,isfirst,p)){
            ischange=true;
        }
        Value *l,*r;
        if(matchbinop(cur,propagateop,l,r))
        {
            for(auto op:{l,r}){
                if(Instruction *opi=dyn_cast<Instruction>(op)){
                    if(canHandle(opi)&&!vis.count(opi))
                    {
                        vis.insert(opi);
                        worklist.push_back(opi);
                    }
                }
            }
        }

    }
    return ischange;
}

bool processNode(BasicBlock *bb) {
    bool ischange = false;
    //若有多个pred,则假定内存值失效
    if (bb->pred.size() != 1)
        ++CSEcur;
    else if (BasicBlock *pred = bb->pred[0]) {
        auto bt = dyn_cast<BranchInst>(pred->getTerminator());
        if (bt && bt->isConditional()) {
            auto cond = dyn_cast<Instruction>(bt->getCondition());
            if (cond && canHandle(cond))
            {
                ischange |= handleBrcond(cond,bt,bb,pred);
                
            }
        }
    }
    Instruction *lastStore=nullptr;
    for(auto x:bb->getInstList().copy())
    {
        if(isinstructionnormaldead(x))
        {
            //TODO memSSA
            x->eraseFromParent();
            #ifdef DEBUG
            std::cerr<<"CSE delete dead inst"<<x->getName()<<"\n";
            #endif
            ischange=true;
            continue;
        }
        if(Value *v=SimplifyInst(x,dt)){
            if(!x->use_empty()){
                x->replaceAllUsesWith(v);
                #ifdef DEBUG
                std::cerr<<"CES simplifyInst"<<x->getName()<<"to  ";
                if(isa<ConstantInt>(v))std::cerr<<(dyn_cast<ConstantInt>(v)->getValue())<<"\n";
                else std::cerr<<"\n";
                #endif
                ischange=true;
            }
            if(isinstructionnormaldead(x)){
                //TODO memSSA
                x->eraseFromParent();
                #ifdef DEBUG
                std::cerr<<"CSE delete simplify inst"<<x->getName()<<"\n";
                #endif
                ischange=true;
                continue;
            }
        }
        if(canHandle(x)){
            if(Value *v=availValue[x])
            {
                #ifdef DEBUG
                std::cerr<<"CSE delete handle inst"<<x->getName()<<"\n";
                #endif
                x->replaceAllUsesWith(v);
                //TODO memSSA
                x->eraseFromParent();
                ischange=true;
                continue;
            }
            availValue[x]=x;
            continue;
        }

        if(auto load=dyn_cast<LoadInst>(x))
        {
            // if(load->getInvariant())
            // {
            //     if(!availInvari.count(load->getPointerOperand()))
            //         availInvari[load->getPointerOperand()]=CSEcur;
            // }
            auto loadVal=AvailableLoads[load->getPointerOperand()];
            if(Value *op=matchingValue(loadVal,load,CSEcur)){
                if(!x->use_empty())
                    x->replaceAllUsesWith(op);
                //TODO memSSA
                x->eraseFromParent();
                ischange=true;
                #ifdef DEBUG
                std::cerr<<"CSE load "<<x<<" to "<< op<<"\n";
                #endif
                continue;
            }
            AvailableLoads[load->getPointerOperand()]={x,CSEcur};
            lastStore=nullptr;
            continue;
        }
        if(x->mayReadFromMemory())
            lastStore=nullptr;
        if(auto ci=dyn_cast<CallInst>(x))
        {
            if(!(ci->getType()->isVoidTy())&&!(ci->getCalledFunction()->writeMemory)){
                auto callVal=AvailableCalls[ci];
                if(callVal.first!=nullptr&&isSameGeneration(callVal.second,CSEcur,callVal.first,ci))
                {
                    if(!ci->use_empty())
                        ci->replaceAllUsesWith(callVal.first);
                    //TODO memSSA
                    #ifdef DEBUG
                    std::cerr<<"CSE same inst"<<x->getName()<<"\n";
                    #endif
                    ci->eraseFromParent();
                    ischange=true;
                    continue;
                }
            }
        }
        if(auto store=dyn_cast<StoreInst>(x))
        {
            auto loadVal=AvailableLoads[store->getPointerOperand()];
            if(loadVal.first&&loadVal.first==matchingValue(loadVal,store,CSEcur)){
                //TODO memSSA
                x->eraseFromParent();
                ischange=true;
                continue;
            }
        }
        if(x->mayWriteToMemory())
        {
            ++CSEcur;
            if(auto store=dyn_cast<StoreInst>(x)){
                if(lastStore){
                    if(overrideStore(lastStore,store)){
                        //TODO memSSA
                        lastStore->eraseFromParent();
                        ischange=true;
                        lastStore=nullptr;
                    }
                }
                AvailableLoads[store->getPointerOperand()]={x,CSEcur};
                lastStore=store;
            }
        }
    }
    return ischange;
}

void CSE(Function *func) {
    if (func->isBuildin()) return;
    #ifdef DEBUG
    std::cerr<<"CSE do func  "<<func->getName()<<"\n";
    #endif
    bool ischange = false;
    if(dt)dt->calDomInfo(func);
    else {
        dt=new Dominate(func->getParent());
        dt->calDomInfo(func);
    }
    availValue.clear();
    AvailableLoads.clear();
    AvailableCalls.clear();
    availInvari.clear();
    std::deque<CSENode *> Q;
    Q.push_back(new CSENode(func->getEntryBlock(), 0,availValue,availInvari,AvailableLoads,AvailableCalls));
    while (!Q.empty()) {
        CSENode *node = Q.back();
        CSEcur = node->cur;
        availInvari=node->availInvari;
        availValue=node->availValue;
        AvailableCalls=node->AvailableCalls;
        AvailableLoads=node->AvailableLoads;
        if (!node->isrun) {
            ischange |= processNode(node->bb);
            node->availInvari=availInvari;
            node->availValue=availValue;
            node->AvailableCalls=AvailableCalls;
            node->AvailableLoads=AvailableLoads;
            node->child = CSEcur;
            node->isrun = true;
        } else if (node->childit != node->end) {
            BasicBlock *child = *(node->childit++);
            Q.push_back(
                new CSENode(child, node->child,availValue,availInvari,AvailableLoads,AvailableCalls));
        } else {
            delete node;
            Q.pop_back();
        }
    }
    return;
}