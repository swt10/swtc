#include "ConstantFolding.h"

Constant *ConstantFoldConstant(Constant *c, std::map<Constant *, Constant *> &foldops) {
    return nullptr;
}


Constant *Const_Eval(Instruction *inst) {
    if (auto phinode = dyn_cast<PHINode>(inst)) {
        Constant *common = nullptr;
        std::map<Constant *, Constant *> foldedops;
        for (int i = 0; i < phinode->getNumIncomingValues(); i++) {
            auto incoming = phinode->getIncomingValue(i);
            if (isa<UndefValue>(incoming)) continue;
            auto *c = dyn_cast<Constant>(incoming);
            if (!c) return nullptr;
            c = ConstantFoldConstant(c, foldedops);
            if (common && c != common) return nullptr;
            common = c;
        }
        return common ? common : UndefValue::get(phinode->getType());
    }

    for (auto i = 0; i < inst->getNumOperands(); i++) {
        if (!isa<Constant>(inst->getOperand(i)))
            return nullptr;
    }

    if (auto *cmpexp = dyn_cast<ICmpInst>(inst)) {
        return ConstFoldICmp(cmpexp->getPredicate(),cmpexp->getOperand(0),cmpexp->getOperand(1));
    }
    if (auto binexp = dyn_cast<BinaryOperator>(inst)) {
        return ConstFoldBin(binexp->getOpcode(), inst->getOperand(0), inst->getOperand(1));
    }
    if (auto unexp = dyn_cast<UnaryInstruction>(inst)) {
        return nullptr;  //目前来说没有单目的常量表达式
    }
    return nullptr;
}
Constant *ConstFoldICmp(ICmpInst::Predicate pred, Value *a, Value *b) {
    auto lhs = dyn_cast<ConstantInt>(a)->getValue();
    auto rhs = dyn_cast<ConstantInt>(b)->getValue();
    int result=0;
    switch (pred) {
        case ICmpInst::ICMP_EQ:
            result = (lhs == rhs);
            break;

        case ICmpInst::ICMP_NE:
            result = lhs != rhs;
            break;
        case ICmpInst::ICMP_SGE:
        case ICmpInst::ICMP_UGE:
            result = lhs >= rhs;
            break;
        case ICmpInst::ICMP_UGT:
        case ICmpInst::ICMP_SGT:
            result = lhs > rhs;
            break;
        case ICmpInst::ICMP_SLE:
        case ICmpInst::ICMP_ULE:
            result = lhs <= rhs;
            break;
        case ICmpInst::ICMP_ULT:
        case ICmpInst::ICMP_SLT:
            result = lhs < rhs;
            break;
    }
    return ConstantInt::get(result);
}
Constant *ConstFoldBin(Instruction::Ops opcode, Value *a, Value *b) {
    auto lhs = dyn_cast<ConstantInt>(a)->getValue();
    auto rhs = dyn_cast<ConstantInt>(b)->getValue();
    int result=0;
    switch (opcode) {
        case Instruction::Add:
            result = lhs + rhs;
            break;
        case Instruction::Sub:
            result = lhs - rhs;
            break;
        case Instruction::Mul:
            result = lhs * rhs;
            break;
        case Instruction::UDiv:
        case Instruction::SDiv:
            result = lhs / rhs;
            break;
        case Instruction::URem:
        case Instruction::SRem:
            result = lhs % rhs;
            break;
        case Instruction::And:
            result = lhs & rhs;
            break;
        case Instruction::Or:
            result = lhs | rhs;
            break;
        case Instruction::Xor:
            result = lhs ^ rhs;
            break;
        case Instruction::Shl:
            result = lhs << rhs;
            break;
        case Instruction::LShr:
            result = (unsigned)lhs >> rhs;
            break;
        case Instruction::AShr:
            result = lhs >> rhs;
            break;
    }
    return ConstantInt::get(result);
}

bool isinstructionnormaldead(Instruction *inst) {
    
    if (!inst->use_empty() || inst->isTerminator() || !inst->willreturn()) {
        return false;
    }
    if (!inst->mayWriteToMemory()) return true;
    if(isa<AllocaInst>(inst))return true;
    return false;
}

bool RecursivelyDeleteInstructions(Value *v)
{
    Instruction *I=dyn_cast<Instruction>(v);
    if(!I||!isinstructionnormaldead(I))
        return false;
    std::vector<Instruction*>deadinsts;
    deadinsts.push_back(I);
    while(!deadinsts.empty())
    {
        Value *v=deadinsts.back();
        deadinsts.pop_back();
        Instruction *I=dyn_cast<Instruction>(v);
        if(!I)continue;
        for(int i=0;i<I->getNumOperands();i++)
        {
            Value *u=I->getOperand(i);
            I->setOperand(i,nullptr);
            if(!u->use_empty())continue;
            if(auto opi=dyn_cast<Instruction>(u))
            {
                if(isinstructionnormaldead(opi))
                    deadinsts.push_back(opi);
            }
        }
        I->eraseFromParent();
    }
    return true;
}

bool ConstantFoldTerminator(BasicBlock *bb,bool deleteDeadCond)
{
    Instruction *t=bb->getTerminator();
    if(auto bi=dyn_cast<BranchInst>(t))
    {
        if(bi->isUnconditional())return false;
        BasicBlock *d1=bi->getSuccessor(0);
        BasicBlock *d2=bi->getSuccessor(1);
        if(d1==d2){
            d1->removePredecessor(bb);
            new BranchInst(d1,bb);
            auto cond=bi->getCondition();
            bi->eraseFromParent();
            d1->pred_init();
            if(deleteDeadCond)
                RecursivelyDeleteInstructions(cond);
            return true;
        }
        if(auto *cond=dyn_cast<ConstantInt>(bi->getCondition()))
        {
            BasicBlock *Dest=cond->getValue()?d1:d2;
            BasicBlock *oldDest=cond->getValue()?d2:d1;
            oldDest->removePredecessor(bb);
            new BranchInst(Dest,bb);
            bi->eraseFromParent();
            return true;
        }
        return false;
    }
    return false;
}
Constant *ConstantFoldConstant(Constant *c)
{
    //TODO
    return nullptr;
}
