#include "MemSSA.h"

#include <list>

extern Dominate *dt;

class MemoryDef;
class MemoryUse;
class MemoryPhi;
class MemoryAccess;
std::map<Value *, std::list<MemoryAccess *>> VtoM;  //call指令会有一串访问

std::map<BasicBlock *, std::list<MemoryAccess *>> BtoA;  //bb to AccessList
std::map<BasicBlock *, std::list<MemoryAccess *>> BtoD;  //bb defList
std::map<Value *, std::vector<MemoryAccess *>> Mstack;
std::set<Value *> Entry;
std::map<Function *, std::set<Value *>> funUse;

MemoryDef *LiveonEntry;
Function *nowfunc;
int NewID = 0;

Value *getOriptr(Value *v) {
    while (isa<GetElementPtrInst>(v)) {
        v = dyn_cast<GetElementPtrInst>(v)->getOperand(0);
    }
    return v;
}

MemoryAccess *MemoryAccess::getOptimized() const {
    if (auto MD = dyn_cast<MemoryDef>(this)) {
        return MD->getOptimized();
    } else
        return dyn_cast<MemoryUse>(this)->getOptimized();
}

void MemoryAccess::setOptimized(MemoryAccess *MA) {
    if (auto MD = dyn_cast<MemoryDef>(this)) {
        return MD->setOptimized(MA);
    } else
        return dyn_cast<MemoryUse>(this)->setOptimized(MA);
}

inline bool MemoryAccess::isOptimized() const {
    if (const auto *MD = dyn_cast<MemoryDef>(this))
        return MD->isOptimized();
    return dyn_cast<MemoryUse>(this)->isOptimized();
}

int MemoryAccess::getID() const {
    if (auto d = dyn_cast<MemoryDef>(this)) return d->getID();
    if(auto d=dyn_cast<MemoryUse>(this))return -1;
    return dyn_cast<MemoryPhi>(this)->getID();
}

void insertMAtoBlock(MemoryAccess *Ac, BasicBlock *bb, bool isfront) {
    std::list<MemoryAccess *> &AccessList = BtoA[bb];
    std::list<MemoryAccess *> &DefList = BtoD[bb];
    if (isfront) {
        if (isa<MemoryPhi>(Ac)) {
            AccessList.push_front(Ac);
            DefList.push_front(Ac);
        } else {
            auto F = std::find_if_not(AccessList.begin(), AccessList.end(), [](const MemoryAccess *MA) {
                return isa<MemoryPhi>(MA);
            });
            AccessList.insert(F, Ac);
            if (!isa<MemoryUse>(Ac)) {
                auto D = std::find_if_not(DefList.begin(), DefList.end(), [](const MemoryAccess *MA) {
                    return isa<MemoryPhi>(MA);
                });
                DefList.insert(D, Ac);
            }
        }
    } else {
        AccessList.push_back(Ac);
        if (!isa<MemoryUse>(Ac)) {
            DefList.push_back(Ac);
        }
    }
}

void BlockRename(BasicBlock *bb) {
    auto L = BtoA.find(bb);
    if (L != BtoA.end()) {
        auto list = L->second;
        for (auto *ac : list) {
            if (!isa<MemoryPhi>(ac)) {
                // std::cerr<<ac->getID()<<":start\n";
                if (ac->getDefiningAccess() == nullptr) {
                    // std::cerr<<"use:"<<Mstack[ac->oriptr].back()->getID()<<"\n";
                    ac->setDefiningAccess(Mstack[ac->oriptr].back());
                    if (Entry.count(ac->oriptr) && 
                    funUse[nowfunc].find(ac->oriptr) == funUse[nowfunc].end())
                        funUse[nowfunc].insert(ac->oriptr);
                }
                if (isa<MemoryDef>(ac)) {
                    // std::cerr<<ac->getID()<<"in: "<<ac->oriptr<<"\n";
                    Mstack[ac->oriptr].push_back(ac);
                }
            } else {
                Mstack[ac->oriptr].push_back(ac);
            }
        }
    }
    bb->succ_init();
    for (auto s : bb->succ) {
        auto list = BtoD.find(s);
        if (list == BtoD.end() || list->second.empty() || !isa<MemoryPhi>(list->second.front()))
            continue;
        for (auto def : list->second)
            if (isa<MemoryPhi>(def)) {
                auto *pn = dyn_cast<MemoryPhi>(def);
                pn->addIncoming(Mstack[pn->oriptr].back(), bb);
                if (Entry.count(pn->oriptr) && funUse[nowfunc].find(pn->oriptr) == funUse[nowfunc].end())
                    funUse[nowfunc].insert(pn->oriptr);
            }
    }
    return;
}

void DfsDT(BasicBlock *bb) {
    BlockRename(bb);
    for (auto x : dt->DTSuccBlocks[bb]) {
        DfsDT(x);
    }
    auto L = BtoA.find(bb);
    if (L != BtoA.end()) {
        auto list = L->second;
        for (auto *ac : list) {
            if (!isa<MemoryPhi>(ac)) {
                if (isa<MemoryDef>(ac)) {
                    // std::cerr<<ac->getID()<<"out: "<<ac->oriptr<<"\n";
                    Mstack[ac->oriptr].pop_back();
                }
            } else {
                Mstack[ac->oriptr].pop_back();
            }
        }
    }
}

void MemRename(Function *func) {
    DfsDT(func->getEntryBlock());
}

struct Clobberinfo {
    bool iscall = false;
    MemLoc st;
    Instruction *inst = nullptr;
    MemoryAccess *stMA = nullptr;
    Clobberinfo(Instruction *I, MemoryAccess *MA)
        : iscall(isa<CallInst>(I)), inst(I), stMA(MA) {
        if (!iscall)
            st = MemLoc::get(I);
    }

} * Q;
struct defpath {
    MemLoc loc;
    MemoryAccess *first;
    MemoryAccess *last;
    defpath(MemLoc &loc, MemoryAccess *f, MemoryAccess *l) : loc(loc), first(f), last(l) {}
};

bool isptrsame(Value *a, Value *b) {
    //TODO 考虑函数参数 别名分析
    //目前store和load都是对int的操作
    int offseta = 0, offsetb = 0;
    int init = 1;
    while (isa<GetElementPtrInst>(a)) {
        auto gep = dyn_cast<GetElementPtrInst>(a);
        auto size = gep->getSourceElementType();
        for (int i = gep->getNumOperands() - 1; i >= 1; i--) {
        }
        a = dyn_cast<GetElementPtrInst>(a)->getOperand(0);
    }
    while (isa<GetElementPtrInst>(b)) {
        b = dyn_cast<GetElementPtrInst>(b)->getOperand(0);
    }
    return a == b;
}

bool isclobber(MemoryDef *MD, MemLoc &loc, Instruction *use) {
    Instruction *def = MD->MemInstruction;
    if (auto CL = dyn_cast<CallInst>(use)) {  //这里函数访问内存与所有def冲突
        return true;
    }
    //Def不会是load指令
    if (isa<LoadInst>(def)) {
        return false;
    }
    if (isa<StoreInst>(def)) {
        Value *ptrd = def->getOperand(1);  //Store的指针
        //初略判断
        return isptrsame(ptrd, loc.ptr);
    }
    return true;
}

MemoryAccess *optPhi(MemoryPhi *phi, MemoryAccess *st, MemLoc &loc) {
    std::vector<defpath> path;
    path.push_back(defpath(loc, st, phi));
}

MemoryAccess *walkphiorclobber(defpath &p) {
    for (MemoryAccess *x = p.last; x; x = x->getDefiningAccess()) {
        p.last = x;
        if (isa<MemoryPhi>(p.last)) break;
        if (auto *MD = dyn_cast<MemoryDef>(x)) {
            if (MD == LiveonEntry) {
                return MD;
            }
            if (isclobber(MD, p.loc, Q->inst)) {
                return MD;
            }
        }
    }
    return nullptr;
}

MemoryAccess *findclobber(MemoryAccess *st, Clobberinfo &info) {
    MemoryAccess *cur = st;
    Q = &info;
    if (auto Mu = dyn_cast<MemoryUse>(st))
        cur = Mu->getDefiningAccess();
    MemoryAccess *result = nullptr;
    defpath F(info.st, cur, cur);
    result = walkphiorclobber(F);

    if (!result) {
        optPhi(dyn_cast<MemoryPhi>(F.last), cur, info.st);
    }
}

MemoryAccess *getClobbering(MemoryAccess *MA) {
    if (isa<MemoryPhi>(MA)) return MA;
    bool isopt = false;
    if (MA->isOptimized()) return MA->getOptimized();
    Instruction *memi = MA->MemInstruction;
    Clobberinfo info(memi, MA);
    if (auto Li = dyn_cast<LoadInst>(memi)) {
        if (Li->getInvariant()) {
            MA->setOptimized(LiveonEntry);
            return LiveonEntry;
        }
    }
    MemoryAccess *opt;
    MemoryAccess *def = MA->getDefiningAccess();
    if (def == LiveonEntry) {
        MA->setOptimized(def);
        return def;
    }
    opt = findclobber(def, info);

    return MA;
}

bool isclobber(MemoryDef *MD, MemoryUse *MU, MemLoc &Loc) {
    return false;
}

void OptUsesBlock(BasicBlock *bb, unsigned long &stackepoch, unsigned long &popepoch, std::vector<MemoryAccess *> &MAStack, std::map<MemLoc, MemStackinfo> &LocStack) {
    auto it = BtoA.find(bb);
    if (it == BtoA.end()) return;
    auto MAlist = it->second;
    if (MAlist.empty()) return;
    while (true) {
        BasicBlock *tmp = MAStack.back()->Block;
        if (dt->b1Domb2(tmp, bb)) break;
        while (MAStack.back()->Block == tmp) MAStack.pop_back();
        ++popepoch;
    }
    for (auto ma : MAlist) {
        auto mu = dyn_cast<MemoryUse>(ma);
        if (!mu) {
            MAStack.push_back(ma);
            ++stackepoch;
            continue;
        }
        if (auto li = dyn_cast<LoadInst>(ma->MemInstruction)) {
            if (li->getInvariant()) {
                mu->setDefiningAccess(LiveonEntry, true);
                continue;
            }
        }
        MemLoc Loc(mu->MemInstruction);
        auto &info = LocStack[Loc];
        if (info.popepoch != popepoch) {
            info.popepoch = popepoch;
            info.StackEpoch = stackepoch;
            if (info.lobb && info.lobb != bb &&
                !dt->b1Domb2(info.lobb, bb)) {
                info.lo = 0;
                info.lobb = MAStack[0]->Block;
                info.lastkillvaild = false;
            }

        } else if (info.StackEpoch != stackepoch) {
            info.popepoch = popepoch;
            info.StackEpoch = stackepoch;
        }
        if (!info.lastkillvaild) {
            info.Lastkill = MAStack.size() - 1;
            info.lastkillvaild = true;
        }
        unsigned long upperbound = MAStack.size() - 1;
        if (upperbound - info.lo > 100) {
            info.lastkillvaild = false;
            continue;
        }
        bool Found = false;
        while (upperbound > info.lo) {
            if (isa<MemoryPhi>(MAStack[upperbound])) {
                MemoryAccess *r = getClobbering(mu);
                while (MAStack[upperbound] != r) {
                    --upperbound;
                }
                Found = true;
                break;
            }
            MemoryDef *md = dyn_cast<MemoryDef>(MAStack[upperbound]);
            if (isclobber(md, mu, Loc)) {
                Found = true;
                break;
            }
            --upperbound;
        }
        if (Found || upperbound < info.Lastkill) {
            mu->setDefiningAccess(MAStack[upperbound], true);
            info.Lastkill = upperbound;
        } else {
            mu->setDefiningAccess(MAStack[info.Lastkill], true);
        }
        info.lo = MAStack.size() - 1;
        info.lobb = bb;
    }
}

std::set<Value *> findAccessinCall(CallInst *CI) {
    std::set<Value *> Accessptr = funUse[CI->getCalledFunction()];
    for (int i = 1; i < CI->getNumOperands(); i++) {
        Value *v = getOriptr(CI->getOperand(i));
        if (isa<PointerType>(v->getType())) {
            if (!Accessptr.count(v)) Accessptr.insert(v);
        }
    }
    return Accessptr;
}

void OptUses(Function *func) {
    std::vector<MemoryAccess *> MAStack;
    std::map<MemLoc, MemStackinfo> LocStack;
    MAStack.push_back(LiveonEntry);
    unsigned long Stackepoch = 1;
    unsigned long popepoch = 1;
    for (auto x : depth_firstDT(func->getEntryBlock())) {
        OptUsesBlock(x, Stackepoch, popepoch, MAStack, LocStack);
    }
}

bool isinvariant(Value *ptr) {
    if (!isa<PointerType>(ptr->getType())) return false;
    while (isa<GetElementPtrInst>(ptr)) {
        auto gep = dyn_cast<GetElementPtrInst>(ptr);
        for (int i = 1; i < gep->getNumOperands(); i++) {
            //TODO 可能还可以更细致的判断
            if (!isa<Constant>(gep->getOperand(i))) {
                return false;
            }
        }
        ptr = gep->getOperand(0);
    }
    return true;
}
std::map<BasicBlock *, std::set<Value *>> S;
void MemSSAinfunc(Function *func) {
    if (func->isBuildin()) return;
    dt->calDomInfo(func);
    BasicBlock *st = func->getEntryBlock();
    std::set<BasicBlock *> DefBlocks;
    DefBlocks.clear();
    nowfunc = func;
    for (auto bb : func->getBasicBlockList()) {
        std::list<MemoryAccess *> &AccessList = BtoA[bb];
        std::list<MemoryAccess *> &DefList = BtoD[bb];
        for (auto inst : bb->getInstList()) {
            bool hasDef = false;
            if (isa<AllocaInst>(inst)) {
                VtoM[inst].push_back(new MemoryDef(inst, nullptr, inst, inst->getParent(), NewID++));
                AccessList.splice(AccessList.end(), std::list<MemoryAccess *>(VtoM[inst]));
                DefList.splice(DefList.end(), std::list<MemoryAccess *>(VtoM[inst]));
                Mstack[inst].push_back(VtoM[inst].front());
                continue;
            }
            if (!inst->mayReadFromMemory() && !inst->mayWriteToMemory())
                continue;
            if (inst->mayWriteToMemory())  //Def
            {
                if (isa<CallInst>(inst)) {
                    auto arr = findAccessinCall(dyn_cast<CallInst>(inst));
                    for (auto x : arr) {
                        hasDef = true;
                        VtoM[inst].push_back(new MemoryDef(x, nullptr, inst, inst->getParent(), NewID++));
                    }
                } else  //store
                {
                    hasDef = true;
                    VtoM[inst].push_back(new MemoryDef(nullptr, nullptr, inst, inst->getParent(), NewID++));
                }
            } else  //use
            {
                if (isa<CallInst>(inst)) {
                    auto arr = findAccessinCall(dyn_cast<CallInst>(inst));
                    for (auto x : arr) {
                        VtoM[inst].push_back(new MemoryUse(x, nullptr, inst, inst->getParent()));
                    }
                } else  //load
                    VtoM[inst].push_back(new MemoryUse(nullptr, nullptr, inst, inst->getParent()));
            }

            AccessList.splice(AccessList.end(), std::list<MemoryAccess *>(VtoM[inst]));
            if (hasDef) {
                DefList.splice(DefList.end(), std::list<MemoryAccess *>(VtoM[inst]));
            }
        }
        if (DefList.size()) DefBlocks.insert(bb);
    }

    S.clear();
    std::vector<BasicBlock*>worklist(DefBlocks.begin(),DefBlocks.end());
    for (int i = 0; i < worklist.size(); i++) {
        auto bb=worklist[i];
        for (auto Dfbb : dt->DF[bb]) {  //支配边界
            for (auto def : BtoD[bb])
                if (!S[Dfbb].count(def->oriptr)) {
                    #ifdef DEBUG
                    std::cerr << def->oriptr->getName() << " phidf" << Dfbb->getName() << "\n";
                    #endif
                    S[Dfbb].insert(def->oriptr);
                    MemoryPhi *Pn = new MemoryPhi(def, Dfbb, NewID++);
                    worklist.push_back(Dfbb);
                    insertMAtoBlock(Pn, Dfbb, true);
                    VtoM[Dfbb].push_front(Pn);
                }
        }
    }
    MemRename(func);

    func->readMemory = false;
    func->writeMemory = false;
    for (auto bb : func->getBasicBlockList()) {
        for (auto ma : BtoA[bb]) {
            if (auto mu = dyn_cast<MemoryUse>(ma)) {
                if (auto Li = dyn_cast<LoadInst>(mu->MemInstruction)) {
                    //TODO 这里现在还有问题
                    //if(isinvariant(Li->getPointerOperand()))Li->setInvariant();
                }
                if (Entry.count(mu->oriptr))
                    func->readMemory = true;
            }
            if (auto md = dyn_cast<MemoryDef>(ma)) {
                if (Entry.count(md->oriptr))
                    func->writeMemory = true;
            }
        }
    }
    //OptUses(func);
}

void MemSSA(Module *mo) {
    Mstack.clear();
    BtoA.clear();
    BtoD.clear();
    VtoM.clear();
    funUse.clear();
    Entry.clear();
    NewID = 0;
    for (auto g : mo->getGlobalList()) {
        VtoM[g].push_back(new MemoryDef(g, nullptr, nullptr, nullptr, NewID++));
        Mstack[g].push_back(VtoM[g].front());
        Entry.insert(g);
    }
    for (auto func : mo->getFunctionList()) {
        for (auto arg : func->getArgumentList()) {
            if (isa<PointerType>(arg->getType())) {
                VtoM[arg].push_back(new MemoryDef(arg, nullptr, nullptr, nullptr, NewID++));
                Mstack[arg].push_back(VtoM[arg].front());
                Entry.insert(arg);
            }
        }
        MemSSAinfunc(func);

        for (auto arg : func->getArgumentList()) {
            if (isa<PointerType>(arg->getType()))
                Entry.erase(Entry.find(arg));
        }
    }
}