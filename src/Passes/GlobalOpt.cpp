#include "GlobalOpt.h"

extern Dominate *dt;


void deleteFunc(Function *func)
{
    func->eraseFromParent();
}

bool removeUnreachBlock(Function *func){
    std::set<BasicBlock*>reachable;
    std::vector<BasicBlock*>worklist;
    worklist.push_back(func->getEntryBlock());
    reachable.insert(func->getEntryBlock());
    bool ischange=false;
    do{
        BasicBlock *bb=worklist.back();
        worklist.pop_back();
        
        ischange |= ConstantFoldTerminator(bb,true);
        bb->succ_init();
        for(auto x:bb->succ)
        {
            if(!reachable.count(x))
            {
                reachable.insert(x);
                worklist.push_back(x);
            }   
        }

    }while(!worklist.empty());
    if(reachable.size()==func->getBasicBlockList().size())return ischange;
    std::vector<BasicBlock*>dead_bb;
    for(auto bb=func->getEntryBlock();bb;bb=bb->next)
    {
        if(reachable.count(bb))continue;
        dead_bb.push_back(bb);
    }
    if(dead_bb.empty())return ischange;
    ischange=true;
    for(auto bb:dead_bb)
    {
        bb->succ_init();
        for(auto successor:bb->succ)
        {
            if(reachable.count(successor))
                successor->removePredecessor(bb);
        }
        bb->dropAllReferences();

    }
    for(auto bb:dead_bb)
        bb->eraseFromParent();
    dt->calDomInfo(func);
    return ischange;
}

bool processGlobal(Function *func)
{

}

bool OptimizeFunctions(Module *mo)
{
    bool ischange=false;
    std::vector<Function*>dead;
    for(auto func=mo->getFunctionList().head;func;func=func->next)
    {
        if(func->isBuildin())continue;
        if(func->use_empty()&&func->getName()!="main")
        {
            #ifdef DEBUG
            std::cerr<<"GlobalOpt delete Func:"<<func->getName()<<"\n";
            #endif
            deleteFunc(func);
            ischange=true;
            continue;
        }
        if(func->getEntryBlock()!=nullptr)
        {
            if(removeUnreachBlock(func))
            {
                dt->calDomInfo();
                ischange=true;
            }
        }
    }
    return ischange;
}
bool OptimizeGlobalVars(Module *m)
{
    bool ischange=false;
    for(auto gv=m->getGlobalList().head;gv;gv=gv->next)
    {
        if(gv->hasInitializer())
            if(auto c=dyn_cast<Constant>(gv->getInitializer()))
            {
                Constant *n=ConstantFoldConstant(c);
                if(n)gv->setInitializer(n);
            }

        if(gv->use_empty())
        {
            gv->eraseFromParent();
            ischange=true;
        }
    }
    return ischange;
}

void GlobalOpt(Module *m)
{
    if(!dt)dt=new Dominate(m);
    bool localchange=true;
    while(localchange)
    {
        localchange=false;

        localchange |= OptimizeFunctions(m);
        localchange |= OptimizeGlobalVars(m);
    }

}