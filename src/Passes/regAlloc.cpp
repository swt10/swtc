#include "regAlloc.h"

#include <functional>

using namespace SWTC;

extern LoopInfo *li;

std::map<Function *, SREGs *> *func2regs = new std::map<Function *, SREGs *>;

void printIR(std::string filename, std::string log = "") {
    std::ofstream ir_file_tmp;
    ir_file_tmp.open(filename, std::ios::trunc | std::ios::in | std::ios::out);
    if (ir_file_tmp) {
        std::cerr << COLOR_CYAN << " print IR begin " << log << "\n";
        std::cerr << "to file: " << filename << "\n";
        ir_file_tmp << mo;
        std::cerr << COLOR_CYAN << " print IR end " << log << "\n";
    }
    ir_file_tmp.close();
}

SREGs *getRegsByFunc(Function *func) {
    auto tmp = func2regs->find(func);
    assert(tmp != func2regs->end());

    return tmp->second;
}

void runRegAllocOnFunc(Function *func) {
    std::map<Value *, Value *> addrs;
    std::map<Value *, std::vector<Value *>> reloads;
    std::map<Value *, Value *> redef;
    /// WAITING: assBBs 要不要加入初始定义
    std::map<Value *, std::set<BasicBlock *>> assBBs;
    // 将对虚拟寄存器 v 的重赋值 u = load 映射到 v

    std::map<BasicBlock *, std::vector<Value *>> ibbs;
    std::map<BasicBlock *, std::vector<Value *>> obbs;

    SREGs *sregs;
    IG *ig;
    int regSize;  /// 寄存器个数

    auto reAlloc = [&](Value *reg) {
        if (!reg) {
            logError("alloca to nullptr");
            return;
        }
        /// DONE: alloca when firstly store
        if (addrs.find(reg) == addrs.end()) {
            addrs.insert({reg,
                          new AllocaInst(
                              reg->getType(),
                              "store",
                              func->getEntryBlock()->getInstList().head)});
            new StoreInst(
                reg,
                addrs.find(reg)->second,
                dyn_cast<Instruction>(reg)->next);
        }
    };

    auto reload = [&](Value *reg, BasicBlock *bb, Instruction *insertBefore) {
        reAlloc(reg);
        auto newVar = new LoadInst(
            addrs.find(reg)->second,
            "reload",
            insertBefore);
        reloads[reg].push_back(newVar);
        assBBs[reg].insert(bb);
        redef[newVar] = reg;
    };

    /// TODO: 若函数参数大于 \c 4 可能需要把多的参数 alloca 出来

    /// WAITING: live variables analysis
    {
        /// WAITING: live variables analysis
        LiveVariables(func);
        sregs = new SREGs(func, assBBs);
        ig = new IG(sregs, func);
        regSize = sregs->getRegSize();
    }

    /// DONE: spillRegs
    {
        ///// 遍历 bb
        // 每个 bb 有一个集合 I(bb)，这里面要算出 bb 中的所有活跃变量
        // 假设有 k 个物理寄存器，从 I(bb) 中选择 k 个出来，记为 Ik(bb)
        // 至此，我们有了每个基本块起始的寄存器信息，已经可以开始算了

        for (auto bb : func->getBasicBlockList()) {
            std::vector<Value *> ibb;
#ifdef DEBUG_REG
            int cnt = 0;
#endif
            for (auto reg : sregs->regs) {
#ifdef DEBUG_REG
                cnt++;
#endif
                // 活跃或者是 phi 的 result
                if (isLiveIn(reg, bb)) {
                    ibb.push_back(reg);
                } else if (auto inst = dyn_cast<Instruction>(reg);
                           inst && inst->getParent() == bb && isa<PHINode>(reg)) {
                    ibb.push_back(reg);
                }
            }

#ifdef DEBUG_REG
            std::cerr << "reg size: " << cnt << std::endl;
            std::cerr << "ibb size: " << ibb.size() << std::endl;
#endif

            /// sort ibb to get the k most spill cost regs in the
            /// \c back of ibb and phi
            std::sort(ibb.begin(), ibb.end(), [&](Value *a, Value *b) -> bool {
                return sregs->getSpillCost(a, bb) < sregs->getSpillCost(b, bb);
            });

            /// WAITING: 函数参数处理_1
            /// 这里 \b 仅仅 把前最多 \c 4 个压入 entry_block 的 ibbs
            if (bb = func->getEntryBlock()) {
                auto args = func->getArgumentList();
                int argCnt = 0;
                for (auto arg : args) {
                    if (argCnt >= 4) {
                        break;
                    }
                    ibb.push_back(arg);
                }
                /// 保证 arg 都在寄存器里（也即后 k 个)
                for (auto arg : args) {
                    auto fa = std::find(ibb.begin(), ibb.end(), arg);
                    auto sa = std::find(fa, ibb.end(), arg);
                    while (sa != ibb.end()) {
                        ibb.erase(fa);
                        fa = sa;
                        sa = std::find(fa, ibb.end(), arg);
                    }
                }
            } else {
                /// WAITING: 非 entry_block 需要处理吗
            }
            /// now ibb can be used as ikbb
            ibbs.insert({bb, ibb});
        }

        // 接下来，根据 Ik(bb) 来处理 bb 中每条指令处的 spill
        std::vector<Value *> realRegs;
        std::vector<Value *> tmpRealRegs;
        std::vector<Value *> newRealRegs;

        for (auto bb : func->getBasicBlockList()) {
            // 初始化每个 BB 开头的物理寄存器占用情况
            int cnt = 0;
            auto ibb = ibbs.find(bb)->second;
            while (!ibb.empty()) {
                if (cnt >= REAL_REG_SIZE) {
                    break;
                }
                cnt++;
                realRegs.push_back(ibb.back());
                ibb.pop_back();
            }

            // spill 每条指令
            for (auto inst : bb->getInstList()) {
                if (isa<PHINode>(inst)) {
                    continue;
                }

                tmpRealRegs.clear();

                /// DONE: 溢出 store 和 load
                for (int i = 0, e = inst->getNumOperands(); i < e; i++) {
                    auto use = inst->getOperand(i);
                    if (!isReg(use)) {
                        continue;
                    }
                    tmpRealRegs.push_back(use);
                }

                newRealRegs = tmpRealRegs;

                std::sort(realRegs.begin(), realRegs.end(), [&](Value *a, Value *b) {
                    return sregs->getSpillCost(a, bb) < sregs->getSpillCost(b, bb);
                });

                /// select to spill and reload
                /// select some most cost regs in \c realRegs to \c newRealRegs
                /// until getting fully \c REAL_REG_SIZE regs
                for (auto ri = realRegs.rbegin(); ri != realRegs.rend(); ri++) {
                    if (newRealRegs.size() >= REAL_REG_SIZE) {
                        break;
                    }
                    newRealRegs.push_back(*ri);
                }

                /// reload
                for (auto x : tmpRealRegs) {
                    if (std::find(realRegs.begin(), realRegs.end(), x) == realRegs.end()) {
                        reload(x, bb, inst);
                    }
                }

                // update info of realRegs
                std::swap(realRegs, newRealRegs);
            }
            // 同时 每个 bb 有一个集合 O(bb)
            // 这里面要算出 bb 最后一条指令之后保留在寄存器当中的所有虚拟寄存器
            obbs.insert({bb, realRegs});
        }

        // 每个 bb 对它的每个前驱 pp，分别计算出 Ik(bb) - O(pp)，将其中差的寄存器在边上建立基本块 (或者 pp 末尾) load 进去
        for (auto bb : func->getBasicBlockList()) {
            auto ibb = ibbs.find(bb)->second;

            bb->pred_init();
            for (auto pp : bb->pred) {
                auto lastBr = pp->getInstList().getLast();
                if (!isa<BranchInst>(lastBr)) {
                    logError("bb not end with br: " + bb->getName());
                }

                /// new bb and link
                auto newbb = new BasicBlock("edge bb", func, bb);
                auto newbr = new BranchInst(bb, newbb);
                for (int i = 0, e = lastBr->getNumOperands(); i < e; i++) {
                    if (lastBr->getOperand(i) == bb) {
                        lastBr->setOperand(i, newbb);
                    }
                }

                /// load Ik(bb) - O(pp)
                auto obb = obbs.find(pp)->second;
                std::vector<Value *> dif;
                for (auto x : ibb) {
                    if (std::find(obb.begin(), obb.end(), x) == obb.end()) {
                        reload(x, newbb, newbr);
                    }
                }
            }
        }
    }

    printIR("spill.ll");

    /// DONE: 转ssa
    {
        // CFG 支配关系重计算
        dt->calDomInfo(func);

        // 会有冗余 phi 节点
        /// DONE: place phi node
        {
            auto placePhi = [&](Value *v) {
                std::map<BasicBlock *, bool> hasPhi;
                std::map<BasicBlock *, bool> processed;
                for (auto bb : func->getBasicBlockList()) {
                    hasPhi[bb] = false;
                    processed[bb] = false;
                }
                std::set<BasicBlock *> workList;
                for (auto bb : assBBs.find(v)->second) {
                    processed[bb] = true;
                    workList.insert(bb);
                }

                while (!workList.empty()) {
                    auto bb = *workList.begin();
                    workList.erase(bb);
                    for (auto dfb : dt->DF[bb]) {
                        if (!hasPhi[dfb]) {
                            /// DONE: place/add phi in \c dfb , with preceder \c bb
                            [&](Value *reg, BasicBlock *bb, BasicBlock *pp) -> void {
                                bool isAdd = false;
                                for (auto inst : bb->getInstList()) {
                                    if (auto x = dyn_cast<PHINode>(inst); x && x == reg) {
                                        isAdd = true;

                                        /// WAITING: 应该不会有这个前驱块来的其他赋值了
                                        for (int i = 0, e = x->getNumIncomingValues(); i < e; i++) {
                                            if (x->getIncomingBlock(i) == pp) {
                                                logError("multiple assign in bb");
                                            }
                                        }
                                        x->addIncoming(reg, pp);
                                    }
                                    if (!isAdd) {
                                        auto phi = new PHINode(
                                            reg->getType(),
                                            "phi in regAlloc",
                                            bb->getInstList().head);
                                        phi->addIncoming(reg, pp);

                                        redef.insert({phi, reg});
                                    }
                                }
                            }(v, dfb, bb);
                            hasPhi[dfb] = true;
                            if (!processed[dfb]) {
                                processed[dfb] = true;
                                workList.insert(dfb);
                            }
                        }
                    }
                }
            };

            for (auto reg : sregs->regs) {
                placePhi(reg);
            }
        }

        /// DONE: rename
        {
            std::function<void(
                std::stack<Value *> & version,
                Value * x,
                BasicBlock * bb)>
                renameVar = [&](
                                std::stack<Value *> &version,
                                Value *x,
                                BasicBlock *bb) {
                    auto ve = version.top();
                    for (auto inst : bb->getInstList()) {
                        if (isa<PHINode>(inst)) {
                            /// WAITING: phi 似乎要留到下面
                        } else {
                            for (int i = 0, e = inst->getNumOperands(); i < e; i++) {
                                auto use = inst->getOperand(i);
                                if (use == x) {
                                    inst->setOperand(i, version.top());
                                }
                            }
                        }

                        /// if inst define \c x
                        if (
                            auto tmp = redef.find(inst);
                            tmp != redef.end() && tmp->second == x) {
                            version.push(inst);
                        }
                    }

                    for (auto suc : bb->succ) {
                        for (auto inst : suc->getInstList()) {
                            if (auto phiInst = dyn_cast<PHINode>(inst)) {
                                for (int i = 0, e = phiInst->getNumIncomingValues(); i < e; i++) {
                                    if (
                                        phiInst->getIncomingValue(i) == x && phiInst->getIncomingBlock(i) == bb) {
                                        phiInst->setIncomingValue(i, version.top());
                                    }
                                }
                            }
                        }
                    }

                    for (auto child : dt->DTSuccBlocks.find(bb)->second) {
                        renameVar(version, x, child);
                    }

                    while (version.top() != ve) {
                        version.pop();
                    }
                };

            for (auto reg : sregs->regs) {
                std::stack<Value *> tmp;
                tmp.push(reg);
                renameVar(tmp, reg, func->getEntryBlock());
            }
        }
    }

    printIR("ssa.ll");

    /// DONE: 可能需要一次消除冗余 phi
    {
        /// WAITING: 消除只有一个前驱的 phi 节点
        /// v1 = phi(v2), 因为是 ssa 所以用 v2 替换 v1 的所有 use
        for (auto bb : func->getBasicBlockList()) {
            for (auto inst : bb->getInstList()) {
                if (auto phi = dyn_cast<PHINode>(inst);
                    phi &&
                    phi->getNumIncomingValues() == 1) {
                    for (auto use : phi->getUseList()) {
                        auto user = use->getUser();
                        for (int i = 0, e = user->getNumOperands(); i < e; i++) {
                            if (user->getOperand(i) == phi) {
                                user->setOperand(i, phi->getIncomingValue(0));
                            }
                        }
                    }

                    phi->removeFromParent();
                }
            }
        }
    }

    /// WAITING: rebuild IG
    {
        /// WAITING: redo live variables analysis
        LiveVariables(func);
        delete ig;
        delete sregs;
        sregs = new SREGs(func, assBBs);
        ig = new IG(sregs, func);
        regSize = sregs->getRegSize();
    }

    /// DONE: colorRegs();
    {
        std::vector<int> PEO(regSize);
        std::vector<int> label(regSize, 0);
        std::vector<std::vector<int>> labels(regSize);
        for (int i = 0; i < regSize; i++) {
            labels.at(0).push_back(i);
        }

        bool *isInPEO = new bool[regSize + 5];  /// 寄存器是否在 PEO 里

        ///// MCS to find PEO
        // 逆序给结点编号，即按从 n 到 1 的顺序给点标号。
        // 设 label_x 表示第 x 个点与多少个已经标号的点相邻
        // labels[i] 是 label_x = i 的结点的链表
        int maxx = 0;
        int now = 0;

        for (auto rpeo = PEO.rbegin(); rpeo != PEO.rend(); rpeo++) {
            bool flag = false;
            while (!flag) {
                for (auto ri = labels.at(maxx).rbegin(); ri != labels.at(maxx).rend(); ri++) {
                    auto j = *ri;
                    if (isInPEO[j]) {
                        labels.at(maxx).pop_back();
                    } else {
                        flag = true;
                        now = j;
                        break;
                    }
                }
                if (!flag) {
                    maxx--;
                }
            }

            isInPEO[now] = true;
            *rpeo = now;

            for (int j = 0; j < regSize - 1; j++) {
                if (j == now || isInPEO[j] || !ig->isInter(now, j)) {
                    continue;
                }
                label.at(j)++;
                labels.at(label.at(j)).push_back(j);
                maxx = std::max(maxx, label.at(j));
            }
        }

        ///// 根据 PEO 上色
        int maxColor = -1;
        for (auto x : PEO) {
            sregs->setColor(x, ig->getGap(x, maxColor));
        }

        logError(
            "Function " + func->getName() +
            ": \n"
            "max color in regAlloc: " +
            std::to_string(maxColor));

        assert(maxColor < REAL_REG_SIZE);

        delete[] isInPEO;
    }

    /// DEBUG:
    // sregs->printRegsColors();

    func2regs->insert({func, sregs});
}

void regAlloc(Module *_mo) {
    dt = dt ? dt : new Dominate(_mo);

    if (isDebugRegAlloc) {
        printIR("before.ll");
    }

    for (auto func : _mo->getFunctionList()) {
        if (func->isBuildin()) {
            continue;
        }
        std::cerr << func->getName() << std::endl;
        runRegAllocOnFunc(func);
    }

    if (isDebugRegAlloc) {
        printIR("after.ll");
    }
}
