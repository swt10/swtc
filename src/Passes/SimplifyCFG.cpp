#include "SimplifyCFG.h"

extern Dominate *dt;
extern GVNimpl g;
std::set<BasicBlock *> head;

bool mergeEmptyReturnBlock(Function *func) {
    bool ischange = false;
    std::set<BasicBlock *> deadBlock;
    BasicBlock *ret = nullptr;
    for (auto bb : func->getBasicBlockList().copy()) {
        ReturnInst *rt = dyn_cast<ReturnInst>(bb->getTerminator());
        if (!rt) continue;
        if(deadBlock.count(bb))continue;
        if (rt != bb->getInstList().head) {
            if (bb->getInstList().size() != 2 ||
                !isa<PHINode>(bb->getInstList().head) ||
                rt->getNumOperands() == 0 ||
                rt->getOperand(0) != bb->getInstList().head) {
                continue;
            }
        }
        if (!ret) {
            ret = bb;
            continue;
        }
        ischange = true;
        if (rt->getNumOperands() == 0 ||
            rt->getOperand(0) == dyn_cast<ReturnInst>(ret->getTerminator())->getOperand(0)) {
            bb->replaceAllUsesWith(ret);
            deadBlock.insert(bb);
            continue;
        }
        PHINode *pn = dyn_cast<PHINode>(ret->getInstList().head);
        if (!pn) {
            Value *val = dyn_cast<ReturnInst>(ret->getTerminator())->getOperand(0);
            pn = new PHINode(rt->getOperand(0)->getType(), "merge", ret->getInstList().head);
            ret->pred_init();
            for (auto p : ret->pred) {
                pn->addIncoming(val, p);
            }
            ret->getTerminator()->setOperand(0, pn);
        }
        pn->addIncoming(rt->getOperand(0), bb);
        bb->getTerminator()->eraseFromParent();
        new BranchInst(ret, bb);
    }
    for (auto bb : deadBlock)
        bb->eraseFromParent();
    // dt->calDomInfo(func);
    return ischange;
}
std::set<BasicBlock*>visited;
//Tarjan过程
void findedge(BasicBlock *bb, std::set<BasicBlock *> &instack, std::vector<std::pair<BasicBlock *, BasicBlock *>> &edge) {
    bb->succ_init();
    instack.insert(bb);
    for (auto x : bb->succ) {
        if (!visited.count(x)) {
            visited.insert(x);
            findedge(x, instack, edge);
        } else {
            if(instack.count(x))
            edge.push_back(std::make_pair(bb, x));
        }
    }
    instack.erase(instack.find(bb));
}

void findfunctionBackEdges(Function *func, std::vector<std::pair<BasicBlock *, BasicBlock *>> &edges) {
    BasicBlock *bb = func->getEntryBlock();
    std::set<BasicBlock *> instack;
    visited.clear();
    findedge(bb, instack, edges);
}

bool EliminateDuplicatePhi(BasicBlock *bb) {
    bool ischange = false;
    for (auto inst = bb->getInstList().head; PHINode *pn = dyn_cast<PHINode>(inst);) {
        inst = inst->next;
        for (auto i = inst; PHINode *other = dyn_cast<PHINode>(i); i = i->next) {
            if (!other->isIdenticalto(pn)) continue;
            #ifdef DEBUG
            std::cerr << "SCFG delete same phi node\n ";
            #endif
            other->replaceAllUsesWith(pn);
            other->eraseFromParent();
            ischange = true;
            inst = bb->getInstList().head;
            break;
        }
    }
    return ischange;
}

bool cansink(std::vector<Instruction *> &lockit, std::map<Instruction *, std::vector<Value *>> &Phiop) {
    bool hasuse = !lockit.front()->use_empty();
    for (auto x : lockit) {
        if (isa<PHINode>(x) || isa<AllocaInst>(x)) return false;
        x->getParent()->succ_init();
        if (x->getParent()->getSingleSuccessor() == x->getParent()) return false;
        if (hasuse && x->getUseList().size() != 1) return false;
        if (!hasuse && !x->use_empty()) return false;
    }

    Instruction *first = lockit.front();
    for (auto x : lockit) {
        if (!x->isSameOperation(first))
            return false;
    }
    if (hasuse) {
        auto pn = dyn_cast<PHINode>(first->getUseList().head->getUser());
        auto succ = first->getParent()->getTerminator()->getOperand(0);  //这里不会是ret所以0一定是succ
        if (!std::all_of(lockit.begin(), lockit.end(), [&pn, &succ](Instruction *I) -> bool {
                auto *U = dyn_cast<Instruction>(I->getUseList().head->getUser());
                return (pn &&
                        pn->getParent() == succ &&
                        pn->getIncomingValueForBlock(I->getParent()) == I) ||
                       U->getParent() == I->getParent();
            }))
            return false;
    }
    if (isa<StoreInst>(first) || isa<LoadInst>(first) || isa<AllocaInst>(first)) return false;
    for (int i = 0; i < first->getNumOperands(); i++) {
        Value *op = first->getOperand(i);
        auto Sameas = [&first, i](Instruction *I) {
            return I->getOperand(i) == first->getOperand(i);
        };
        if (std::all_of(lockit.begin(), lockit.end(), Sameas)) {
            if (isa<CallInst>(first) && i == first->getNumOperands() - 1)
                return false;
        }
        for (auto x : lockit) {
            Phiop[x].push_back(x->getOperand(i));
        }
    }
    return true;
}

BasicBlock *splitBlockpred(BasicBlock *bb, std::vector<BasicBlock *> preds, const char *suffix) {
    BasicBlock *newbb = new BasicBlock(bb->getName() + suffix, bb->getParent(), bb);
    BranchInst *bi = new BranchInst(bb, newbb);
    BasicBlock *oldlatch = nullptr;
    for (int i=0;i<preds.size();i++)
    {
        preds[i]->getTerminator()->replaceUsesOfWith(bb,newbb);
    }
    if (preds.empty()) {
        for (auto inst : bb->getInstList()) {
            if (auto pn = dyn_cast<PHINode>(inst)) {
                pn->addIncoming(UndefValue::get(pn->getType()), newbb);
            }
        }
    }
    // dt->calDomInfo(bb->getParent());

    if (!preds.empty()) {
        std::set<BasicBlock *> predset(preds.begin(), preds.end());
        for (auto inst : bb->getInstList()) {
            if (auto pn = dyn_cast<PHINode>(inst)) {
                Value *inval = pn->getIncomingValueForBlock(preds[0]);
                for (int i = 0; i < pn->getNumIncomingValues(); i++) {
                    if (!predset.count(pn->getIncomingBlock(i)))
                        continue;
                    if (!inval)
                        inval = pn->getIncomingValue(i);
                    else if (inval != pn->getIncomingValue(i)) {
                        inval = nullptr;
                        break;
                    }
                }
                if (inval) {
                    for (int i = pn->getNumIncomingValues() - 1; i >= 0; i--) {
                        if (predset.count(pn->getIncomingBlock(i)))
                            pn->removeIncomingValue(i, false);
                    }
                    pn->addIncoming(inval, newbb);
                    continue;
                }
                PHINode *newphi = new PHINode(pn->getType(), pn->getName() + ".ph", bi);
                for (int i = pn->getNumIncomingValues() - 1; i >= 0; i--) {
                    BasicBlock *inbb = pn->getIncomingBlock(i);
                    if (predset.count(inbb)) {
                        Value *V = pn->removeIncomingValue(i, false);
                        newphi->addIncoming(V, inbb);
                    }
                }
                pn->addIncoming(newphi, newbb);
            }
        }
    }
    return newbb;
}
bool isvaild(std::vector<Instruction *> &lockit)
{
    for(auto x:lockit)
    if(x==nullptr)return false;
    return true;
}

bool sinkcommoncode(BasicBlock *bb) {
    std::vector<BasicBlock *> uncondpred;
    bb->pred_init();
    Instruction *cond = nullptr;
    for (auto b : bb->pred) {
        auto ti = b->getTerminator();
        if (isa<BranchInst>(ti) && dyn_cast<BranchInst>(ti)->isUnconditional())
            uncondpred.push_back(b);
        else if (isa<BranchInst>(ti) && !cond)
            cond = ti;
        else
            return false;
    }
    if (uncondpred.size() < 2) return false;
    int cnt = 0;
    std::set<Value *> Itosink;
    std::map<Instruction *, std::vector<Value *>> Phiop;
    std::vector<Instruction *> lockit;
    for (auto x : uncondpred) {
        if (x->getTerminator()->prev != nullptr)
            lockit.push_back(x->getTerminator()->prev);
        else
            return false;
    }

    while (isvaild(lockit)&&cansink(lockit, Phiop)) {
        Itosink.insert(lockit.begin(), lockit.end());
        cnt++;
        for (auto &x : lockit) {
            x = x->prev;
        }
    }
    if (cnt == 0) return false;
    bool ischange = false;
    auto ProfitableToSinkInstruction = [&](std::vector<Instruction *> &LRI) {
        unsigned NumPHIdValues = 0;
        for (auto I : LRI)
            for (auto V : Phiop[I])
                if (Itosink.count(V) == 0)
                    ++NumPHIdValues;
        unsigned NumPHIInsts = NumPHIdValues / uncondpred.size();
        if ((NumPHIdValues % uncondpred.size()) != 0)
            NumPHIInsts++;

        return NumPHIInsts <= 1;
    };
    if (cond) {
        lockit.clear();
        for (auto x : uncondpred) {
            lockit.push_back(x->getTerminator()->prev);
        }
        int idx = 0;
        while (ProfitableToSinkInstruction(lockit) && idx < cnt) {
            for (auto &x : lockit) {
                x = x->prev;
            }
            idx++;
        }
        if (!splitBlockpred(bb, uncondpred, ".sink.split"))
            return false;
        ischange = true;
    }

    int sinkidx = 0;
    for (; sinkidx != cnt; sinkidx++) {
        lockit.clear();
        for (auto x : uncondpred) {
            lockit.push_back(x->getTerminator()->prev);
        }
        if (!ProfitableToSinkInstruction(lockit)) {
            break;
        }
        auto bbend = dyn_cast<BasicBlock>(uncondpred[0]->getTerminator()->getOperand(0));
        std::vector<Instruction *> insts;
        for (auto bb : uncondpred) {
            Instruction *I = bb->getTerminator();
            I = I->prev;
            insts.push_back(I);
        }
        Instruction *I0 = insts.front();
        if (!I0->use_empty()) {
            auto Pnuse = dyn_cast<PHINode>(I0->use_begin()->getUser());
            if (!std::all_of(insts.begin(), insts.end(), [&Pnuse](Instruction *I) -> bool {
                    auto *U = dyn_cast<Instruction>(I->use_begin()->getUser());
                    return U == Pnuse;
                }))
                break;
        }
        std::vector<Value *> Newop;
        for (int i = 0; i < I0->getNumOperands(); i++) {
            bool needphi = std::any_of(insts.begin(), insts.end(), [&I0, i](Instruction *I) {
                return I->getOperand(i) != I0->getOperand(i);
            });
            if (!needphi) {
                Newop.push_back(I0->getOperand(i));
                continue;
            }
            auto op = I0->getOperand(i);
            auto pn = new PHINode(op->getType(), op->getName() + ".sink", bbend->getInstList().head);
            for (auto x : insts) {
                pn->addIncoming(x->getOperand(i), x->getParent());
            }
            Newop.push_back(pn);
        }
        for (int i = 0; i < I0->getNumOperands(); i++)
            I0->setOperand(i, Newop[i]);
        I0->moveBefore(bbend->getFirstNonPHI());
        if (!I0->use_empty()) {
            auto pn = dyn_cast<PHINode>(I0->use_begin()->getUser());
            pn->replaceAllUsesWith(I0);
            pn->eraseFromParent();
        }
        for (auto x : insts) {
            if (x != I0)
                x->eraseFromParent();
        }
        ischange = true;
    }

    return ischange;
}

bool canmergevalue(Value *a, Value *b) {
    return a == b || isa<UndefValue>(a) || isa<UndefValue>(b);
}

bool canpropagateforphis(BasicBlock *bb, BasicBlock *succ) {
    succ->pred_init();
    if (succ->pred.size() == 1) return true;
    bb->pred_init();
    std::set<BasicBlock *> bbpred(bb->pred.begin(), bb->pred.end());
    for (auto inst : succ->getInstList()) {
        if (PHINode *pn = dyn_cast<PHINode>(inst)) {
            PHINode *bbpn = dyn_cast<PHINode>(pn->getIncomingValueForBlock(bb));
            if (bbpn && bbpn->getParent() == bb) {
                for (int i = 0; i < pn->getNumIncomingValues(); i++) {
                    BasicBlock *inbb = pn->getIncomingBlock(i);
                    if (bbpred.count(inbb) && !canmergevalue(bbpn->getIncomingValueForBlock(inbb), pn->getIncomingValue(i))) {
                        return false;
                    }
                }
            } else {
                Value *val = pn->getIncomingValueForBlock(bb);
                for (int i = 0; i < pn->getNumIncomingValues(); i++) {
                    BasicBlock *inbb = pn->getIncomingBlock(i);
                    if (bbpred.count(inbb) && !canmergevalue(val, pn->getIncomingValue(i))) {
                        return false;
                    }
                }
            }
        }
    }
    return true;
}

void replaceUndefValuesInPhi(PHINode *PN, std::map<BasicBlock *, Value *> &IncomingValues) {
    std::vector<int> TrueUndefOps;
    for (int i = 0, e = PN->getNumIncomingValues(); i != e; ++i) {
        Value *V = PN->getIncomingValue(i);

        if (!isa<UndefValue>(V)) continue;

        BasicBlock *BB = PN->getIncomingBlock(i);
        std::map<BasicBlock *, Value *>::iterator It = IncomingValues.find(BB);

        if (It == IncomingValues.end()) {
            TrueUndefOps.push_back(i);
            continue;
        }

        PN->setIncomingValue(i, It->second);
    }

    unsigned PoisonCount = std::count_if(TrueUndefOps.begin(), TrueUndefOps.end(), [&](int i) {
        return isa<UndefValue>(PN->getIncomingValue(i));
    });
    if (PoisonCount != 0 && PoisonCount != TrueUndefOps.size()) {
        for (unsigned i : TrueUndefOps)
            PN->setIncomingValue(i, UndefValue::get(PN->getType()));
    }
}

bool trytosimplifyUncondBr(BasicBlock *bb) {
    BasicBlock *succ = dyn_cast<BranchInst>(bb->getTerminator())->getSuccessor(0);
    if (bb == succ) return false;
    if (!canpropagateforphis(bb, succ)) return false;
    if (!succ->getSingleSuccessor()) {
        for (auto inst : bb->getInstList()) {
            if (auto pn = dyn_cast<PHINode>(inst)) {
                for (auto u : pn->getUseList()) {
                    if (auto upn = dyn_cast<PHINode>(u->getUser())) {
                        if (upn->getIncomingBlock(u) != bb) return false;
                    } else {
                        return false;
                    }
                }
            }
        }
    }
    bb->pred_init();
    if (isa<PHINode>(succ->getInstList().head)) {
        std::vector<BasicBlock *> bbpred(bb->pred.begin(), bb->pred.end());
        for (auto inst : succ->getInstList()) {
            if (auto phinode = dyn_cast<PHINode>(inst)) {
                //redirect bb bbpred,pn
                Value *oldVal = phinode->removeIncomingValue(bb, false);
                std::map<BasicBlock *, Value *> incomingmap;
                for (int i = 0; i < phinode->getNumIncomingValues(); i++) {
                    BasicBlock *b = phinode->getIncomingBlock(i);
                    Value *v = phinode->getIncomingValue(i);
                    if (!isa<UndefValue>(v)) incomingmap.insert(std::make_pair(b, v));
                }
                if (isa<PHINode>(oldVal) && dyn_cast<PHINode>(oldVal)->getParent() == bb) {
                    auto oldphi = dyn_cast<PHINode>(oldVal);
                    for (int i = 0; i < oldphi->getNumIncomingValues(); i++) {
                        BasicBlock *prebb = oldphi->getIncomingBlock(i);
                        Value *prev = oldphi->getIncomingValue(i);
                        Value *sel = nullptr;
                        if (!isa<UndefValue>(prev)) {
                            incomingmap.insert(std::make_pair(prebb, prev));
                            sel = prev;
                        } else {
                            if (incomingmap.find(prebb) != incomingmap.end())
                                sel = incomingmap.find(prebb)->second;
                            else
                                sel = prev;
                        }
                        phinode->addIncoming(sel, prebb);
                    }
                } else {
                    for (int i = 0; i < bbpred.size(); i++) {
                        BasicBlock *pre = bbpred[i];
                        Value *sel = nullptr;
                        if (!isa<UndefValue>(oldVal)) {
                            incomingmap.insert(std::make_pair(pre, oldVal));
                            sel = oldVal;
                        } else {
                            if (incomingmap.find(pre) != incomingmap.end())
                                sel = incomingmap.find(pre)->second;
                            else
                                sel = oldVal;
                        }
                        phinode->addIncoming(sel, pre);
                    }
                }
                replaceUndefValuesInPhi(phinode, incomingmap);
            }
        }
    }
    succ->pred_init();
    if (succ->pred.size() == 1) {
        bb->getTerminator()->eraseFromParent();
        Instruction *pos = succ->getFirstNonPHI();
        succ->getInstList().splice(pos, bb->getInstList());
        succ->resetParent();
    } else {
        while (PHINode *pn = dyn_cast<PHINode>(bb->getInstList().head)) {
            pn->eraseFromParent();
        }
    }
    bb->replaceAllUsesWith(succ);
    succ->takeName(bb);
    bb->eraseFromParent();
    // dt->calDomInfo(succ->getParent());
    return true;
}

bool simplifyBr(BranchInst *br) {
    if (!br->isConditional()) {
        BasicBlock *bb = br->getParent();
        BasicBlock *succ = br->getSuccessor(0);
        bb->pred_init();
        bool needcanoicalLoop = (!head.empty() && bb->pred.size() >= 2 && (head.count(bb) || head.count(succ)));

        auto inst = bb->getFirstNonPHI();
        if (inst->isTerminator() && bb != bb->getParent()->getEntryBlock() &&
            !needcanoicalLoop && trytosimplifyUncondBr(bb))
            return true;
        //TODO 合并pred的br到相同dst
        return false;
    } else {
        //TODO 条件分支
    }
    return false;
}

bool Simplifycondbr(BranchInst *bi, ReturnInst *pos) {
    auto bb = bi->getParent();
    BasicBlock *tsucc = bi->getSuccessor(0);
    BasicBlock *fsucc = bi->getSuccessor(1);
    ReturnInst *trt = dyn_cast<ReturnInst>(tsucc->getTerminator());
    ReturnInst *frt = dyn_cast<ReturnInst>(fsucc->getTerminator());

    if (!tsucc->getFirstNonPHI()->isTerminator())
        return false;
    if (!fsucc->getFirstNonPHI()->isTerminator())
        return false;
    if (frt->getNumOperands() == 0) {
        tsucc->removePredecessor(bb);
        fsucc->removePredecessor(bb);
        new ReturnInst(nullptr, bi);
        auto cond = bi->getCondition();
        bi->eraseFromParent();
        RecursivelyDeleteInstructions(cond);
        return true;
    }
    //TODO selection
    return false;
}

bool simplifyRt(ReturnInst *rt) {
    BasicBlock *bb = rt->getParent();
    if (!bb->getFirstNonPHI()->isTerminator()) return false;
    std::vector<BasicBlock *> uncond;
    std::vector<BranchInst *> cond;
    bb->pred_init();
    for (auto x : bb->pred) {
        auto i = x->getTerminator();
        if (auto br = dyn_cast<BranchInst>(i)) {
            if (br->isConditional())
                cond.push_back(br);
            else
                uncond.push_back(x);
        }
    }
    //TODO uncond就不合并了
    while (!cond.empty()) {
        BranchInst *bi = cond.back();
        cond.pop_back();
        if (isa<ReturnInst>(bi->getSuccessor(0)->getTerminator()) &&
            isa<ReturnInst>(bi->getSuccessor(1)->getTerminator()) &&
            Simplifycondbr(bi, rt))
            return true;
    }
    return false;
}
bool simplifyblock(BasicBlock *bb) {
    bool ischange = false;
    bb->pred_init();
    if ((bb->pred.empty() && bb != bb->getParent()->getEntryBlock()) ||
        (bb->pred.size() == 1 && bb->pred[0] == bb)) {
        bb->eraseFromParent();
        return true;
    }

    bool isrun = false;
    do {
        isrun = false;
        ischange |= ConstantFoldTerminator(bb, true);
        ischange |= EliminateDuplicatePhi(bb);
        if (g.mergeblockintopred(bb)) return true;
        ischange |= sinkcommoncode(bb);
        //TODO foldTwoEntryPhiNode
        Instruction *ti = bb->getTerminator();
        if (ti->getOpcode() == Instruction::Br)
            ischange |= simplifyBr(dyn_cast<BranchInst>(ti));
        else if (ti->getOpcode() == Instruction::Ret)
            ischange |= simplifyRt(dyn_cast<ReturnInst>(ti));

    } while (isrun);
    return ischange;
}

bool runsimplifyCFG(Function *f) {
    bool ischange = false;
    std::vector<std::pair<BasicBlock *, BasicBlock *>> edges;
    findfunctionBackEdges(f, edges);
    head.clear();
    for (int i = 0; i < edges.size(); i++) {
        if (!head.count(edges[i].second))
            head.insert(edges[i].second);
    }
    bool isupdate = true;
    while (isupdate) {
        isupdate = false;
        for (auto bb : f->getBasicBlockList().copy()) 
        if(bb->getParent()!=nullptr){
            if (simplifyblock(bb)) {
                // dt->calDomInfo(f);
                isupdate = true;
            }
        }
        ischange |= isupdate;
    }
    return ischange;
}

void SimplifyCFG(Function *func) {
    if (func->isBuildin()) return;
    if (!dt) dt = new Dominate(func->getParent());
    dt->calDomInfo(func);
    bool ischange = false;
    ischange |= removeUnreachBlock(func);
    ischange |= mergeEmptyReturnBlock(func);
    ischange |= runsimplifyCFG(func);
    if (!ischange) return;
    if (!removeUnreachBlock(func)) return;
    do {
        ischange = runsimplifyCFG(func);
        ischange |= removeUnreachBlock(func);
    } while (ischange);
    return;
}