#include "Casting.h"
#include "Mem2Reg.h"
#include <iostream>
#include <ostream>
#include <unordered_map>

std::map<Value *, std::vector<Value *>> IncomingVals;
std::unordered_map<PHINode *, Value *> phiVal;
std::vector<PHINode *> newphis;


extern Dominate *dt;

void deleteonevalPhi(Function *f)
{
    for(auto bb:f->getBasicBlockList())
    {
        for(auto inst:bb->getInstList().copy())
        {
            if(auto pn=dyn_cast<PHINode>(inst))
            {
                if(pn->getNumIncomingValues()==1)
                {
                    auto val=pn->getIncomingValue(0);
                    pn->replaceAllUsesWith(val);
                    pn->eraseFromParent();
                }
            }
        }
    }
}

void mem2Reg(Module *M){

    if(!dt)dt=new Dominate(M);
    for(auto F = M->getFunctionList().head; F; F = F->next){
        if(F->getEntryBlock() != nullptr){
            IncomingVals.clear();
            phiVal.clear();
            newphis.clear();
            dt->calDomInfo(F);    
            placingPhi(F);
            reName(F->getEntryBlock());
            deleteonevalPhi(F);
        }
        std::vector<Instruction *> deletelist;
        for (auto BB = F->getEntryBlock(); BB; BB = BB->next){
            for (auto inst = BB->getInstList().head; inst; inst = inst->next){
                if (auto x = dyn_cast<AllocaInst>(inst)){
                    if(x->getAllocatedType()->getTypeID() == Type::TypeID::IntegerTyID){
                        deletelist.push_back(inst);
                    }
                }
            }
        }
        for(auto inst : deletelist){
            inst->eraseFromParent();
        }
    }
}

void placingPhi(Function *F){
    std::map<Value *, unsigned int> Allocas;
    for(auto BB = F->getEntryBlock(); BB; BB = BB->next){
        for(auto inst = BB->getInstList().head; inst; inst = inst->next){
            if(auto x = dyn_cast<AllocaInst>(inst)){
                if(x->getAllocatedType()->getTypeID() == Type::TypeID::IntegerTyID){
                    Allocas.insert({x, Allocas.size()});
                }
            }
        }
    }
    std::set<Value *> alloca_op1;
    std::map<Value *, std::set<BasicBlock *>> op1_BB;
    for(auto BB = F->getEntryBlock(); BB; BB = BB->next){
        for(auto inst = BB->getInstList().head; inst; inst = inst->next){
            if(auto x = dyn_cast<StoreInst>(inst)){
                if(Allocas.find(x->getOperand(1))!=Allocas.end()){
                    alloca_op1.insert(x->getOperand(1));
                }
                op1_BB[x->getOperand(1)].insert(BB);
            }
        }
    }

    std::map<BasicBlock *, std::set<Value *>> phis;
    for(auto V : alloca_op1){
        std::vector<BasicBlock *> worklist;
        worklist.assign(op1_BB[V].begin(), op1_BB[V].end());
        for(int i = 0; i < worklist.size(); ++i){
            auto BB = worklist[i];
            for(auto p : dt->DF[BB]){
                if(phis[p].find(V) == phis[p].end()){
                    auto phi = new PHINode(dyn_cast<PointerType>(V->getType())->getElementType(), "PHI", p->getInstList().head);
                    phiVal[phi] = V;
                    worklist.push_back(p);
                    phis[p].insert(V);
                    newphis.push_back(phi);                    
                }
            }
        }
    }
}

void reName(BasicBlock *BB){
    BB->succ_init();
    std::vector<Instruction *> deletelist;
    for(auto inst = BB->getInstList().head; inst; inst = inst->next){
        if(auto x = dyn_cast<PHINode>(inst)){
            for(int i = 0; i < newphis.size(); ++i){
                if(x == newphis[i]){
                    IncomingVals[phiVal[x]].push_back(inst);
                }
            }
        } else if(auto x = dyn_cast<LoadInst>(inst)){
            auto op = x->getPointerOperand();
            if(!isa<GlobalVariable>(op) && !isa<GetElementPtrInst>(op)){
                if(IncomingVals.find(op) != IncomingVals.end()){
                    auto it = IncomingVals.find(op);
                    auto V = it->second[it->second.size() - 1];
                    dyn_cast<Value>(inst)->replaceAllUsesWith(V);
                }
                deletelist.push_back(inst);
            }
        } else if(auto x = dyn_cast<StoreInst>(inst)){
            auto op0 = x->getOperand(0);
            auto op1 = x->getOperand(1);
            if(!isa<GlobalVariable>(op1) && !isa<GetElementPtrInst>(op1)){
                IncomingVals[op1].push_back(op0);
                deletelist.push_back(inst);
            }
        }
    }

    for(auto BB1 : BB->succ){
        for(auto inst = BB1->getInstList().head; inst; inst = inst->next){
            if(auto x = dyn_cast<PHINode>(inst)){
                for(int i = 0; i < newphis.size(); ++i){
                    if(x == newphis[i]){
                        auto val = phiVal[x];
                        if(IncomingVals.find(val) != IncomingVals.end()){
                            if(IncomingVals.find(val)->second.size() != 0){
                                auto V = IncomingVals.find(val)->second[IncomingVals[val].size() - 1];
                                x->addIncoming(V, BB);
                            }
                        }
                    }
                }
            }
        }
    }
    for(auto BB1 : dt->DTSuccBlocks[BB]){
        reName(BB1);
    }
    for(auto inst = BB->getInstList().head; inst; inst = inst->next){
        if(auto x = dyn_cast<StoreInst>(inst)){
            auto op1 = x->getOperand(1); 
            if(!isa<GlobalVariable>(op1) && !isa<GetElementPtrInst>(op1)){
                IncomingVals[op1].pop_back();
            }
        } else if(auto x = dyn_cast<PHINode>(inst)){
            for(int i = 0; i < newphis.size(); ++i){
                if(x == newphis[i]){
                    if(IncomingVals.find(phiVal[x]) != IncomingVals.end()){
                        IncomingVals[phiVal[x]].pop_back();
                    }
                }
            }
        }
    }
    for(auto inst : deletelist){
        inst->eraseFromParent();
    }
}