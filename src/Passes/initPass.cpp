#include "initPass.h"


#define FOR_FUNC(mo) \
	for(auto iterFunc :mo->getFunctionList())		

#define FOR_BB(iterFunc) \
	for(auto iterBB: iterFunc->getBasicBlockList())

void eliminateSelfLoop(SWTC::Module *mo){
	FOR_FUNC(mo){
		if(iterFunc->isBuildin()){
			continue;
		}
		FOR_BB(iterFunc){
			if(iterBB == iterFunc->getEntryBlock()){
				continue;
			}
			if(iterBB->succ.size() != 1){
				// empty or more than one
				continue;
			}
			if(iterBB->succ.front() == iterBB){
				// TODO:
				// iterBB->removeFromParent();
				// 维护前驱后继节点
				// 递归地消除前驱节点的自环
			}
			
		}
	}
}

void removeNoPreBB(SWTC::Module *mo){
	FOR_FUNC(mo){
		if(iterFunc->isBuildin()){
			continue;
		}
		FOR_BB(iterFunc){
			if(iterBB == iterFunc->getEntryBlock()){
				continue;
			}
			if(iterBB->pred.empty()){
				// TODO:
				// iterBB->removeFromParent();
				// 维护前驱后继节点
				// 递归地消除后继基本块中的无前驱基本块
			}
		}
	}

}