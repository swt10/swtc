#include "config.h"

#define COLOR_RESET "\033[0m"
#define COLOR_BLACK "\033[30m"              /* Black */
#define COLOR_RED "\033[31m"                /* Red */
#define COLOR_GREEN "\033[32m"              /* Green */
#define COLOR_YELLOW "\033[33m"             /* Yellow */
#define COLOR_BLUE "\033[34m"               /* Blue */
#define COLOR_MAGENTA "\033[35m"            /* Magenta */
#define COLOR_CYAN "\033[36m"               /* Cyan */
#define COLOR_WHITE "\033[37m"              /* White */
#define COLOR_BOLDBLACK "\033[1m\033[30m"   /* Bold Black */
#define COLOR_BOLDRED "\033[1m\033[31m"     /* Bold Red */
#define COLOR_BOLDGREEN "\033[1m\033[32m"   /* Bold Green */
#define COLOR_BOLDYELLOW "\033[1m\033[33m"  /* Bold Yellow */
#define COLOR_BOLDBLUE "\033[1m\033[34m"    /* Bold Blue */
#define COLOR_BOLDMAGENTA "\033[1m\033[35m" /* Bold Magenta */
#define COLOR_BOLDCYAN "\033[1m\033[36m"    /* Bold Cyan */
#define COLOR_BOLDWHITE "\033[1m\033[37m"   /* Bold White */

std::string ast_file_path = "ast.json";
std::string IR_file_path = "ir.ll";
std::string input_file_path;
std::string output_file_path = "asm.s";

std::fstream ast_file;
std::ofstream ir_file;
std::fstream output_file;
std::ofstream asm_file;

bool isPrintAst;
bool isPrintIR = false;

bool isOutputFilePathGot = false;
bool isInputFilePathGot = false;

bool isDebugAST = false;
bool isPrintIRInst = false;
bool isPrintIRBB = false;
bool isPrintPredSucc = false;
bool isNoRunPass = false;
bool isCodeGen = false;
bool isRead = false;
bool isMem2Reg = true;
bool isDebugRegAlloc = false;

void logError(std::string e) {
    std::cerr << COLOR_BOLDRED << "error: " << e << COLOR_RESET << std::endl;
}