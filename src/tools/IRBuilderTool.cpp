#include "IRBuilderTool.h"

#include "Casting.h"
#include "Type.h"

#define COLOR_RESET "\033[0m"
#define COLOR_BLACK "\033[30m"              /* Black */
#define COLOR_RED "\033[31m"                /* Red */
#define COLOR_GREEN "\033[32m"              /* Green */
#define COLOR_YELLOW "\033[33m"             /* Yellow */
#define COLOR_BLUE "\033[34m"               /* Blue */
#define COLOR_MAGENTA "\033[35m"            /* Magenta */
#define COLOR_CYAN "\033[36m"               /* Cyan */
#define COLOR_WHITE "\033[37m"              /* White */
#define COLOR_BOLDBLACK "\033[1m\033[30m"   /* Bold Black */
#define COLOR_BOLDRED "\033[1m\033[31m"     /* Bold Red */
#define COLOR_BOLDGREEN "\033[1m\033[32m"   /* Bold Green */
#define COLOR_BOLDYELLOW "\033[1m\033[33m"  /* Bold Yellow */
#define COLOR_BOLDBLUE "\033[1m\033[34m"    /* Bold Blue */
#define COLOR_BOLDMAGENTA "\033[1m\033[35m" /* Bold Magenta */
#define COLOR_BOLDCYAN "\033[1m\033[36m"    /* Bold Cyan */
#define COLOR_BOLDWHITE "\033[1m\033[37m"   /* Bold White */

#define ADD_PRED_SUCC(p, s)   \
    {                         \
        p->succ.push_back(s); \
        s->pred.push_back(p); \
    }

using namespace SWTC;

void logCERR(std::string str) {
    // return;
    /*std::cerr << COLOR_BOLDRED << str << "\n"
              << COLOR_RESET;*/
}

namespace CONTEXT {
bool isConst;
bool isGlobal;

// 符号表
SymbolTable st;

// 全局常量/常量数组映射
std::map<std::string, int> constVarMap;

std::map<std::string, ArrayInfo> constArrayMap;

// 函数映射到对映Function*
std::map<std::string, SWTC::Function *> funcMap;

SWTC::Function *func_cur;
SWTC::BasicBlock *bb_cur;

// continue用，存储while循环的第一个bb
std::vector<SWTC::BasicBlock *> bb_first_loop;
// break用，存储while循环的下一个bb
std::vector<SWTC::BasicBlock *> bb_after_loop;

int ifCnt = 0;
int whileCnt = 0;
int breakCnt = 0;
int continueCnt = 0;

/*
---------------------------------------------------------
====================    函数定义    ======================
---------------------------------------------------------
*/

void addFunction(bool isVoid, const std::string &name, bool isVarArg,
                 std::vector<AST_EXP::Exp *> fparams) {
    // 先把数组参数的下标变成常量
    std::vector<Type *> tmpParams;
    /*std::cerr << COLOR_MAGENTA << "addFunction begin\n";*/
    for (auto x : fparams) {
        if (x->isVar) {
            /*std::cerr << "\tparam var: " << x->ident <<std::endl;*/
            // 变量
            tmpParams.emplace_back(mo->getInt32Ty());
        } else if (x->isArray) {
            // 数组
            auto initType = mo->getInt32PtrTy();
            if (x->vecs.size() == 0) {
                // 一维数组，就是一个指针
                /*std::cerr << "\tparam 1 array: " << x->ident <<std::endl;*/
                tmpParams.emplace_back(initType);
            } else {
                // 二维及以上数组，
                /*std::cerr << "\tparam more array: " << x->ident <<std::endl;*/
                std::vector<int> tmpSubs;
                for (auto fx : x->vecs) {
                    tmpSubs.emplace_back(getConstVal(fx));
                }
                tmpParams.emplace_back(
                    new PointerType(getArrayType(mo->getInt32Ty(), tmpSubs)));
            }
        }
    }

    func_cur = new SWTC::Function(
        FunctionType::get(
            isVoid ? mo->getVoidTy() : mo->getInt32Ty(), tmpParams,
            isVarArg),
        name,
        mo);

    funcMap.insert_or_assign(name, func_cur);  // 没有重名可能

    /*std::cerr << COLOR_MAGENTA << "addFunction End\n"
              << COLOR_RESET;*/
}

void addFuncParams(std::vector<AST_EXP::Exp *> params) {
    /*std::cerr << COLOR_YELLOW << "addFuncParams begin\n"
              << COLOR_RESET;*/
    auto argNow = func_cur->arg_begin();
    for (auto x : params) {
        if (x->isVar) {
            auto tmpIns = new AllocaInst(
                mo->getInt32Ty(),
                "AllocaInst",
                bb_cur);

            new StoreInst(
                argNow,
                tmpIns,
                bb_cur);

            CONTEXT::st.add(
                x->ident,
                tmpIns,
                true);
        } else if (x->isArray) {
            // if(isa<PointerType>(argNow->getType())){
            //     logError("is ptr");
            // } else {
            //     logError("not a ptr");
            // }
            CONTEXT::st.add(
                x->ident,
                argNow,
                true);
        }

        logCERR(st.isParam(x->ident) ? "is param" : "not a param");

        argNow = argNow->getNext();
    }

    BasicBlock * next = new BasicBlock("next", func_cur);
    new BranchInst(next, bb_cur);
    bb_cur = next;

    /*std::cerr << COLOR_YELLOW << "addFuncParams end\n"
              << COLOR_RESET;*/
}

// 插入全局变量
void addGlobalVar(const std::string &name, int x) {
    // Module的符号表
    Value *p = new GlobalVariable(mo->getInt32Ty(), CONTEXT::isConst,
                                  ConstantInt::get(x), name, mo);

    // 加入st
    CONTEXT::st.add(name, p);
}

// 插入全局数组
void addGlobalArray(const std::string &name, const ArrayInfo &array) {
    /*std::cerr << COLOR_YELLOW << "addGlobalArray begin\n"
              << COLOR_RESET;*/
    ArrayType *tmpArrayTy = getArrayType(mo->getInt32Ty(), array.subs);

    std::vector<Constant *> tmpVals = getArrayVals(array);

    GlobalVariable *ptr =
        new GlobalVariable(tmpArrayTy, CONTEXT::isConst,
                           ConstantArray::get(tmpArrayTy, tmpVals), name, mo);

    /*std::cerr << COLOR_YELLOW << "addGlobalArray end\n"
              << COLOR_RESET;*/

    // 加入st
    CONTEXT::st.add(name, ptr);
}

// 插入局部变量
void addLocalVar(const std::string &name, bool isInit,
                 AST_EXP::Exp *val = nullptr) {
    // 先声明

    Instruction *tmpIns;

    auto tmpHead = func_cur->getEntryBlock()->getInstList().head;
    if (tmpHead) {
        tmpIns = new SWTC::AllocaInst(mo->getInt32Ty(), "alloca local var " + name, tmpHead);
    } else {
        tmpIns = new SWTC::AllocaInst(mo->getInt32Ty(), "alloca local var " + name, func_cur->getEntryBlock());
    }

    // 加入st
    st.add(name, tmpIns);

    // Instruction *tmpAddr = new SWTC::GetElementPtrInst(
    //     tmpIns, {ConstantInt::get(0)}, "", bb_cur
    // );

    // 若有初值则store (const 不用管)
    new StoreInst(isInit ? getExpValue(val) : ConstantInt::getZeroVal(), tmpIns, bb_cur);
}

void initLocalArray(
    Value *tmpAddr,
    int depth,
    int &cnt,
    const std::vector<int> &subs,
    const std::vector<Value *> &vals,
    std::vector<Value *> &idxs) {
    if (depth == subs.size()) {
        if(!isa<UndefValue>(vals[cnt]))
        new StoreInst(
            vals[cnt],
            new GetElementPtrInst(
                tmpAddr,
                idxs,
                "gep local array",
                bb_cur),
            bb_cur);
        cnt++;
    } else {
        int len = subs.at(depth);
        logError(std::to_string(len));
        for (int i = 0; i < len; i++) {
            idxs.emplace_back(ConstantInt::get(i));
            initLocalArray(tmpAddr, depth + 1, cnt, subs, vals, idxs);
            idxs.pop_back();
        }
    }
}

// 插入局部数组
void addLocalArray(const std::string &name, const std::vector<int> &subs,
                   const std::vector<AST_EXP::Exp *> &vals) {
    auto initType = mo->getInt32Ty();
    auto tmpArrayType = getArrayType(initType, subs);

    Instruction *tmpIns;

    auto tmpHead = func_cur->getEntryBlock()->getInstList().head;
    if (tmpHead) {
        tmpIns = new SWTC::AllocaInst(tmpArrayType, "alloca local array " + name, tmpHead);
    } else {
        tmpIns = new SWTC::AllocaInst(tmpArrayType, "alloca local array " + name, func_cur->getEntryBlock());
    }

    // 加入st
    st.add(name, tmpIns);

    Value *tmpAddr = tmpIns;
    // new GetElementPtrInst(tmpIns, {ConstantInt::getZeroVal()}, "", bb_cur);

    if (subs.empty()) {
        return;
    } else {
        int arraySize = subs.empty() ? 0 : 1;
        for (auto x : subs) {
            arraySize *= x;
        }
        std::vector<Value *> tmpVals;
        int cnt = 0;
        for (auto x : vals) {
            tmpVals.emplace_back(getExpValue(x));
            cnt++;
        }
        while (cnt <= arraySize) {
            tmpVals.emplace_back(UndefValue::get(Module::getInt32Ty()));
            cnt++;
        }

        std::vector<Value *> tmpIdxs = {ConstantInt::getZeroVal()};
        int tmpCnt = 0;
        initLocalArray(tmpAddr, 0, tmpCnt, subs, tmpVals, tmpIdxs);
    }
}

void addAssign(const std::string &l_name,
               const std::vector<AST_EXP::Exp *> &l_subs, bool isVar,
               const AST_EXP::Exp *r_val) {
    Value *tmpLVal = st.find(l_name);
    Value *tmpGlobalLVal = st.findRoot(l_name);
    Value *tmpAddr;

    if (isVar) {
        if (tmpGlobalLVal) {
            // 我也不知道区分全局和局部有什么用，但先留着。
            // 全局变量
            /*std::cerr << COLOR_BOLDWHITE << "assigne global var\n";*/
            tmpAddr = tmpGlobalLVal;
        } else {
            // 局部
            /*std::cerr << COLOR_BOLDWHITE << "assigne local var\n";*/
            tmpAddr = tmpLVal;
        }
    } else {
        std::vector<Value *> tmpIdxs;
        if (!st.isParam(l_name)) {
            // 不是函数参数，补一个 0 的偏移量
            tmpIdxs.emplace_back(SWTC::ConstantInt::getZeroVal());
        }
        for (auto x : l_subs) {
            tmpIdxs.emplace_back(CONTEXT::getExpValue(x));
        }
        if (tmpGlobalLVal) {
            /*std::cerr << COLOR_BOLDWHITE << "assign global array val\n";*/
            tmpAddr = new GetElementPtrInst(tmpGlobalLVal, tmpIdxs, "gep assign global array", bb_cur);
        } else {
            /*std::cerr << COLOR_BOLDWHITE << "assign local array val\n";*/
            tmpAddr = new GetElementPtrInst(tmpLVal, tmpIdxs, "gep assign local array", bb_cur);
        }
    }

    new StoreInst(CONTEXT::getExpValue(r_val), tmpAddr, bb_cur);
}

// 插入/更新全局const
void updateGlobalConst(const std::string &name, const int &x) {
    constVarMap.insert_or_assign(name, x);
}

// 插入/更新全局const array
void updateGlobalConst(const std::string &name, const ArrayInfo &array) {
    constArrayMap.insert_or_assign(name, array);
}

int getGlobalConst(const std::string &name) {
    return constVarMap.find(name)->second;
}

int getGlobalConst(const std::string &name, const std::vector<int> &valSubs) {
    auto &tmp = constArrayMap.find(name)->second;
    return tmp.getVal(valSubs);
}

int getConstVal(AST_EXP::Exp *exp) {
    if (exp->isNum) {
        return exp->val;
    }

    if (exp->isVar) {
        return CONTEXT::getGlobalConst(exp->ident);
    }

    if (exp->isArray) {
        std::vector<int> tmp;
        for (auto x : exp->vecs) {
            tmp.emplace_back(getConstVal(x));
        }
        return CONTEXT::getGlobalConst(exp->ident, tmp);
    }

    if (exp->isUna) {
        return AST_CAL::cal(true, exp->op, getConstVal(exp->expl));
    }

    if (exp->isBin) {
        return AST_CAL::cal(false, exp->op, getConstVal(exp->expl),
                            getConstVal(exp->expr));
    }

    logError("invalid attempt in getConstVal");
}

Value *getBinValue(AST::OP op, const AST_EXP::Exp *exp) {
    /*std::cerr << COLOR_GREEN << "BinaryOperator::create: begin\n"
              << COLOR_RESET;*/

    OPS tmp;
    std::string tmpStr;
    Value *tmpAns, *lval, *rval;

    lval = getExpValue(exp->expl);

    auto shortLogic = [&](bool l) -> Value * {
        // 左侧等于 l 时短路右侧
        BasicBlock *before = bb_cur;
        BasicBlock *next = new BasicBlock("short_result", func_cur, bb_cur->next);
        BasicBlock *right = new BasicBlock("short_compute_right", func_cur, bb_cur->next);

        new BranchInst(
            l ? next : right, 
            l ? right : next, 
            lval,
            bb_cur);
        
        bb_cur = right;
        rval = getExpValue(exp->expr);
        right=bb_cur;
       // auto rcmp = BinaryOperator::create(tmp, lval, rval, tmpStr, CONTEXT::bb_cur);
        new BranchInst(next, bb_cur);

        bb_cur = next;
        auto phi = new PHINode(mo->getInt1Ty(), "short", bb_cur);

        phi->addIncoming(lval, before);
        phi->addIncoming(rval, right);
    
        // ADD_PRED_SUCC(before, right);
        // ADD_PRED_SUCC(before, next);
        // ADD_PRED_SUCC(right, next);

        return phi;
    };

    switch (op) {
        case AST::OP_ADD:
            tmp = OPS::Add;
            tmpStr = "OP_ADD";
            break;
        case AST::OP_MIN:
            tmp = OPS::Sub;
            tmpStr = "OP_MIN";
            break;
        case AST::OP_TIM:
            tmp = OPS::Mul;
            tmpStr = "OP_TIM";
            break;
        case AST::OP_DIV:
            tmp = OPS::SDiv;
            tmpStr = "OP_DIV";
            break;
        case AST::OP_MOD:
            tmp = OPS::SRem;
            tmpStr = "OP_MOD";
            break;
        case AST::OP_AND:
            tmp = OPS::And;
            tmpStr = "OP_AND";
            return shortLogic(false);
        case AST::OP_OR:
            tmp = OPS::Or;
            tmpStr = "OP_OR";
            return shortLogic(true);
        default:
            logError("invalid attempt in getExpValue(unary)");
    }

    rval = getExpValue(exp->expr);

    tmpAns = BinaryOperator::create(tmp, lval, rval, tmpStr, CONTEXT::bb_cur);

    /*std::cerr << COLOR_GREEN << "BinaryOperator::create: end\n"
              << COLOR_RESET;*/

    return tmpAns;
}

Value *getIcmpValue(AST::OP op, const AST_EXP::Exp *exp) {
    /*std::cerr << COLOR_GREEN << "getIcmpValue: begin\n"
              << COLOR_RESET;*/

    PRED tmp;
    std::string tmpStr;

    switch (op) {
        case AST::OP_EQ:
            tmp = PRED::ICMP_EQ;
            tmpStr = "ICMP_EQ";
            break;
        case AST::OP_NE:
            tmp = PRED::ICMP_NE;
            tmpStr = "ICMP_NE";
            break;
        case AST::OP_GT:
            tmp = PRED::ICMP_SGT;
            tmpStr = "ICMP_SGT";
            break;
        case AST::OP_LT:
            tmp = PRED::ICMP_SLT;
            tmpStr = "ICMP_SLT";
            break;
        case AST::OP_GE:
            tmp = PRED::ICMP_SGE;
            tmpStr = "ICMP_SGE";
            break;
        case AST::OP_LE:
            tmp = PRED::ICMP_SLE;
            tmpStr = "ICMP_SLE";
            break;
        default:
            return nullptr;
    }

    assert(exp->expl != nullptr);
    Value *O1 = getExpValue(exp->expl), *O2 = getExpValue(exp->expr);

    assert(O1 != nullptr);
    auto tmpAns = new ICmpInst(tmp, O1, O2, tmpStr, bb_cur);

    /*std::cerr << COLOR_GREEN << "getIcmpValue: end\n"
              << COLOR_RESET;*/

    return tmpAns;
}

Value *getExpValue(const AST_EXP::Exp *exp, bool isPtr) {
    if (exp->isNum) {
        /*std::cerr << COLOR_GREEN << "getExpValue: num: " << exp->val << "\n";*/
        return ConstantInt::get(exp->val);
        // checked
    }

    if (exp->isVar) {
        /*std::cerr << COLOR_GREEN << "getExpValue: var: " << exp->ident << "\n";*/
        auto tmpPtr = st.find(exp->ident);

        if (dyn_cast<PointerType>(tmpPtr->getType())->getElementType()->isIntegerTy()) {
        // if(!isa<PointerType>(tmpPtr)){
            // 指向的是一个 int
            if(isPtr){
                // 是函数参数且是一维数组
                /*std::cerr << COLOR_GREEN << "getExpValue: var2ptr(funcParam)\n";*/
                return tmpPtr;
            }
            auto tmpAns = new LoadInst(tmpPtr, "load var " + exp->ident, bb_cur);
            /*std::cerr << COLOR_GREEN << "getExpValue: var2val\n";*/
            return tmpAns;
        } else {
            // 指向的是一个 ptr
            /*std::cerr << COLOR_GREEN << "getExpValue: var2ptr\n";*/
            // return tmpPtr;
            return new GetElementPtrInst(tmpPtr, {ConstantInt::getZeroVal(), ConstantInt::getZeroVal()}, "gep ptr", bb_cur);
        }
    }

    if (exp->isArray) {
        /*std::cerr << COLOR_GREEN << "getExpValue: arr: " << exp->ident << "\n";*/
        // 数组调用返回指针
        std::vector<Value *> tmpIdx;
        if (!st.isParam(exp->ident)) {
            tmpIdx.emplace_back(ConstantInt::getZeroVal());
        }
        for (auto x : exp->vecs) {
            tmpIdx.emplace_back(getExpValue(x));
        }
        auto tmpPtr =
            new GetElementPtrInst(st.find(exp->ident), tmpIdx, "", bb_cur);

        if (dyn_cast<PointerType>(tmpPtr->getType())->getElementType()->isIntegerTy()) {
            // 如果 tmpPtr 指向的是一个 int
            std::string tmpStr = "load array ";
            tmpStr += exp->ident;
            auto tmpAns = new LoadInst(tmpPtr, tmpStr, bb_cur);
            /*std::cerr << COLOR_GREEN << "getExpValue: arr2val\n";*/
            return tmpAns;
        } else {
            // 指向的是一个 ptr
            /*std::cerr << COLOR_GREEN << "getExpValue: arr2ptr\n";*/
            // return tmpPtr;
            return new GetElementPtrInst(tmpPtr, {ConstantInt::getZeroVal(), ConstantInt::getZeroVal()}, "gep ptr", bb_cur);
        }
    }

    if (exp->hadFunc) {
        /*std::cerr << COLOR_GREEN << "getExpValue: func: " << exp->ident << "\n";*/
        Function *tmpFunc = funcMap.find(exp->ident)->second;

        auto funcArgs = tmpFunc->getArgumentList();
        std::vector<Value *> tmpArgs;
        

        int i = 0;
        for(auto arg: funcArgs){
            if(arg->getType()->isPointerTy()){
                tmpArgs.emplace_back(getExpValue(exp->vecs.at(i), true));
            }
            else{
                tmpArgs.emplace_back(getExpValue(exp->vecs.at(i)));
            }
            i++;
        }

        // 等待修改
        return new CallInst(
            tmpFunc->getFunctionType(),
            tmpFunc,
            tmpArgs,
            exp->ident,
            bb_cur);
    }

    if (exp->isUna) {
        /*std::cerr << COLOR_GREEN << "getExpValue: una " << exp->ident << "\n";*/
        switch (exp->op) {
            case AST::OP::OP_ADD:
                return getExpValue(exp->expl);

            case AST::OP::OP_MIN: {
                auto tmpAns = BinaryOperator::create(OPS::Sub, ConstantInt::getZeroVal(),
                                                     getExpValue(exp->expl), "nagative", bb_cur);
                return tmpAns;
            }

            case AST::OP::OP_NOT:
                return ICmpInst::createNot(getExpValue(exp->expl), "nagative", bb_cur);

            default:
                logError("invalid attempt in getExpValue(unary)");
        }
    }

    if (exp->isBin) {
        /*std::cerr << COLOR_GREEN << "getExpValue: bin " << exp->ident << "\n";*/
        if (exp->op == AST::OP_BINARY_BUT_ONE) {
            return getExpValue(exp->expl);
        }

        Value *tmpVal = getIcmpValue(exp->op, exp);

        /*std::cerr << COLOR_GREEN << "getExpValue: end " << exp->ident << "\n";*/

        return tmpVal == nullptr ? getBinValue(exp->op, exp) : tmpVal;
    }

    logError("invalid attempt in getConstVal: none matched operator");
}

ArrayType *getArrayType(Type *init, std::vector<int> subs) {
    // 此时保证 subs.size > 0
    auto tmpType = ArrayType::get(init, subs.back());

    for (auto ri = subs.rbegin() + 1; ri != subs.rend(); ri++) {
        tmpType = ArrayType::get(tmpType, *ri);
    }

    return tmpType;
}

std::vector<Constant *> getArrayVals(const ArrayInfo &array) {
    std::vector<Constant *> tmp, ans, tmpCNM;
    int len = array.subs.back();
    int cnt = 0;

    ArrayType *tmpArrayTy = new ArrayType(Type::Int32Ty, len);

    for (auto x : array.vals) {
        cnt++;
        tmp.emplace_back(ConstantInt::get(x));

        if (cnt == len) {
            ans.emplace_back(ConstantArray::get(tmpArrayTy, tmp));
            tmp.clear();
            cnt = 0;
        }
    }

    for (auto ri = array.subs.rbegin() + 1; ri != array.subs.rend(); ++ri) {
        tmp = ans;
        ans.clear();

        int len = *ri;
        int cnt = 0;

        tmpArrayTy = new ArrayType(tmpArrayTy, len);
        for (auto x : tmp) {
            tmpCNM.emplace_back(x);
            cnt++;

            if (cnt == len) {
                ans.emplace_back(ConstantArray::get(tmpArrayTy, tmpCNM));
                tmpCNM.clear();
                cnt = 0;
            }
        }
    }

    return ans;
}

void addExternFunc() {
    // int getint();
    funcMap.insert_or_assign(
        "getint",
        new Function(
            FunctionType::get(
                mo->getInt32Ty(),
                false),
            "getint",
            mo,
            true));

    // int getch();
    funcMap.insert_or_assign(
        "getch",
        new Function(
            FunctionType::get(
                mo->getInt32Ty(),
                false),
            "getch",
            mo,
            true));

    // int getarray(int arr[]);
    funcMap.insert_or_assign(
        "getarray",
        new Function(
            FunctionType::get(
                mo->getInt32Ty(),
                std::vector<Type *>{
                    mo->getInt32PtrTy()},
                false),
            "getarray",
            mo,
            true));

    // void putint(int num);
    funcMap.insert_or_assign(
        "putint",
        new Function(
            FunctionType::get(
                mo->getVoidTy(),
                std::vector<Type *>{
                    mo->getInt32Ty()},
                false),
            "putint",
            mo,
            true));

    // void putch(int ascii);
    funcMap.insert_or_assign(
        "putch",
        new Function(
            FunctionType::get(
                mo->getVoidTy(),
                std::vector<Type *>{
                    mo->getInt32Ty()},
                false),
            "putch",
            mo,
            true));

    // void putarray(int cnt, int arr[]);
    funcMap.insert_or_assign(
        "putarray",
        new Function(
            FunctionType::get(
                mo->getVoidTy(),
                std::vector<Type *>{
                    mo->getInt32Ty(),
                    mo->getInt32PtrTy()},
                false),
            "putarray",
            mo,
            true));

    // void _sysy_starttime(int lineno);
    funcMap.insert_or_assign(
        "_sysy_starttime",
        new Function(
            FunctionType::get(
                mo->getVoidTy(),
                std::vector<Type *>{
                    mo->getInt32Ty()},
                false),
            "_sysy_starttime",
            mo,
            true));

    // void _sysy_stoptime(int lineno);
    funcMap.insert_or_assign(
        "_sysy_stoptime",
        new Function(
            FunctionType::get(
                mo->getVoidTy(),
                std::vector<Type *>{
                    mo->getInt32Ty()},
                false),
            "_sysy_stoptime",
            mo,
            true));
}

}  // namespace CONTEXT
