#include "AST_cal.h"

int AST_CAL::cal(bool isUnary, AST::OP op, int l, int r) {
    if (isUnary) {
        // 一元
        switch (op) {
            case AST::OP::OP_ADD: return l;
            case AST::OP::OP_MIN: return -l;
            case AST::OP::OP_NOT: return !l;
            default:
                logError("invalid unary op");
        }
    } else {
        // 二元
        switch (op) {
            case AST::OP::OP_ADD: return l + r;
            case AST::OP::OP_MIN: return l - r;
            case AST::OP::OP_TIM: return l * r;
            case AST::OP::OP_DIV: return l / r;
            case AST::OP::OP_MOD: return l % r;
            case AST::OP::OP_AND: return l && r;
            case AST::OP::OP_OR: return l || r;
            case AST::OP::OP_GT: return l > r;
            case AST::OP::OP_LT: return l < r;
            case AST::OP::OP_GE: return l >= r;
            case AST::OP::OP_LE: return l <= r;
            case AST::OP::OP_EQ: return l == r;
            case AST::OP::OP_NE: return l != r;
        }
    }
}