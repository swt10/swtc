#include "ASMStruct.h"

#include <cstdlib>
#include <iostream>
#include <string>

#define self (*this)
#define CROR(value, bits) ((value >> bits) | (value << (0x20 - bits)))


static bool Judge(unsigned int val) {
    for (int i = 0; i < 32; i+=2) {
        if ( CROR(val, i) <= 0x000000ff)
            return true;
    }

    return false;
}

inline bool Register::isCustom() { return this->register_prototype == RP::Custom; }
inline bool Register::isFP() { return this->register_prototype == RP::FP; }
inline bool Register::isIP() { return this->register_prototype == RP::IP; }
inline bool Register::isSP() { return this->register_prototype == RP::SP; }
inline bool Register::isLR() { return this->register_prototype == RP::LR; }
inline bool Register::isPC() { return this->register_prototype == RP::PC; }

Register::Register(Register::RP register_prototype) : register_prototype(register_prototype),
                                                      register_id(getIDByRegisterPrototype(register_prototype)) {}

Register::Register(int register_id) : register_prototype(getRegisterPrototypeByID(register_id)),
                                      register_id(register_id) {}

int Register::getID() const { return this->register_id; }
Register::RP Register::getPrototype() const { return this->register_prototype; }

void Register::setRegisterID(int register_id) {
    this->register_id = register_id;
    this->register_prototype = this->getRegisterPrototypeByID(register_id);
}

Register::operator std::string() const {
    switch (this->register_prototype) {
        case R0: return "R0";
        case R1: return "R1";
        case R2: return "R2";
        case R3: return "R3";
        case R4: return "R4";
        case R5: return "R5";
        case R6: return "R6";
        case R7: return "R7";
        case R8: return "R8";
        case R9: return "R9";
        case R10: return "R10";
        case R11: return "R11";
        case R12: return "R12";
        case R13: return "R13";
        case R14: return "R14";
        case R15: return "R15";
    }
}

std::string to_string(Cond condition) {
    switch (condition) {
        case Cond::AL: return "";
        case Cond::EQ: return "EQ";
        case Cond::NE: return "NE";
        case Cond::GE: return "GE";
        case Cond::GT: return "GT";
        case Cond::LE: return "LE";
        case Cond::LT: return "LT";
        case Cond::HS: return "HS";
        case Cond::LO: return "LO";
        case Cond::HI: return "HI";
        case Cond::LS: return "LS";
    }
}

const std::string f8(std::string s) {
    while (s.size() <= 8) {
        s.push_back(' ');
    }
    return s;
}

const std::string format(std::string s, int count) {
    while (s.size() <= count) {
        s.push_back(' ');
    }
    return s;
}

Op2::Op2(Register reg, int imm, std::string shift, bool reg_used) : reg(reg),
                                                                    imm(imm),
                                                                    shift(shift),
                                                                    reg_used(reg_used) {}

bool Op2::isRegUsed() { return this->reg_used; }

bool Op2::isValidImm() {
    // return Judge(self.imm)||Judge(~self.imm);
    return this->imm >= 0 && this->imm <= IMM_8_MAX;
}

Op2::operator std::string() const {

    if (this->reg_used) {
        return std::string(this->reg) + ", " + this->shift + " #" + std::to_string(this->imm);
    } else {
        return "#" + std::to_string(this->imm);
    }
}

Op2 Op2::LSL(Register reg, int shift_imm) {
    return Op2(reg, 0, "LSL", true);
}
Op2 Op2::ASL(Register reg, int shift_imm) {
    return Op2(reg, 0, "ASL", true);
}
Op2 Op2::LSR(Register reg, int shift_imm) {
    return Op2(reg, 0, "LSR", true);
}
Op2 Op2::ASR(Register reg, int shift_imm) {
    return Op2(reg, 0, "ASR", true);
}

Op2 Op2::Imm(int imm) {
    return Op2(Register(Register::R0), imm, "", false);
}

int Op2::getImm() { return this->imm; }

Inst::Inst(std::string operation) : operation(operation),
                                    s_flag(""),
                                    condition(Cond::AL),
                                    Rd_used(false),
                                    Rn_used(false),
                                    Rm_used(false),
                                    Rs_used(false),
                                    Op2_used(false),
                                    label_used(false),
                                    Rd(Register::R0),
                                    Rn(Register::R0),
                                    Rm(Register::R0),
                                    Rs(Register::R0),
                                    operand2(Op2::Imm(0)),
                                    label("") {}

std::string Inst::address(Register reg, int offset) {
    return "[" + std::string(reg) + ", #" + std::to_string(offset) + "]";
}

Inst Inst::SUB(Register Rd, Register Rn, Op2 op2) {
    return Inst("SUB").useRd(Rd).useRn(Rn).useOp2(op2);
}
Inst Inst::ADD(Register Rd, Register Rn, Op2 op2) {
    return Inst("ADD").useRd(Rd).useRn(Rn).useOp2(op2);
}
Inst Inst::MLA(Register Rd, Register Rn, Register Rm, Register Ra) {
    return Inst("MLA").useRd(Rd).useRn(Rn).useRm(Rm).useRs(Ra);
}

Inst Inst::AND(Register Rd, Register Rn, Op2 op2) {
    return Inst("AND").useRd(Rd).useRn(Rn).useOp2(op2);
}
Inst Inst::ORR(Register Rd, Register Rn, Op2 op2) {
    return Inst("ORR").useRd(Rd).useRn(Rn).useOp2(op2);
}
Inst Inst::EOR(Register Rd, Register Rn, Op2 op2) {
    return Inst("EOR").useRd(Rd).useRn(Rn).useOp2(op2);
}
Inst Inst::LSL(Register Rd, Register Rn, Op2 op2) {
    return Inst("LSL").useRd(Rd).useRn(Rn).useOp2(op2);
}
Inst Inst::ASR(Register Rd, Register Rn, Op2 op2) {
    return Inst("ASR").useRd(Rd).useRn(Rn).useOp2(op2);
}
Inst Inst::LSR(Register Rd, Register Rn, Op2 op2) {
    return Inst("LSR").useRd(Rd).useRn(Rn).useOp2(op2);
}
Inst Inst::CMP(Register Rd, Op2 op2) {
    return Inst("CMP").useRd(Rd).useOp2(op2);
}
Inst Inst::MUL(Register Rd, Register Rn, Register Rs) {
    return Inst("MUL").useRd(Rd).useRn(Rn).useRs(Rs);
}

Inst Inst::MOVi(Register Rd, Op2 op2) {
    return Inst("MOV").useRd(Rd).useOp2(op2);
}

std::string Inst::MOV(Register Rd, Op2 op2) {
    int imm=op2.getImm();
    if( imm>=0&&imm<= IMM_16_MAX)
    {
        return  f8("MOV") + std::string(Rd) + ", #" + std::to_string(imm);
    }
    else if(-IMM_16_MAX <= imm && imm <= 0)
    {
        return f8("MVN") + std::string(Rd) + ", #" + std::to_string(-imm-1);
    }
    else 
    {
        uint32_t p=op2.getImm();
        std::string tmp = f8("MOVW") + std::string(Rd) + ", #" + std::to_string(p & 0x0000ffff);
        tmp += ENDL + INDENT8 +
               f8("MOVT") + std::string(Rd) + ", #" + std::to_string(p >> 16);
        return tmp;
    }
    
}

std::string Inst::STR_base_offset(Register Rd, Register Rn, Register Rs, int shift) {
    return f8("STR") + std::string(Rd) + ", [" + std::string(Rn) + ", +" + std::string(Rs) + ", lsl " + std::to_string(shift) + "]";
}

std::string Inst::STR_safe(Register Rd, Register Rn, int offset) {
    if (offset > IMM_12_MAX || offset < -IMM_12_MAX) {
        return LDR_D(TEMP_REG_ID, offset) + ENDL + INDENT8 +
               f8("STR") + std::string(Rd) + ", [" + std::string((Rn)) +
               ", " + std::string(Register(TEMP_REG_ID)) + "]";
    } else {
        return f8("STR") + std::string(Rd) + ", " + address(Rn, offset);
    }
}

std::string Inst::LDR(Register reg, std::string address) {
    return f8("LDR") + std::string(reg) + ", " + address;
}

std::string Inst::LDR_base_offset(Register Rd, Register Rn, Register Rs, int shift) {
    return f8("LDR") + std::string(Rd) + ", [" + std::string(Rn) + ", +" + std::string(Rs) + ", lsl " + std::to_string(shift) + "]";
}

std::string Inst::LDR_G(Register Rd, int idx,int blockcnt) {

    std::string tmp = Inst::LDR(Rd, ".LT" + std::to_string(blockcnt + 1) + "+" + std::to_string(idx*4));
    //  tmp += ENDL + INDENT8f8("LDR") + std::string(Rd) + ", ["  + std::string(Register(TEMP_REG_ID)) + "]";
    return tmp;
}

std::string Inst::ADRL(Register Rd, std::string address) {
    return f8("ADRL") + std::string(Rd) + ", " + address;
}

std::string Inst::LDR_safe(Register Rd, Register Rn, int offset) {
    if (offset < -IMM_12_MAX || offset > IMM_12_MAX) {
        return LDR_D(TEMP_REG_ID, offset) + ENDL + INDENT8 + f8("LDR") + std::string(Rd) + ", [" + std::string(Rn) + ", " + std::string(Register(TEMP_REG_ID)) + "]";
    } else {
        return LDR(Rd, address(Rn, offset));
    }
}

std::string Inst::LDR_D(Register reg, unsigned int imm) {
    if(imm<= IMM_16_MAX)
    {
        return  f8("MOV") + std::string(reg) + ", #" + std::to_string(imm);
    }
    else if(-IMM_16_MAX <= (int)(imm) && (int)(imm) <= 0)
    {
        return f8("MVN") + std::string(reg) + ", #" + std::to_string(-(int)(imm)-1);
    }
    else 
    {
        std::string tmp = f8("MOVW") + std::string(reg) + ", #" + std::to_string(imm & 0x0000ffff);
        tmp += ENDL + INDENT8 +
               f8("MOVT") + std::string(reg) + ", #" + std::to_string(imm >> 16);
        return tmp;
    }
}

std::string Inst::MOVW(Register reg, int imm) {
    return f8("MOVW") + std::string(reg) + ", #" + std::to_string(imm);
}
std::string Inst::MOVT(Register reg, unsigned int imm) {
    return f8("MOVT") + std::string(reg) + ", #" + std::to_string(imm);
}
std::string Inst::LDR_D(Register reg, std::string label) {
    /// TODO : 优化全局寻址
    return f8("LDR") + std::string(reg) + ", " + label;
    // std::string tmp=std::string(f8("MOVW") + std::string(reg) + ", :lower16:" + label);
    //     tmp+=ENDL + INDENT8+
    //     f8("MOVT") + std::string(reg) + ", :upper16:" +label;
    //    return tmp;
}

std::string Inst::PUSH(std::vector<Register> regs) {
    std::string result = "{";
    for (int i = 0; i < regs.size(); i++) {
        result += std::string(regs[i]);
        if (i == regs.size() - 1) {
            result += "}";
        } else {
            result += ", ";
        }
    }
    return f8("PUSH") + result;
}

std::string Inst::POP(std::vector<Register> regs) {
    std::string result = "{";
    for (int i = 0; i < regs.size(); i++) {
        result += std::string(regs[i]);
        if (i == regs.size() - 1) {
            result += "}";
        } else {
            result += ", ";
        }
    }
    return f8("POP") + result;
}

std::string Inst::BL(std::string func_name) {
    return f8("BL") + func_name;
}

Inst Inst::B(std::string label) {
    return Inst("B").useLabel(label);
}

Inst::operator std::string() {
    std::string code_string;
    code_string += f8(this->operation + to_string(this->condition));
    if (this->Rd_used) {
        code_string += std::string(this->Rd) + ", ";
    }
    if (this->Rn_used) {
        code_string += std::string(this->Rn) + ", ";
    }
    if (this->Rm_used) {
        code_string += std::string(this->Rm) + ", ";
    }
    if (this->Rs_used) {
        code_string += std::string(this->Rs) + ", ";
    }
    if (this->Op2_used) {
        if (this->operand2.isValidImm()) {
            return code_string + std::string(this->operand2);
        } else {
            return LDR_D(Register(Register::R11), this->operand2.getImm()) + "\n" + INDENT8 + code_string + "R11";
        }
    }
    if (this->label_used) {
        code_string += this->label + ", ";
    }
    return code_string.substr(0, code_string.size() - 2);
}

Register::RP Register::getRegisterPrototypeByID(int register_id) {
    switch (register_id) {
        case 0: return RP::R0;
        case 1: return RP::R1;
        case 2: return RP::R2;
        case 3: return RP::R3;
        case 4: return RP::R4;
        case 5: return RP::R5;
        case 6: return RP::R6;
        case 7: return RP::R7;
        case 8: return RP::R8;
        case 9: return RP::R9;
        case 10: return RP::R10;
        case 11: return RP::R11;
        case 12: return RP::R12;
        case 13: return RP::R13;
        case 14: return RP::R14;
        case 15: return RP::R15;
        default: return RP::Custom;
    }
}
int Register::getIDByRegisterPrototype(RP register_prototype) {
    switch (register_prototype) {
        case R0: return 0;
        case R1: return 1;
        case R2: return 2;
        case R3: return 3;
        case R4: return 4;
        case R5: return 5;
        case R6: return 6;
        case R7: return 7;
        case R8: return 8;
        case R9: return 9;
        case R10: return 10;
        case R11: return 11;
        case R12: return 12;
        case R13: return 13;
        case R14: return 14;
        case R15: return 15;
        default: return -1;
    }
}

Inst &Inst::useRd(Register rd) {
    this->Rd_used = true;
    this->Rd = rd;
    return self;
}
Inst &Inst::useRn(Register rn) {
    this->Rn_used = true;
    this->Rn = rn;
    return self;
}
Inst &Inst::useRs(Register rs) {
    this->Rs_used = true;
    this->Rs = rs;
    return self;
}
Inst &Inst::useRm(Register rm) {
    this->Rm_used = true;
    this->Rm = rm;
    return self;
}
Inst &Inst::useOp2(Op2 operand2) {
    this->Op2_used = true;
    this->operand2 = operand2;
    return self;
}
Inst &Inst::useLabel(std::string label) {
    this->label_used = true;
    this->label = label;
    return self;
}