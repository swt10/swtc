#include <iostream>

#include "ASMBuilder.h"
#include "Value.h"

#define for_in(element_name, ilist) for (auto element_name = ilist.head; element_name != nullptr; element_name = element_name->next)

using namespace SWTC;

RTAllocator::RTAllocator() : register_mapping(),
                             stack_mapping(),
                             allocated(),
                             stack_size(0),
                             global_offset_table() {}

bool RTAllocator::isAllocatable(Register reg) {
    return reg.getID() <= 10 && reg.getID() >= 0;
}

bool RTAllocator::isCalleeStorable(Register reg) {
    return reg.getID() <= 11 && reg.getID() >= 4;
}

bool RTAllocator::isCallerStorable(Register reg) {
    auto id = reg.getID();
    return (id >= 0 && id <= 3) || id == 12 || id == 14;
}
void RTAllocator::allocateFunction(Function *func) {
    this->stack_size = 0;
    this->stack_mapping.clear();
    // for_in(glo, func->getParent()->getGlobalList()){
    //     auto val_size = 4;
    //     if(!this->register_mapping.count(glo)){
    //         stack_mapping[glo] = this->stack_size;
    //         this->stack_size+=val_size;
    //     }
    // }
    for_in(arg, func->getArgumentList()) {
        auto val_size = align4byte(arg->getType()->size(false));
        assert(val_size == 4);
        if (!this->register_mapping.count(arg)) {
            stack_mapping[arg] = this->stack_size;
            this->stack_size += val_size;
        }
    }
    for_in(basic_block, func->getBasicBlockList()) {
        for_in(inst, basic_block->getInstList()) {
            if (this->register_mapping.count(inst)) {
                auto map_reg = this->register_mapping.at(inst);  // TODO: 检查为什么会产生不合法ID
                if (map_reg > RTAllocator::REG_MAX) {
                    register_mapping.erase(inst);
                }
                if (!this->isAllocatable(map_reg)) {
                    std::cerr << "RTAllocator allocateFunction Failed:" << std::endl;
                    std::cerr << "Reg[" << map_reg << "]"
                              << "is not allocatable." << std::endl;
                    abort();
                }
            }
            if (this->register_mapping.count(inst)) {
                std::cerr << "No Register Mapping found." << std::endl;
                continue;
            }
            if (this->stack_mapping.count(inst)) {
                continue;
            }
            // if (isa<AllocaInst>(inst)) {
            //     continue;
            // }
            auto val_size = align4byte(inst->getType()->size(inst->getOpcode() == Instruction::Alloca));
            //std::cerr << "Allocate " << val_size << " For Inst:" << Instruction::getOpcodeName(inst->getOpcode()) << std::endl;
            if (val_size > 0) {
                this->stack_mapping[inst] = this->stack_size;
                this->stack_size += val_size;
            }
        }
    }

    auto stack_bits = CACHE_LINE_BITS;
    while ((1 << stack_bits) < this->stack_size) {
        stack_bits++;
    }
    this->stack_size = 1 << stack_bits;

}

void RTAllocator::allocateValue(Value *value) {
    this->allocated.emplace(value);
}