#include <cassert>
#include <cstdlib>
#include <iostream>
#include <ostream>
#include <string>

#include "ASMBuilder.h"
#include "BasicBlock.h"
#include "Casting.h"
#include "IRhead.h"
#include "Value.h"

#define self (*this)
#define for_in(element_name, ilist) for (auto element_name = ilist.head; element_name != nullptr; element_name = element_name->next)
#define print_isnullptr(ptr) std::cerr << "Nullptr Check[" << #ptr << "]: " << (ptr ? "Some" : "None") << std::endl;
using namespace SWTC;

Cond condMapIRtoASM(ICmpInst::Predicate ir_cond) {
    switch (ir_cond) {
        case ICmpInst::ICMP_EQ: return Cond::EQ;
        case ICmpInst::ICMP_NE: return Cond::NE;
        case ICmpInst::ICMP_SGE: return Cond::GE;
        case ICmpInst::ICMP_SGT: return Cond::GT;
        case ICmpInst::ICMP_SLE: return Cond::LE;
        case ICmpInst::ICMP_SLT: return Cond::LT;
        case ICmpInst::ICMP_UGE: return Cond::HS;
        case ICmpInst::ICMP_UGT: return Cond::HI;
        case ICmpInst::ICMP_ULE: return Cond::LS;
        case ICmpInst::ICMP_ULT: return Cond::LO;
    }
}

Cond condMapIRtoASM_N(ICmpInst::Predicate ir_cond) {
    switch (ir_cond) {
        case ICmpInst::ICMP_EQ: return Cond::NE;
        case ICmpInst::ICMP_NE: return Cond::EQ;
        case ICmpInst::ICMP_SGE: return Cond::LT;
        case ICmpInst::ICMP_SGT: return Cond::LE;
        case ICmpInst::ICMP_SLE: return Cond::GT;
        case ICmpInst::ICMP_SLT: return Cond::GE;
        case ICmpInst::ICMP_UGE: return Cond::LO;
        case ICmpInst::ICMP_UGT: return Cond::LS;
        case ICmpInst::ICMP_ULE: return Cond::HI;
        case ICmpInst::ICMP_ULT: return Cond::HS;
    }
}

Register ASMBuilder::getRegElse(Value *val, int else_reg_id) {
    return this->allocator.register_mapping.count(val)
               ? this->allocator.register_mapping.at(val)
               : else_reg_id;
}

bool isInstBinaryorMLA(Instruction *inst) {
    switch (inst->getOpcode()) {
        case Instruction::Add: return true;
        case Instruction::Sub: return true;
        case Instruction::Mul: return true;
        case Instruction::MLA: return true;
        case Instruction::UDiv: return true;
        case Instruction::SDiv: return true;
        case Instruction::URem: return true;
        case Instruction::SRem: return true;
        case Instruction::And: return true;
        case Instruction::Or: return true;
        case Instruction::Xor: return true;
        case Instruction::Shl: return true;
        case Instruction::LShr: return true;
        case Instruction::AShr: return true;
        case Instruction::ICmp: return true;
        default: return false;
    }
}

void ASMBuilder::buildInstRet(Instruction *inst) {
    ReturnInst *rt=dyn_cast<ReturnInst>(inst);
    if (rt->getReturnValue()!=nullptr) {
        self.locationToReg(rt->getReturnValue(), Register::R0);
    }
    self.buildRetFunction(inst->getFunction());
}

void ASMBuilder::buildInstAlloca(Instruction *inst) {
    if (self.allocator.register_mapping.count(inst)) {
        auto offset = self.allocator.stack_mapping.at(inst);
        Register target = self.allocator.register_mapping.at(inst);
        self += Inst::ADD(target, Register::SP, Op2::Imm(offset));
    }
    self.allocator.allocateValue(inst);
}

void ASMBuilder::buildInstCall(Instruction *inst) {
    std::string func_name = inst->getOperand(0)->getName();
    std::vector<Value *> args;
    for (int i = 1; i < inst->getNumOperands(); i++) {
        args.emplace_back(inst->getOperand(i));
    }
    self.buildCallFunction(inst, func_name, args, 0, 0);
}

void ASMBuilder::buildInstLoad(Instruction *inst) {
    auto operand0 = inst->getOperand(0);
    auto operand1 = inst->getNumOperands() > 1 ? inst->getOperand(1) : nullptr;
    auto operand2 = inst->getNumOperands() > 2 ? inst->getOperand(2) : nullptr;
    Register operand0_reg = self.getRegElse(operand0, OP_REG_0);
    Register return_reg = self.getRegElse(inst, OP_REG_0);
    self.locationToReg(operand0, operand0_reg);
    // self += Inst::LDR_safe(return_reg, return_reg, 0);
    if (operand1 && operand2) {
        ConstantInt *op1_tval = dyn_cast_nullable<ConstantInt>(operand1);
        ConstantInt *op2_shift = dyn_cast_nullable<ConstantInt>(operand2);
        // TODO: Check if op2 is nullptr / if shift between 0 ~ 32
        int shift = op2_shift ? op2_shift->getValue() : 0;
        if (op1_tval) {
            self += Inst::LDR_safe(return_reg, operand0_reg, op1_tval->getValue() << shift);
        } else {
            Register operand1_reg = self.getRegElse(operand1, OP_REG_1);
            self.locationToReg(operand1, operand1_reg);
            self += Inst::LDR_base_offset(return_reg, operand0_reg, operand1_reg, shift);
        }
    } else {
        self += Inst::LDR_safe(return_reg, operand0_reg, 0);
    }
    self.regToLocation(inst, return_reg);
}

void ASMBuilder::buildInstStore(Instruction *inst) {
    auto operand0 = inst->getOperand(0);  //src
    auto operand1 = inst->getOperand(1);  //dst
    auto operand2 = inst->getNumOperands() > 2 ? inst->getOperand(2) : nullptr;
    auto operand3 = inst->getNumOperands() > 3 ? inst->getOperand(3) : nullptr;
    Register operand0_reg = self.allocator.register_mapping.count(operand0) ? self.allocator.register_mapping.at(operand0) : OP_REG_0;
    Register operand1_reg = self.allocator.register_mapping.count(operand1) ? self.allocator.register_mapping.at(operand1) : OP_REG_1;
    Instruction *inst_t = dyn_cast_nullable<Instruction>(operand1);
    self.locationToReg(operand0, operand0_reg);
    self.locationToReg(operand1, operand1_reg);
    // self += Inst::STR_safe(operand0_reg, operand1_reg, 0);
    // self.regToLocation(operand1, operand0_reg);
    if (operand2) {
        ConstantInt *op2_tval = dyn_cast_nullable<ConstantInt>(operand2);
        ConstantInt *op3_shift = dyn_cast_nullable<ConstantInt>(operand3);
        // TODO: Check if op2 is nullptr / if shift between 0 ~ 32
        int shift = op3_shift ? op3_shift->getValue() : 0;
        if (op2_tval) {
            self += Inst::STR_safe(operand0_reg, operand1_reg, op2_tval->getValue() << shift);
        } else {
            Register operand2_reg = self.allocator.register_mapping.count(operand2) ? self.allocator.register_mapping.at(operand2) : OP_REG_2;
            self.locationToReg(operand2, operand2_reg);
            self += Inst::STR_base_offset(operand0_reg, operand1_reg, operand2_reg, shift);
        }
    } else {
        self += Inst::STR_safe(operand0_reg, operand1_reg, 0);
    }
}

void ASMBuilder::buildInstBr(Instruction *inst) {
    auto branch_inst = dyn_cast<BranchInst>(inst);
    BasicBlock *inst_bb = inst->getParent();
    if (branch_inst->isConditional()) {
        auto if_true_bb = static_cast<BasicBlock *>(inst->getOperand(0));
        auto if_false_bb = static_cast<BasicBlock *>(inst->getOperand(1));
        bool next_if_true = inst_bb->next == if_true_bb;
        bool next_if_false = inst_bb->next == if_false_bb;
        if (auto cond = dyn_cast<ConstantInt>(branch_inst->getCondition())) {
            if (cond->getValue()) {
                self += Inst::B("." + if_true_bb->getParent()->getName() + "_" + if_true_bb->getName());
            } else {
                self += Inst::B("." + if_false_bb->getParent()->getName() + "_" + if_false_bb->getName());
            }
            if(waiting_ltorg)
            {
                self.waiting_ltorg=false;
                self.instruction_counter=0;
                self += Directive::ltorg();
                // GenWordPool();
            }
            return;
        }
        Register operand0_reg = getRegElse(inst->getOperand(2), OP_REG_0);
        self.locationToReg(branch_inst->getCondition(), operand0_reg);
        self += Inst::CMP(operand0_reg, Op2::Imm(0));
        if (!next_if_false && !next_if_false) {
            self += Inst::B("." + if_false_bb->getParent()->getName() + "_" + if_false_bb->getName()).cond(Cond::EQ);
            self += Inst::B("." + if_true_bb->getParent()->getName() + "_" + if_true_bb->getName());
            if(waiting_ltorg)
            {
                self.waiting_ltorg=false;
                self.instruction_counter=0;
                self += Directive::ltorg();
                // GenWordPool();
            }
            return;
        }
        auto target_bb = next_if_true ? if_false_bb : if_true_bb;
        auto cond = next_if_false ? Cond::NE : Cond::EQ;
        self += Inst::B("." + target_bb->getParent()->getName() + "_" + target_bb->getName()).cond(cond);
    } else {
        auto target_bb = static_cast<BasicBlock *>(inst->getOperand(0));
        self += Inst::B("." + target_bb->getParent()->getName() + "_" + target_bb->getName());
        if(waiting_ltorg)
            {
                self.waiting_ltorg=false;
                self.instruction_counter=0;
                self += Directive::ltorg();
                // GenWordPool();
            }
    }
}

void ASMBuilder::buildInstruction(Instruction *inst) {
    auto inst_type = inst->getOpcode();
    // if(!isa<ReturnInst>(inst))return ;
    // std::cerr << "Building Inst: " << Instruction::getOpcodeName(inst_type) << std::endl;
    self.comment(Instruction::getOpcodeName(inst_type));
    switch (inst_type) {
        case Instruction::Ret: buildInstRet(inst); break;
        case Instruction::Alloca: buildInstAlloca(inst); break;
        case Instruction::Call: buildInstCall(inst); break;
        case Instruction::Load: buildInstLoad(inst); break;
        case Instruction::Store: buildInstStore(inst); break;
        case Instruction::Br: buildInstBr(inst); break;
        case Instruction::GetElementPtr: break;
        case Instruction::PHI: break;
        case Instruction::Move: {
            auto operand0 = dyn_cast<MoveInst>(inst)->Dst;
            auto operand1 = inst->getOperand(0);
            locationToLocation(operand1, operand0, 0);
            break;
        }
        default: {
            if (isInstBinaryorMLA(inst)) {
                // Prepare operands for binary instructions
                auto operand0 = inst->getOperand(0);
                auto operand1 = inst->getOperand(1);
                auto operand2 = inst->getNumOperands() > 2 ? inst->getOperand(2) : nullptr;
                Register operand0_reg = getRegElse(operand0, OP_REG_0);
                Register operand1_reg = operand1 ? getRegElse(operand1, OP_REG_1) : Register(-1);
                Register operand2_reg = operand2 ? getRegElse(operand2, OP_REG_2) : Register(-1);
                Register return_reg = getRegElse(inst, OP_REG_0);
                ConstantInt *operand1_val = operand1 ? dyn_cast_nullable<ConstantInt>(operand1) : nullptr;
                ConstantInt *operand2_val = operand2 ? dyn_cast_nullable<ConstantInt>(operand2) : nullptr;
                int shift = operand2_val ? operand2_val->getValue() : 0;
                // TODO: Check if operand1_val is nullptr
                if (inst->getOpcode() != Instruction::UDiv && inst->getOpcode() != Instruction::URem && inst->getOpcode() != Instruction::SDiv && inst->getOpcode() != Instruction::SRem) {
                    self.locationToReg(operand0, operand0_reg);
                }
                //self.locationToReg(operand0, operand0_reg);
                switch (inst_type) {
                    case Instruction::MLA: {
                        self.locationToReg(operand1, operand1_reg);
                        self.locationToReg(operand2, operand2_reg);
                        self += Inst::MLA(return_reg, operand0_reg, operand1_reg, operand2_reg);
                        self.regToLocation(inst, return_reg);
                        break;
                    }
                    case Instruction::Add: {
                        if (operand1_val) {
                            self += Inst::ADD(return_reg, operand0_reg, Op2::Imm(operand1_val->getValue() << shift));
                        } else {
                            // self.comment("End Add main");
                            self.locationToReg(operand1, operand1_reg);
                            self += Inst::ADD(return_reg, operand0_reg, Op2::LSL(operand1_reg, shift));
                        }
                        self.regToLocation(inst, return_reg);
                        break;
                    }
                    case Instruction::Sub: {
                        if (operand1_val) {
                            self += Inst::SUB(return_reg, operand0_reg, Op2::Imm(operand1_val->getValue() << shift));
                        } else {
                            self.locationToReg(operand1, operand1_reg);
                            self += Inst::SUB(return_reg, operand0_reg, Op2::LSL(operand1_reg, shift));
                        }
                        self.regToLocation(inst, return_reg);
                        break;
                    }
                    case Instruction::Mul: {
                        if (operand1_val) {
                            self += Inst::LDR_D(operand1_reg, operand1_val->getValue());
                            self += Inst::MUL(return_reg, operand0_reg, operand1_reg);
                        } else {
                            self.locationToReg(operand1, operand1_reg);
                            self += Inst::MUL(return_reg, operand0_reg, operand1_reg);
                        }
                        self.regToLocation(inst, return_reg);
                        break;
                    }
                    case Instruction::UDiv: {
                        self.buildCallFunction(inst, "__aeabi_uidiv", {operand0, operand1}, 0, 0);
                        self.regToLocation(inst, 0);
                        break;
                    }
                    case Instruction::SDiv: {
                        self.buildCallFunction(inst, "__aeabi_idiv", {operand0, operand1}, 0, 0);
                        self.regToLocation(inst, 0);
                        break;
                    }
                    case Instruction::URem: {
                        self.buildCallFunction(inst, "__aeabi_uidivmod", {operand0, operand1}, 1, 0);
                        self.regToLocation(inst, 1);
                        break;
                    }
                    case Instruction::SRem: {
                        self.buildCallFunction(inst, "__aeabi_idivmod", {operand0, operand1}, 1, 0);
                        self.regToLocation(inst, 1);
                        break;
                    }
                    case Instruction::And: {
                        if (operand1_val) {
                            self += Inst::AND(return_reg, operand0_reg, Op2::Imm(operand1_val->getValue() << shift));
                        } else {
                            self.locationToReg(operand1, operand1_reg);
                            self += Inst::AND(return_reg, operand0_reg, Op2::LSL(operand1_reg, shift));
                        }
                        self.regToLocation(inst, return_reg);
                        break;
                    }
                    case Instruction::Or: {
                        if (operand1_val) {
                            self += Inst::ORR(return_reg, operand0_reg, Op2::Imm(operand1_val->getValue() << shift));
                        } else {
                            self.locationToReg(operand1, operand1_reg);
                            self += Inst::ORR(return_reg, operand0_reg, Op2::LSL(operand1_reg, shift));
                        }
                        self.regToLocation(inst, return_reg);
                        break;
                    }
                    case Instruction::Xor: {
                        if (operand1_val) {
                            self += Inst::EOR(return_reg, operand0_reg, Op2::Imm(operand1_val->getValue() << shift));
                        } else {
                            self.locationToReg(operand1, operand1_reg);
                            self += Inst::EOR(return_reg, operand0_reg, Op2::LSL(operand1_reg, shift));
                        }
                        self.regToLocation(inst, return_reg);
                        break;
                    }
                    case Instruction::Shl: {
                        if (operand1_val) {
                            self += Inst::LSL(return_reg, operand0_reg, Op2::Imm(operand1_val->getValue() << shift));
                        } else {
                            self.locationToReg(operand1, operand1_reg);
                            self += Inst::LSL(return_reg, operand0_reg, Op2::LSL(operand1_reg, shift));
                        }
                        self.regToLocation(inst, return_reg);
                        break;
                    }
                    case Instruction::LShr: {
                        if (operand1_val) {
                            self += Inst::LSR(return_reg, operand0_reg, Op2::Imm(operand1_val->getValue() << shift));
                        } else {
                            self.locationToReg(operand1, operand1_reg);
                            self += Inst::LSR(return_reg, operand0_reg, Op2::LSL(operand1_reg, shift));
                        }
                        self.regToLocation(inst, return_reg);
                        break;
                    }
                    case Instruction::AShr: {
                        if (operand1_val) {
                            self += Inst::ASR(return_reg, operand0_reg, Op2::Imm(operand1_val->getValue() << shift));
                        } else {
                            self.locationToReg(operand1, operand1_reg);
                            self += Inst::ASR(return_reg, operand0_reg, Op2::LSL(operand1_reg, shift));
                        }
                        self.regToLocation(inst, return_reg);
                        break;
                    }
                    case Instruction::ZExt: {
                        self += Inst::MOV(return_reg, Op2::LSL(operand0_reg, 0));
                        self.regToLocation(inst, return_reg);
                        break;
                    }
                    case Instruction::ICmp: {
                        auto cond = condMapIRtoASM(static_cast<ICmpInst *>(inst)->getPredicate());
                        if (operand1_val) {
                            self += Inst::CMP(operand0_reg, Op2::Imm(operand1_val->getValue() << shift));
                        } else {
                            self.locationToReg(operand1, operand1_reg);
                            self += Inst::CMP(operand0_reg, Op2::LSL(operand1_reg, shift));
                        }
                        self += Inst::MOVi(return_reg, Op2::Imm(0));
                        self += Inst::MOVi(return_reg, Op2::Imm(1)).cond(cond);
                        self.regToLocation(inst, return_reg);
                        break;
                    }
                }
            } else if (false) {
            } else {
                std::cerr << "Unhandled Instruction:" << inst->getOpcode() << std::endl;
                std::cerr << inst->getOpcodeName(inst->getOpcode()) << std::endl;
                abort();
            }
        }
    }
}
