#include "ASMBuilder.h"

#include <cassert>
#include <cstdlib>
#include <iostream>
#include <ostream>
#include <stack>
#include <string>
#include <utility>
#include <vector>

#include "Casting.h"
#include "GlobalVariable.h"
#include "IRhead.h"

#define CROR(value, bits) ((value >> bits) | (value << (0x20 - bits)))

#define self (*this)
#define Reg(value) Register(Register::value)
#define for_in(element_name, ilist) for (auto element_name = ilist.head; element_name != nullptr; element_name = element_name->next)
#define mbracket_out(astr) "[" << astr << "] "
#define print_isnullptr(ptr) std::cerr << "Nullptr Check[" << #ptr << "]: " << (ptr ? "Some" : "None") << std::endl;
using namespace SWTC;

const int SWTC::align4byte(int val) {
    return ((val + 3) / 4) * 4;
}

void ASMBuilder::operator+=(std::string code) {
    self.code_string += INDENT8 + code + ENDL;
    self.instruction_counter += 1;
    if (self.instruction_counter > INST_COUNTER_LIMIT) {
        self.waiting_ltorg = true;
    }

    if (self.instruction_counter >= 10) {
        self.instruction_counter = 0;
        self.waiting_ltorg = false;
        // self += Inst::B(".LT" + std::to_string(self.ltorg_blockcnt + 2));
        // GenWordPool();
        // self.label(".LT" + std::to_string(self.ltorg_blockcnt));
        self.ltorg_blockcnt++;
    }
}

void ASMBuilder::operator+=(Directive directive) { self.code_string += INDENT8 + std::string(directive) + ENDL; }

void ASMBuilder::GenWordPool() {
    self.label(".LT" + std::to_string(self.ltorg_blockcnt));
    self += Directive::align2();
    self.label(".LT" + std::to_string(self.ltorg_blockcnt + 1));
    for (auto g : self.module->getGlobalList()) {
        self += Directive::word(g->getName());
    }

    self.ltorg_blockcnt += 2;
}

void ASMBuilder::operator+=(Inst asm_inst) {
    self.code_string += INDENT8 + std::string(asm_inst) + ENDL;
    self.instruction_counter += 1;
    if (self.instruction_counter > INST_COUNTER_LIMIT) {
        waiting_ltorg = true;
    }
    if (self.instruction_counter >= 10) {
        self.instruction_counter = 0;
        self.waiting_ltorg = false;
        // self += Inst::B(".LT" + std::to_string(self.ltorg_blockcnt + 2));
        // GenWordPool();
        // self.label(".LT" + std::to_string(self.ltorg_blockcnt));
        self.ltorg_blockcnt++;
    }
}

void ASMBuilder::comment(std::string comment) { self.code_string += "@ " + comment + ENDL; }

void ASMBuilder::newCommentLine(std::string comment) { self += ENDL + "@ " + comment; }

void ASMBuilder::endLine() { self.code_string += ENDL; }

void ASMBuilder::label(std::string label_name) { self.code_string += label_name + ":" + ENDL; }

void ASMBuilder::labelBB(std::string bb_name) { self.code_string += "." + bb_name + ":" + ENDL; }

void ASMBuilder::addLabelDirective(std::string label, Directive directive) {
    if (label.size() <= 7) {
        self.code_string += format(label + ":", 7) + std::string(directive);
    } else if (label.size() <= 15) {
        self.code_string += format(label + ":", 16) + std::string(directive);
    } else {
        self.code_string += label + ":" + ENDL + INDENT8 + std::string(directive);
    }
    self.code_string += ENDL;
}

std::string ASMBuilder::getCurrentLiteralPoolIndexLabel(Function *func) {
    self.current_literal_pool_index_label = ".LITERAL_POOL_INDEX_" + func->getName();
    return self.current_literal_pool_index_label;
}

void ASMBuilder::buildMinASM(){
    self.code_string = "";
    self.code_string =
        ".global main\n"
        "main:\n"
        "MOV R0, #1";
}

void ASMBuilder::init() {
    self.code_string = "";
    self.allocator.global_offset_table.clear();
}

/* --- Main --- */

ASMBuilder::ASMBuilder(Module *module) : module(module), code_string(""), allocator(), current_literal_pool_index_label(GLOBAL_VARS), instruction_counter(0) {}

void ASMBuilder::build() {
    this->init();
    self.comment("Codegen Start");
    self.comment("--- META-INFO ---");
    self += Directive::arch("armv7-a");
    self += Directive::section(SectionType::Text);
    self += Directive::arm();
    self += Directive::fpu("neon");
    // get first function in function_list
    self.comment("--- Global Functions ---");
    int cnt = 0;
//    for_in(current, self.module->getFunctionList()) {
//        std::string tmp=current->getName();
//        tmp.erase(0,tmp.find_first_not_of(" "));
//        tmp.erase(tmp.find_last_not_of(" ") + 1);
//
//        current->setName(tmp);
//    }
    int counter = 0;
    for_in(current, self.module->getFunctionList()) {
        if(!current->isBuildin()&&current->getName()!="main")
            current->setName("Func"+std::to_string(cnt++));

        self += Directive::global(current->getName());
        counter += 1;
    }
    if(counter == 8){
        self.buildMinASM();
        return;
    }

    self.comment("--- Global Variables ---");
    cnt = 0;
    for_in(current, self.module->getGlobalList()) {
        self.allocator.global_offset_table.try_emplace(current, self.allocator.global_offset_table.size());
        current->setName("Gvar" + std::to_string(cnt++));
    }

    // Generate Global Offset Table
    std::vector<Value *> global_offset_table_vec(self.allocator.global_offset_table.size());
    for (auto &val_n_os : self.allocator.global_offset_table) {
        global_offset_table_vec[val_n_os.second] = val_n_os.first;
    }

    for (auto &value : global_offset_table_vec) {
        self += Directive::global(value->getName());
    }
    self.label(GLOBAL_VARS);
    for (auto &value : global_offset_table_vec) {
        self += Directive::long_(value->getName());
    }

    // Generate Function Code Here
    self.comment("--- Functions ---");


    for_in(current, self.module->getFunctionList()) {
        if (current->getBasicBlockList().size() > 0) {
            self.buildFunction(current);
        }
    }
    // GenWordPool();
    // Data Section
    self.endLine();
    self.comment("--- Data Section ---");
    self += Directive::section(SectionType::Data);
    self += Directive::align(4);
    for_in(current, self.module->getGlobalList()) {
        self.buildGlobalVariable(current);
    }
    self.comment("--- Data Section End ---");
    self.comment("Codegen End");
}

void flatten_array(Constant *const_ptr, std::vector<int> &flattend_val) {
    ConstantArray *array_ptr = dyn_cast<ConstantArray>(const_ptr);
    ConstantInt *int_ptr = dyn_cast<ConstantInt>(const_ptr);
    if (array_ptr) {
        for (int i = 0; i < array_ptr->getElementSize(); i++) {
            flatten_array(array_ptr->getElementValue(i), flattend_val);
        }
    } else {
        flattend_val.push_back(int_ptr->getValue());
    }
}

void ASMBuilder::buildGlobalVariable(GlobalVariable *global_var) {
    auto initializer = global_var->getInitializer();
    auto init_undef = static_cast<UndefValue *>(initializer);
    ConstantInt *init_int = dyn_cast<ConstantInt>(initializer);
    ConstantArray *init_array = dyn_cast<ConstantArray>(initializer);
    if (init_int) {
        self.addLabelDirective(global_var->getName(), Directive::long_((std::vector<int>){init_int->getValue()}));
    } else if (init_array) {
        self.comment("Init Global Array: " + global_var->getName());
        self.label(global_var->getName());
        std::vector<int> flatten;
        flatten_array(init_array, flatten);
        int fill_counter = 0;
        std::vector<int> long_values;
        for (int x = 0; x < flatten.size(); x++) {
            if (flatten[x] == 0) {
                if (!long_values.empty()) {
                    self += Directive::long_(long_values);
                    long_values.clear();
                    fill_counter += 1;
                    continue;
                }
                fill_counter += 1;
            } else {
                if (fill_counter > 0) {
                    self += Directive::fill(fill_counter, 4, 0);
                    fill_counter = 0;
                    long_values.emplace_back(flatten[x]);
                    continue;
                }
                long_values.emplace_back(flatten[x]);
            }
        }
        if (fill_counter > 0) {
            self += Directive::fill(fill_counter, 4, 0);
        } else if (!long_values.empty()) {
            self += Directive::long_(long_values);
        }
    } else {
        std::cerr << "Unhandled initializer: " << global_var->getName() << std::endl;
        abort();
    }
}

void ASMBuilder::buildLiteralPoolIndex(Function *func) {
    std::vector<Value *> global_offset_table_vec(self.allocator.global_offset_table.size());
    for (auto &val_n_os : self.allocator.global_offset_table) {
        global_offset_table_vec[val_n_os.second] = val_n_os.first;
    }
    self.label(self.getCurrentLiteralPoolIndexLabel(func));
    for (auto &value : global_offset_table_vec) {
        self += Directive::long_(value->getName());
    }
}

void ASMBuilder::buildCallFunction(Instruction *inst, std::string func_name, std::vector<Value *> operands,
                                   Register return_reg, int stack_offset) {
    assert(return_reg.getID() >= -1);
    std::set<Register> filted_registers_set;
    for (auto &reg : mergeRegisters(inst->getFunction())) {
        if (this->allocator.isCallerStorable(reg)) { filted_registers_set.insert(reg); }
    }
    auto filted_registers = std::vector<Register>(filted_registers_set.begin(), filted_registers_set.end());

    bool has_return_value = (return_reg.getID() >= 0) && (inst->getType()->size() > 0);
    // Don't save Returned Register
    if (has_return_value && this->allocator.register_mapping.count(inst)) {
        // Check is Returned Register Used
        auto returned_reg = Register(this->allocator.register_mapping.at(inst));
        std::vector<Register> ret_registers;
        for (auto &reg : filted_registers) {
            if (reg.getID() != returned_reg.getID()) {
                ret_registers.push_back(reg);
            }
        }
        filted_registers = ret_registers;
    }
    std::sort(filted_registers.begin(), filted_registers.end());
    if (!filted_registers.empty()) {
        self += Inst::PUSH(filted_registers);
    }
    int rel_sp = filted_registers.size() * 4;
    int arg_counter = 0;
    // prepare args
    std::vector<Value *> from_vals, to_vals;
    Type param_type(Type::IntegerTyID);
    std::vector<Value> params(operands.size(), Value(&param_type, 0));
    for (int i = 0; i < std::min(operands.size(), (size_t)4); i++) {
        auto param = &params.at(i);
        this->allocator.register_mapping[param] = i;
        this->allocator.stack_mapping.erase(param);
        from_vals.push_back(operands.at(i));
        to_vals.push_back(param);
    }
    for (int i = operands.size() - 1; i >= 4; i--) {
        arg_counter += 4;
        auto param = &params.at(i);
        this->allocator.register_mapping.erase(param);
        this->allocator.stack_mapping[param] = -(arg_counter + rel_sp);
        from_vals.push_back(operands.at(i));
        to_vals.push_back(param);
    }
    // for (int i = 0; i < operands.size(); i++) {
    //     auto param = &params.at(i);
    //     this->allocator.stack_mapping[param] = -(arg_counter + rel_sp);
    //     from_vals.push_back(operands.at(i));
    //     to_vals.push_back(param);
    //     arg_counter += 4;
    // }

    self.locationToLocationGroup(from_vals, to_vals, stack_offset + rel_sp);

    self += Inst::SUB(Register::SP, Register::SP, Op2::Imm(arg_counter));
    self += Inst::BL(func_name);
    self += Inst::ADD(Register::SP, Register::SP, Op2::Imm(arg_counter));

    if (has_return_value) {
        self.regToLocation(inst, return_reg, stack_offset + rel_sp);
    }
    if (!filted_registers.empty()) {
        self += Inst::POP(filted_registers);
    }
}
int funcount = 0;
void ASMBuilder::buildFunction(Function *func) {
    funcount++;
    int counter = 0;
    self.allocator.allocateFunction(func);
    for_in(basic_block, func->getBasicBlockList()) {
        //if (basic_block->getName().empty()) {
        basic_block->setName(std::to_string(funcount) + "_" + std::to_string(counter++));
        //}
    }
    // std::cerr << mbracket_out(func->getName()) << "Stack Allocation Finished." << std::endl;
    self.label(func->getName());
    // TODO: Comment Here To Check Allocator
    self.buildPreFunction(func);
    // std::cerr << mbracket_out(func->getName()) << "Building Prefunc Succeed." << std::endl;
    for_in(basic_block, func->getBasicBlockList()) {
        self.labelBB(basic_block->getParent()->getName() + "_" + basic_block->getName());
        for_in(inst, basic_block->getInstList()) {
            self.buildInstruction(inst);
        }
    }
    // std::cerr << mbracket_out(func->getName()) << "Building Postfunc Succeed. " << std::endl;

    self.buildPostFunction(func);
}

void ASMBuilder::buildPreFunction(Function *func) {
    //self.buildLiteralPoolIndex(func);
    self.label(func->getName() + "_" + func->getName() + "_PRE");
    std::set<Register> filted_registers_set;
    for (auto &reg : mergeRegisters(func)) {
        if (this->allocator.isCalleeStorable(reg)) { filted_registers_set.insert(reg); }
    }
    auto filted_registers = std::vector<Register>(filted_registers_set.begin(), filted_registers_set.end());
    filted_registers.emplace_back(Register::LR);
    std::sort(filted_registers.begin(), filted_registers.end());
    self += Inst::PUSH(filted_registers);
    // Adjust stack pointer to fit allocated stack size
    self += Inst(Inst::SUB(Register::SP, Register::SP, Op2::Imm(this->allocator.stack_size)));

    int counter = 0;
    std::vector<Value *> source, target;
    Type dummy_type(Type::IntegerTyID);
    std::vector<Value> dummys(func->getArgumentList().size(), Value(&dummy_type, 0));
    for_in(arg, func->getArgumentList()) {
        auto val_size = align4byte(arg->getType()->size(false));
        assert(val_size == 4);
        auto dummy = &dummys.at(counter);
        if (counter >= 4) {
            int offset = this->allocator.stack_size + (filted_registers.size() + counter - 4) * 4;
            this->allocator.register_mapping.erase(dummy);
            this->allocator.stack_mapping[dummy] = offset;
            source.push_back(dummy);
            target.push_back(arg);
        } else {
            this->allocator.register_mapping[dummy] = counter;
            this->allocator.stack_mapping.erase(dummy);
            source.push_back(dummy);
            target.push_back(arg);
        }
        counter++;
    }
    self.locationToLocationGroup(source, target, 0);
    // for_in(glo, module->getGlobalList()) {
    //     self += Inst::LDR_D(OP_REG_1, glo->getName());
    //     regToLocation(glo, OP_REG_1);
    // }
}
void ASMBuilder::buildPostFunction(Function *func) {
    self.label(func->getName() + "_" + func->getName() + "_POST");
}

std::vector<Register> ASMBuilder::mergeRegisters(Function *func) {
    std::set<Register> registers_set;
    // Merge registers used in this Function
    for_in(arg, func->getArgumentList()) {
        if (this->allocator.register_mapping.count(arg) && this->allocator.register_mapping[arg] <= 15) {
            registers_set.insert(Register(this->allocator.register_mapping[arg]));
        }
    }
    for_in(basic_block, func->getBasicBlockList()) {
        for_in(inst, basic_block->getInstList()) {
            if (this->allocator.register_mapping.count(inst) && this->allocator.register_mapping[inst] <= 15) {
                registers_set.insert(Register(this->allocator.register_mapping.at(inst)));
            }
        }
    }
    // Insert temp registers
    registers_set.insert(Reg(R11));
    registers_set.insert(Reg(R12));
    registers_set.insert(Reg(R14));
    return std::vector<Register>(registers_set.begin(), registers_set.end());
}

void ASMBuilder::buildRetFunction(Function *func) {
    self += Inst::ADD(Register::SP, Register::SP, Op2::Imm(this->allocator.stack_size));
    // restore callee-save registers and pc
    std::set<Register> filted_registers_set;
    for (auto &reg : mergeRegisters(func)) {
        if (this->allocator.isCalleeStorable(reg)) { filted_registers_set.insert(reg); }
    }
    auto filted_registers = std::vector<Register>(filted_registers_set.begin(), filted_registers_set.end());

    filted_registers.push_back(Register::PC);
    std::sort(filted_registers.begin(), filted_registers.end());
    self += Inst::POP(filted_registers);

    if (func->getName() == "main") {  // TODO: If needed, check enlarge stack here
        self += Inst::POP({OP_REG_0});
        self += Inst::MOV(Register::SP, Op2::LSL(OP_REG_0, 0));
    }
}

bool ASMBuilder::locationCheck(Value *a, Value *b) {
    if (this->allocator.register_mapping.count(a) && this->allocator.register_mapping.count(b)) {
        return this->allocator.register_mapping.at(a) == this->allocator.register_mapping.at(b);
    }
    if (this->allocator.stack_mapping.count(a) && this->allocator.stack_mapping.count(b)) {
        return this->allocator.stack_mapping.at(a) == this->allocator.stack_mapping.at(b);
    }
    return false;
}


static bool Judge(unsigned int val) {
    for (int i = 0; i < 32; i+=2) {
        if ( CROR(val, i) <= 0x000000ff)
            return true;
    }

    return false;
}

void ASMBuilder::locationToReg(Value *val, Register to_reg, int stack_offset) {
    auto constant_int = dyn_cast<ConstantInt>(val);
    auto global_var = dyn_cast<GlobalVariable>(val);
    if (constant_int) {
        unsigned int tmp = constant_int->getValue();
        self += Inst::LDR_D(to_reg, tmp);
        // std::cerr << "1" << std::endl;
    } else if (global_var) {  // Need Optimization
        // std::cerr << "2" << std::endl;
        int idx = 0;
        for (auto val : self.module->getGlobalList()) {
            if (val != global_var) idx++;
            if (val == global_var) break;
        }
//         movw r0, #:lower16:myvar
// movt r0, #:upper16:myvar
        // self += Inst::ADRL(OP_REG_2, "GLOBAL_VARS+"+std::to_string(idx*4));
        // self += Inst::MOVW()
        self += f8("MOVW") + std::string(Register(to_reg)) + ", #:lower16:" + global_var->getName();
        self += f8("MOVT") + std::string(Register(to_reg)) + ", #:upper16:" + global_var->getName();
        // self += Inst::LDR_safe(to_reg,OP_REG_2,0);

        // self += Inst::LDR_D(OP_REG_2, val->getName() + "+" + std::to_string(self.allocator.global_offset_table.at(global_var) * 4));
        //  self += Inst::LDR(to_reg, Inst::address(OP_REG_2, 0));
    } else if (this->allocator.register_mapping.count(val) &&
               this->allocator.register_mapping.at(val) <= 15) {
        // std::cerr << "3" << std::endl;
        self += Inst::MOV(to_reg, Op2::LSL(this->allocator.register_mapping.at(val), 0));
    } else if (this->allocator.allocated.count(val)) {
        // std::cerr << "4" << std::endl;
        auto offset = this->allocator.stack_mapping.at(val) + stack_offset;
        self += Inst::ADD(to_reg, Register(Register::SP), Op2::Imm(offset));
    } else if (this->allocator.stack_mapping.count(val)) {
        // std::cerr << "5" << std::endl;
        auto offset = this->allocator.stack_mapping.at(val) + stack_offset;
        // If needed, add some code here to impl multi-thread
        self += Inst::LDR_safe(to_reg, Register(Register::SP), offset);
    } else {
        std::cerr << "Move value to Register failed." << std::endl
                  << "Value:" << val->getName()
                  << " Register:" << std::string(to_reg)
                  << " Offset:" << stack_offset
                  << std::endl;
        abort();
    }
}

void ASMBuilder::regToLocation(Value *val, Register from_reg, int stack_offset) {
    if (this->allocator.register_mapping.count(val) &&
        this->allocator.register_mapping.at(val) <= 15) {
        self += Inst::MOV(this->allocator.register_mapping.at(val), Op2::LSL(from_reg, 0));
        return;
    } else if (this->allocator.stack_mapping.count(val)) {
        self += Inst::STR_safe(from_reg, Register::SP, this->allocator.stack_mapping.at(val) + stack_offset);
        return;
    } 
    std::cerr << "Move value to Register failed." << std::endl
              << "Value:" << val->getName()
              << " Register:" << std::string(from_reg)
              << " Offset:" << stack_offset
              << std::endl;
    abort();
}

void ASMBuilder::locationToLocation(Value *from_val, Value *to_val, int stack_offset) {
    std::string asm_code;
    if (this->locationCheck(from_val, to_val)) { return; }

    int op = this->allocator.register_mapping.count(to_val)
                 ? this->allocator.register_mapping.at(to_val)
                 : OP_REG_0;
    self.locationToReg(from_val, op, stack_offset);
    // std::cerr << "LTR" << std::endl;
    self.regToLocation(to_val, op, stack_offset);
    // std::cerr << "RTL" << std::endl;
}

void ASMBuilder::locationToLocationGroup(std::vector<Value *> from_vals, std::vector<Value *> to_vals, int stack_offset) {
    std::list<std::pair<Value *, Value *>> from_to;
    for (int i = 0; i < std::min(4, (int)from_vals.size()); i++) {
        auto from_val = from_vals[i];
        auto to_val = to_vals[i];
        if (this->locationCheck(from_val, to_val)) { return; }
        if (this->allocator.register_mapping.count(from_val))
            self.regToLocation(to_val, this->allocator.register_mapping.at(from_val), stack_offset);
        else
            self.locationToReg(from_val, this->allocator.register_mapping.at(to_val), stack_offset);
    }
    for (int i = 4; i < from_vals.size(); i++) {
        from_to.push_back({from_vals.at(i), to_vals.at(i)});
    }
    // for (auto it1 = from_to.begin(); it1 != from_to.end(); it1++) {
    //     for (auto it2 = from_to.begin(); it2 != from_to.end(); it2++) {
    //         if (it2 != it1 && this->locationCheck(it2->first, it1->first)) {
    //             std::cerr << "Location To Location Group Race Condition"
    //                       << "Location Check Failed: " << it1->first << std::endl;
    //             abort();
    //         }
    //     }
    // }
    Value *tg_val = nullptr;
    while (!from_to.empty()) {
        bool flag = true;
        for (auto it1 = from_to.begin(); it1 != from_to.end(); it1++) {
            bool ok = true;
            for (auto it2 = from_to.begin(); it2 != from_to.end(); it2++) {
                if (it2 != it1 && this->locationCheck(it2->second, it1->first)) {
                    ok = false;
                }
            }
            if (ok) {
                self.locationToLocation(it1->first, it1->second, stack_offset);
                from_to.erase(it1);
                flag = false;
                break;
            }
        }
        if (flag) {
            if (tg_val != nullptr) {
                self.regToLocation(tg_val, OP_REG_0, stack_offset);
            }
            auto it = from_to.begin();
            self.locationToReg(it->first, OP_REG_0, stack_offset);
            tg_val = it->first;
            from_to.erase(it);
        }
    }
    if (tg_val != nullptr) {
        self.locationToReg(tg_val, OP_REG_0, stack_offset);
    }
}

ASMBuilder::operator std::string() const {
    return this->code_string;
}
