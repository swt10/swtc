#include "Type.h"

#include "DerivedTypes.h"

using namespace SWTC;
Type *Type::getElementType() {
    if (this->isPointerTy())
        return static_cast<PointerType *>(this)->getElementType();
    else
        return nullptr;
}

int Type::size(bool origin) {
    if (this->isIntegerTy()) {
        // TODO: 此处使用了subclass_data来替代numbits，需要验证是否合法
        auto bits = static_cast<IntegerType *>(this)->getSubclassData() / 8;
        return bits > 0 ? bits : 1;
    }
    if (this->isArrayTy()) {
        auto element_size =
            static_cast<ArrayType *>(this)->getElementType()->size();
        uint64_t num_elements = static_cast<ArrayType *>(this)->getNumElements();
        return element_size * num_elements;
    }
    if (this->isPointerTy()) {
        if (origin && this->getElementType()->isArrayTy()) {
            return this->getElementType()->size();
        } else {
            return 4;
        }
    }
    return 0;
}

bool Type::operator==(Type rhs) {
    if (this->ID != rhs.ID) {
        return false;
    } else if (this->isPointerTy()) {
        return *this->getElementType() == (*rhs.getElementType());
    } else {
        return true;
    }
}