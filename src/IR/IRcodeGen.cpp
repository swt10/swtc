#include <iostream>
#include <map>
#include <ostream>

#include "Casting.h"
#include "Constant.h"
#include "DerivedTypes.h"
#include "IRheader.h"
#include "MemSSA.h"
#include "Type.h"
#include "Value.h"
#include "config.h"

extern std::map<BasicBlock *, std::list<MemoryAccess *>> BtoA;  //bb to AccessList
extern std::map<Value *, std::list<MemoryAccess *>> VtoM;
extern std::map<Function *, std::set<Value *>> funUse;

namespace SWTC {

class IndexMap {
public:
    Function *F = nullptr;
    std::map<Value *, unsigned int> idxmap;
    unsigned int idx = 0;
    unsigned int alloca1() { return idx++; }
    unsigned int getidx(Value *V) {
        if (idxmap.find(V) == idxmap.end()) { idxmap[V] = idx++; }
        return idxmap[V];
    }
    IndexMap(Function *f) : F(f) {}
    ~IndexMap() = default;
};
IndexMap *v_index;

void printValue(std::ostream &os, Value *v) {
    if (auto x = dyn_cast<ConstantInt>(v)) {
        os << x->getValue();
    } else if (auto x = dyn_cast<GlobalVariable>(v)) {
        os << "%_glob_" << x->getName();
    } else if (auto x = dyn_cast<Argument>(v)) {
        os << "%a" << v_index->getidx(v);
    } else if (isa<UndefValue>(v)) {
        os << "undef";
    } else if (isa<StoreInst>(v)) {
        os << "store" << v_index->getidx(v);
    } else {
        os << "%x" << v_index->getidx(v);
    }
}

void printOpname(std::ostream &os, Instruction::Ops op) {
    switch (op) {
        case Instruction::Ops::Add: os << "add"; break;
        case Instruction::Ops::MLA: os << "mla"; break;
        case Instruction::Ops::Sub: os << "sub"; break;
        case Instruction::Ops::Mul: os << "mul"; break;
        case Instruction::Ops::UDiv: os << "udiv"; break;
        case Instruction::Ops::SDiv: os << "sdiv"; break;
        case Instruction::Ops::URem: os << "urem"; break;
        case Instruction::Ops::SRem: os << "srem"; break;
        case Instruction::Ops::Shl: os << "shl"; break;
        case Instruction::Ops::LShr: os << "lshr"; break;
        case Instruction::Ops::AShr: os << "ashr"; break;
        case Instruction::Ops::And: os << "and"; break;
        case Instruction::Ops::Or: os << "or"; break;
        case Instruction::Ops::Xor: os << "xor"; break;
        case Instruction::Ops::ICmp: os << "icmp"; break;
        default: break;
    }
}
void printIcmp(std::ostream &os, ICmpInst::Predicate pred) {
    switch (pred) {
        case ICmpInst::Predicate::ICMP_EQ: os << "eq"; break;
        case ICmpInst::Predicate::ICMP_NE: os << "ne"; break;
        case ICmpInst::Predicate::ICMP_UGT: os << "sgt"; break;
        case ICmpInst::Predicate::ICMP_UGE: os << "sge"; break;
        case ICmpInst::Predicate::ICMP_ULT: os << "slt"; break;
        case ICmpInst::Predicate::ICMP_ULE: os << "sle"; break;
        case ICmpInst::Predicate::ICMP_SGT: os << "sgt"; break;
        case ICmpInst::Predicate::ICMP_SGE: os << "sge"; break;
        case ICmpInst::Predicate::ICMP_SLT: os << "slt"; break;
        case ICmpInst::Predicate::ICMP_SLE: os << "sle"; break;
        default: break;
    }
}

void printArraySize(std::ostream &os, Type *A) {
    if (A->getTypeID() == Type::TypeID::IntegerTyID) {
        os << "i32 ";
    } else if (auto ty = dyn_cast<ArrayType>(A)) {
        int n = 0;
        os << "[" << ty->getNumElements() << " x ";
        while (ty->getElementType()->getTypeID() != Type::TypeID::IntegerTyID) {
            ty = dyn_cast<ArrayType>(ty->getElementType());
            os << "[" << ty->getNumElements() << " x ";
            n++;
        }
        os << "i32";
        for (int i = 0; i < n; ++i) {
            os << "]";
        }
        os << "] ";
    } else {
        if (A->getTypeID() == Type::TypeID::PointerTyID) {
            os << "pointer";
        } else {
            TEST_TYPE(A)
        }
    }
}

void printAllArray(std::ostream &os, ConstantArray *C) {
    int n = 0;
    os << "[" << C->getElementSize() << " x ";
    auto D = C;
    while (!isa<ConstantInt>(D->getElementValue(0))) {
        D = dyn_cast<ConstantArray>(D->getElementValue(0));
        os << "[" << D->getElementSize() << " x ";
        n++;
    }
    os << "i32";
    for (int i = 0; i < n; ++i) {
        os << "]";
    }
    os << "] [";
    for (int i = 0; i < C->getElementSize(); ++i) {
        if (auto ty = dyn_cast<ConstantInt>(C->getElementValue(i))) {
            os << "i32 " << ty->getValue();
        } else {
            printAllArray(os, dyn_cast<ConstantArray>(C->getElementValue(i)));
        }
        if (i + 1 < C->getElementSize()) {
            os << ", ";
        }
    }
    os << "]";
}

std::ostream &operator<<(std::ostream &os, const Module *M) {
    using std::endl;
    std::cerr << "\033[34m";
    for (auto d = M->getGlobalList().head; d; d = d->next) {
        if (isRead) {
            break;
        }
        os << "@_" << d->getName() << " = global ";
        auto ty = d->getValueType();
        if (d->hasInitializer()) {
            if (d->getValueType()->getTypeID() == Type::TypeID::IntegerTyID) {
                os << "i32 " << dyn_cast<ConstantInt>(d->getInitializer())->getValue();
            } else {
                printAllArray(os, dyn_cast<ConstantArray>(dyn_cast<ConstantArray>(d->getInitializer())->getElementValue(0)));
            }
        } else {
            printArraySize(os, ty);
            os << "zeroinitializer" << endl;
        }
        os << endl;
    }

    for (auto F = M->getFunctionList().head; F; F = F->next) {
        v_index = new IndexMap(F);
        if (!F->isBuildin()) {
            //const char *decl =
            // os << "--ir func\n" ;
            std::cerr << (F->getFunctionType()->getNumParams()) << "\n";
            const char *ret = F->getReturnType()->isVoidTy() ? "void" : "i32";
            os << "define"
               << " " << ret << " @";
            os << F->getName() << "(";
            if (!F->isVarArg()) {
                for (auto p = F->getArgumentList().head; p; p = p->next) {
                    if (p->getType()->getTypeID() == Type::TypeID::IntegerTyID) {
                        os << "i32 ";
                    } else {
                        printArraySize(os, dyn_cast<PointerType>(p->getType())->getElementType());
                        os << "* ";
                    }
                    printValue(os, p);
                    if (p != F->getArgumentList().tail) {
                        os << ", ";
                    }
                }
            }
            os << ") {" << endl;

            if (!funUse.empty()) {
                for (auto x : funUse[F]) {
                    os << "; " << x->getName() << " = MemDef (" << VtoM[x].front()->getID() << ")"
                       << "\n";
                }
            }

            os << "_entry:" << endl;

            for (auto d = M->getGlobalList().head; d; d = d->next) {
                os << "\t%_glob_" << d->getName() << " = getelementptr inbounds ";
                if (d->getValueType()->getTypeID() == Type::TypeID::IntegerTyID) {
                    os << "i32, i32";
                } else {
                    printArraySize(os, d->getValueType());
                    os << ", ";
                    printArraySize(os, d->getValueType());
                }
                os << "* @_" << d->getName() << ", i32 0" << endl;
            }
            os << "\tbr label %_0" << endl;

            std::map<BasicBlock *, unsigned int> BB_index;
            for (auto BB = F->getEntryBlock(); BB; BB = BB->next) {
                unsigned int idx = BB_index.size();
                BB_index.insert({BB, idx});
            }

            // os<< "------------- ouput entryBlock in bb \n";
            int abcdefghijklmn = 0;

            for (auto BB = F->getEntryBlock(); BB; BB = BB->next) {
                unsigned int index = BB_index.find(BB)->second;

                // debug begin
                if (isPrintIRBB) {
                    os << "\n; _" << index << ":(" << BB->getName() << ")\n";
                }
                // debug end
                if (BB->getInstList().head == nullptr) {
                    goto DEBUG_PRINT_PRED_SUCC;
                }

                os << "_" << index << ": ; preds = ";
                BB->pred_init();
                for (unsigned int i = 0; i < BB->pred.size(); ++i) {
                    if (i != 0) {
                        os << ", ";
                    }
                    os << "%_" << BB_index.find(BB->pred[i])->second;
                }
                os << endl;
            // debug begin
            DEBUG_PRINT_PRED_SUCC:
                if (isPrintPredSucc) {
                    BB->pred_init();
                    os << "\t; BB preds: ";
                    for (auto x : BB->pred) {
                        os << "(" << x->getName() << ") ";
                    }
                    BB->succ_init();
                    os << "\n\t; BB succs: ";
                    for (auto x : BB->succ) {
                        os << "(" << x->getName() << ") ";
                    }
                    os << "\n";
                }
                auto ac = BtoA.find(BB);

                if (ac != BtoA.end()) {
                    for (auto phi : ac->second) {
                        auto pn = dyn_cast<MemoryPhi>(phi);
                        if (pn) {
                            os << "; " << pn->oriptr->getName() << " " << pn->getID() << " = MemPhi : ";
                            for (int i = 0; i < pn->getNumOperands(); i += 2) {
                                auto def = dyn_cast<MemoryDef>(pn->getOperand(i));
                                auto phi = dyn_cast<MemoryPhi>(pn->getOperand(i));
                                auto mbb = dyn_cast<BasicBlock>(pn->getOperand(i + 1));
                                os << "[MemDef " << (def?(def->getID()):(phi->getID())) << " , %_" << BB_index.find(mbb)->second << "] ";
                            }
                            os << "\n";
                        }
                    }
                }
                if (BB->getInstList().head == nullptr) continue;
                // debug end
                ////phi  +  dom info

                // os<< "------------- ouput lists in bb \n";

                for (auto inst = BB->getInstList().head; inst; inst = inst->next) {
                    // begin PrintIRInst
                    if (isPrintIRInst) {
                        os << "; inst" << abcdefghijklmn++ << " : " << inst->getName() << '\n';
                    }
                    // end PrintIRInst
                    auto M = VtoM.find(inst);
                    if (M != VtoM.end()) {
                        for (auto minst : M->second) {
                            if (auto x = dyn_cast<MemoryUse>(minst)) {
                                if (isa<MemoryPhi>(x->getDefiningAccess())) {
                                    auto def = dyn_cast<MemoryPhi>(x->getDefiningAccess());
                                    os << "; MemUse (" << def->getID() << ")\t";
                                }
                                if (isa<MemoryDef>(x->getDefiningAccess())) {
                                    auto def = dyn_cast<MemoryDef>(x->getDefiningAccess());
                                    os << "; MemUse (" << def->getID() << ")\t";
                                }
                            }
                            if (auto x = dyn_cast<MemoryDef>(minst)) {
                                if (isa<MemoryPhi>(x->getDefiningAccess())) {
                                    auto def = dyn_cast<MemoryPhi>(x->getDefiningAccess());
                                    os << "; " << minst->getID() << " = MemDef (" << def->getID() << ")\t";
                                }
                                if (isa<MemoryDef>(x->getDefiningAccess())) {
                                    auto def = dyn_cast<MemoryDef>(x->getDefiningAccess());
                                    os << "; " << minst->getID() << " = MemDef (" << def->getID() << ")\t";
                                }
                            }
                        }
                        os << "\n";
                    }

                    os << "\t";
                    if (auto x = dyn_cast<AllocaInst>(inst)) {
                        //unsigned int temp = v_index.alloca1();
                        printValue(os, inst);
                        os << " = alloca ";
                        if (x->getAllocatedType()->getTypeID() == Type::TypeID::IntegerTyID) {
                            os << "i32";
                        } else {
                            printArraySize(os, x->getAllocatedType());
                        }
                        os << ", align 4" << endl;
                    } else if (auto x = dyn_cast<GetElementPtrInst>(inst)) {  //
                        os << "; getelementptr " << v_index->getidx(inst) << endl
                           << "\t";
                        printValue(os, inst);
                        os << " = getelementptr inbounds ";
                        if (dyn_cast<PointerType>(x->getOperand(0)->getType())->getElementType()->getTypeID() == Type::TypeID::IntegerTyID) {
                            os << "i32 , i32 ";
                        } else {
                            printArraySize(os, dyn_cast<PointerType>(x->getOperand(0)->getType())->getElementType());
                            os << ", ";
                            printArraySize(os, dyn_cast<PointerType>(x->getOperand(0)->getType())->getElementType());
                        }
                        os << "* ";
                        printValue(os, x->getOperand(0));
                        for (int i = 1; i < x->getNumOperands(); ++i) {
                            os << ", i32 ";
                            printValue(os, x->getOperand(i));
                        }
                        os << endl;
                    } else if (auto x = dyn_cast<StoreInst>(inst)) {
                        os << "\t\t; store " << v_index->getidx(x) << endl
                           << "\t";
                        os << "store i32 ";
                        printValue(os, x->getOperand(0));
                        os << ", i32* ";
                        printValue(os, x->getOperand(1));
                        os << ",  align 4" << endl;
                    } else if (auto x = dyn_cast<LoadInst>(inst)) {
                        printValue(os, inst);
                        os << " = load i32, i32* ";
                        printValue(os, x->getPointerOperand());
                        os << ", align 4" << endl;
                    } else if (auto x = dyn_cast<ICmpInst>(inst)) {
                        unsigned int temp = v_index->alloca1();
                        os << "%t" << temp << " = ";
                        printOpname(os, x->getOpcode());
                        os << " ";
                        printIcmp(os, x->getPredicate());
                        os << " i32 ";
                        printValue(os, x->getOperand(0));
                        os << ", ";
                        printValue(os, x->getOperand(1));
                        os << endl
                           << "\t";
                        printValue(os, inst);
                        os << " = zext i1 %t" << temp << " to i32" << endl;
                    } else if (auto x = dyn_cast<BinaryOperator>(inst)) {
                        printValue(os, inst);
                        os << " = ";
                        printOpname(os, x->getOpcode());
                        os << " i32 ";
                        printValue(os, x->getOperand(0));
                        os << ", ";
                        printValue(os, x->getOperand(1));
                        os << endl;
                    } else if (auto x = dyn_cast<MLAInst>(inst)) {
                        printValue(os, inst);
                        os << " = ";
                        printOpname(os, x->getOpcode());
                        os << " i32 ";
                        printValue(os, x->getOperand(0));
                        os << ", ";
                        printValue(os, x->getOperand(1));
                        os << ", ";
                        printValue(os, x->getOperand(2));
                        os << endl;
                    } else if (auto x = dyn_cast<BranchInst>(inst)) {
                        if (x->getOperand(1) && x->getOperand(2)) {
                            // add comment
                            os << "; if ";
                            printValue(os, x->getOperand(2));
                            os << " then _" << BB_index.find(dyn_cast<BasicBlock>(x->getOperand(0)))->second << " else _" << BB_index.find(dyn_cast<BasicBlock>(x->getOperand(1)))->second << endl;
                            unsigned int temp = v_index->alloca1();
                            os << "\t%t" << temp << " = icmp ne i32 ";
                            printValue(os, x->getOperand(2));
                            os << ", 0" << endl;
                            os << "\tbr i1 %t" << temp << ", label %_" << BB_index.find(dyn_cast<BasicBlock>(x->getOperand(0)))->second << ", label %_" << BB_index.find(dyn_cast<BasicBlock>(x->getOperand(1)))->second << endl;
                        } else {
                            unsigned int temp = v_index->alloca1();
                            os << "\tbr label %_" << BB_index.find(dyn_cast<BasicBlock>(x->getOperand(0)))->second << endl;
                        }
                    } else if (auto x = dyn_cast<ReturnInst>(inst)) {
                        if (x->getOperand(0)) {
                            os << "ret i32 ";
                            printValue(os, x->getOperand(0));
                            os << endl;
                        } else {
                            os << "ret void" << endl;
                        }
                    } else if (auto x = dyn_cast<CallInst>(inst)) {
                        Function *callee = x->getCalledFunction();
                        if (callee->getReturnType()->isIntegerTy()) {
                            printValue(os, inst);
                            os << " = call i32";
                        } else {
                            os << "call void";
                        }
                        os << " @" << callee->getName() << "(";
                        if (!callee->isVarArg()) {
                            auto y = dyn_cast<User>(x);
                            auto z = callee->getFunctionType();
                            for (unsigned int i = 1; i < y->getNumOperands(); ++i) {
                                if (z->getParamType(i - 1)->getTypeID() == Type::TypeID::IntegerTyID) {
                                    // simple
                                    os << "i32 ";
                                } else {
                                    // array param
                                    printArraySize(os, dyn_cast<PointerType>(z->getParamType(i - 1))->getElementType());
                                    os << "* ";
                                }
                                // arg
                                printValue(os, y->getOperand(i));
                                if (i + 1 < y->getNumOperands()) {
                                    // not last element
                                    os << ", ";
                                }
                            }
                        }
                        os << ")" << endl;
                    } else if (auto x = dyn_cast<PHINode>(inst)) {
                        printValue(os, inst);
                        os << " = phi i32 ";
                        for (unsigned int i = 0; i < x->getNumIncomingValues(); ++i) {
                            if (i != 0) os << ", ";
                            os << "[";
                            printValue(os, x->getIncomingValue(i));
                            os << ", %_"
                               << BB_index.find(x->getIncomingBlock(i))->second << "]";
                        }
                        os << endl;
                    } else if (auto x = dyn_cast<MoveInst>(inst)) {
                        printValue(os, x->getOperand(0));
                        os << " = ";
                        printValue(os, x->getOperand(1));
                        os << " ;\n";
                    }
                }

                // os<< "------------- end ouput lists in bb \n";
            }
        } else {
            const char *ret = F->getReturnType()->isVoidTy() ? "void" : "i32";
            os << "declare"
               << " " << ret << " @";
            os << F->getName() << "(";
            if (!F->isVarArg()) {
                for (auto p = F->getArgumentList().head; p; p = p->next) {
                    if (p->getType()->getTypeID() == Type::TypeID::IntegerTyID) {
                        os << "i32 ";
                    } else {
                        printArraySize(os, dyn_cast<PointerType>(p->getType())->getElementType());
                        os << "* ";
                    }
                    printValue(os, p);
                    if (p != F->getArgumentList().tail) {
                        os << ", ";
                    }
                }
            }
            os << ")" << endl;
            continue;
        }
        // os<< "------------- end ouput entryBlock in bb \n";
        os << "}\n\n";
    }
    std::cerr << "\033[0m";
    return os;
}

}  // namespace SWTC
