#include "IRheader.h"

using namespace SWTC;

IntegerType *Type::Int1Ty = new IntegerType(1);
IntegerType *Type::Int32Ty = new IntegerType(32);
PointerType *Type::Int32PtrTy = new PointerType(Type::Int32Ty);

Type *Type::LabelTy = new Type(Type::LabelTyID);
Type *Type::VoidTy = new Type(Type::VoidTyID);

ConstantInt *ConstantInt::TheTrueVal = nullptr;
ConstantInt *ConstantInt::TheFalseVal = nullptr;
ConstantInt *ConstantInt::TheZeroVal = nullptr;

std::map<i32, ConstantInt *> ConstantInt::Map;