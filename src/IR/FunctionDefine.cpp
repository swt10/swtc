#include <string>

#include "IRheader.h"
#include "Instruction.h"
#include "Value.h"

using namespace SWTC;

namespace SWTC {

void Argument::setParent(Function *parent) {
    Parent = parent;
}

void Function::removeFromParent() {
    getParent()->getFunctionList().remove(this);
};

Function::Function(FunctionType *Ty, const std::string &N, Module *M, bool isBuildin)
    : GlobalValue(Ty, Value::FunctionVal, nullptr, 0, N), isBuiltin(isBuildin) {
    for (unsigned i = 0, e = Ty->getNumParams(); i != e; ++i) {
        ArgumentList.insertAtEnd(new Argument(Ty->getParamType(i)));
    }
    if (M)
        M->getFunctionList().insertAtEnd(this);
    setParent(M);
}

const Module *Instruction::getModule() const {
    return getParent()->getModule();
}

Function *Instruction::getFunction() const {
    return getParent()->getParent();
}

void Instruction::removeFromParent() {
    getParent()->getInstList().remove(this);
}

Instruction::Instruction(Type *Ty, unsigned iType, Use *Ops, unsigned NumOps,
                         Instruction *InsertBefore)
    : User(Ty, Value::InstructionVal + iType, Ops, NumOps), Parent(nullptr) {
    // std::cerr<<"insertatend\n";
    if (InsertBefore) {
        InsertBefore->getParent()->getInstList().insertBefore(this, InsertBefore);
        setParent(InsertBefore->getParent());
    }
}

Instruction::Instruction(Type *Ty, unsigned iType, Use *Ops, unsigned NumOps,
                         BasicBlock *InsertAtEnd)
    : User(Ty, Value::InstructionVal + iType, Ops, NumOps), Parent(InsertAtEnd) {
    if (InsertAtEnd) InsertAtEnd->getInstList().insertAtEnd(this);
    setParent(InsertAtEnd);
}

const Module *BasicBlock::getModule() const {
    return getParent()->getParent();
}

void BasicBlock::insertInto(Function *Parent, BasicBlock *InsertBefore) {
    if (InsertBefore) {
        Parent->getBasicBlockList().insertBefore(this, InsertBefore);
    } else {
        Parent->getBasicBlockList().insertAtEnd(this);
    }
    setParent(Parent);
}

void CmpInst::swapOperands() {
    if (ICmpInst *IC = dyn_cast<ICmpInst>(this))
        IC->swapOperands();
}

GlobalVariable::GlobalVariable(Type *Ty, bool isConstant, Constant *InitVal, const std::string &Name,
                               GlobalVariable *InsertBefore)
    : GlobalValue(Ty, Value::GlobalVariableVal,
                  &Initializer, InitVal != 0, Name),
      isConstantGlobal(isConstant) {
    if (InitVal) {
        Initializer.init(InitVal, this);
    } else {
        Initializer.init(0, this);
    }
    if (InsertBefore)
        InsertBefore->getParent()->getGlobalList().insertBefore(this, InsertBefore);
}
GlobalVariable::GlobalVariable(Type *Ty, bool isConstant, Constant *InitVal, const std::string &Name,
                               Module *Parent)
    : GlobalValue(Ty, Value::GlobalVariableVal, &Initializer, InitVal != 0, Name),
      isConstantGlobal(isConstant) {
    if (InitVal) {
        Initializer.init(InitVal, this);
    } else {
        Initializer.init(0, this);
    }
    if (Parent) {
        Parent->getGlobalList().insertAtEnd(this);
        setParent(Parent);
    }
}
void GlobalVariable::removeFromParent() {
    getParent()->getGlobalList().remove(this);
}
void GlobalVariable::eraseFromParent() {
    getParent()->getGlobalList().remove(this);
    dropAllReferences();
}

void Use::init(Value *v, User *user) {
    Val = v;
    Parent = user;
    if (Val)
        Val->addUse(this);
}

Use::~Use() {
    if (Val)
        Val->killUse(this);
}
void Use::set(Value *V) {
    if (Val)
        Val->killUse(this);
    Val = V;
    if (V)
        V->addUse(this);
}

uint64_t Type::getArrayNumElements() const {
    return dyn_cast<ArrayType>(this)->getNumElements();
}

void BasicBlock::dropAllReferences() {
    for (auto it = InstList.head; it; it = it->next) {
        it->dropAllReferences();
    }
}

void BasicBlock::eraseFromParent() {
    getParent()->getBasicBlockList().remove(this);
    dropAllReferences();
    this->setParent(nullptr);
}
void Instruction::eraseFromParent() {
    getParent()->getInstList().remove(this);
    this->dropAllReferences();
    this->setParent(nullptr);
}
bool Instruction::willreturn() const {
    if (auto c = dyn_cast<CallInst>(this)) {
        return c->getCalledFunction()->getReturnType() == Module::getInt32Ty();
    }
    return true;
}

void BasicBlock::pred_init() {
    pred.clear();
    std::set<BasicBlock *> s;
    for (auto use = use_begin(); use; use = use->next) {
        if (!isa<BranchInst>(use->getUser())) continue;
        BasicBlock *tmp = dyn_cast<Instruction>(use->getUser())->getParent();
        if (s.count(tmp)) continue;
        s.insert(tmp);
        pred.push_back(tmp);
    }

    s.clear();
}
void BasicBlock::succ_init() {
    succ.clear();
    std::set<BasicBlock *> s;
    BranchInst *ti = dyn_cast<BranchInst>(this->getTerminator());
    if (isa<ReturnInst>(getTerminator())) return;
    for (int i = 0; i < ti->getNumSuccessors(); i++)
        if (!s.count(ti->getSuccessor(i))) {
            succ.push_back(ti->getSuccessor(i));
            s.insert(ti->getSuccessor(i));
        }
    s.clear();
}
StoreInst *StoreInst::clone() const { return new StoreInst(*this); }
ReturnInst *ReturnInst::clone() const { return new ReturnInst(*this); }
CmpInst *CmpInst::clone() const {
    return create(getOpcode(), getPredicate(), Ops[0], Ops[1]);
}

CmpInst *CmpInst::create(Instruction::Ops Op, unsigned short predicate, Value *S1, Value *S2,
                         const std::string &Name, Instruction *InsertBefore) {
    return new ICmpInst(ICmpInst::Predicate(predicate), S1, S2, Name,
                        InsertBefore);
}
CmpInst *CmpInst::create(Instruction::Ops Op, unsigned short predicate, Value *S1, Value *S2,
                         const std::string &Name, BasicBlock *InsertAtEnd) {
    return new ICmpInst(ICmpInst::Predicate(predicate), S1, S2, Name,
                        InsertAtEnd);
}

bool Instruction::comesBefore(Instruction *other) const {
    Parent->renumberInstructions();
    return Order < other->Order;
}
void BasicBlock::removePredecessor(BasicBlock *Pred, bool KeepOneInputPHIs) {
    if (InstList.size() == 0 || !isa<PHINode>(getInstList().head))
        return;
    int numpred = dyn_cast<PHINode>(getInstList().head)->getNumIncomingValues();
    for( auto inst : getInstList().copy())
    {
        if(auto pn=dyn_cast<PHINode>(inst)){
            pn->removeIncomingValue(Pred,!KeepOneInputPHIs);
            if(KeepOneInputPHIs)continue;
            if(numpred==1)continue;
            if (Value *phiconst = pn->hasConstantValue()) {
                pn->replaceAllUsesWith(phiconst);
                pn->eraseFromParent();
            }
        }
    }
}
Value *PHINode::removeIncomingValue(unsigned Idx, bool DeletePHIIfEmpty) {
    unsigned NumOps = getNumOperands();
    Use *OL = OperandList;
    Value *Removed = OL[Idx * 2];

    for (unsigned i = (Idx + 1) * 2; i != NumOps; i += 2) {
        OL[i - 2] = OL[i];
        OL[i - 2 + 1] = OL[i + 1];
    }

    // Nuke the last value.
    OL[NumOps - 2].set(0);
    OL[NumOps - 2 + 1].set(0);
    NumOperands = NumOps - 2;

    if (NumOps == 2 && DeletePHIIfEmpty) {
        replaceAllUsesWith(UndefValue::get(getType()));
        eraseFromParent();
    }
    return Removed;
}

Value *PHINode::hasConstantValue() const {
    Value *ConstantValue = getIncomingValue(0);
    for (unsigned i = 1, e = getNumIncomingValues(); i != e; ++i)
        if (getIncomingValue(i) != ConstantValue && getIncomingValue(i) != this) {
            if (ConstantValue != this)
                return nullptr;
            ConstantValue = getIncomingValue(i);
        }
    if (ConstantValue == this)
        return UndefValue::get(getType());
    return ConstantValue;
}

bool Instruction::mayWriteToMemory() {
    switch (getOpcode()) {
        default:
            return false;
        case Instruction::Store:
            return true;
        case Instruction::Call:
            return dyn_cast<CallInst>(this)->getCalledFunction()->writeMemory;
    }
};

/// Return true if this instruction may read memory.
bool Instruction::mayReadFromMemory() {
    switch (getOpcode()) {
        default:
            return false;
        case Instruction::Load:
            return true;
        case Instruction::Call:
            return dyn_cast<CallInst>(this)->getCalledFunction()->readMemory;
    }
};
std::string Instruction::getOpcodeName(unsigned OpCode) {
    switch (OpCode) {
        case Instruction::Ops::Add: return "Add";
        case Instruction::Ops::Alloca: return "Alloca";
        case Instruction::Ops::And: return "And";
        case Instruction::Ops::AShr: return "AShr";
        case Instruction::Ops::Br: return "Br";
        case Instruction::Ops::Call: return "Call";
        case Instruction::Ops::GetElementPtr: return "GetElementPtr";
        case Instruction::Ops::ICmp: return "ICmp";
        case Instruction::Ops::Load: return "Load";
        case Instruction::Ops::LShr: return "LShr";
        case Instruction::Ops::Mul: return "Mul";
        case Instruction::Ops::Move: return "Move";
        case Instruction::Ops::Or: return "Or";
        case Instruction::Ops::PHI: return "PHI";
        case Instruction::Ops::Ret: return "Ret";
        case Instruction::Ops::SDiv: return "SDiv";
        case Instruction::Ops::Shl: return "Shl";
        case Instruction::Ops::SRem: return "SRem";
        case Instruction::Ops::Store: return "Store";
        case Instruction::Ops::Sub: return "Sub";
        case Instruction::Ops::UDiv: return "UDiv";
        case Instruction::Ops::URem: return "URem";
        case Instruction::Ops::Xor: return "Xor";
        case Instruction::Ops::ZExt: return "ZExt";
        default: return "Uncaught Opcode " + std::to_string(OpCode);
    }
}
};  // namespace SWTC

bool Instruction::isIdenticalto(Instruction *I) {
    if (getOpcode() != I->getOpcode() || getNumOperands() != I->getNumOperands() || getType() != I->getType())
        return false;
    if (getNumOperands() == 0) return true;
    for (int i = 0; i < getNumOperands(); i++)
        if (getOperand(i) != I->getOperand(i)) return false;
    return true;
}
const BasicBlock *BasicBlock::getSingleSuccessor() const {
    if (succ.empty()) return nullptr;
    if (succ.size() == 1) return succ[0];
    return nullptr;
}
bool Instruction::isSameOperation(Instruction *I) {
    if (getOpcode() != I->getOpcode() || getNumOperands() != I->getNumOperands() || getType() != I->getType())
        return false;
    for (int i = 0; i < getNumOperands(); i++)
        if (getOperand(i)->getType() != I->getOperand(i)->getType()) return false;
    return true;
}
void Instruction::moveBefore(Instruction *MovePos) {
    removeFromParent();
    setParent(MovePos->getParent());
    MovePos->getParent()->getInstList().insertBefore(this, MovePos);
}
