#include "AST.h"

void AST::ArraySub::add(AST::ASTBasic *para) {
    subscripts.emplace_back((AST::Exp *)para);
}

void AST::CompUnit::add(AST::ASTBasic *para) {
    contents.emplace_back(para);
}

void AST::Defs::add(AST::ASTBasic *para) {
    defs.emplace_back((AST::Def *)para);
}

void AST::InitVal::add(AST::ASTBasic *para) {
    initVals.emplace_back((AST::InitVal *)para);
}

void AST::Block::add(AST::ASTBasic *para) {
    blockItems.emplace_back(para);
}

void AST::FuncFParams::add(AST::ASTBasic *para) {
    fParams.emplace_back((AST::FuncFParam *)para);
}

void AST::FuncRParams::add(AST::ASTBasic *para) {
    rParams.emplace_back((AST::Exp *)para);
}