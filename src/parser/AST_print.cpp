#include "AST.h"
#include "config.h"

void printStr(const char *str) {
    ast_file << '\"' << str << '\"';
}

void printNum(int x){
    ast_file << x ;
}

void printKey(const char *str) {
    printStr(str);
    ast_file << ": ";
}

void printComma() {
    ast_file << ",\n";
}

void printKeyVal(const char *str1, const char *str2, bool _printComma = true) {
    printKey(str1);
    printStr(str2);
    if (!_printComma) {
        return;
    }
    printComma();
}

void printKeyVal(const char *str1, int num, bool _printComma = true) {
    printKey(str1);
    ast_file << num;
    if (!_printComma) {
        return;
    }
    printComma();
}

void printKeyVal(const char *str1, bool bl, bool _printComma = true) {
    printKey(str1);
    ast_file << bl;
    if (!_printComma) {
        return;
    }
    printComma();
}

void printBrace1() {
    ast_file << "{\n";
}

void printBrace2() {
    ast_file << "}\n";
}

void printBracket1() {
    ast_file << "[\n";
}

void printBracket2() {
    ast_file << "]\n";
}

const std::string op_table[20] = {
    "+",
    "-",
    "*",
    "/",
    "%",

    "&&",
    "||",
    "!",

    "==",
    "!=",
    "<",
    ">",
    "<=",
    ">=",

    "empty",
};

std::string op2string(AST::OP op) {
    return op_table[op];
}

void AST::ArraySub::print() {
    printBracket1();
    for (auto iter = subscripts.begin(); iter != subscripts.end(); iter++) {
    AST:
        Exp *tmp = *iter;
        tmp->print();
        if (iter + 1 == subscripts.end()) {
            break;
        }
        printComma();
    }
    printBracket2();
}

void AST::PrintNothing::print() {
    printStr("print nothing");
}

void AST::CompUnit::print() {
    printBrace1();
    printKeyVal("node type", "CompUnits");

    printKey("Units");
    printBracket1();

    for (auto iter = contents.begin(); iter != contents.end(); iter++) {
        (*iter)->print();
        if (iter + 1 == contents.end()) {
            break;
        }
        printComma();
    }

    printBracket2();

    printBrace2();
}

void AST::Decl::print() {
    printBrace1();

    printKeyVal("type", "Decl");

    printKeyVal("isConst", isConst);

    printKey("Defs");
    defs->print();

    printBrace2();
}

void AST::Defs::print() {
    printBracket1();

    for (auto iter = defs.begin(); iter != defs.end(); iter++) {
        (*iter)->print();
        if (iter + 1 == defs.end()) {
            break;
        }
        printComma();
    }

    printBracket2();
}

void AST::Def::print() {
    printBrace1();
    printKeyVal("type", "Def");
    printKeyVal("isInit", isInit);

    printKeyVal("ident", ident->c_str());

    printKey("subscripts");
    if (subs->isPrintNothing) {
        printStr("none");
    } else {
        subs->print();
    }
    printComma();

    printKey("InitVal");
    if (initVal->isPrintNothing) {
        printStr("none");
    } else {
        printBracket1();
        initVal->print();
        printBracket2();
    }

    printBrace2();
}

void AST::InitVal::print() {
    // for (auto iter = initVals.begin(); iter != initVals.end(); iter++) {
    //     AST::Exp *tmp = *iter;
    //     tmp->print();
    //     if (iter + 1 == initVals.end()) {
    //         break;
    //     }
    //     printComma();
    // }

    if(isVar()){
        printStr("val");
        printComma();
        selfInitVal->print();
        // printComma();
    }
    else{
        if(initVals.empty()){
            printNum(0);
            return;
        }
        printBracket1();
        for (auto iter = initVals.begin(); iter != initVals.end(); iter++) {
            AST::InitVal *tmp = *iter;
            tmp->print();
            if (iter + 1 == initVals.end()) {
                break;
            }
            printComma();
        }
        printBracket2();
    }
}

void AST::FuncDef::print() {
    printBrace1();

    printKeyVal("type", "FuncDef");

    std::string tmpPrintType;
    switch (funcType) {
        case AST::FuncType::typeInt:
            tmpPrintType = "int";
            break;
        case AST::FuncType::typeVoid:
            tmpPrintType = "void";
        default:
            break;
    }
    printKeyVal("funcType", tmpPrintType.c_str());

    printKeyVal("funcIdent", ident->c_str());

    printKey("funcFParams");
    funcFParams->print();
    printComma();

    printKey("funcBody");
    if (funcBody->isPrintNothing) {
        printStr("empty func body");
    } else {
        funcBody->print();
    }

    printBrace2();
}

void AST::Block::print() {
    printBrace1();

    printKeyVal("type", "block");

    printKey("blockItems");
    printBracket1();
    for (auto iter = blockItems.begin(); iter != blockItems.end(); iter++) {
        (*iter)->print();
        if (iter + 1 == blockItems.end()) {
            break;
        }
        printComma();
    }

    printBracket2();

    printBrace2();
}

void AST::Stmt_If::print() {
    printBrace1();

    if (stmt_2->isPrintNothing) {
        printKeyVal("type", "stmt if");
    } else {
        printKeyVal("type", "stmt if else");
    }

    printKey("cond");
    cond->print();
    printComma();

    printKey("stmt_1");
    stmt_1->print();
    printComma();

    printKey("stmt_2");
    if (stmt_2->isPrintNothing) {
        printStr("none else");
    } else {
        stmt_2->print();
    }

    printBrace2();
}

void AST::Stmt_While::print() {
    printBrace1();

    printKeyVal("type", "stmt_while");

    printKey("cond");
    cond->print();
    printComma();

    printKey("stmt_1");
    stmt_1->print();

    printBrace2();
}

void AST::Stmt_Ass::print() {
    printBrace1();

    printKeyVal("type", "stmt_ass");

    printKey("lval");
    lval->print();
    printComma();

    printKey("rval");
    rval->print();

    printBrace2();
}

void AST::Stmt_Break::print() {
    printBrace1();

    printKeyVal("type", "stmt_break", false);

    printBrace2();
}

void AST::Stmt_Continue::print() {
    printBrace1();

    printKeyVal("type", "stmt_continue", false);

    printBrace2();
}

void AST::Stmt_Return::print() {
    printBrace1();

    printKeyVal("type", "stmt_return");

    printKey("return value");
    if (retVal->isPrintNothing) {
        printStr("void retVal");
    } else {
        retVal->print();
    }

    printBrace2();
}

void AST::Stmt_Block::print() {
    printBrace1();

    printKeyVal("type", "stmt_block");

    printKey("block");
    if (block->isPrintNothing) {
        printStr("empty block");
    } else {
        block->print();
    }

    printBrace2();
}

void AST::Stmt_Exp::print() {
    printBrace1();

    printKeyVal("type", "stmt_exp");

    printKey("exp");
    if (exp->isPrintNothing) {
        printStr("none");
    } else {
        exp->print();
    }

    printBrace2();
}

void AST::LVal::print() {
    printBrace1();

    printKeyVal("type", "lVal");
    printKeyVal("ident", ident->c_str());

    printKey("subscripts");
    if (subs->isPrintNothing) {
        printStr("none");
    } else {
        subs->print();
    }

    printBrace2();
}

void AST::Exp_Func::print() {
    printBrace1();

    printKeyVal("type", "callOfFunc");

    printKeyVal("ident", ident->c_str());

    printKey("funcRParams");
    if (funcRParams->isPrintNothing) {
        printStr("none real parameters");
    } else {
        funcRParams->print();
    }

    printBrace2();
}

void AST::Exp_Num::print() {
    printBrace1();

    printKeyVal("type", "number");
    printKeyVal("num", val, false);

    printBrace2();
}

void AST::Exp_Paren::print() {
    printBrace1();

    printKeyVal("type", "parenthesesExp");

    printKey("expInParen");
    expParen->print();

    printBrace2();
}

void AST::Exp_LVal::print() {
    printBrace1();

    printKeyVal("type", "lValExp");

    printKey("expInParen");
    lval->print();

    printBrace2();
}

void AST::Exp_Unary::print() {
    printBrace1();

    printKeyVal("type", "unaryExp");

    printKeyVal("op", op2string(op).c_str());

    printKey("exp");
    exp->print();

    printBrace2();
}

void AST::Exp_Binary::print() {
    printBrace1();

    printKeyVal("type", "binaryExp");

    printKeyVal("op", op2string(op).c_str());

    printKey("exp1");
    expl->print();
    printComma();

    printKey("exp2");
    if (expr->isPrintNothing) {
        printStr("none");
    } else {
        expr->print();
    }

    printBrace2();
}

void AST::FuncFParams::print() {
    printBrace1();

    printKeyVal("type", "funcFParams");

    printKey("fParams");
    printBracket1();

    for (auto iter = fParams.begin(); iter != fParams.end(); iter++) {
        AST::FuncFParam *tmp = *iter;
        tmp->print();
        if (iter + 1 == fParams.end()) {
            break;
        }
        printComma();
    }

    printBracket2();

    printBrace2();
}

void AST::FuncRParams::print() {
    printBrace1();

    printKeyVal("type", "funcRParams");

    printKey("rParams");
    printBracket1();

    for (auto iter = rParams.begin(); iter != rParams.end(); iter++) {
        AST::Exp *tmp = *iter;
        tmp->print();
        if (iter + 1 == rParams.end()) {
            break;
        }
        printComma();
    }

    printBracket2();

    printBrace2();
}

void AST::FuncFParam::print() {
    printBrace1();

    printKeyVal("type", "funcFParam");
    printKeyVal("ident", ident->c_str());

    printKey("subscripts");
    if (subs->isPrintNothing) {
        printStr("none");
    } else {
        printStr("[]");
        printComma();
        printKey("afterSubs");
        subs->print();
    }

    printBrace2();
}