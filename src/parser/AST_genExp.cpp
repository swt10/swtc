
#include <cassert>
#include <iostream>

#include "AST.h"
#include "AST_exp.h"

#define COLOR_RESET "\033[0m"
#define COLOR_BLACK "\033[30m"              /* Black */
#define COLOR_RED "\033[31m"                /* Red */
#define COLOR_GREEN "\033[32m"              /* Green */
#define COLOR_YELLOW "\033[33m"             /* Yellow */
#define COLOR_BLUE "\033[34m"               /* Blue */
#define COLOR_MAGENTA "\033[35m"            /* Magenta */
#define COLOR_CYAN "\033[36m"               /* Cyan */
#define COLOR_WHITE "\033[37m"              /* White */
#define COLOR_BOLDBLACK "\033[1m\033[30m"   /* Bold Black */
#define COLOR_BOLDRED "\033[1m\033[31m"     /* Bold Red */
#define COLOR_BOLDGREEN "\033[1m\033[32m"   /* Bold Green */
#define COLOR_BOLDYELLOW "\033[1m\033[33m"  /* Bold Yellow */
#define COLOR_BOLDBLUE "\033[1m\033[34m"    /* Bold Blue */
#define COLOR_BOLDMAGENTA "\033[1m\033[35m" /* Bold Magenta */
#define COLOR_BOLDCYAN "\033[1m\033[36m"    /* Bold Cyan */
#define COLOR_BOLDWHITE "\033[1m\033[37m"   /* Bold White */

#define COLOR_genIR " -------============------- "

AST_EXP::Exp *AST::Exp_Func::genExp() {
    #ifdef DEBUG
    std::cerr << COLOR_BLACK << COLOR_genIR << "genExp: func \n";
    #endif
    std::vector<AST_EXP::Exp *> tmp;

    if (hadParam()) {
        for (auto iter : (funcRParams->rParams)) {
            tmp.emplace_back(iter->genExp());
        }
    }

    return new AST_EXP::Exp(
        true,
        *ident,
        tmp);
}

AST_EXP::Exp *AST::Exp_Num::genExp() {
    #ifdef DEBUG
    std::cerr << COLOR_BLACK << COLOR_genIR << "genExp: num: " << val <<"\n";
    #endif
    return new AST_EXP::Exp(val);
}

AST_EXP::Exp *AST::Exp_Paren::genExp() {
    #ifdef DEBUG
    std::cerr << COLOR_BLACK << COLOR_genIR << "genExp: paren \n";
    #endif
    return expParen->genExp();
}

AST_EXP::Exp *AST::Exp_LVal::genExp() {
    #ifdef DEBUG
    std::cerr << COLOR_BLACK << COLOR_genIR << "genExp: lval: " << *(lval->ident) << "\n";
    #endif
    if (lval->subs == nullptr || lval->subs->isPrintNothing) {
        AST_EXP::Exp *p = new AST_EXP::Exp((*(lval->ident)));
        assert(p != nullptr && "p is null\n");
        return p;
    }
    return new AST_EXP::Exp(*(lval->ident), lval->subs->getSubs());
}

AST_EXP::Exp *AST::Exp_Unary::genExp() {
    #ifdef DEBUG
    std::cerr << COLOR_BLACK << COLOR_genIR << "genExp: uanry\n";
    #endif
    return new AST_EXP::Exp(op, exp->genExp());
}

AST_EXP::Exp *AST::Exp_Binary::genExp() {
    #ifdef DEBUG
    std::cerr << COLOR_BLACK << COLOR_genIR << "genExp: bin\n";
    #endif
    if (op == AST::OP::OP_BINARY_BUT_ONE) {
        return expl->genExp();
    }

    auto tmp = new AST_EXP::Exp(op, expl->genExp(), expr->genExp());
    assert(tmp->expl != nullptr && "null");
    #ifdef DEBUG
    std::cerr << COLOR_BLACK << COLOR_genIR << "genExp: bin end\n";
    #endif
    return tmp;
}

AST_EXP::Exp *AST::InitVal::genExp() {
    // initVal进来似乎一定是{x}
    std::vector<AST_EXP::Exp *> tmpInitVals;

    if(isVar()){
        tmpInitVals.emplace_back(selfInitVal->genExp());
    }
    else{
        for (auto x : initVals) {
            AST_EXP::Exp *tmp = x->genExp();
            if (tmp->isJustArr()) {
                // 单纯 AST_EXP::Exp* 数组
                for (auto y : tmp->vecs) {
                    tmpInitVals.emplace_back(y);
                }
            } else {
                // 其他表达式
                // tmpInitVals.emplace_back(tmp);
                logError("AST::InitVal::genExp");
            }
        }
    }

    AST_EXP::Exp *tmpAns = new AST_EXP::Exp(
        false,
        "",
        tmpInitVals);

    return tmpAns;
}