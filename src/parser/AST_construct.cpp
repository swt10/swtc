#include "AST.h"

AST::PrintNothing::PrintNothing() { isPrintNothing = true; }

AST::Decl::Decl(bool _isConst, AST::ASTBasic *_defs)
    : isConst(_isConst),
      defs((AST::Defs *)_defs) {
    compUnitType = AST::CompUnitType::typeDecl;
    blockItemType = AST::BlockItemType::typeDecl_block;
}

AST::Def::Def(bool _isInit, std::string *_ident, AST::ASTBasic *_subs, AST::ASTBasic *_initVal)
    : isInit(_isInit),
      ident(_ident),
      subs((AST::ArraySub *)_subs),
      initVal((AST::InitVal *)_initVal) {}

AST::FuncDef::FuncDef(std::string *_ident, AST::FuncType _funcType, AST::ASTBasic *_funcFParams, AST::ASTBasic *_funcBody)
    : ident(_ident),
      funcType(_funcType),
      funcFParams((AST::FuncFParams *)_funcFParams),
      funcBody((AST::Block *)_funcBody) {
    compUnitType = AST::CompUnitType::typeFuncDef;
}

// AST::Stmt::Stmt()
//     {
//         blockItemType = AST::BlockItemType::typeStmt;
//     }

AST::Stmt_If::Stmt_If(AST::ASTBasic *_cond, AST::ASTBasic *_stmt_1, AST::ASTBasic *_stmt_2)
    : cond((AST::Exp *)_cond),
      stmt_1((AST::Stmt *)_stmt_1),
      stmt_2((AST::Stmt *)_stmt_2) {
    blockItemType = AST::BlockItemType::typeStmt;
}

AST::Stmt_While::Stmt_While(AST::ASTBasic *_cond, AST::ASTBasic *_stmt_1)
    : cond((AST::Exp *)_cond),
      stmt_1((AST::Stmt *)_stmt_1) {
    blockItemType = AST::BlockItemType::typeStmt;
}

AST::Stmt_Ass::Stmt_Ass(AST::ASTBasic *_lval, AST::ASTBasic *_rval)
    : lval((AST::LVal *)_lval),
      rval((AST::Exp *)_rval) {
    blockItemType = AST::BlockItemType::typeStmt;
}

AST::Stmt_Break::Stmt_Break() {}

AST::Stmt_Continue::Stmt_Continue() {}

AST::Stmt_Return::Stmt_Return(AST::ASTBasic *_retVal)
    : retVal((AST::Exp *)_retVal) {
    blockItemType = AST::BlockItemType::typeStmt;
}

AST::Stmt_Block::Stmt_Block(AST::ASTBasic *_block)
    : block((AST::Block *)_block) {
    blockItemType = AST::BlockItemType::typeStmt;
}

AST::Stmt_Exp::Stmt_Exp(AST::ASTBasic *_exp)
    : exp((AST::Exp *)_exp) {
    blockItemType = AST::BlockItemType::typeStmt;
}

AST::LVal::LVal(std::string *_ident, AST::ASTBasic *_subs)
    : ident(_ident),
      subs((AST::ArraySub *)_subs) {}

AST::Exp_Func::Exp_Func(std::string *_ident, AST::ASTBasic *_funcRParams)
    : ident(_ident),
      funcRParams((AST::FuncRParams *)_funcRParams) {}

AST::Exp_Num::Exp_Num(int _val) : val(_val){};

AST::Exp_Paren::Exp_Paren(AST::ASTBasic *_expParen)
    : expParen((AST::Exp *)_expParen) {}

AST::Exp_LVal::Exp_LVal(AST::ASTBasic *_lval)
    : lval((AST::LVal *)_lval) {}

AST::Exp_Unary::Exp_Unary(AST::OP _op, AST::ASTBasic *_exp)
    : op(_op),
      exp((AST::Exp *)_exp) {}

AST::Exp_Binary::Exp_Binary(AST::OP _op, AST::ASTBasic *_expl, AST::ASTBasic *_expr)
    : op(_op),
      expl((AST::Exp *)_expl),
      expr((AST::Exp *)_expr) {}

AST::FuncFParam::FuncFParam(std::string *_ident, AST::ASTBasic *_subs)
    : ident(_ident),
      subs((AST::ArraySub *)_subs) {}
