/* A Bison parser, made by GNU Bison 3.5.1.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Undocumented macros, especially those whose name start with YY_,
   are private implementation details.  Do not rely on them.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.5.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 1 "parser.y"

	#include <iostream>
	#include <string>
	#include "config.h"
	#include "AST.h"
	#include "FlexLexer.h"

	extern AST::CompUnit *root;
	extern AST::ASTBasic *ppnt;
    extern int yylex(void);
    extern yyFlexLexer lexer;
    //此处行号可能有问题
	void yyerror(char *errmsg){
		std::cerr << errmsg << " line: " << lexer.yyget_lineno() << "\n";
	}

	void show(char * str){
		if(!isDebugAST){
			return;
		}
		std::cerr << str << "\n";
	}


#line 95 "parser.cpp"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Use api.header.include to #include this header
   instead of duplicating it here.  */
#ifndef YY_YY_PARSER_HPP_INCLUDED
# define YY_YY_PARSER_HPP_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    KEY_VOID = 258,
    KEY_INT = 259,
    KEY_CONST = 260,
    KEY_IF = 261,
    KEY_ELSE = 262,
    KEY_WHILE = 263,
    KEY_BREAK = 264,
    KEY_CONTINUE = 265,
    KEY_RETURN = 266,
    TOK_IDENT = 267,
    INT_DEC = 268,
    INT_OCT = 269,
    INT_HEX = 270,
    T_OP_EQ = 271,
    T_OP_NE = 272,
    T_OP_LE = 273,
    T_OP_GE = 274,
    T_OP_LT = 275,
    T_OP_GT = 276,
    T_OP_NOT = 277,
    T_OP_AND = 278,
    T_OP_OR = 279,
    T_OP_ADD = 280,
    T_OP_MIN = 281,
    T_OP_TIM = 282,
    T_OP_DIV = 283,
    T_OP_MOD = 284,
    END_LINE = 285,
    LOWER_THAN_ELSE = 286
  };
#endif
/* Tokens.  */
#define KEY_VOID 258
#define KEY_INT 259
#define KEY_CONST 260
#define KEY_IF 261
#define KEY_ELSE 262
#define KEY_WHILE 263
#define KEY_BREAK 264
#define KEY_CONTINUE 265
#define KEY_RETURN 266
#define TOK_IDENT 267
#define INT_DEC 268
#define INT_OCT 269
#define INT_HEX 270
#define T_OP_EQ 271
#define T_OP_NE 272
#define T_OP_LE 273
#define T_OP_GE 274
#define T_OP_LT 275
#define T_OP_GT 276
#define T_OP_NOT 277
#define T_OP_AND 278
#define T_OP_OR 279
#define T_OP_ADD 280
#define T_OP_MIN 281
#define T_OP_TIM 282
#define T_OP_DIV 283
#define T_OP_MOD 284
#define END_LINE 285
#define LOWER_THAN_ELSE 286

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 29 "parser.y"

	int num;
	std::string *str;
	AST::ASTBasic *node;

#line 215 "parser.cpp"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_PARSER_HPP_INCLUDED  */



#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))

/* Stored state numbers (used for stacks). */
typedef yytype_uint8 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && ! defined __ICC && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                            \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  2
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   321

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  40
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  37
/* YYNRULES -- Number of rules.  */
#define YYNRULES  97
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  180

#define YYUNDEFTOK  2
#define YYMAXUTOK   286


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      38,    39,     2,     2,    33,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    31,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    34,     2,    35,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    36,     2,    37,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    32
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,    95,    95,    99,   104,   110,   114,   122,   127,   132,
     138,   141,   146,   151,   159,   165,   169,   175,   180,   189,
     194,   199,   207,   212,   216,   220,   227,   233,   238,   244,
     248,   255,   259,   264,   269,   276,   282,   290,   294,   298,
     305,   309,   314,   318,   324,   327,   369,   374,   379,   384,
     390,   395,   400,   405,   410,   415,   420,   427,   432,   438,
     441,   446,   450,   456,   459,   462,   467,   470,   473,   478,
     481,   484,   492,   496,   500,   511,   516,   520,   526,   529,
     532,   535,   540,   543,   546,   551,   554,   557,   560,   563,
     568,   571,   574,   579,   582,   587,   590,   595
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "KEY_VOID", "KEY_INT", "KEY_CONST",
  "KEY_IF", "KEY_ELSE", "KEY_WHILE", "KEY_BREAK", "KEY_CONTINUE",
  "KEY_RETURN", "TOK_IDENT", "INT_DEC", "INT_OCT", "INT_HEX", "T_OP_EQ",
  "T_OP_NE", "T_OP_LE", "T_OP_GE", "T_OP_LT", "T_OP_GT", "T_OP_NOT",
  "T_OP_AND", "T_OP_OR", "T_OP_ADD", "T_OP_MIN", "T_OP_TIM", "T_OP_DIV",
  "T_OP_MOD", "END_LINE", "'='", "LOWER_THAN_ELSE", "','", "'['", "']'",
  "'{'", "'}'", "'('", "')'", "$accept", "CompUnit", "Decl", "ConstDecl",
  "ConstDefs", "ConstDef", "ConstArrayExp", "ConstInitVal",
  "ConstInitVals", "VarDecl", "VarDefs", "VarDef", "InitVal", "InitVals",
  "FuncDef", "FuncFParams", "FuncFParam", "Block", "BlockItems",
  "BlockItem", "Stmt", "Exp", "Cond", "LVal", "ArrayExp", "PrimaryExp",
  "Number", "UnaryExp", "FuncRParams", "Exps", "MulExp", "AddExp",
  "RelExp", "EqExp", "LAndExp", "LOrExp", "ConstExp", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_int16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,    61,   286,    44,    91,    93,   123,   125,    40,    41
};
# endif

#define YYPACT_NINF (-143)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-1)

#define yytable_value_is_error(Yyn) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
    -143,    60,  -143,    26,    28,    10,  -143,  -143,  -143,  -143,
      29,   -18,     9,  -143,    71,     4,   241,   283,     5,    27,
    -143,    79,    47,    52,  -143,    87,    68,   -16,  -143,    -6,
    -143,  -143,  -143,   283,   283,   283,   207,   283,  -143,  -143,
    -143,  -143,  -143,  -143,    74,    20,    20,    80,    68,    -8,
     241,   283,    55,  -143,   256,    59,  -143,    71,    75,   145,
    -143,   112,    68,   283,   186,    83,  -143,  -143,  -143,  -143,
    -143,    -7,    81,   283,   283,   283,   283,   283,  -143,  -143,
      68,  -143,    84,   224,  -143,  -143,   256,  -143,    88,    79,
      89,    90,    92,    99,   261,  -143,  -143,  -143,  -143,   180,
    -143,  -143,   100,   101,  -143,  -143,    96,  -143,  -143,    94,
      85,   283,   241,  -143,  -143,  -143,  -143,  -143,    74,    74,
    -143,  -143,  -143,  -143,    -4,  -143,   102,   283,   283,  -143,
    -143,  -143,   104,  -143,  -143,  -143,   283,  -143,  -143,   283,
     103,  -143,   256,  -143,   105,    98,    20,    36,    78,   117,
     111,   113,  -143,   131,  -143,  -143,  -143,    62,   283,   283,
     283,   283,   283,   283,   283,   283,    62,  -143,   155,    20,
      20,    20,    20,    36,    36,    78,   117,  -143,    62,  -143
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_int8 yydefact[] =
{
       2,     0,     1,     0,     0,     0,     3,     5,     6,     4,
       0,    24,     0,    20,     0,     0,     0,     0,     0,    22,
      19,     0,     0,     0,     8,     0,     0,     0,    35,    59,
      66,    68,    67,     0,     0,     0,     0,     0,    25,    26,
      64,    69,    65,    78,    82,    57,    97,     0,     0,     0,
       0,     0,    24,    21,     0,     0,     7,     0,    37,     0,
      34,     0,     0,     0,     0,    60,    74,    72,    73,    27,
      29,     0,     0,     0,     0,     0,     0,     0,    12,    33,
       0,    23,     0,     0,    10,    14,     0,     9,     0,     0,
       0,     0,     0,     0,     0,    56,    40,    44,    48,     0,
      42,    45,     0,    64,    36,    31,     0,    70,    76,     0,
      75,     0,     0,    28,    63,    79,    80,    81,    83,    84,
      32,    13,    15,    17,     0,    11,    38,     0,     0,    52,
      53,    54,     0,    41,    43,    47,     0,    61,    71,     0,
       0,    30,     0,    16,    39,     0,    85,    90,    93,    95,
      58,     0,    55,     0,    77,    62,    18,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    46,    49,    88,
      89,    86,    87,    91,    92,    94,    96,    51,     0,    50
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -143,  -143,   162,  -143,  -143,   107,   -20,   -80,  -143,  -143,
    -143,   144,   -32,  -143,  -143,   148,   108,   -21,  -143,    69,
    -142,   -15,    44,   -52,  -143,  -143,  -143,   -23,  -143,  -143,
      31,   -17,   -50,    12,     8,  -143,     2
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     1,    97,     7,    23,    24,    19,    84,   124,     8,
      12,    13,    38,    71,     9,    27,    28,    98,    99,   100,
     101,   102,   145,    40,    65,    41,    42,    43,   109,   110,
      44,    45,   147,   148,   149,   150,    85
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_uint8 yytable[] =
{
      46,    39,    55,   123,    70,    60,   125,   103,    25,    25,
      66,    67,    68,    16,    14,   168,    17,    61,    81,    47,
      18,    39,    72,    62,   177,    61,   112,    79,    63,   142,
     113,    80,    64,   143,    46,    39,   179,    46,    10,    20,
      11,   105,    21,    26,    48,    76,    77,   103,   106,   108,
     115,   116,   117,    82,   158,   159,   160,   161,    50,   120,
       2,    51,   156,     3,     4,     5,    46,    15,    90,    46,
      91,    92,    93,    94,    29,    30,    31,    32,    54,   132,
     141,    17,    56,    22,    33,    57,    16,    34,    35,    17,
      86,    52,    95,    51,   162,   163,   140,    39,    59,    58,
      37,    73,    74,    75,    59,   103,   144,   118,   119,    88,
     146,   146,   173,   174,   103,    78,    25,   111,   139,   121,
     114,   153,   129,   126,   154,    46,   103,   127,   128,   130,
     135,   137,   136,   138,   152,   165,    17,   157,   155,    51,
     164,   169,   170,   171,   172,   146,   146,   146,   146,    89,
       5,    90,   166,    91,    92,    93,    94,    29,    30,    31,
      32,   167,   178,     6,    87,    53,    49,    33,   134,   104,
      34,    35,   151,   176,     0,    95,   175,     0,     0,     0,
       0,    59,    96,    37,    89,     5,    90,     0,    91,    92,
      93,    94,    29,    30,    31,    32,     0,     0,    29,    30,
      31,    32,    33,     0,     0,    34,    35,     0,    33,     0,
      95,    34,    35,     0,     0,     0,    59,   133,    37,    29,
      30,    31,    32,     0,    37,   107,     0,     0,     0,    33,
       0,     0,    34,    35,     0,     0,    29,    30,    31,    32,
       0,     0,     0,    36,    69,    37,    33,     0,     0,    34,
      35,     0,     0,    29,    30,    31,    32,     0,     0,     0,
      83,   122,    37,    33,     0,     0,    34,    35,    29,    30,
      31,    32,     0,    29,    30,    31,    32,    36,    33,    37,
       0,    34,    35,    33,     0,     0,    34,    35,     0,     0,
       0,   131,    83,     0,    37,    29,    30,    31,    32,    37,
       0,     0,     0,     0,     0,    33,     0,     0,    34,    35,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    37
};

static const yytype_int16 yycheck[] =
{
      17,    16,    22,    83,    36,    26,    86,    59,     4,     4,
      33,    34,    35,    31,     4,   157,    34,    33,    50,    17,
      38,    36,    37,    39,   166,    33,    33,    48,    34,    33,
      37,    39,    38,    37,    51,    50,   178,    54,    12,    30,
      12,    62,    33,    39,    39,    25,    26,    99,    63,    64,
      73,    74,    75,    51,    18,    19,    20,    21,    31,    80,
       0,    34,   142,     3,     4,     5,    83,    38,     6,    86,
       8,     9,    10,    11,    12,    13,    14,    15,    31,    94,
     112,    34,    30,    12,    22,    33,    31,    25,    26,    34,
      31,    12,    30,    34,    16,    17,   111,   112,    36,    12,
      38,    27,    28,    29,    36,   157,   126,    76,    77,    34,
     127,   128,   162,   163,   166,    35,     4,    34,    33,    35,
      39,   136,    30,    35,   139,   142,   178,    38,    38,    30,
      30,    35,    31,    39,    30,    24,    34,    39,    35,    34,
      23,   158,   159,   160,   161,   162,   163,   164,   165,     4,
       5,     6,    39,     8,     9,    10,    11,    12,    13,    14,
      15,    30,     7,     1,    57,    21,    18,    22,    99,    61,
      25,    26,   128,   165,    -1,    30,   164,    -1,    -1,    -1,
      -1,    36,    37,    38,     4,     5,     6,    -1,     8,     9,
      10,    11,    12,    13,    14,    15,    -1,    -1,    12,    13,
      14,    15,    22,    -1,    -1,    25,    26,    -1,    22,    -1,
      30,    25,    26,    -1,    -1,    -1,    36,    37,    38,    12,
      13,    14,    15,    -1,    38,    39,    -1,    -1,    -1,    22,
      -1,    -1,    25,    26,    -1,    -1,    12,    13,    14,    15,
      -1,    -1,    -1,    36,    37,    38,    22,    -1,    -1,    25,
      26,    -1,    -1,    12,    13,    14,    15,    -1,    -1,    -1,
      36,    37,    38,    22,    -1,    -1,    25,    26,    12,    13,
      14,    15,    -1,    12,    13,    14,    15,    36,    22,    38,
      -1,    25,    26,    22,    -1,    -1,    25,    26,    -1,    -1,
      -1,    30,    36,    -1,    38,    12,    13,    14,    15,    38,
      -1,    -1,    -1,    -1,    -1,    22,    -1,    -1,    25,    26,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    38
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_int8 yystos[] =
{
       0,    41,     0,     3,     4,     5,    42,    43,    49,    54,
      12,    12,    50,    51,     4,    38,    31,    34,    38,    46,
      30,    33,    12,    44,    45,     4,    39,    55,    56,    12,
      13,    14,    15,    22,    25,    26,    36,    38,    52,    61,
      63,    65,    66,    67,    70,    71,    71,    76,    39,    55,
      31,    34,    12,    51,    31,    46,    30,    33,    12,    36,
      57,    33,    39,    34,    38,    64,    67,    67,    67,    37,
      52,    53,    61,    27,    28,    29,    25,    26,    35,    57,
      39,    52,    76,    36,    47,    76,    31,    45,    34,     4,
       6,     8,     9,    10,    11,    30,    37,    42,    57,    58,
      59,    60,    61,    63,    56,    57,    61,    39,    61,    68,
      69,    34,    33,    37,    39,    67,    67,    67,    70,    70,
      57,    35,    37,    47,    48,    47,    35,    38,    38,    30,
      30,    30,    61,    37,    59,    30,    31,    35,    39,    33,
      61,    52,    33,    37,    46,    62,    71,    72,    73,    74,
      75,    62,    30,    61,    61,    35,    47,    39,    18,    19,
      20,    21,    16,    17,    23,    24,    39,    30,    60,    71,
      71,    71,    71,    72,    72,    73,    74,    60,     7,    60
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_int8 yyr1[] =
{
       0,    40,    41,    41,    41,    42,    42,    43,    44,    44,
      45,    45,    46,    46,    47,    47,    47,    48,    48,    49,
      50,    50,    51,    51,    51,    51,    52,    52,    52,    53,
      53,    54,    54,    54,    54,    55,    55,    56,    56,    56,
      57,    57,    58,    58,    59,    59,    60,    60,    60,    60,
      60,    60,    60,    60,    60,    60,    60,    61,    62,    63,
      63,    64,    64,    65,    65,    65,    66,    66,    66,    67,
      67,    67,    67,    67,    67,    68,    69,    69,    70,    70,
      70,    70,    71,    71,    71,    72,    72,    72,    72,    72,
      73,    73,    73,    74,    74,    75,    75,    76
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     0,     2,     2,     1,     1,     4,     1,     3,
       3,     4,     3,     4,     1,     2,     3,     1,     3,     3,
       1,     3,     2,     4,     1,     3,     1,     2,     3,     1,
       3,     6,     6,     5,     5,     1,     3,     2,     4,     5,
       2,     3,     1,     2,     1,     1,     4,     2,     1,     5,
       7,     5,     2,     2,     2,     3,     1,     1,     1,     1,
       2,     3,     4,     3,     1,     1,     1,     1,     1,     1,
       3,     4,     2,     2,     2,     1,     1,     3,     1,     3,
       3,     3,     1,     3,     3,     1,     3,     3,     3,     3,
       1,     3,     3,     1,     3,     1,     3,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyoutput = yyo;
  YYUSE (yyoutput);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyo, yytoknum[yytype], *yyvaluep);
# endif
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyo, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyo, yytype, yyvaluep);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[+yyssp[yyi + 1 - yynrhs]],
                       &yyvsp[(yyi + 1) - (yynrhs)]
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen(S) (YY_CAST (YYPTRDIFF_T, strlen (S)))
#  else
/* Return the length of YYSTR.  */
static YYPTRDIFF_T
yystrlen (const char *yystr)
{
  YYPTRDIFF_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYPTRDIFF_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYPTRDIFF_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            else
              goto append;

          append:
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (yyres)
    return yystpcpy (yyres, yystr) - yyres;
  else
    return yystrlen (yystr);
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYPTRDIFF_T *yymsg_alloc, char **yymsg,
                yy_state_t *yyssp, int yytoken)
{
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat: reported tokens (one for the "unexpected",
     one per "expected"). */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Actual size of YYARG. */
  int yycount = 0;
  /* Cumulated lengths of YYARG.  */
  YYPTRDIFF_T yysize = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[+*yyssp];
      YYPTRDIFF_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
      yysize = yysize0;
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYPTRDIFF_T yysize1
                    = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)
                    yysize = yysize1;
                  else
                    return 2;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
    default: /* Avoid compiler warnings. */
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    /* Don't count the "%s"s in the final size, but reserve room for
       the terminator.  */
    YYPTRDIFF_T yysize1 = yysize + (yystrlen (yyformat) - 2 * yycount) + 1;
    if (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)
      yysize = yysize1;
    else
      return 2;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          ++yyp;
          ++yyformat;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    yy_state_fast_t yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss;
    yy_state_t *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYPTRDIFF_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYPTRDIFF_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    goto yyexhaustedlab;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
# undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2:
#line 95 "parser.y"
                     {
	show("CompUnit empty");
}
#line 1544 "parser.cpp"
    break;

  case 3:
#line 99 "parser.y"
                             {
		root->add((yyvsp[0].node));
		show("CompUnit Decl");
	}
#line 1553 "parser.cpp"
    break;

  case 4:
#line 104 "parser.y"
                                {
		root->add((yyvsp[0].node));
show("CompUnit FuncDef");
	}
#line 1562 "parser.cpp"
    break;

  case 5:
#line 110 "parser.y"
                 {
		(yyval.node) = (yyvsp[0].node);show("Decl ConstDecl");
	}
#line 1570 "parser.cpp"
    break;

  case 6:
#line 114 "parser.y"
                       {
		(yyval.node) = (yyvsp[0].node);show("Decl VarDecl");
	}
#line 1578 "parser.cpp"
    break;

  case 7:
#line 122 "parser.y"
                                                    {
		(yyval.node) = new AST::Decl(true, (yyvsp[-1].node));show("ConstDecl");
	}
#line 1586 "parser.cpp"
    break;

  case 8:
#line 127 "parser.y"
                        {
		(yyval.node) = new AST::Defs;
		(yyval.node)->add((yyvsp[0].node));show("ConstDefs");
	}
#line 1595 "parser.cpp"
    break;

  case 9:
#line 132 "parser.y"
                                      {
		(yyval.node)->add((yyvsp[0].node));show("ConstDefs");
	}
#line 1603 "parser.cpp"
    break;

  case 10:
#line 138 "parser.y"
                                          {
		(yyval.node) = new AST::Def(true, (yyvsp[-2].str) , ((AST::ASTBasic*)(new AST::PrintNothing())), (yyvsp[0].node));show("ConstDef");
	}
#line 1611 "parser.cpp"
    break;

  case 11:
#line 141 "parser.y"
                                                        {
		(yyval.node) = new AST::Def(true, (yyvsp[-3].str), (yyvsp[-2].node), (yyvsp[0].node));show("ConstDef");
	}
#line 1619 "parser.cpp"
    break;

  case 12:
#line 146 "parser.y"
                                {
		(yyval.node) = new AST::ArraySub();
		(yyval.node)->add((yyvsp[-1].node));show("ConstArrayExp");
	}
#line 1628 "parser.cpp"
    break;

  case 13:
#line 151 "parser.y"
                                              {
		(yyval.node) = (yyvsp[-3].node);
		(yyval.node)->add((yyvsp[-1].node));
		show("ConstArrayExp");
	}
#line 1638 "parser.cpp"
    break;

  case 14:
#line 159 "parser.y"
                      {
	auto x = new AST::InitVal();
	x->setSelf((yyvsp[0].node));
	(yyval.node) = x;
	show("ConstInitVal");
}
#line 1649 "parser.cpp"
    break;

  case 15:
#line 165 "parser.y"
                       {
		(yyval.node) = new AST::InitVal();
		show("ConstInitVal");
	}
#line 1658 "parser.cpp"
    break;

  case 16:
#line 169 "parser.y"
                                     {
		(yyval.node) = (yyvsp[-1].node);
		show("ConstInitVal");
	}
#line 1667 "parser.cpp"
    break;

  case 17:
#line 175 "parser.y"
                           {
	show("ConstInitVals");
	(yyval.node) = new AST::InitVal();
	(yyval.node)->add((yyvsp[0].node));
}
#line 1677 "parser.cpp"
    break;

  case 18:
#line 180 "parser.y"
                                        {
		show("ConstInitVals");
		(yyval.node) = (yyvsp[-2].node);
		(yyval.node)->add((yyvsp[0].node));
	}
#line 1687 "parser.cpp"
    break;

  case 19:
#line 189 "parser.y"
                                        {show("VarDecl");
	(yyval.node) = new AST::Decl(false, (yyvsp[-1].node));
}
#line 1695 "parser.cpp"
    break;

  case 20:
#line 194 "parser.y"
                       {
	show("VarDefs");
	(yyval.node) = new AST::Defs();
	(yyval.node)->add((yyvsp[0].node));
}
#line 1705 "parser.cpp"
    break;

  case 21:
#line 199 "parser.y"
                                  {
		show("VarDefs ',' VarDef");
		(yyval.node) = (yyvsp[-2].node);
		(yyval.node)->add((yyvsp[0].node));
	}
#line 1715 "parser.cpp"
    break;

  case 22:
#line 207 "parser.y"
                                       {
		show("VarDef ConstArrayExp");
		(yyval.node) = new AST::Def(false, (yyvsp[-1].str), (yyvsp[0].node), ((AST::ASTBasic*)(new AST::PrintNothing())));
	}
#line 1724 "parser.cpp"
    break;

  case 23:
#line 212 "parser.y"
                                                   {show("VarDef ConstArrayExp '=' InitVal");
		(yyval.node) = new AST::Def(true, (yyvsp[-3].str), (yyvsp[-2].node), (yyvsp[0].node));
	}
#line 1732 "parser.cpp"
    break;

  case 24:
#line 216 "parser.y"
                          {show("VarDef Var");
		(yyval.node) = new AST::Def(false, (yyvsp[0].str), ((AST::ASTBasic*)(new AST::PrintNothing())), ((AST::ASTBasic*)(new AST::PrintNothing())));
	}
#line 1740 "parser.cpp"
    break;

  case 25:
#line 220 "parser.y"
                                     {show("VarDef Var '=' InitVal");
		(yyval.node) = new AST::Def(true, (yyvsp[-2].str), ((AST::ASTBasic*)(new AST::PrintNothing())), (yyvsp[0].node));

	}
#line 1749 "parser.cpp"
    break;

  case 26:
#line 227 "parser.y"
                    {
		auto x = new AST::InitVal();
		x->setSelf((yyvsp[0].node));
		(yyval.node) = x;
		show("InitVal");
	}
#line 1760 "parser.cpp"
    break;

  case 27:
#line 233 "parser.y"
                       {
		show("InitVal");
		(yyval.node) = new AST::InitVal();
	}
#line 1769 "parser.cpp"
    break;

  case 28:
#line 238 "parser.y"
                                {
		(yyval.node) = (yyvsp[-1].node);
		show("InitVal");
	}
#line 1778 "parser.cpp"
    break;

  case 29:
#line 244 "parser.y"
                       {show("InitVals");
	(yyval.node) = new AST::InitVal();
	(yyval.node)->add((yyvsp[0].node));
}
#line 1787 "parser.cpp"
    break;

  case 30:
#line 248 "parser.y"
                                    {show("InitVals");
		(yyval.node) = (yyvsp[-2].node);
		(yyval.node)->add((yyvsp[0].node));
	}
#line 1796 "parser.cpp"
    break;

  case 31:
#line 255 "parser.y"
                                                            {
		show("FuncDef Void HadParam");
		(yyval.node) = new AST::FuncDef((yyvsp[-4].str), AST::FuncType::typeVoid, (yyvsp[-2].node), (yyvsp[0].node));
}
#line 1805 "parser.cpp"
    break;

  case 32:
#line 259 "parser.y"
                                                           {
		show("FuncDef Int HadParam");
		(yyval.node) = new AST::FuncDef((yyvsp[-4].str), AST::FuncType::typeInt, (yyvsp[-2].node), (yyvsp[0].node));
	}
#line 1814 "parser.cpp"
    break;

  case 33:
#line 264 "parser.y"
                                                {
		show("FuncDef Int");
		(yyval.node) = new AST::FuncDef((yyvsp[-3].str), AST::FuncType::typeInt, new AST::PrintNothing(), (yyvsp[0].node));
	}
#line 1823 "parser.cpp"
    break;

  case 34:
#line 269 "parser.y"
                                                 {
		show("FuncDef Void");
		(yyval.node) = new AST::FuncDef((yyvsp[-3].str), AST::FuncType::typeVoid, new AST::PrintNothing(), (yyvsp[0].node));
	}
#line 1832 "parser.cpp"
    break;

  case 35:
#line 276 "parser.y"
                           {
			show("FuncFParams");
		(yyval.node) = new AST::FuncFParams();
		(yyval.node)->add((yyvsp[0].node));
	}
#line 1842 "parser.cpp"
    break;

  case 36:
#line 282 "parser.y"
                                          {
		show("FuncFParams ',' FuncFParam");
		(yyval.node) = (yyvsp[-2].node);
		(yyval.node)->add((yyvsp[0].node));
	}
#line 1852 "parser.cpp"
    break;

  case 37:
#line 290 "parser.y"
                                  {show("FuncFParam");
		(yyval.node) = new AST::FuncFParam((yyvsp[0].str), ((AST::ASTBasic*)(new AST::PrintNothing())));	
	}
#line 1860 "parser.cpp"
    break;

  case 38:
#line 294 "parser.y"
                                         {show("FuncFParam");
		(yyval.node) = new AST::FuncFParam((yyvsp[-2].str), new AST::ArraySub());
	}
#line 1868 "parser.cpp"
    break;

  case 39:
#line 298 "parser.y"
                                                       {show("FuncFParam");
		(yyval.node) = new AST::FuncFParam((yyvsp[-3].str), (yyvsp[0].node));
	}
#line 1876 "parser.cpp"
    break;

  case 40:
#line 305 "parser.y"
                       {show("Block");
		(yyval.node) = ((AST::ASTBasic*)(new AST::PrintNothing()));
	}
#line 1884 "parser.cpp"
    break;

  case 41:
#line 309 "parser.y"
                                  {show("Block");
		(yyval.node) = (yyvsp[-1].node);
	}
#line 1892 "parser.cpp"
    break;

  case 42:
#line 314 "parser.y"
                          {show("BlockItems");
		(yyval.node) = new AST::Block();
		(yyval.node)->add((yyvsp[0].node));
}
#line 1901 "parser.cpp"
    break;

  case 43:
#line 318 "parser.y"
                                    {show("BlockItems");
		(yyval.node) = (yyvsp[-1].node);
		(yyval.node)->add((yyvsp[0].node));
	}
#line 1910 "parser.cpp"
    break;

  case 44:
#line 324 "parser.y"
                    {show("BlockItem");
	(yyval.node) = (yyvsp[0].node);
}
#line 1918 "parser.cpp"
    break;

  case 45:
#line 327 "parser.y"
                    {show("BlockItem");
		(yyval.node) = (yyvsp[0].node);
	}
#line 1926 "parser.cpp"
    break;

  case 46:
#line 369 "parser.y"
                                     {
	show("lval Stmt");
	(yyval.node) = new AST::Stmt_Ass((yyvsp[-3].node), (yyvsp[-1].node));
	}
#line 1935 "parser.cpp"
    break;

  case 47:
#line 374 "parser.y"
                            {
		show("exp Stmt");
		(yyval.node) = new AST::Stmt_Exp((yyvsp[-1].node));
	}
#line 1944 "parser.cpp"
    break;

  case 48:
#line 379 "parser.y"
                     {
		show("block Stmt");
		(yyval.node) = new AST::Stmt_Block((yyvsp[0].node));
	}
#line 1953 "parser.cpp"
    break;

  case 49:
#line 385 "parser.y"
        {
			show("if Stmt");
			(yyval.node) = new AST::Stmt_If((yyvsp[-2].node), (yyvsp[0].node), new AST::PrintNothing());
	}
#line 1962 "parser.cpp"
    break;

  case 50:
#line 390 "parser.y"
                                                       {
			show("if else Stmt");
			(yyval.node) = new AST::Stmt_If((yyvsp[-4].node), (yyvsp[-2].node), (yyvsp[0].node));
		}
#line 1971 "parser.cpp"
    break;

  case 51:
#line 395 "parser.y"
                                                 {
		show("while Stmt");
		(yyval.node) = new AST::Stmt_While((yyvsp[-2].node), (yyvsp[0].node));
	}
#line 1980 "parser.cpp"
    break;

  case 52:
#line 400 "parser.y"
                                  {
		show("break Stmt");
		(yyval.node) = new AST::Stmt_Break();
	}
#line 1989 "parser.cpp"
    break;

  case 53:
#line 405 "parser.y"
                                     {
		show("continue Stmt");
		(yyval.node) = new AST::Stmt_Continue();
	}
#line 1998 "parser.cpp"
    break;

  case 54:
#line 410 "parser.y"
                                   {
		show("return Stmt");
		(yyval.node) = new AST::Stmt_Return(((AST::ASTBasic*)(new AST::PrintNothing())));
	}
#line 2007 "parser.cpp"
    break;

  case 55:
#line 415 "parser.y"
                                       {
		show("return val Stmt");
		(yyval.node) = new AST::Stmt_Return((yyvsp[-1].node));
	}
#line 2016 "parser.cpp"
    break;

  case 56:
#line 420 "parser.y"
                        {
		show("empty Stmt");
		(yyval.node) = new AST::Stmt_Exp(((AST::ASTBasic*)(new AST::PrintNothing())));
	}
#line 2025 "parser.cpp"
    break;

  case 57:
#line 427 "parser.y"
              {show("Exp <- AddExp");
	(yyval.node) = (yyvsp[0].node);
}
#line 2033 "parser.cpp"
    break;

  case 58:
#line 432 "parser.y"
              {show("Cond <- LOrExp");
	(yyval.node) = (yyvsp[0].node);
}
#line 2041 "parser.cpp"
    break;

  case 59:
#line 438 "parser.y"
                  {show("LVal is Var");
	(yyval.node) = new AST::LVal((yyvsp[0].str), ((AST::ASTBasic*)(new AST::PrintNothing())));
}
#line 2049 "parser.cpp"
    break;

  case 60:
#line 441 "parser.y"
                                  {show("LVal is ArrayExp");
		(yyval.node) = new AST::LVal((yyvsp[-1].str), (yyvsp[0].node));
	}
#line 2057 "parser.cpp"
    break;

  case 61:
#line 446 "parser.y"
                            {show("ArrayExp '[' Exp ']'");
	(yyval.node) = new AST::ArraySub;
	(yyval.node)->add((yyvsp[-1].node));
}
#line 2066 "parser.cpp"
    break;

  case 62:
#line 450 "parser.y"
                              {show("ArrayExp '[' Exp ']'");
		(yyval.node) = (yyvsp[-3].node);
		(yyval.node)->add((yyvsp[-1].node));
	}
#line 2075 "parser.cpp"
    break;

  case 63:
#line 456 "parser.y"
                           {show("PrimaryExp '(' Exp ')'");
	(yyval.node) = new AST::Exp_Paren((yyvsp[-1].node));
}
#line 2083 "parser.cpp"
    break;

  case 64:
#line 459 "parser.y"
                    {show("PrimaryExp <- LVal");
		(yyval.node) = new AST::Exp_LVal((yyvsp[0].node));
	}
#line 2091 "parser.cpp"
    break;

  case 65:
#line 462 "parser.y"
                      {show("PrimaryExp Number");
		(yyval.node) = new AST::Exp_Num((yyvsp[0].num));
	}
#line 2099 "parser.cpp"
    break;

  case 66:
#line 467 "parser.y"
               {show("Number dec");
	(yyval.num) = (yyvsp[0].num);
}
#line 2107 "parser.cpp"
    break;

  case 67:
#line 470 "parser.y"
                       {show("Number hex");
		(yyval.num) = (yyvsp[0].num);
	}
#line 2115 "parser.cpp"
    break;

  case 68:
#line 473 "parser.y"
                       {show("Number oct");
		(yyval.num) = (yyvsp[0].num);
	}
#line 2123 "parser.cpp"
    break;

  case 69:
#line 478 "parser.y"
                          {show("UnaryExp <- PrimaryExp");
	(yyval.node) = (yyvsp[0].node);
}
#line 2131 "parser.cpp"
    break;

  case 70:
#line 481 "parser.y"
                                 {show("UnaryExp ()");
		(yyval.node) = new AST::Exp_Func((yyvsp[-2].str), ((AST::ASTBasic*)(new AST::PrintNothing())));
	}
#line 2139 "parser.cpp"
    break;

  case 71:
#line 484 "parser.y"
                                             {show("UnaryExp ( param )");
		(yyval.node) = new AST::Exp_Func((yyvsp[-3].str), (yyvsp[-1].node));
	}
#line 2147 "parser.cpp"
    break;

  case 72:
#line 492 "parser.y"
                                 {show("UnaryExp ADD");
		(yyval.node) = new AST::Exp_Unary(AST::OP::OP_ADD, (yyvsp[0].node));
	}
#line 2155 "parser.cpp"
    break;

  case 73:
#line 496 "parser.y"
                                 {show("UnaryExp MIN");
		(yyval.node) = new AST::Exp_Unary(AST::OP::OP_MIN, (yyvsp[0].node));
	}
#line 2163 "parser.cpp"
    break;

  case 74:
#line 500 "parser.y"
                                 {show("UnaryExp NOT");
		(yyval.node) = new AST::Exp_Unary(AST::OP::OP_NOT, (yyvsp[0].node));
	}
#line 2171 "parser.cpp"
    break;

  case 75:
#line 511 "parser.y"
                    {show("FuncRParams");
	(yyval.node) = (yyvsp[0].node);
}
#line 2179 "parser.cpp"
    break;

  case 76:
#line 516 "parser.y"
            {show("Exps");
	(yyval.node) = new AST::FuncRParams();
	(yyval.node)->add((yyvsp[0].node));
}
#line 2188 "parser.cpp"
    break;

  case 77:
#line 520 "parser.y"
                            {show("Exps");
		(yyval.node) = (yyvsp[-2].node);
		(yyval.node)->add((yyvsp[0].node));
	}
#line 2197 "parser.cpp"
    break;

  case 78:
#line 526 "parser.y"
                {show("MulExp <- UnaryExp");
	(yyval.node) = (yyvsp[0].node);
}
#line 2205 "parser.cpp"
    break;

  case 79:
#line 529 "parser.y"
                                        {show("MulExp T_OP_TIM");
		(yyval.node) = new AST::Exp_Binary(AST::OP::OP_TIM, (yyvsp[-2].node), (yyvsp[0].node));
	}
#line 2213 "parser.cpp"
    break;

  case 80:
#line 532 "parser.y"
                                        {show("MulExp T_OP_DIV");
		(yyval.node) = new AST::Exp_Binary(AST::OP::OP_DIV, (yyvsp[-2].node), (yyvsp[0].node));
	}
#line 2221 "parser.cpp"
    break;

  case 81:
#line 535 "parser.y"
                                        {show("MulExp T_OP_MOD");
		(yyval.node) = new AST::Exp_Binary(AST::OP::OP_MOD, (yyvsp[-2].node), (yyvsp[0].node));
	}
#line 2229 "parser.cpp"
    break;

  case 82:
#line 540 "parser.y"
              {
	show("AddExp <- MulExp");
}
#line 2237 "parser.cpp"
    break;

  case 83:
#line 543 "parser.y"
                                      {show("AddExp T_OP_ADD");
		(yyval.node) = new AST::Exp_Binary(AST::OP::OP_ADD, (yyvsp[-2].node), (yyvsp[0].node));
	}
#line 2245 "parser.cpp"
    break;

  case 84:
#line 546 "parser.y"
                                      {show("AddExp T_OP_MIN");
		(yyval.node) = new AST::Exp_Binary(AST::OP::OP_MIN, (yyvsp[-2].node), (yyvsp[0].node));
	}
#line 2253 "parser.cpp"
    break;

  case 85:
#line 551 "parser.y"
               {
	show("RelExp <- AddExp");
}
#line 2261 "parser.cpp"
    break;

  case 86:
#line 554 "parser.y"
                                     {show("RelExp T_OP_LT");
		(yyval.node) = new AST::Exp_Binary(AST::OP::OP_LT, (yyvsp[-2].node), (yyvsp[0].node));
	}
#line 2269 "parser.cpp"
    break;

  case 87:
#line 557 "parser.y"
                                     {show("RelExp T_OP_GT");
		(yyval.node) = new AST::Exp_Binary(AST::OP::OP_GT, (yyvsp[-2].node), (yyvsp[0].node));
	}
#line 2277 "parser.cpp"
    break;

  case 88:
#line 560 "parser.y"
                                     {show("RelExp T_OP_LE");
		(yyval.node) = new AST::Exp_Binary(AST::OP::OP_LE, (yyvsp[-2].node), (yyvsp[0].node));
	}
#line 2285 "parser.cpp"
    break;

  case 89:
#line 563 "parser.y"
                                     {show("RelExp T_OP_GE");
		(yyval.node) = new AST::Exp_Binary(AST::OP::OP_GE, (yyvsp[-2].node), (yyvsp[0].node));
	}
#line 2293 "parser.cpp"
    break;

  case 90:
#line 568 "parser.y"
              {
	show("EqExp <- RelExp");
}
#line 2301 "parser.cpp"
    break;

  case 91:
#line 571 "parser.y"
                                    {show("EqExp T_OP_EQ");
		(yyval.node) = new AST::Exp_Binary(AST::OP::OP_EQ, (yyvsp[-2].node), (yyvsp[0].node));
	}
#line 2309 "parser.cpp"
    break;

  case 92:
#line 574 "parser.y"
                                    {show("EqExp T_OP_NE");
		(yyval.node) = new AST::Exp_Binary(AST::OP::OP_NE, (yyvsp[-2].node), (yyvsp[0].node));
	}
#line 2317 "parser.cpp"
    break;

  case 93:
#line 579 "parser.y"
                     {
	show("LAndExp <- EqExp");
}
#line 2325 "parser.cpp"
    break;

  case 94:
#line 582 "parser.y"
                                      {show("LAndExp T_OP_AND");
		(yyval.node) = new AST::Exp_Binary(AST::OP::OP_AND, (yyvsp[-2].node), (yyvsp[0].node));
	}
#line 2333 "parser.cpp"
    break;

  case 95:
#line 587 "parser.y"
               {
		show("LOrExp <- LAndExp");
	}
#line 2341 "parser.cpp"
    break;

  case 96:
#line 590 "parser.y"
                                      {show("LOrExp T_OP_OR");
		(yyval.node) = new AST::Exp_Binary(AST::OP::OP_OR, (yyvsp[-2].node), (yyvsp[0].node));
	}
#line 2349 "parser.cpp"
    break;

  case 97:
#line 595 "parser.y"
                      {show("ConstExp");
	(yyval.node) = (yyvsp[0].node);
	(yyval.node)->isConst = true;
}
#line 2358 "parser.cpp"
    break;


#line 2362 "parser.cpp"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = YY_CAST (char *, YYSTACK_ALLOC (YY_CAST (YYSIZE_T, yymsg_alloc)));
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;


#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif


/*-----------------------------------------------------.
| yyreturn -- parsing is finished, return the result.  |
`-----------------------------------------------------*/
yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[+*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
