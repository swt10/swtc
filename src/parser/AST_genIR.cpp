#include <memory>

#include "AST.h"
#include "IRBuilderTool.h"
#include "IRhead.h"

#define COLOR_RESET "\033[0m"
#define COLOR_BLACK "\033[30m"              /* Black */
#define COLOR_RED "\033[31m"                /* Red */
#define COLOR_GREEN "\033[32m"              /* Green */
#define COLOR_YELLOW "\033[33m"             /* Yellow */
#define COLOR_BLUE "\033[34m"               /* Blue */
#define COLOR_MAGENTA "\033[35m"            /* Magenta */
#define COLOR_CYAN "\033[36m"               /* Cyan */
#define COLOR_WHITE "\033[37m"              /* White */
#define COLOR_BOLDBLACK "\033[1m\033[30m"   /* Bold Black */
#define COLOR_BOLDRED "\033[1m\033[31m"     /* Bold Red */
#define COLOR_BOLDGREEN "\033[1m\033[32m"   /* Bold Green */
#define COLOR_BOLDYELLOW "\033[1m\033[33m"  /* Bold Yellow */
#define COLOR_BOLDBLUE "\033[1m\033[34m"    /* Bold Blue */
#define COLOR_BOLDMAGENTA "\033[1m\033[35m" /* Bold Magenta */
#define COLOR_BOLDCYAN "\033[1m\033[36m"    /* Bold Cyan */
#define COLOR_BOLDWHITE "\033[1m\033[37m"   /* Bold White */

#define COLOR_genIR " -------------------------- "

#define ADD_PRED_SUCC(p, s)   \
    {                         \
        p->succ.push_back(s); \
        s->pred.push_back(p); \
    }

bool isBr() {
    if (CONTEXT::bb_cur->getTerminator()) {
        if (
            isa<BranchInst>(CONTEXT::bb_cur->getTerminator()) || isa<ReturnInst>(CONTEXT::bb_cur->getTerminator())) {
            return true;
        }
    }
    return false;
}

void AST::PrintNothing::genIR() {
    return;
}

void AST::ArraySub::genIR() {
    return;
}

void AST::CompUnit::genIR() {
    CONTEXT::addExternFunc();
    for (auto iter : contents) {
        CONTEXT::isGlobal = true;
        iter->genIR();
        CONTEXT::isGlobal = false;
    }
}

void AST::Decl::genIR() {
    CONTEXT::isConst = isConst;  // not essential actually
    defs->genIR();
    CONTEXT::isConst = false;
}

void AST::Defs::genIR() {
    for (auto iter : defs) {
        iter->genIR();
    }
}

// void getExpDfs(std::vector<AST_EXP::Exp *> &tmpVals, AST_EXP::Exp *now) {
//     if (now->isJustArr()) {
//         for (AST_EXP::Exp *x : now->vecs) {
//             getExpDfs(tmpVals, x);
//         }
//     } else {
//         tmpVals.emplace_back(now);
//     }
// }

AST::InitVal *extendInitVal(
    AST::InitVal *e,
    const std::vector<int> &subs,
    int depth) {
    if (e->isVar()) {
        return e;
    }
    // 第一次进来时是数组，因此 initSelf == nullptr
    int size = 1;
    int need = 1;

    for (int len = subs.size(), i = depth; i < len; i++) {
        size *= subs.at(i);
    }
    // 考虑 int a[2][3] = {{{{b}}}}
    if (depth < subs.size()) {
        need = size / subs.at(depth);
    }

    AST::InitVal *ans = new AST::InitVal();

    int cnt = 0;
    for (auto x : e->initVals) {
        ans->add(extendInitVal(x, subs, depth + 1));
        if (x->isVar()) {
            cnt++;
        } else {
            cnt += need;
        }
    }

    for (; cnt < size; cnt++) {
        auto tmp = new AST::InitVal();
        tmp->setSelf(new AST::Exp_Num(0));
        ans->add(tmp);
    }

    return ans;
}

void AST::Def::genIR() {
    /*std::cerr << COLOR_BOLDCYAN << COLOR_genIR << "def begin " << *ident << "\n";*/
    if (CONTEXT::isGlobal) {
        // 全局定义
        // 全局定义右侧必可求值，若未初始化则默认初始化 0
        if (isVar()) {
            // 变量
            int tmpVal = isInit ? CONTEXT::getConstVal(initVal->getVar()) : 0;

            // const （现不区分)
            // if (CONTEXT::isConst) {
            //     CONTEXT::updateGlobalConst(
            //         *ident,
            //         tmpVal);
            // }

            CONTEXT::updateGlobalConst(
                *ident,
                tmpVal);

            CONTEXT::addGlobalVar(
                *ident,
                tmpVal);
        } else {
            /*std::cerr << COLOR_BOLDCYAN << COLOR_genIR << "Def Global Array\n";*/
            // 数组
            std::vector<int> tmpSubs;
            for (auto iter : subs->getSubs()) {
                /*std::cerr << COLOR_BOLDCYAN << COLOR_genIR << "tmpSubs mid\n";*/
                tmpSubs.emplace_back(
                    CONTEXT::getConstVal(iter));
            }
            /*std::cerr << COLOR_BOLDCYAN << COLOR_genIR << "tmpSubs end\n";*/

            std::vector<int> tmpVals;
            if (isInit) {
                // auto tmpInitVal = std::make_unique<AST::InitVal>(extendInitVal(initVal, tmpSubs));
                AST::InitVal *tmpInitVal = extendInitVal(initVal, tmpSubs, 0);
                AST_EXP::Exp *tmpExps = tmpInitVal->genExp();
                delete tmpInitVal;

                if (tmpExps->isJustArr()) {
                    for (auto i : tmpExps->vecs) {
                        tmpVals.emplace_back(
                            CONTEXT::getConstVal(i));
                    }
                } else {
                    /*std::cerr << COLOR_BOLDCYAN << COLOR_genIR << "initVals: is there some other situation?\n";*/
                }

                // for(auto iter: initVal->initVals){
                //     tmpVals.emplace_back(
                //         CONTEXT::getConstVal(
                //             iter->genExp()
                //         )
                //     );
                // }
            }

            ArrayInfo tmpArray(tmpSubs, tmpVals);
            /*std::cerr << COLOR_BOLDCYAN << COLOR_genIR << "tmpVals end\n";*/
            // const （现不区分)
            // if (CONTEXT::isConst) {
            //     CONTEXT::updateGlobalConst(
            //         *ident,
            //         tmpArray);
            // }

            CONTEXT::updateGlobalConst(
                *ident,
                tmpArray);

            CONTEXT::addGlobalArray(
                *ident,
                tmpArray);
        }
    } else {
        // 局部定义
        if (isVar()) {
            // 变量

            // const
            if (CONTEXT::isConst) {
                CONTEXT::updateGlobalConst(
                    *ident,
                    isInit ? CONTEXT::getConstVal(initVal->getVar()) : 0);
            }

            CONTEXT::addLocalVar(
                *ident,
                isInit,
                isInit ? initVal->getVar() : nullptr);
        } else {
            logError("localArray add begin");
            // 数组
            std::vector<int> tmpSubs;
            for (auto iter : subs->getSubs()) {
                tmpSubs.emplace_back(
                    CONTEXT::getConstVal(iter));
            }

            AST::InitVal *tmpInitVal = nullptr;
            AST_EXP::Exp *tmpExps = nullptr;
            if (isInit) {
                tmpInitVal = extendInitVal(
                    initVal, tmpSubs, 0);
                tmpExps = tmpInitVal->genExp();
            }

            if (CONTEXT::isConst) {
                std::vector<int> tmpVals;
                // 求出常量值
                if (tmpExps->isJustArr()) {
                    for (auto i : tmpExps->vecs) {
                        tmpVals.emplace_back(
                            CONTEXT::getConstVal(i));
                    }
                } else {
                    /*std::cerr << COLOR_BOLDCYAN << COLOR_genIR << "initVals: is there some other situation?\n";*/
                }

                ArrayInfo tmpArray(tmpSubs, tmpVals);

                // 生成 tmpArray;
                CONTEXT::updateGlobalConst(
                    *ident,
                    tmpArray);
            }

            CONTEXT::addLocalArray(*ident, tmpSubs, tmpExps?(tmpExps->vecs):(std::vector<AST_EXP::Exp*>()));
        }
    }
}

void AST::InitVal::genIR() {
    return;
}

void AST::FuncDef::genIR() {
    CONTEXT::isGlobal = false;

    // 函数参数表
    /*std::cerr << COLOR_BOLDCYAN << COLOR_genIR << "Function:\t " << *ident << "\n";*/
    std::vector<AST_EXP::Exp *> fParams;

    if (hadParam()) {
        for (auto x : (funcFParams->fParams)) {
            if (x->isVar()) {
                // 变量
                /*std::cerr << COLOR_BOLDWHITE 
                << "param(var)\t: " << *(x->ident) <<"\n";*/
                fParams.emplace_back(new AST_EXP::Exp(*(x->ident)));
            } else {
                /*std::cerr << COLOR_BOLDWHITE 
                << "param(array):\t " << *(x->ident) <<"\n";*/
                // 一维数组 || 多维数组
                // 一维数组返回空vector
                fParams.emplace_back(new AST_EXP::Exp(*(x->ident), x->subs->getSubs()));
            }
        }
    }

    CONTEXT::addFunction(
        funcType == AST::FuncType::typeVoid ? 1 : 0,
        *ident,
        false,
        fParams);
    /*std::cerr << COLOR_BOLDCYAN << COLOR_genIR << "Function End!\n";*/

    std::string funcEntryName = *ident;
    funcEntryName += "_entry";

    CONTEXT::bb_cur = new BasicBlock(funcEntryName, CONTEXT::func_cur);
    CONTEXT::st.getInto();
    CONTEXT::addFuncParams(fParams);

    funcBody->genIR();
    CONTEXT::st.getBack();

    auto lastIns = CONTEXT::func_cur->getBasicBlockList().getLast()->getInstList().getLast();
    /*std::cerr << COLOR_BOLDGREEN 
    << "last bb in function " + *ident + " in bb " + 
    CONTEXT::func_cur->getBasicBlockList().getLast()->getName() + " is " + 
    (lastIns ? lastIns->getName() : "nullptr") + "\n";*/
    if (lastIns == nullptr || !(isa<ReturnInst>(lastIns))) {
        // 函数基本块列表不可能为空

        /*std::cerr << COLOR_BOLDCYAN << COLOR_genIR << "add a return in " << (*ident) << "\n";*/
        new ReturnInst(
            funcType == FuncType::typeInt ? ConstantInt::getZeroVal() : nullptr,
            CONTEXT::func_cur->getBasicBlockList().getLast());
    }

    CONTEXT::isGlobal = true;
}

void AST::Block::genIR() {
    CONTEXT::st.getInto();

    /*std::cerr << COLOR_BOLDCYAN << COLOR_genIR << "block \n";*/

    for (auto x : blockItems) {
        /*std::cerr << COLOR_BOLDCYAN << COLOR_genIR << "blockItems begin\n";*/
        if (isBr()) {
            /*std::cerr << COLOR_BOLDCYAN << COLOR_genIR << "blockItems br or ret so breaked\n";*/
            break;
        }

        x->genIR();

        /*std::cerr << COLOR_BOLDCYAN << COLOR_genIR << "blockItems end\n";*/
    }

    CONTEXT::st.getBack();
}

void AST::Stmt_If::genIR() {
    BasicBlock *br1, *br2, *bb_next;
    if (hadElse()) {
        /*std::cerr << COLOR_BOLDCYAN << COLOR_genIR << "if else \n";*/
        // BasicBlock *br1, *br2, *bb_next;
        // 倒着 new 以保持顺序正确
        bb_next = new BasicBlock("if_" + std::to_string(CONTEXT::ifCnt) + "_next", CONTEXT::func_cur, CONTEXT::bb_cur->next);
        br2 = new BasicBlock("if_" + std::to_string(CONTEXT::ifCnt) + "_else", CONTEXT::func_cur, CONTEXT::bb_cur->next);
        br1 = new BasicBlock("if_" + std::to_string(CONTEXT::ifCnt) + "_then", CONTEXT::func_cur, CONTEXT::bb_cur->next);
        CONTEXT::ifCnt++;

        // ADD_PRED_SUCC(br1, bb_next);
        // ADD_PRED_SUCC(br2, bb_next);

        new BranchInst(
            br1,
            br2,
            new ICmpInst(ICmpInst::ICMP_NE, CONTEXT::getExpValue(cond->genExp()), ConstantInt::getFalse(), "ifbricmp", CONTEXT::bb_cur),
            CONTEXT::bb_cur);

        // cond 内有短路，所以可能有多个基本块。在 cond->getExp() 之后再添加前驱后继
        ADD_PRED_SUCC(CONTEXT::bb_cur, br1)
        ADD_PRED_SUCC(CONTEXT::bb_cur, br2)

        CONTEXT::bb_cur = br1;
        stmt_1->genIR();

        if (isBr()) {
        } else {
            ADD_PRED_SUCC(CONTEXT::bb_cur, bb_next)
            new BranchInst(bb_next, CONTEXT::bb_cur);
        }

        CONTEXT::bb_cur = br2;
        stmt_2->genIR();

        if (isBr()) {
        } else {
            ADD_PRED_SUCC(CONTEXT::bb_cur, bb_next);
            new BranchInst(bb_next, CONTEXT::bb_cur);
        }
    }

    else {
        /*std::cerr << COLOR_BOLDCYAN << COLOR_genIR << "if no else \n";*/
        // BasicBlock *br1, *bb_next;
        bb_next = new BasicBlock("if_" + std::to_string(CONTEXT::ifCnt) + "_next", CONTEXT::func_cur, CONTEXT::bb_cur->next);

        br1 = new BasicBlock("if_" + std::to_string(CONTEXT::ifCnt) + "_then", CONTEXT::func_cur, CONTEXT::bb_cur->next);
        CONTEXT::ifCnt++;

        // /*std::cerr << COLOR_BOLDCYAN << COLOR_genIR << "new branchInst \n";*/

        new BranchInst(
            br1,
            bb_next,
            CONTEXT::getExpValue(cond->genExp()),
            CONTEXT::bb_cur);

        ADD_PRED_SUCC(CONTEXT::bb_cur, br1)
        ADD_PRED_SUCC(CONTEXT::bb_cur, bb_next);

        // /*std::cerr << COLOR_BOLDCYAN << COLOR_genIR << "new branchInst end \n";*/

        CONTEXT::bb_cur = br1;
        stmt_1->genIR();

        if (isBr()) {
        } else {
            ADD_PRED_SUCC(CONTEXT::bb_cur, bb_next);
            new BranchInst(bb_next, CONTEXT::bb_cur);
        }
    }
    CONTEXT::bb_cur = bb_next;
}

void AST::Stmt_While::genIR() {
    BasicBlock *bb_cond, *bb_loop, *bb_next;
    // 倒着 new 以保持顺序正确
    bb_next = new BasicBlock("while_" + std::to_string(CONTEXT::whileCnt) + "_next", CONTEXT::func_cur, CONTEXT::bb_cur->next);
    bb_loop = new BasicBlock("while_" + std::to_string(CONTEXT::whileCnt) + "_loop", CONTEXT::func_cur, CONTEXT::bb_cur->next);
    bb_cond = new BasicBlock("while_" + std::to_string(CONTEXT::whileCnt) + "_cond", CONTEXT::func_cur, CONTEXT::bb_cur->next);

    ADD_PRED_SUCC(CONTEXT::bb_cur, bb_cond)
    new BranchInst(bb_cond, CONTEXT::bb_cur);

    CONTEXT::whileCnt++;
    CONTEXT::bb_cur = bb_cond;
    new BranchInst(
        bb_loop,
        bb_next,
        CONTEXT::getExpValue(cond->genExp()),
        CONTEXT::bb_cur);

    ADD_PRED_SUCC(CONTEXT::bb_cur, bb_loop);
    ADD_PRED_SUCC(CONTEXT::bb_cur, bb_next);

    CONTEXT::bb_cur = bb_loop;
    CONTEXT::bb_first_loop.emplace_back(bb_cond);
    CONTEXT::bb_after_loop.emplace_back(bb_next);
    stmt_1->genIR();
    CONTEXT::bb_first_loop.pop_back();
    CONTEXT::bb_after_loop.pop_back();

    if (isBr()) {
    } else {
        ADD_PRED_SUCC(CONTEXT::bb_cur, bb_cond)
        new BranchInst(bb_cond, CONTEXT::bb_cur);
    }

    CONTEXT::bb_cur = bb_next;
    //
}

void AST::Stmt_Ass::genIR() {
    // 在当前块插入一条赋值指令
    std::vector<AST_EXP::Exp *> tmpSubs;
    if (!lval->isVar()) {
        for (auto x : (lval->subs->subscripts)) {
            tmpSubs.emplace_back(x->genExp());
        }
    }

    /*std::cerr << COLOR_BOLDCYAN << COLOR_genIR << "ass\n"
              << *(lval->ident) << "\n";*/

    CONTEXT::addAssign(
        *(lval->ident),
        tmpSubs,
        lval->isVar(),
        rval->genExp());
}

void AST::Stmt_Break::genIR() {
    logError("break");

    ADD_PRED_SUCC(CONTEXT::bb_cur, CONTEXT::bb_after_loop.back())
    new BranchInst(
        CONTEXT::bb_after_loop.back(),
        CONTEXT::bb_cur);
}

void AST::Stmt_Continue::genIR() {
    logError("continue");

    ADD_PRED_SUCC(CONTEXT::bb_cur, CONTEXT::bb_first_loop.back())
    new BranchInst(
        CONTEXT::bb_first_loop.back(),
        CONTEXT::bb_cur);
}

void AST::Stmt_Return::genIR() {
    if (hadRetVal()) {
        /*std::cerr << COLOR_BOLDCYAN << COLOR_genIR << "return \n";*/
        new ReturnInst(CONTEXT::getExpValue(retVal->genExp()), CONTEXT::bb_cur);

    } else {
        new ReturnInst(CONTEXT::bb_cur);
    }
}

void AST::Stmt_Block::genIR() {
    block->genIR();
}

void AST::Stmt_Exp::genIR() {
    /*
        单独的，不需要返回值的 Exp 调用只出现在这里
        也就是这里是唯一不会被 CONTEXT::getExpValue(exp) 的 exp 出现；
    */
    if (isEmpty()) {
        /*std::cerr << COLOR_BOLDCYAN << COLOR_genIR << "empty stmt \n";*/
    } else {
        CONTEXT::getExpValue(exp->genExp());
    }
}

void AST::LVal::genIR() {
    /*
        Lval 在推导右边只出现在两处：
        1. Stmt -> Lval '=' Exp ';'
        2. Exp - ... -> PrimaryExp -> Lval 

        前者会在赋值过程调用, 后者会在父 exp 求值（CONTEXT::getExpValue())时被调用
    */
    return;
}

void AST::Exp_Func::genIR() {
    return;
}

void AST::Exp_Num::genIR() {
    return;
}

void AST::Exp_Paren::genIR() {
    return;
}

void AST::Exp_LVal::genIR() {
    return;
}

void AST::Exp_Unary::genIR() {
    return;
}

void AST::Exp_Binary::genIR() {
    return;
}

void AST::FuncFParams::genIR() {
    return;
}

void AST::FuncRParams::genIR() {
    return;
}

void AST::FuncFParam::genIR() {
    return;
}
