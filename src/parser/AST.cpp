#include "AST.h"

#include "iostream"

AST::ASTBasic::~ASTBasic() {}
void AST::ASTBasic::print() { std::cerr << "nmsl\n"; }
void AST::ASTBasic::add(AST::ASTBasic *para) {}
void AST::ASTBasic::genIR() {}
AST_EXP::Exp *AST::ASTBasic::genExp() {}

AST::CompUnit *root = new AST::CompUnit();

// 是否是变量（不是数组）
bool AST::ArraySub::isVar() { return subscripts.empty(); }

// 是否是变量（不是数组）
bool AST::Def::isVar() { return subs->isPrintNothing; }

// 是否是变量（不是数组）
bool AST::LVal::isVar() { return subs->isPrintNothing; }

// 是否是变量（不是数组）
bool AST::FuncFParam::isVar() { return subs->isPrintNothing; }

// 是否有参数
bool AST::FuncDef::hadParam() { return !(funcFParams->isPrintNothing); }

bool AST::Exp_Func::hadParam() { return !(funcRParams->isPrintNothing); }

// if是否有else
bool AST::Stmt_If::hadElse() { return !(stmt_2->isPrintNothing); }

// return 是否有返回值
bool AST::Stmt_Return::hadRetVal() { return !(retVal->isPrintNothing); }

bool AST::Stmt_Exp::isEmpty() { return this->exp->isPrintNothing; }

AST_EXP::Exp *AST::InitVal::getVar() { return selfInitVal->genExp(); }
void AST::InitVal::setSelf(AST::ASTBasic *_selfInitVal) {
    selfInitVal = static_cast<AST::Exp*>(_selfInitVal);
}
bool AST::InitVal::isVar(){return selfInitVal != nullptr;}

std::vector<AST_EXP::Exp *> AST::ArraySub::getSubs() {
    std::vector<AST_EXP::Exp *> tmp;
    for (auto iter : subscripts) {
        tmp.emplace_back(iter->genExp());
    }
    return tmp;
}
