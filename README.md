
# 绝赞开发中...

in design, the command of this compiler is:
```
compiler [-x] [-o output_file_path] input_file_path
```

argument `[-x]` is for debuging, `-x` could be as follows:

`-a` means `print out the AST into ast.json`

`-r` means `print out the readable ir file into ir.ll`

`-nr` means `don't gen AST to IR`

`-ri` means `print instrutions in ir.ll(if exist)`

`-rb` means `print BasicBlocks in ir.ll(if exist)`

`-rp` means `print BB's pred and succ in ir.ll(if exist)`


```mermaid
gantt         
    dateFormat  MM-DD   
    title TIMELINE
    section OUTLINE
    AST         :done,      desAST,         06-27,          1d
    IR          :active,    desIR,          06-28,          3d
    IRPRINTER   :           desIRPrinter,   after desIR,    6d 
    IRBUILDER   :           desIRBuilder,   after desIR,    7d
    PASSMANAGER :           desIRManager,   after desIR,    2d
    PASSES      :           desPasses,      after desIRManager, 10d
    CODEGEN     :           desCodeGen,     after desIR,    7d
    TESTER      :done,      desTester,      06-28,          1d
```

