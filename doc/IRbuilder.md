# IR构造指南

--------------------------

## 1.上下文

Module里的符号表


## 2.全局常量

放进符号表中，无需其他操作

---

## 3.函数

这里要先定义一个BasicBlock
将其放入Function中

定义上下文，记录当前函数返回类型和参数列表

### 3.1参数处理

>(1)不带数组
申请Tyint32空间

>(2)带数组 不用申请空间

调用

```c++
FunctionType::get(返回类型，参数类型vector)得到一个完整的函数类型
```

```c++
Function()
```

生成函数对象

### 3.2文段处理



### 3.3处理异常

若函数没有返回操作手动添加

```c++
new ReturnInst
```

### 3.4构造数据流

为每个BasicBlock重新设置它可能的前驱与后继


## 4 文段

### 4.1声明局部变量

```c++
AllocaInst
```

>若有初始值
>>单个值

直接分析表达式然后

```c++
StoreInst
```

>>多个值

对每个表达式convert返回Value
然后对刚刚申请的每个空间

```c++
StoreInst
```


```c++

```

### 4.2 赋值语句

先计算维度同上
然后计算右值表达式
>若单个值

```c++
Storeinst
```

>数组

调用

```c++
GetElementPtrInst//倒着
```

最后

```c++
Storeinst
```

### 4.3IF条件句

cond用表达式返回Value
生成三个BasicBlock：then,else,end

```c++
BranchInst cond then else
```

分别文段分析，
分析完后若不以终止指令结尾则手动添加

```
br end
```

### 4.4while

同上 四个BasicBlock cond1 loop cond2 end
添加对当前BasicBlock 的br->Cond1
然后Br->loop|end
loop->cond2
br->loop|end

### 4.5block

直接进，没啥好说的。
block下面很多stmt
遇到Continue,Break,Return 直接break,后面的没用

### 4.6Return 

分析value的表达式然后

```c++
ReturnInst
```

### 4.7Continue/Break

开个vector当栈记一下循环头或者end，Br就行了

cond1->->loop>

```c++

```

## 5 表达式

### 5.1二目

左操作数同样套表达式计算
注意一点 ARM没有取模得自己手动算
$a-b*a/b$
三条指令解决
算条件语句的时候分个块 两个BasicBlock rhs,after

And op 

```c++
br lhs rhs end
```

Or op 

```c++
br lhs==0 rhs end
```

这样少许优化

右操作数算完br end

end上加一个PHInode

```c++
new 
```

### 5.2 常量

```c++
ConstInt::get
```

### 5.3 数值

先看是不是数组
是的话分别求表达式
然后
>不是数组

```c++
LoadInst
```

>是数组//维数与实际相同

倒着迭代

```c++
GetElementPtrInst()
```

最后

```c++
LoadInst()
```

```c++
int a[10][2]={1,2,3,4,5,6};

左值 <vector>l <10,2>



```

>是数组//维数不同，返回指针

同上最后不用`LoadInst`

>是数组//没有维数

`GetElementPtrInst`一次返回

### 5.4Call

分别求一下参数值，`CallInst`    


# 拾遗

## 1.数组相关

数组在IR中有一个专用类型ArrayType
它的构建函数是这样的

```c++
ArrayType(Type *ElType, uint64_t NumE1);//其中ElType是数组元素类型，NumE1是元素个数
```

如果想要生成int [10]的类型，就这样

```c++
Type *ty=new ArrayType(Type::Int32Ty,10);
```

如果想要生成int [2][10]的类型，就这样

```c++
Type *ty=new ArrayType(Type::Int32Ty,10);
ty = new ArrayType(ty,2);
```

...以此类推

数组类型不是唯一的，由于实现，就算是两个元素类型和大小都一样的Type，也不会一样

### 1.数组声明

数组的声明首先要构造出它的类型，然后用这个类型去Alloca,如下

```c++
//int a[2][10]
BasicBlock *bb;
Type *ty=new ArrayType(Type::Int32Ty,10);
ty = new ArrayType(ty,2);
Value *a = new AllocaInst(ty,"",bb);
```

如果要初始化数组，看下面的例子

```c++
//int a[2][4]={1,2,3,4,5,6,7}根据规则数组应该是这样的
//a={{1,2,3,4},{5,6,7,0}}
BasicBlock *bb;
Type *ty=new ArrayType(Type::Int32Ty,4);
ty = new ArrayType(ty,2);//注意要倒着new
Value *a = new AllocaInst(ty,"a",bb);
Value *Ptr = new GetElementPtrInst(a,{ConstantInt::getZeroVal()},"",bb);
//这里相当于取a[0],Prt的类型变成了int [4]
for(int i=0;i<a.dim(0);i++)//a.dim(0)是a第一维大小，这里是两个
{
    if(i!=0)
    {
        Value *p = new GetElementPtrInst(Ptr,{ConstantInt::get(i)},"",bb);//这里取a的每一维的指针
        new StoreInst(b[i],p,bb);//这里b[i]是一个int[4]类型
    }
    else{
        new StoreInst(b[i],p,bb);//这里b[i]是一个int[4]类型
    }
}
```

### 2.函数声明

函数也有自己的类型`FunctionType`,构造函数如下:

```c++
FunctionType(Type *Result, std::vector<Type*>&Params, bool isVarArg);//第一个是函数返回类型，第二个是参数类型，第三个是是否有参数
```

当函数参数中有数组类型时,其参数都为指针类型，例如

```c++
//void f(int a[], int b[][10])
//a的参数类型为int *,b的参数类型为 int *[10]
Type *Ret=Type::VoidTy;
Type *a1=Type::Int32PtrTy;
Type *a2=new PointerType(new ArrayType(Type::Int32Ty,10));
Type *f=new FunctionType(Ret,std::vector<Type*>(a1,a2),true);
```