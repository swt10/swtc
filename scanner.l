%{
    #define  YY_NO_UNISTD_H
	#include "config.h"
	#include "AST.h"
	#include "parser.hpp"

    yyFlexLexer lexer;

    int yylex(void){
        return lexer.yylex();
    }

    void yyFlexLexer::yyset_lineno(int lineno){
        this->yylineno = lineno;
    }

    int yyFlexLexer::yyget_lineno(){
        return this->yylineno;
    }

%}


%option yylineno
%option noyywrap
/*%option header-file="scanner.lex.h"*/



SPACE       [[:space:]\t\f\v\a\b]+
COMMENT     ("//"[^\n\r]*(\n|\r))
LAZY_MULTI_COMMENT  "/*"(?:[^"*"]|"*"+[^"/*"])*"*"+"/"
IDENT		[a-zA-Z_][a-zA-Z0-9_]*


INT_DEC 	0|[1-9][0-9]*
INT_OCT		0[0-7]+
INT_HEX		(0x|0X)[0-9a-fA-F]+



%%


{COMMENT}	          ;
{LAZY_MULTI_COMMENT}  ;

"void"  return KEY_VOID;
"int"	return KEY_INT;
"const"	return KEY_CONST;

"if"	return KEY_IF;
"else"	return KEY_ELSE;
"while"	return KEY_WHILE;
"break"	return KEY_BREAK;
"continue"	return KEY_CONTINUE;
"return"	return KEY_RETURN;


{IDENT}	{
	
	yylval.str = new std::string(yytext);

	return TOK_IDENT;
}

{INT_DEC}	{
	// return int
	sscanf(yytext, "%d", &(yylval.num));
	return INT_DEC;
}
{INT_OCT}	{
	// return int
	sscanf(yytext, "%o", &(yylval.num));
	return INT_OCT;
}
{INT_HEX}	{
	// return int
	sscanf(yytext, "%x", &(yylval.num));
	return INT_HEX;
}

"=="	return T_OP_EQ;
"!="	return T_OP_NE;
"<="	return T_OP_LE;
">="	return T_OP_GE;
"<"		return T_OP_LT;
">"		return T_OP_GT;

"!"		return T_OP_NOT;
"&&"	return T_OP_AND;
"||"	return T_OP_OR;

"+"		return T_OP_ADD;
"-"		return T_OP_MIN;
"*"		return T_OP_TIM;
"/"		return T_OP_DIV;
"%"		return T_OP_MOD;

";"		return END_LINE;

"("		return *yytext;
")"		return *yytext;
"["		return *yytext;
"]"		return *yytext;
"{"		return *yytext;
"}"		return *yytext;

","		return *yytext;
"="		return *yytext;





.	{
	std::cerr
	<<	"error: unexpected character \"" << yytext
	<<	"\" in line: " << yylineno << std::endl;
}



%%
