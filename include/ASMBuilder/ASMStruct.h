#pragma once
#include <algorithm>
#include <string>
#include <vector>

const std::string CM = ", ";
const std::string ENDL = "\n";
const std::string INDENT4 = "    ";
const std::string INDENT8 = "        ";
const std::string GLOBAL_VARS = "GLOBAL_VARS";
const int TEMP_REG_ID = 11;
const int IMM_16_MAX = 65535;
const int IMM_12_MAX = 4095;
const int IMM_8_MAX = 255;
const int INST_COUNTER_LIMIT = 800;

const int OP_REG_0 = 12;
const int OP_REG_1 = 14;
const int OP_REG_2 = 11;

const std::string f8(std::string s);

const std::string format(std::string s, int count);

enum class Cond {
    /* 
        Condition Prototype 
    */
    AL,
    EQ,  // ==
    NE,  // !=
    /* Signed CMP */
    GE,  // >=
    GT,  // >
    LE,  // <=
    LT,  // <
    /* Unsigned CMP */
    HS,  // >=
    LO,  // <
    HI,  // >
    LS,  // <=
};
std::string to_string(Cond condition);

enum class SP {
    /* 
        Shift Prototype 
    */
    NOS,  // NO SHIFT
    ASR,
    LSL,
    LSR,
    ROR,
    RRX
};

enum class SectionType {
    Data,
    Text
};

enum class SymbolType {
    Function,
    GNUIndirectFunction,
    Object,
    TLSObject,
    Common,
    NoType,
    GNUUniqueObject
};

class Register {
public:
    enum RP {  // Register Prototype
        Custom,
        R0,
        R1,
        R2,
        R3,
        R4,
        R5,
        R6,
        R7,
        R8,
        R9,
        R10,
        R11,
        R12,
        R13,
        R14,
        R15,
        FP = R11,
        IP = R12,
        SP = R13,
        LR = R14,
        PC = R15
    } register_prototype;

    int register_id;

    Register(RP register_prototype);
    Register(int register_id);
    inline bool isFP();
    inline bool isIP();
    inline bool isSP();
    inline bool isLR();
    inline bool isPC();
    inline bool isCustom();

    int getID() const;
    RP getPrototype() const;

    void setRegisterID(int register_id);

    operator std::string() const;
    const bool operator<(const Register &reg) const { return this->getID() < reg.getID(); }
    const bool operator==(const Register &reg) const { return this->getID() == reg.getID(); }
    const bool operator!=(const Register &reg) const { return this->getID() != reg.getID(); }

    static RP getRegisterPrototypeByID(int register_id);
    static int getIDByRegisterPrototype(RP register_prototype);  // Default Custom Register ID is -1
};

class Directive {
    std::vector<std::string> container;

    Directive(std::vector<std::string> c) : container(c) {}

public:
    /*  ARM Directives  */

    static Directive arch(std::string architecture = "armv7-a") {
        return Directive({".arch", architecture});
    }
    static Directive pool() {
        return Directive({".pool"});
    }
    static Directive align(int alignment = 0, int fill_value = 0) {
        return Directive({".align", std::to_string(alignment) + ", " + std::to_string(fill_value)});
    }
    static Directive align2() {
        return Directive({".align", std::to_string(2)});
    }
    static Directive arm() { return Directive({".arm"}); }
    static Directive fpu(std::string fpu_type) { return Directive({".fpu", fpu_type}); }

    /*  GNU Directives  */

    static Directive section(SectionType section_type) {
        switch (section_type) {
            case SectionType::Data: return Directive({".section", ".data"}); break;
            case SectionType::Text: return Directive({".section", ".text"}); break;
        }
    }
    static Directive type(std::string symbol_name, SymbolType symbol_type) {
        std::string symbol_type_string;
        switch (symbol_type) {
            case SymbolType::Common: symbol_type_string = "common";
            case SymbolType::Function: symbol_type_string = "function";
            case SymbolType::GNUIndirectFunction: symbol_type_string = "gnu_indirect_function";
            case SymbolType::GNUUniqueObject: symbol_type_string = "gnu_unique_object";
            case SymbolType::NoType: symbol_type_string = "notype";
            case SymbolType::Object: symbol_type_string = "object";
            case SymbolType::TLSObject: symbol_type_string = "tls_object";
        }
        return Directive({".type", symbol_name + ", %" + symbol_type_string});
    }
    static Directive global(std::string symbol_name) {
        return Directive({".global", symbol_name});
    }

    static Directive ltorg() { return Directive({".LTORG"}); }

    static Directive word(std::string s) {
        return Directive({".word",s});
    }

    static Directive long_(std::vector<int> values) {
        std::vector<std::string> container = {".long"};
        for (auto value : values) {
            container.emplace_back(std::to_string(value));
            container.emplace_back(",");
        }
        container.pop_back();
        return Directive(container);
    }
    static Directive long_(std::string value_string) {
        return Directive({".long", value_string});
    }
    static Directive fill(int repeat, int size = 1, int value = 0) {
        return Directive({".fill", std::to_string(repeat) + ",", std::to_string(size) + ",", std::to_string(value)});
    }
    explicit operator std::string() const {
        std::string result("");
        for (int i = 0; i < this->container.size(); i++) {
            result += this->container[i] + " ";
        }
        return result;
    }
};
// TODO: Refactor
class Op2 {
    int imm;
    Register reg;
    std::string shift;
    bool reg_used;

public:
    Op2(Register reg, int imm, std::string shift, bool reg_used);
    bool isRegUsed();
    bool isValidImm();
    int getImm();
    const inline bool isValidShift(int shift_imm);
    static Op2 Imm(int imm);
    static Op2 LSL(Register reg, int shift_imm);
    static Op2 ASL(Register reg, int shift_imm);
    static Op2 LSR(Register reg, int shift_imm);
    static Op2 ASR(Register reg, int shift_imm);

    operator std::string() const;
};

// TODO: Refactor
class Inst {
    std::string operation;
    std::string s_flag;
    Cond condition;

    bool Rd_used, Rn_used, Rm_used, Rs_used, Op2_used, label_used;
    Register Rd;
    Register Rn;
    Register Rm;
    Register Rs;
    Op2 operand2;
    std::string label;

    Inst(std::string operation);

public:
    Inst &s() {
        this->s_flag = "S";
        return *this;
    }

    Inst &cond(Cond condition) {
        this->condition = condition;
        return *this;
    }

    Inst &useRd(Register rd);
    Inst &useRn(Register rn);
    Inst &useRs(Register rs);
    Inst &useRm(Register rs);
    Inst &useOp2(Op2 operand2);
    Inst &useLabel(std::string label);

    std::vector<std::string> build();

    static std::string address(Register reg, int offset);

    /* 算术运算 */
    static Inst ADD(Register Rd, Register Rn, Op2 op2);
    static Inst SUB(Register Rd, Register Rn, Op2 op2);
    static Inst AND(Register Rd, Register Rn, Op2 op2);
    static Inst ORR(Register Rd, Register Rn, Op2 op2);
    static Inst EOR(Register Rd, Register Rn, Op2 op2);
    static Inst LSL(Register Rd, Register Rn, Op2 op2);
    static Inst ASR(Register Rd, Register Rn, Op2 op2);
    static Inst LSR(Register Rd, Register Rn, Op2 op2);
    static Inst MUL(Register Rd, Register Rn, Register Rs);
    static Inst MLA(Register Rd, Register Rn, Register Rm, Register Rs);
    static Inst CMP(Register Rd, Op2 op2);

    /* 数据操作 */
    static std::string STR_base_offset(Register Rd, Register Rn, Register Rs, int shift);
    static std::string STR_safe(Register Rd, Register Rn, int offset);
    static std::string LDR(Register reg, std::string address);
    static std::string LDR_base_offset(Register Rd, Register Rn, Register Rs, int shift);
    static std::string LDR_safe(Register Rd, Register Rn, int offset);
    static std::string LDR_D(Register reg, unsigned int imm);   // Directive, Load a Imm to target Reg
    static std::string LDR_D(Register reg, std::string label);  // Directive, Load a label to target Reg
    static std::string LDR_G(Register reg, int idx,int blockcnt);
    static std::string MOVW(Register reg, int imm);
    static std::string MOVT(Register reg, unsigned int imm);
    static std::string ADRL(Register Rd, std::string address);
    static Inst MOVi(Register Rd, Op2 op2);
    static std::string MOV(Register Rd, Op2 op2);
    /* 栈操作 */
    static std::string PUSH(std::vector<Register> regs);
    static std::string POP(std::vector<Register> regs);

    /* 跳转指令 */
    static std::string BL(std::string func_name);
    static Inst B(std::string label);

public:
    explicit operator std::string();
};