#include <map>
#include <set>

#include "ASMStruct.h"
#include "GlobalVariable.h"
#include "IRhead.h"
#include "Value.h"

namespace SWTC {

const int align4byte(int val);

class RTAllocator {
public:
    static const int REG_MAX = 15;
    static const int CACHE_LINE_BITS = 7;
    static const int CACHE_LINE_SIZE = 1 << CACHE_LINE_BITS;

public:
    std::map<Value*, int> register_mapping;
    std::map<Value*, int> stack_mapping;
    std::set<Value*> allocated;
    std::map<Value*, int> global_offset_table;
    int stack_size;

    RTAllocator();

    void allocateFunction(Function* func);
    void allocateValue(Value* value);
    bool isAllocatable(Register reg);
    bool isCalleeStorable(Register reg);
    bool isCallerStorable(Register reg);
};

class ASMBuilder {
public:
    std::string code_string;
    Module* module=nullptr;
    RTAllocator allocator;
    std::string current_literal_pool_index_label;
    int instruction_counter;
    bool waiting_ltorg=false;
    int ltorg_blockcnt=0;

public:
    void newCommentLine(std::string comment_string);

    void comment(std::string comment);

    void endLine();

    void label(std::string label_name);

    void labelBB(std::string bb_name);

    void addLabelDirective(std::string label, Directive directive);

    void operator+=(std::string code);

    void operator+=(Directive directive);

    void operator+=(Inst asm_inst);

    operator std::string() const;

    void GenWordPool() ;

    std::string getCurrentLiteralPoolIndexLabel(Function* func);

    void init();

    Register getRegElse(Value* val, int else_reg_id);

    void buildLiteralPoolIndex(Function* func);

    void buildCallFunction(Instruction* inst, std::string func_name, std::vector<Value*> operands, Register return_reg, int stack_offset);

    void buildFunction(Function* func);

    void buildPreFunction(Function* func);

    void buildRetFunction(Function* func);

    void buildPostFunction(Function* func);

    void buildInstruction(Instruction* inst);

    void buildGlobalVariable(GlobalVariable* global_var);

    void buildMinASM();

    std::vector<Register> mergeRegisters(Function* func);

    ASMBuilder(Module* module);
    bool locationCheck(Value* a, Value* b);
    void locationToLocation(Value* from_val, Value* to_val, int stack_offset);
    void locationToLocationGroup(std::vector<Value*> from_vals, std::vector<Value*> to_vals, int stack_offset);
    void locationToReg(Value* val, Register reg_to, int stack_offset = 0);
    void regToLocation(Value* val, Register reg_from, int stack_offset = 0);
    void build();

    /* Build Tool Methods */

    void buildInstRet(Instruction* inst);
    void buildInstAlloca(Instruction* inst);
    void buildInstCall(Instruction* inst);
    void buildInstLoad(Instruction* inst);
    void buildInstStore(Instruction* inst);
    void buildInstBr(Instruction* inst);
    void buildInstGetElementPtr(Instruction* inst);
    void buildInstBrRPHI(Instruction* inst);
};
}  // namespace SWTC
