#pragma once

#include <fstream>
#include <iostream>
#include <string>

// global variables
// note that use "extern" to declare and define them in config.cpp

namespace SWTC {
extern class Module;
}

namespace AST {
extern struct CompUnit;
}

extern AST::CompUnit *root;
extern SWTC::Module *mo;

extern std::string ast_file_path;
extern std::string IR_file_path;
extern std::string input_file_path;
extern std::string output_file_path;

extern std::fstream ast_file;
extern std::ofstream ir_file;
extern std::fstream output_file;
extern std::ofstream asm_file;

extern bool isPrintAst;
extern bool isPrintIR;

extern bool isOutputFilePathGot;
extern bool isInputFilePathGot;

extern bool isDebugAST;
extern bool isPrintIRInst;
extern bool isPrintIRBB;
extern bool isPrintPredSucc;

extern bool isNoRunPass;

extern bool isCodeGen;
extern bool isMem2Reg;
extern bool isDebugRegAlloc;

// ir 不输出全局变量
extern bool isRead;

void logError(std::string e);
