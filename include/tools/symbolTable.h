#pragma once

#include <malloc.h>

#include <map>
#include <vector>

#include "IRhead.h"
#include "config.h"

using val = std::pair<SWTC::Value *, bool>;
using SymbolTableMap = std::map<std::string, val>;

struct stm {
    SymbolTableMap tab;
    stm *parent=nullptr;

    stm(stm *_parent = nullptr) : parent(_parent) {}

    void add(const std::string &name, SWTC::Value *val, bool isParam = false) {
        tab.insert_or_assign(name, std::make_pair(val, isParam));
    }

    SWTC::Value *find(const std::string &name) {
        auto tmp = tab.find(name);
        if (tmp == tab.end()) {
            return nullptr;
        }
        return tmp->second.first;
    }

    bool isParam(const std::string &name) {
        auto tmp = tab.find(name);
        if (tmp == tab.end()) {
            logError("symbolTab isParam(): no such param");
            return false;
        }
        return tmp->second.second;
    }
};

struct SymbolTable {
    stm *tab_cur=nullptr, *tab_root=nullptr;

    SymbolTable() {
        tab_cur = tab_root = new stm();
    }

    bool isRoot() {
        return tab_cur == tab_root;
    }

    void getInto() {
        tab_cur = new stm(tab_cur);
    }

    void getBack() {
        if (isRoot()) {
            logError("error: invalid attempt SymbolTable");
        }
        stm *tmp = tab_cur;
        tab_cur = tab_cur->parent;
        delete tmp;
    }

    void add(const std::string &name, SWTC::Value *val, bool isParam = false) {
        tab_cur->add(name, val, isParam);
    }

    SWTC::Value *find(const std::string &name) {
        stm *tmp = tab_cur;
        while (tmp != nullptr) {
            auto ans = tmp->find(name);
            if (ans != nullptr) {
                return ans;
            }
            tmp = tmp->parent;
        }
        return nullptr;
    }

    SWTC::Value *findRoot(const std::string &name) {
        stm *tmp = tab_cur;
        while (tmp != nullptr) {
            auto ans = tmp->find(name);
            if (ans != nullptr) {
                if (tmp == tab_root) {
                    return ans;
                } else {
                    return nullptr;
                }
            }
            tmp = tmp->parent;
        }
        return nullptr;
    }

    bool isParam(const std::string &name) {
        stm *tmp = tab_cur;
        while (tmp != nullptr) {
            auto ans = tmp->find(name);
            if (ans != nullptr) {
                return tmp->isParam(name);
            }
            tmp = tmp->parent;
        }
        logError("symbolTab isParam(): no such param");
        return false;
    }

    ~SymbolTable() {
        malloc_trim(0);
    }
};
