#pragma once

#include <algorithm>
#include <list>
#include <map>
#include <string>
#include <vector>

#include "AST.h"
#include "AST_cal.h"
#include "ArrayInfo.h"
#include "IRModule.h"
#include "IRhead.h"
#include "ilist.h"
#include "symbolTable.h"

using namespace SWTC;

namespace CONTEXT {

// extern SWTC::Module *mo;

extern bool isConst;
extern bool isGlobal;

// 符号表
extern SymbolTable st;

// 全局常量/常量数组映射
extern std::map<std::string, int> constVarMap;

extern std::map<std::string, ArrayInfo> constArrayMap;

// 函数映射到对映Function*
extern std::map<std::string, SWTC::Function *> funcMap;

extern SWTC::Function *func_cur;
extern SWTC::BasicBlock *bb_cur;

// continue用，存储while循环的第一个bb
extern std::vector<SWTC::BasicBlock *> bb_first_loop;
// break用，存储while循环的下一个bb
extern std::vector<SWTC::BasicBlock *> bb_after_loop;

extern int ifCnt;
extern int whileCnt;
extern int breakCnt;
extern int continueCnt;

using GlobalListType = std::list<SWTC::GlobalVariable *>;
using FunctionListType = SWTC::ilist<SWTC::Function>;
using ValueSymbolTable = std::map<const std::string, const SWTC::Value *>;
using InstListType = ilist<Instruction>;
using BasicBlockListType = ilist<BasicBlock>;
using ArgumentListType = ilist<Argument>;
using ValueSymbolTable = std::map<const std::string, const Value *>;
using OPS = Instruction::Ops;
using PRED = ICmpInst::Predicate;

/*
---------------------------------------------------------
==================== 上下文变量声明 =======================
---------------------------------------------------------
*/

void addExternFunc();

/*
---------------------------------------------------------
==================== 工具函数前置声明 ======================
---------------------------------------------------------
*/

// 从 AST_EXP::Exp* 获取 Value*
// isPtr = true 表示这个 Exp 是函数参数，并且值为指针
Value *getExpValue(const AST_EXP::Exp *exp, bool isPtr = false);

// 从 AST_EXP::Exp* 获取 int （常量表达式求值）
int getConstVal(AST_EXP::Exp *exp);

// 获取一个特定类型数组的type
ArrayType *getArrayType(Type *init, std::vector<int> subs);

// 从常量数组获取 vector<Constant*>
std::vector<Constant *> getArrayVals(const ArrayInfo &array);

// 全局const变量取值
int getGlobalConst(const std::string &name);

// 全局const数组取值
int getGlobalConst(
    const std::string &name,
    const std::vector<int> &valSubs);

// 更新全局const变量值
void updateGlobalConst(
    const std::string &name,
    const int &x);

// 更新全局const数组值
void updateGlobalConst(
    const std::string &name,
    const ArrayInfo &array);

/*
---------------------------------------------------------
==================== 其他函数前置声明 ======================
---------------------------------------------------------
*/

// 新建 Function*
void addFunction(
    bool isVoid,
    const std::string &name,
    bool isVarArg,
    std::vector<AST_EXP::Exp *> fparams);

// 添加函数参数
void addFuncParams(std::vector<AST_EXP::Exp *> params);

// 添加全局变量
void addGlobalVar(
    const std::string &name,
    int x);

// 添加全局数组
void addGlobalArray(
    const std::string &name,
    const ArrayInfo &array);

// 添加局部变量
void addLocalVar(
    const std::string &name,
    bool isInit,
    AST_EXP::Exp *val);

// 添加局部数组
void addLocalArray(
    const std::string &name,
    const std::vector<int> &subs,
    const std::vector<AST_EXP::Exp *> &vals);

// 添加赋值语句
void addAssign(
    // Lval:
    const std::string &l_name,
    const std::vector<AST_EXP::Exp *> &l_subs,
    bool isVar,
    // Rexp
    const AST_EXP::Exp *r_val);

}  // namespace CONTEXT
