// simulate an Array
#pragma once

#include <vector>

struct ArrayInfo {
    // 数组总大小
    int size;
    // 维度信息
    std::vector<int> subs;
    // 值
    std::vector<int> vals;

    ArrayInfo(const std::vector<int> &_subs, const std::vector<int> &_vals) : subs(_subs) {
        size = 1;
        for (auto iter : subs) { size *= iter; }

        vals.resize(size, 0);
        std::copy(_vals.begin(), _vals.end(), vals.begin());
    }

    int getVal(const std::vector<int> &_subs) {
        int pos = 0;
        int len = _subs.size();
        for (int i = 0; i < len - 1; i++) {
            pos += _subs.at(i);
            pos *= subs.at(i + 1);
        }
        pos += _subs.at(len - 1);
        return vals.at(pos);
    }
};

// struct rArrayInfo{
//     bool isInt;
//     std::vector<int> vals;
//     std::vector<rArrayInfo*> As;

//     rArrayInfo(std::vector<int> _vals):
//     isInt(true),
//     vals(_vals){}

//     rArrayInfo(std::vector<rArrayInfo*> _As):
//     isInt(false),
//     As(_As){}

//     void act(void (*a)(rArrayInfo*)){
//         a(this);

//         if(isInt){
//             return;
//         }

//         for(auto x: As){
//             x->act(a);
//         }
//     }

//     ~rArrayInfo(){
//         for(auto x: As){
//             delete x;
//         }
//     }
// };