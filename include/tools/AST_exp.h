#pragma once
#include <iostream>
#include <string>
#include <vector>

#include "AST_define.h"

namespace AST_EXP {

struct Exp {
    bool isNum;
    bool isVar;
    bool isArray;
    bool hadFunc;

    bool isUna;
    bool isBin;

    AST::OP op;
    int val;
    std::string ident;        // var or func or array ident
    std::vector<Exp *> vecs;  // subs of array or func params

    Exp *expl, *expr;

    // 数
    Exp(int _val) : val(_val),
                    isNum(true),
                    isVar(false),
                    isArray(false),
                    hadFunc(false),
                    isUna(false),
                    isBin(false) {}

    // 变量
    Exp(const std::string _ident) : ident(_ident),
                                    isNum(false),
                                    isVar(true),
                                    isArray(false),
                                    hadFunc(false),
                                    isUna(false),
                                    isBin(false) {
        // std::cerr<<"genExp: lval --------------------------\n";
    }

    // 数组
    Exp(const std::string &_ident, const std::vector<Exp *> &_subs) : ident(_ident),
                                                                      vecs(_subs),
                                                                      isNum(false),
                                                                      isVar(false),
                                                                      isArray(true),
                                                                      hadFunc(false),
                                                                      isUna(false),
                                                                      isBin(false) {}

    // 函数调用
    Exp(bool _isFunc, const std::string &_ident, const std::vector<Exp *> &_subs) : ident(_ident),
                                                                                    vecs(_subs),
                                                                                    isNum(false),
                                                                                    isVar(false),
                                                                                    isArray(false),
                                                                                    hadFunc(_isFunc),
                                                                                    isUna(false),
                                                                                    isBin(false) {}
    // initvals设计失误，isFunc= false 时
    // 用来给 initvals传递初值
    // 所有bool全为false时，只是个单纯的Exp数组

    bool isJustArr() {
        return ((!isNum) && (!isVar) && (!isArray) && (!hadFunc) && (!isUna) && (!isBin));
    }

    // 一元表达式
    Exp(AST::OP _op, Exp *_expl) : op(_op),
                                   expl(_expl),
                                   isNum(false),
                                   isVar(false),
                                   isArray(false),
                                   hadFunc(false),
                                   isUna(true),
                                   isBin(false) {
        // hadFunc = _expl->hadFunc;
    }

    // 二元表达式
    Exp(AST::OP _op, Exp *_expl, Exp *_expr) : op(_op),
                                               expl(_expl),
                                               expr(_expr),
                                               isNum(false),
                                               isVar(false),
                                               isArray(false),
                                               hadFunc(false),
                                               isUna(false),
                                               isBin(true) {
        // hadFunc = _expl->hadFunc || _expr->hadFunc;
    }

    ~Exp() {
        delete expl, expr;
        for (auto x : vecs) {
            delete x;
        }
    }
};

}  // namespace AST_EXP
