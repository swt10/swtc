#pragma once

#include "AST_define.h"
#include "config.h"

namespace AST_CAL {

int cal(bool isUnary, AST::OP op, int l, int r = 0);

}