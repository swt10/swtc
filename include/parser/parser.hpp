/* A Bison parser, made by GNU Bison 3.5.1.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* Undocumented macros, especially those whose name start with YY_,
   are private implementation details.  Do not rely on them.  */

#ifndef YY_YY_PARSER_HPP_INCLUDED
# define YY_YY_PARSER_HPP_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    KEY_VOID = 258,
    KEY_INT = 259,
    KEY_CONST = 260,
    KEY_IF = 261,
    KEY_ELSE = 262,
    KEY_WHILE = 263,
    KEY_BREAK = 264,
    KEY_CONTINUE = 265,
    KEY_RETURN = 266,
    TOK_IDENT = 267,
    INT_DEC = 268,
    INT_OCT = 269,
    INT_HEX = 270,
    T_OP_EQ = 271,
    T_OP_NE = 272,
    T_OP_LE = 273,
    T_OP_GE = 274,
    T_OP_LT = 275,
    T_OP_GT = 276,
    T_OP_NOT = 277,
    T_OP_AND = 278,
    T_OP_OR = 279,
    T_OP_ADD = 280,
    T_OP_MIN = 281,
    T_OP_TIM = 282,
    T_OP_DIV = 283,
    T_OP_MOD = 284,
    END_LINE = 285,
    LOWER_THAN_ELSE = 286
  };
#endif
/* Tokens.  */
#define KEY_VOID 258
#define KEY_INT 259
#define KEY_CONST 260
#define KEY_IF 261
#define KEY_ELSE 262
#define KEY_WHILE 263
#define KEY_BREAK 264
#define KEY_CONTINUE 265
#define KEY_RETURN 266
#define TOK_IDENT 267
#define INT_DEC 268
#define INT_OCT 269
#define INT_HEX 270
#define T_OP_EQ 271
#define T_OP_NE 272
#define T_OP_LE 273
#define T_OP_GE 274
#define T_OP_LT 275
#define T_OP_GT 276
#define T_OP_NOT 277
#define T_OP_AND 278
#define T_OP_OR 279
#define T_OP_ADD 280
#define T_OP_MIN 281
#define T_OP_TIM 282
#define T_OP_DIV 283
#define T_OP_MOD 284
#define END_LINE 285
#define LOWER_THAN_ELSE 286

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 29 "parser.y"

	int num;
	std::string *str;
	AST::ASTBasic *node;

#line 125 "parser.hpp"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_PARSER_HPP_INCLUDED  */
