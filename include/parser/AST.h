#pragma once

#include <string>
#include <vector>

#include "AST_define.h"
#include "AST_exp.h"
#include "config.h"

struct AST::ASTBasic {
    AST::CompUnitType compUnitType;
    AST::BlockItemType blockItemType;
    bool isConst = false;
    bool isPrintNothing = false;

    ASTBasic() = default;
    virtual ~ASTBasic() = 0;

    virtual void add(AST::ASTBasic *para);
    virtual void print() = 0;
    virtual void genIR() = 0;

    virtual AST_EXP::Exp *genExp();
};

struct AST::PrintNothing : AST::ASTBasic {
    PrintNothing();
    ~PrintNothing(){};

    void print();
    void genIR();
};

// subscripts of array
struct AST::ArraySub : AST::ASTBasic {
    std::vector<AST::Exp *> subscripts;

    ~ArraySub() {
        for (auto x : subscripts) {
            delete x;
        }
    };

    void add(AST::ASTBasic *para);

    void print();
    void genIR();

    std::vector<AST_EXP::Exp *> getSubs();
    bool isVar();
};

struct AST::CompUnit : AST::ASTBasic {
    // AST::CompUnitType type;
    std::vector<AST::ASTBasic *> contents;

    ~CompUnit() {
        for (auto x : contents) {
            delete x;
        }
    };

    void add(AST::ASTBasic *para);

    void print();
    void genIR();
};



struct AST::Decl : AST::ASTBasic {
    bool isConst = false;
    AST::Defs *defs = nullptr;

    Decl(bool _isConst, AST::ASTBasic *_defs);
    ~Decl() {
        delete defs;
    };

    void print();
    void genIR();
};

struct AST::Defs : AST::ASTBasic {
    std::vector<AST::Def *> defs;

    ~Defs() {
        for (auto x : defs) {
            delete x;
        }
    };

    void add(AST::ASTBasic *para);

    void print();
    void genIR();
};

struct AST::Def : AST::ASTBasic {
    bool isInit=false;

    std::string *ident = nullptr;
    AST::ArraySub *subs = nullptr;

    AST::InitVal *initVal = nullptr;

    Def(bool _isInit, std::string *_ident, AST::ASTBasic *_subs, AST::ASTBasic *_initVal);
    ~Def() {
        delete ident;
        delete subs;
        delete initVal;
    };

    bool isVar();

    void print();
    void genIR();
};

struct AST::InitVal : AST::ASTBasic {
    AST::Exp * selfInitVal = nullptr;
    std::vector<AST::InitVal *> initVals;

    InitVal(){
        selfInitVal = nullptr;
        initVals.clear();
    }

    ~InitVal() {
        delete selfInitVal;
        // for (auto x : initVals) {
        //     delete x;
        // }
    };

    // 若是变量，则返回AST_EXP::Exp*
    AST_EXP::Exp *getVar();
    
    void setSelf(AST::ASTBasic * _selfInitVal);
    void add(AST::ASTBasic *para);
    bool isVar();

    AST_EXP::Exp *genExp();
    // AST_EXP::Exp *genExp(int);

    void print();
    void genIR();
};

struct AST::FuncDef : AST::ASTBasic {
    std::string *ident = nullptr;

    AST::FuncType funcType;
    AST::FuncFParams *funcFParams = nullptr;
    AST::Block *funcBody = nullptr;

    FuncDef(std::string *_ident, AST::FuncType _funcType, AST::ASTBasic *_funcFParams, AST::ASTBasic *_funcBody);
    ~FuncDef() {
        delete ident;
        delete funcFParams;
        delete funcBody;
    };

    bool hadParam();

    void print();
    void genIR();
};

// struct AST::ConstDecl: AST::Decl{
//     ConstDecl();

// };

// struct AST::VarDecl: AST::Decl {
//     VarDecl();
// };

struct AST::Block : AST::ASTBasic {
    std::vector<AST::ASTBasic *> blockItems;

    ~Block() {
        for (auto x : blockItems) {
            delete x;
        }
    };

    void add(AST::ASTBasic *para);

    void print();
    void genIR();
};

// struct AST::BlockItem: AST::ASTBasic{
//     AST::BlockItemType type;
//     AST::BlockItemContent content;

//     ~BlockItem(){};

//     void print();
//     void genIR();
// };

struct AST::Stmt : AST::ASTBasic {
    // AST::StmtType type;
    // Stmt();
};

struct AST::Stmt_If : AST::Stmt {
    AST::Exp *cond = nullptr;
    AST::Stmt *stmt_1 = nullptr;
    AST::Stmt *stmt_2 = nullptr;

    Stmt_If(AST::ASTBasic *_cond, AST::ASTBasic *_stmt_1, AST::ASTBasic *_stmt_2);
    ~Stmt_If() {
        delete cond;
        delete stmt_1;
        delete stmt_2;
    };

    bool hadElse();

    void print();
    void genIR();
};

struct AST::Stmt_While : AST::Stmt {
    AST::Exp *cond = nullptr;
    AST::Stmt *stmt_1 = nullptr;

    Stmt_While(AST::ASTBasic *_cond, AST::ASTBasic *_stmt_1);
    ~Stmt_While() {
        delete cond;
        delete stmt_1;
    };

    void print();
    void genIR();
};

struct AST::Stmt_Ass : AST::Stmt {
    AST::LVal *lval = nullptr;
    AST::Exp *rval = nullptr;

    Stmt_Ass(AST::ASTBasic *_lval, AST::ASTBasic *_rval);
    ~Stmt_Ass() {
        delete lval;
        delete rval;
    };

    void print();
    void genIR();
};

struct AST::Stmt_Break : AST::Stmt {
    Stmt_Break();
    ~Stmt_Break(){};
    void print();
    void genIR();
};

struct AST::Stmt_Continue : AST::Stmt {
    Stmt_Continue();
    ~Stmt_Continue(){};
    void print();
    void genIR();
};

struct AST::Stmt_Return : AST::Stmt {
    AST::Exp *retVal = nullptr;

    Stmt_Return(AST::ASTBasic *_retVal);
    ~Stmt_Return() {
        delete retVal;
    };

    bool hadRetVal();

    void print();
    void genIR();
};

struct AST::Stmt_Block : AST::Stmt {
    AST::Block *block = nullptr;

    Stmt_Block(AST::ASTBasic *_block);
    ~Stmt_Block() {
        delete block;
    };

    void print();
    void genIR();
};

struct AST::Stmt_Exp : AST::Stmt {
    AST::Exp *exp = nullptr;

    Stmt_Exp(AST::ASTBasic *_exp);
    ~Stmt_Exp() {
        delete exp;
    };

    bool isEmpty();

    void print();
    void genIR();
};

struct AST::LVal : AST::ASTBasic {
    std::string *ident = nullptr;
    AST::ArraySub *subs = nullptr;

    LVal(std::string *_ident, AST::ASTBasic *_subs);
    ~LVal() {
        delete ident;
        delete subs;
    };

    bool isVar();

    void print();
    void genIR();
};

struct AST::Exp : AST::ASTBasic {
};

struct AST::Exp_Func : AST::Exp {
    std::string *ident = nullptr;
    AST::FuncRParams *funcRParams = nullptr;

    Exp_Func(std::string *_ident, AST::ASTBasic *_funcRParams);
    ~Exp_Func() {
        delete ident;
        delete funcRParams;
    };

    bool hadParam();

    void print();
    void genIR();

    AST_EXP::Exp *genExp();
};

struct AST::Exp_Num : AST::Exp {
    int val = 0;

    Exp_Num(int _val);
    ~Exp_Num(){};

    void print();
    void genIR();

    AST_EXP::Exp *genExp();
};

struct AST::Exp_Paren : AST::Exp {
    AST::Exp *expParen = nullptr;

    Exp_Paren(AST::ASTBasic *_expParen);
    ~Exp_Paren() {
        delete expParen;
    };

    void print();
    void genIR();

    AST_EXP::Exp *genExp();
};

struct AST::Exp_LVal : AST::Exp {
    AST::LVal *lval = nullptr;

    Exp_LVal(AST::ASTBasic *_lval);
    ~Exp_LVal() {
        delete lval;
    };

    void print();
    void genIR();

    AST_EXP::Exp *genExp();
};

struct AST::Exp_Unary : AST::Exp {
    AST::OP op;
    AST::Exp *exp = nullptr;

    Exp_Unary(AST::OP _op, AST::ASTBasic *_exp);
    ~Exp_Unary() {
        delete exp;
    };

    void print();
    void genIR();

    AST_EXP::Exp *genExp();
};

struct AST::Exp_Binary : AST::Exp {
    AST::OP op;
    AST::Exp *expl = nullptr, *expr = nullptr;

    Exp_Binary(AST::OP _op, AST::ASTBasic *_expl, AST::ASTBasic *_expr);
    ~Exp_Binary() {
        delete expl;
        delete expr;
    };

    void print();
    void genIR();

    AST_EXP::Exp *genExp();
};

struct AST::FuncFParams : AST::ASTBasic {
    std::vector<AST::FuncFParam *> fParams;

    ~FuncFParams() {
        for (auto x : fParams) {
            delete x;
        }
    };

    void add(AST::ASTBasic *para);

    void print();
    void genIR();
};

struct AST::FuncRParams : AST::ASTBasic {
    std::vector<AST::Exp *> rParams;

    ~FuncRParams() {
        for (auto x : rParams) {
            delete x;
        }
    };

    void add(AST::ASTBasic *para);

    void print();
    void genIR();
};

struct AST::FuncFParam : AST::ASTBasic {
    std::string *ident = nullptr;
    AST::ArraySub *subs = nullptr;

    FuncFParam(std::string *_ident, AST::ASTBasic *_subs);
    ~FuncFParam() {
        delete ident;
        delete subs;
    };

    bool isVar();

    void print();
    void genIR();
};
