#pragma once

namespace AST {

// basic class of AST.
// including function print() and genIR()
struct ASTBasic;
// array of interger type or recursive array
struct ArraySub;

// used for variables who are not array
struct PrintNothing;

// start of the code
struct CompUnit;
// declaration of variables, may header more than one defination
struct Decl;
// definations in Decl
struct Defs;
// defination of variables
struct Def;
// initial value in defining variables
struct InitVal;

// declaration and defination of function
struct FuncDef;

//     // declaration of constant variables. initVal is essential and must be constant value
// struct ConstDecl;
//     // declaration of variables. initVal is optional
// struct VarDecl;

//     // subClass of Def;
// struct ConstDef;
// struct VarDef;

// Block with a array of blockItems
struct Block;

struct Stmt;
// statement if
struct Stmt_If;
// statement while
struct Stmt_While;
// statement assignment
struct Stmt_Ass;
// statement break
struct Stmt_Break;
// statement continue
struct Stmt_Continue;
// statement return
struct Stmt_Return;
// statement of a block;
struct Stmt_Block;
// statement with a exp or nothing;
struct Stmt_Exp;

// statement of expressions, i think it's ignarable
// struct Stmt_Exps;

// left value
struct LVal;

// expression
struct Exp;
// expression of function calling
struct Exp_Func;
// expression of number
struct Exp_Num;
// expression in parentheses
struct Exp_Paren;
// expression of left value
struct Exp_LVal;
// expression with unary operation
struct Exp_Unary;
// expression with binary operation
struct Exp_Binary;

// parameters of function.
struct FuncFParams;
struct FuncFParam;
// real parameters of function
struct FuncRParams;

// enum ArrayType{
//     typeInt,
//     typeArray
// };
enum OP {
    // cal
    OP_ADD,
    OP_MIN,
    OP_TIM,
    OP_DIV,
    OP_MOD,

    // bool
    OP_AND,
    OP_OR,
    OP_NOT,

    // rel
    OP_EQ,
    OP_NE,
    OP_LT,
    OP_GT,
    OP_LE,
    OP_GE,

    OP_BINARY_BUT_ONE
};
enum CompUnitType {
    typeDecl,
    typeFuncDef
};
enum FuncType {
    typeInt,
    typeVoid,
};
// enum StmtType{
//     typeAss,
//     typeExp,
//     typeBlock,
//     typeIfElse,
//     typeWhile,
//     typeBreak,
//     typeContinue,
//     typeReturn
// };
enum BlockItemType {
    typeDecl_block,
    typeStmt
};

// union ArrayContent{
//     int contInt;
//     Array* contArray;
// };

union CompUnitContent {
    Decl* contDecl;
    FuncDef* contFuncDef;
};

union BlockItemContent {
    Decl* contDecl;
    Stmt* contStmt;
};

}  // namespace AST
