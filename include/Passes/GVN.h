#pragma once
#include <map>
#include <functional>
#include <unordered_map>
#include <numeric>

#include "IRhead.h"
#include "Dominate.h"
#include "InstSimplify.h"
#include "Mem2Reg.h"
#include "MemSSA.h"

#include "Mem2Reg.h"

using namespace SWTC;


class GVNimpl{
private:
struct ltentry {
    Value *val=nullptr;
    BasicBlock *b=nullptr;
    ltentry *next=nullptr;
};

struct Expression{
    int opcode;
    bool commutative =false;
    Type *type =nullptr;
    std::vector<int> varargs;

    Expression(int op= ~2U):opcode(op){}
    
    bool operator==(const Expression &other)const{
        if(opcode != other.opcode)
            return false;
        if(opcode ==~0U||opcode==~1U)return true;
        if(type != other.type)return false;
        if(varargs != other.varargs)return false;
        return true;
    }

};
struct ExpHash
{
std::size_t operator()(const Expression & exp)const{
    auto _hash=[f=std::hash<int>{}](const std::vector<int>& arr)->std::size_t{
        return std::accumulate(arr.begin(),arr.end(),0u,[&](std::size_t acc,int num){return acc+f(num);});
    };
    
    return std::hash<int>()(exp.opcode)+
    std::hash<int>()(reinterpret_cast<long long>(exp.type))+
    _hash(exp.varargs);
} 
};
std::map<Value *, int> vn;  //值编号
std::unordered_map<Expression,int,ExpHash> en; //exp编号
std::map<int,PHINode*>nphi; //phi编号->phi
std::map<std::pair<int ,BasicBlock*>,int> phiTable;

std::vector<Expression> exps;
std::vector<int> expidx;

int nextvaluenumber = 1;
int nextexpnumber = 0;


std::map<int, ltentry> lt;  //leaderTable 从值编号到对应的集合

std::map<BasicBlock*,int>bRPOn;
bool InvalidbRPOn=true;

void addlt(int num,Value *val,BasicBlock *bb)
{
    ltentry &t=lt[num];
    if(!t.val){
        t.val=val;
        t.b=bb;
        return;
    }
    ltentry *n=new ltentry();
    n->val=val;
    n->b=bb;
    n->next=t.next;
    t.next=n;
}

void removelt(int num,Instruction *inst,BasicBlock *bb)
{
    ltentry *t=&lt[num],*p=nullptr;
    while(t&&(t->val!=inst||t->b!=bb))
    {
        p=t;t=t->next;
    }
    if(!t)return;
    if(p){
        p->next=t->next;
    }
    else {
        if(!t->next){
            t->val=nullptr;
            t->b=nullptr;
        }else {
            ltentry *n=t->next;
            t->val=n->val;
            t->b=n->b;
            t->next=n->next;
        }
    }
}

Value* findlt(BasicBlock *bb,int num)
{
    ltentry t=lt[num];
    if(!t.val)return nullptr;
    Value *v=nullptr;
    if(dt->b1Domb2(t.b,bb)){
        v=t.val;
        if(isa<Constant>(v))return v;
    }
    ltentry *n=t.next;
    while(n)
    {
        if(dt->b1Domb2(n->b,bb)){
            if(isa<Constant>(n->val))return n->val;
            if(!v)v=n->val;
        }
        n=n->next;
    }
    return v;

}

std::set<BasicBlock *> deadblock;
// Dominate *dt;
std::vector<Instruction*>insterase;
std::vector<std::pair<Instruction*,int>> toSplit;

public:
//除了GVN外，还进行了代数化简和指令化简和部分冗余消除
bool run(Function *func);
int findoraddvn(Value *v);
bool iterateGVN(Function *func);
bool processBlock(BasicBlock *bb);
bool processInst(Instruction *inst);
bool mergeblockintopred(BasicBlock *bb);
BasicBlock* findcriticaledge(BasicBlock *pred,BasicBlock *succ);
void adddeadblock(BasicBlock *bb);
bool propagateEq(Value *l,Value *r,std::pair<BasicBlock*,BasicBlock*>&edge);
bool isCritical(Instruction *inst,BasicBlock *bb);
bool edgedominate(std::pair<BasicBlock*,BasicBlock*> &edge,BasicBlock *bb);
int findoraddcall(CallInst *c);
Expression createExpr(Instruction *inst);
Expression createCmpExpr(unsigned Opcode,ICmpInst::Predicate Predicate,Value *LHS, Value *RHS);
bool foldSinglePhi(BasicBlock *bb);
bool performPRE(Function *func);
bool performScalarPRE(Instruction *inst);
int phiTranslate(BasicBlock *pre,BasicBlock* phibb,int num);
int phiTranslateimpl(BasicBlock *pre,BasicBlock* phibb,int num);
bool PREinsertion(Instruction*inst,BasicBlock*pre,BasicBlock* cur,int num);
bool splitCriticalEdges();
void Erase(Value *v);
BasicBlock *splitCriticalEdge(Instruction *br,int i);
std::pair<int,bool> addexpvn(Expression exp);
bool edgedomuse(std::pair<BasicBlock *, BasicBlock *> &edge, Use *bb);
int replacedomuse(Value *from, Value *to, std::pair<BasicBlock *, BasicBlock *> &edge) ;
bool processLoad(LoadInst *l);
void clear();


GVNimpl(){
    
}
};


void GVN(Function *m);
