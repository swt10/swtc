#pragma once

#include <algorithm>
#include <iostream>
#include <map>
#include <ostream>
#include <set>
#include <unordered_map>

#include "IRhead.h"
using namespace SWTC;

class Dominate {

public:
    Module *M = nullptr;
    std::list<BasicBlock *> RPO;
    std::map<BasicBlock *, unsigned int> dfn; //深度优先编号，entry最小
    std::map<BasicBlock *, std::set<BasicBlock *>> DFST; //深度优先搜索树    
    std::unordered_map<BasicBlock *, std::unordered_set<BasicBlock *>> DF; // 每个BB的支配边界
    std::unordered_map<BasicBlock *, std::unordered_set<BasicBlock *>> DTSuccBlocks; //dom树里每个BB的后继(前驱为idom
    
    Dominate(Module *mo) : M(mo){}
    ~Dominate()=default;
    
    void calDomInfo();
    void calDomInfo(Function *F);

    void calReversePostOrder(Function *F);
    void search(BasicBlock *BB);
    void calIDom(Function *F);
    BasicBlock *intersect(BasicBlock *b1, BasicBlock *b2);
    void calDominanceFrontier(Function *F);
    void getDomTreeSucc(Function *F);
    void getDomsAndDomby(BasicBlock *BB);

    bool b1Domb2 (BasicBlock *b1, BasicBlock *b2);//b1是否支配b2

    bool dominates(Value*def, Instruction *usebb);
    bool dominates(BasicBlock *a,BasicBlock*b);
    bool dominates(Instruction* def,BasicBlock* usebb);

};


std::vector<BasicBlock*> depth_first(BasicBlock* bb);
std::vector<BasicBlock*> depth_firstDT(BasicBlock* bb);
std::vector<BasicBlock*> reverse_post_order(BasicBlock* bb);