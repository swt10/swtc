#pragma once

#include <cassert>
#include <utility>
#include <variant>
#include <vector>

#include "IRheader.h"
#include "initPass.h"
#include "Mem2Reg.h"
#include "GVN.h"
#include "splitgep.h"
#include "ConstantFolding.h"
#include "CSE.h"
#include "GlobalOpt.h"
#include "Loop.h"
#include "LoopInvariant.h"
#include "LiveVariables.h"
#include "EliminatePhi.h"
#include "MemSSA.h"
#include "FunctionInline.h"
#include "DCE.h"
#include "SimplifyCFG.h"
#include "regAlloc.h"


namespace SWTC {

class Value;
class BasicBlock;
class Function;
class Module;
class Function;

using PassTarget = std::variant<Module *>;

void run_passes(PassTarget p, bool opt);

void print_passes();
}  // namespace SWTC
