#pragma once

#include <cstdio>
#include <set>
#include <stack>
#include <vector>

// #include <forward_list>
#include <algorithm>
#include <cmath>

#include "IRhead.h"
#include "LiveVariables.h"
#include "Loop.h"
#include "LoopInvariant.h"
#include "Mem2Reg.h"
#include "config.h"

#define DEBUG_REG

template <typename... T>
auto sum(T... t) {
    return (t + ...);
}

static const int REAL_REG_SIZE = 14;
static bool isLiveIn(Value *reg, BasicBlock *bb);
/// WAITING: 判断 inst/value 有没有分配寄存器
static bool isReg(Value *v);

using namespace SWTC;

struct SREGs {
    Function *func;
    std::vector<Value *> regs;
    std::vector<Value *> &int2reg = regs;
    std::vector<int> int2color;
    std::map<Value *, int> reg2int;
    LoopInfo *li;

    SREGs(Function *_func, std::map<Value *, std::set<BasicBlock *>> &_assBBs) : func(_func) {
        /// DONE: 添加所有虚拟寄存器

        /// WAITING:
        /// \c 0: 函数参数占用虚拟寄存器
        for (auto arg : func->getArgumentList()) {
            if (!isReg(arg)) {
                continue;
            }
            _assBBs[arg].insert(func->getEntryBlock());
            add(arg);
        }

        /// \c 1: 指令产生虚拟寄存器
        for (auto bb : func->getBasicBlockList()) {
            for (auto inst : bb->getInstList()) {
                if (!isReg(inst)) {
                    continue;
                }
                _assBBs[inst].insert(bb);
                add(inst);
            }
        }
    }

    ~SREGs() = default;

    int getRegSize() {
        return regs.size();
    }

    bool isExist(Value *v) {
        auto tmp = reg2int.find(v);
        if (tmp == reg2int.end()) {
            return false;
        }
        return true;
    }
    void add(Value *v) {
        if (isExist(v)) {
            logError("add(): reg had existed");
        }

        regs.push_back(v);
        int2color.push_back(-1);
        reg2int.insert({v, getRegSize() - 1});
    }

    Value *getRegByInt(int i) {
        return regs.at(i);
    }

    int getIntByReg(Value *v) {
        if (!isExist(v)) {
            logError("getInt(v): reg not exists");
        }
        return reg2int.find(v)->second;
    }

    void setColor(int reg, int color) {
        assert(int2color.at(reg) == -1);
        int2color.at(reg) = color;
    }

    void setColor(Value *v, int color) {
        setColor(getIntByReg(v), color);
    }

    int getColor(int i) {
        return int2color.at(i);
    }

    int getColor(Value *v) {
        return getColor(getIntByReg(v));
    }

    int getSpillCost(int i, BasicBlock *bb) {
        return getSpillCost(getRegByInt(i), bb);
    }

    int getSpillCost(Value *v, BasicBlock *bb) {
        /// TODO: calculate spill cost

        if (!isLiveIn(v, bb)) {
            /// 在 bb 内不活跃，随意 spill
            return 0;
        }

        /// WAITING: loop nested depth cost
        int nestedCost = pow(10, li->getBBDepth(bb));
        /// bb 内的下次 use 越近，代价越大
        /// 如果在 bb 内没有被 use，代价为 0
        int nextUseCostInBB = 0;
        for (auto use : v->getUseList()) {
            if (auto tmp = dyn_cast<Instruction>(use->getUser()); tmp->getParent() == bb) {
                nextUseCostInBB = 1;
                break;
            }
        }

        /// 如果并不在本块内被 use，那么嵌套深度意义有待考察
        nestedCost *= nextUseCostInBB;
        nextUseCostInBB *= 10;

        /// bb 外的下次 use 越近，代价越大
        int nextUseCostOutBB = 0;

        return sum(nestedCost, nextUseCostInBB, nextUseCostOutBB);
    }

    void printRegsColors(std::ostream &o = std::cerr) {
        o << "regs:\tcolors\n";
        for (int i = 0, e = getRegSize(); i < e; i++) {
            o << i << '\t' << getColor(getRegByInt(i)) << '\n';
        }
    }
};

struct IG {
    bool **ig;
    int size;  /// 此 size 仅指示数组大小，与实际寄存器个数无关
    SREGs *sregs;
    int realRegs[REAL_REG_SIZE + 5];

    int getRealRegs(int realCnt) {
        if (realCnt < 0 || realCnt >= REAL_REG_SIZE) {
            logError("invalid use of real register");
        }
        return realRegs[realCnt];
    }

    void buildIG() {
        /// DONE: build IG
        for (auto reg1 : sregs->regs) {
            for (auto reg2 : sregs->regs) {
                if (isclobberforValue(reg1, reg2)) {
                    add(reg1, reg2);
                }
            }
        }

        int cnt = sregs->getRegSize() + 1;
        for (int i = 0; i < REAL_REG_SIZE; i++) {
            realRegs[i] = cnt++;
        }

        logError("build IG");
    }

    void addCons(Function *func) {
        /// TODO: 添加约束
        /// \c 1. 物理寄存器和函数参数
        int cnt = 0;
        for (auto arg : func->getArgumentList()) {
            add(sregs->getIntByReg(arg), getRealRegs(cnt++));
        }

        /// \c 2. return 值和 R0 冲突
        for (auto bb : func->getBasicBlockList()) {
            for (auto inst : bb->getInstList()) {
                if (auto ret = dyn_cast<ReturnInst>(inst);
                    ret && ret->getNumOperands() == 1) {
                    add(sregs->getIntByReg(ret->getOperand(0)), getRealRegs(0));
                }
            }
        }
    }

    IG(SREGs *_sregs, Function *func) : size(_sregs->getRegSize() + REAL_REG_SIZE + 5),
                                        sregs(_sregs) {
        ig = new bool *[size]();
        for (int i = 0; i < size; i++) {
            ig[i] = new bool[size]();
        }

        buildIG();
        addCons(func);
    }

    ~IG() {
        for (int i = 0; i < size; i++) {
            delete[] ig[i];
        }
        delete[] ig;
    }

    void add(int p, int v) {
        ig[p][v] = ig[v][p] = true;
    }

    void add(Value *p, Value *v) {
        add(sregs->getIntByReg(p), sregs->getIntByReg(v));
    }

    void remove(int p, int v) {
        ig[p][v] = ig[v][p] = false;
    }

    void remove(Value *p, Value *v) {
        remove(sregs->getIntByReg(p), sregs->getIntByReg(v));
    }

    bool isInter(int reg1, int reg2) {
        return ig[reg1][reg2];
    }

    bool isInter(Value *reg1, Value *reg2) {
        return this->isInter(sregs->getIntByReg(reg1), sregs->getIntByReg(reg2));
    }

    int getGap(int reg, int &maxColor) {
        bool *tmp = new bool[maxColor + 5]();

        for (int i = 0, e = sregs->getRegSize(); i < e; i++) {
            if (i == reg || sregs->getColor(i) == -1) {
                continue;
            }
            tmp[sregs->getColor(i)] = true;
        }

        for (int i = 0; i <= maxColor; i++) {
            if (tmp[i] == false) {
                return i;
            }
        }

        delete[] tmp;
        maxColor++;
        return maxColor;
    }

    int getGap(Value *v, int &maxColor) {
        return getGap(sregs->getIntByReg(v), maxColor);
    }

    void print(std::ostream &o = std::cerr) {
        o << "IG: \n";
        for (int i = 0; i < size; i++) {
            o << "reg " << i << "\t: ";
            for (int j = 0; j < size; j++) {
                if (!ig[i][j]) {
                    continue;
                }
                o << j << ' ';
            }
            o << "\n";
        }
    }
};

SREGs *getRegsByFunc(Function *func);
void runRegAlloc(Module *_mo);

static bool isLiveIn(Value *reg, BasicBlock *bb) {
    /// WAITNG: return true if reg is live in bb
    ///  reg is not live if it's only live in phi
    return getVarInfo(reg).isLiveIn(bb, reg);
}

#define CROR(value, bits) ((value >> bits) | (value << (0x20 - bits)))



static bool Judge(unsigned int val) {
    for (int i = 0; i < 32; i+=2) {
        if (CROR(val, i) <= 0x000000ff)
            return true;
    }

    return false;
}
static bool isReg(Value *v) {
    // if (
    //     isa<StoreInst>(v) || isa<BranchInst>(v) || isa<ReturnInst>(v) || isa<Constant>(v) || isa<GlobalValue>(v) || isa<BasicBlock>(v) || isa<Function>(v)
    //     /// WAITING: Alloca 似乎不该算个寄存器
    //     || isa<AllocaInst>(v)) {
    //     return false;
    // }
    if (
        /// store
        isa<StoreInst>(v)
        /// branch
        || isa<BranchInst>(v)
        /// return
        || isa<ReturnInst>(v)
        /// TODO: 大常数需要寄存器来存，也许需要特判一下
        // || (isa<ConstantInt>(v)&&!Judge(dyn_cast<ConstantInt>(v)->getValue()))
        /// WAITING: 全局变量应该在堆上，使用的时候需要分配寄存器来存指针
        // || isa<GlobalValue>(v)
        || isa<BasicBlock>(v)
        || isa<Function>(v)
        
        /// WAITING: 函数参数
        // ||	isa<Argument>(v)

        /// WAITING: Alloca 该算个寄存器
        // || isa<AllocaInst>(v)
        ) {
        return false;
    }

    if (auto tmp = dyn_cast<CallInst>(v);
        tmp && tmp->getCalledFunction()->getFunctionType()->isVoidTy()) {
        return false;
    }

    return true;
}
void regAlloc(Module *mo);