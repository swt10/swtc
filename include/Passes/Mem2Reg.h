#pragma once
#include <algorithm>
#include <iostream>
#include <map>
#include <ostream>
#include <set>
#include <unordered_map>

#include "IRhead.h"
#include "Dominate.h"
using namespace SWTC;

void placingPhi(Function *F);
void reName(BasicBlock *BB);
void mem2Reg(Module *M);

extern Dominate *dt;