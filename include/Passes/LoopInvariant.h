#pragma once

#include <algorithm>
#include <iostream>
#include <map>
#include <ostream>
#include <set>
#include <unordered_set>
#include <unordered_map>
#include <vector>

#include "IRhead.h"
#include "Loop.h"

using namespace SWTC;

void runLoopInvariantMove(Function *F);
void findInvariants(Loop *loop);
void InvariantsMoveOut(Loop *loop);

extern LoopInfo *li;
