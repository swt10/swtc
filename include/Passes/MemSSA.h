#pragma once
#include "Dominate.h"
#include "IRhead.h"
#include "ilist.h"

using namespace SWTC;
//内存依赖，指针分析，别名分析都在这里做
class MemoryAccess : public User {
public:
    MemoryAccess *next, *prev;
    MemoryAccess(Value *ptr, MemoryAccess *def, unsigned Vty, Instruction *I, BasicBlock *BB, unsigned NumOperands, Use *U)
        : User(Module::getVoidTy(), Vty, U, NumOperands), Block(BB), MemInstruction(I) {
        if (I) {
            if (isa<LoadInst>(I)) {
                oriptr = I->getOperand(0);
                while (isa<GetElementPtrInst>(oriptr)) {
                    oriptr = dyn_cast<GetElementPtrInst>(oriptr)->getOperand(0);
                }
            }
            if (isa<StoreInst>(I)) {
                oriptr = I->getOperand(1);
                while (isa<GetElementPtrInst>(oriptr)) {
                    oriptr = dyn_cast<GetElementPtrInst>(oriptr)->getOperand(0);
                }
            }
        }

        if (ptr) oriptr = ptr;
        return;
    }
    MemoryAccess(MemoryAccess *def, unsigned Vty, BasicBlock *BB, unsigned NumOperands)
        : User(Module::getVoidTy(), Vty, nullptr, NumOperands), Block(BB) {
        oriptr = def->oriptr;
    }
    int getID() const;
    static bool classof(const Value *V) {
        if(!V)return nullptr;
        unsigned ID = V->getValueID();
        return ID == MemoryUseVal || ID == MemoryPhiVal || ID == MemoryDefVal;
    }
    MemoryAccess *getOptimized() const;
    void setOptimized(MemoryAccess *MA);
    MemoryAccess *getDefiningAccess() const { return dyn_cast<MemoryAccess>(getOperand(0)); }
    void setDefiningAccess(MemoryAccess *DMA, bool Optimized = false) {
        if (!Optimized) {
            setOperand(0, DMA);
            return;
        }
        setOptimized(DMA);
    }
    bool isOptimized() const;
    BasicBlock *Block;
    Instruction *MemInstruction;
    Value *oriptr;
};
class MemoryDef : public MemoryAccess {
public:
    MemoryDef(Value *ptr, MemoryAccess *ma, Instruction *inst, BasicBlock *bb, int id)
        : MemoryAccess(ptr, ma, Value::MemoryDefVal, inst, bb, 2, Op), ID(id) {
        Op[0].init(nullptr, nullptr);
    }
    static bool classof(const Value *V) {
        if (!V) return false;
        return V->getValueID() == Value::MemoryDefVal;
    }
    void setOptimized(MemoryAccess *MA) {
        setOperand(1, MA);
        OptID = MA->getID();
    }
    int getID() const { return ID; }
    MemoryAccess *getOptimized() const {
        return dyn_cast<MemoryAccess>(getOperand(1));
    }
    bool isOptimized() const {
        return getOptimized() && OptID == getOptimized()->getID();
    }

private:
    const int ID;
    int OptID = -1;
    Use Op[2];
};

class MemoryUse : public MemoryAccess {
public:
    MemoryUse(Value *ptr, MemoryAccess *ma, Instruction *inst, BasicBlock *bb)
        : MemoryAccess(ptr, ma, Value::MemoryUseVal, inst, bb, 1, &Op) {
        Op.init(nullptr, nullptr);
    }
    static bool classof(const Value *V) {
        if (!V) return false;
        return V->getValueID() == Value::MemoryUseVal;
    }
    void setOptimized(MemoryAccess *MA) {
        setOperand(0, MA);
        OptID = MA->getID();
    }
    void setDefiningAccess(MemoryAccess *DMA, bool Optimized = false) {
        if (!Optimized) {
            setOperand(0, DMA);
            return;
        }
        setOptimized(DMA);
    }
    MemoryAccess *getDefiningAccess() const { return dyn_cast<MemoryAccess>(getOperand(0)); }
    bool isOptimized() const {
        return getDefiningAccess() && OptID == getDefiningAccess()->getID();
    }

private:
    int OptID = -1;
    Use Op;
};

class MemoryPhi : public MemoryAccess {
public:
    MemoryPhi(MemoryAccess *def, BasicBlock *bb, int id, unsigned Numpreds = 0)
        : MemoryAccess(def, Value::MemoryPhiVal, bb, 0), ID(id), ReservedSpace(Numpreds) {
        resizeOperands(ReservedSpace);
    }
    static bool classof(const Value *V) {
        if (!V) return false;
        return V->getValueID() == Value::MemoryPhiVal;
    }
    void setOptimized(MemoryAccess *MA) {
        setOperand(1, MA);
        OptID = MA->getID();
    }
    int getID() const { return ID; }
    void addIncoming(MemoryAccess *V, BasicBlock *BB) {
        unsigned OpNo = NumOperands;
        if (OpNo + 2 > ReservedSpace)
            resizeOperands(0);  // Get more space!
        // Initialize some new operands.
        NumOperands = OpNo + 2;
        OperandList[OpNo].init(V, this);
        OperandList[OpNo + 1].init(reinterpret_cast<Value *>(BB), this);
    }
    unsigned getNumIncomingValues() const { return getNumOperands() / 2; }

    /// getIncomingValue - Return incoming value number x
    ///
    MemoryAccess *getIncomingValue(unsigned i) const {
        return dyn_cast<MemoryAccess>(getOperand(i * 2));
    }
    void setIncomingValue(unsigned i, MemoryAccess *V) {
        setOperand(i * 2, V);
    }
    bool isOptimized() const {
        return false;
    }

private:
    const int ID;
    int OptID = -1;
    unsigned ReservedSpace;
    void resizeOperands(unsigned NumofOp) {
        if (NumofOp == 0) {
            NumofOp = (getNumOperands()) * 3 / 2;
            if (NumofOp < 4) NumofOp = 4;
        } else if (NumofOp * 2 > NumOperands) {
            if (ReservedSpace >= NumofOp) return;
        } else if (NumofOp == NumOperands) {
            if (ReservedSpace == NumofOp) return;
        } else {
            return;
        }

        ReservedSpace = NumofOp;
        Use *NewOps = new Use[NumofOp];
        Use *OldOps = OperandList;
        for (int i = 0, e = getNumOperands(); i < e; i++) {
            NewOps[i].init(OldOps[i], this);
            OldOps[i].set(nullptr);
        }
        delete[] OldOps;
        OperandList = NewOps;
    }
};

class MemLoc {
public:
    Value *ptr=nullptr;  //size默认32位
    bool iscall = false;
    CallInst *call=nullptr;
    MemLoc() : ptr(nullptr), iscall(false), call(nullptr) {}
    MemLoc(Instruction *inst) {
        if (auto c = dyn_cast<CallInst>(inst)) {
            iscall = true;
            call = c;
        } else {
            iscall = false;
            if (auto Li = dyn_cast<LoadInst>(inst)) {
                ptr = Li->getPointerOperand();
            }
            if (auto Si = dyn_cast<StoreInst>(inst)) {
                ptr = Si->getPointerOperand();
            }
        }
    }
    static MemLoc get(Instruction *inst) {
        MemLoc st;
        if (auto c = dyn_cast<CallInst>(inst)) {
            st.iscall = true;
            st.call = c;
        } else {
            st.iscall = false;
            if (auto Li = dyn_cast<LoadInst>(inst)) {
                st.ptr = Li->getPointerOperand();
            }
            if (auto Si = dyn_cast<StoreInst>(inst)) {
                st.ptr = Si->getPointerOperand();
            }
        }
        return st;
    }

    bool operator==(const MemLoc &Other) const {
        if (iscall != Other.iscall)
            return false;

        if (!iscall)
            return ptr == Other.ptr;

        if (call->getCalledFunction() != Other.call->getCalledFunction())
            return false;
        for (int i = 0; i < call->getNumOperands(); i++) {
            if (call->getOperand(i) != Other.call->getOperand(i)) return false;
        }
        return true;
    }
    bool operator<(const MemLoc &Other) const {
        return (ptr == Other.ptr) ? call < Other.call : ptr < Other.ptr;
    }
};

class MemStackinfo {
public:
    unsigned long StackEpoch = 0;
    unsigned long popepoch = 0;
    unsigned long lo = 0;
    BasicBlock *lobb = nullptr;
    unsigned long Lastkill = 0;
    bool lastkillvaild = false;
};

void MemSSA(Module *func);

void removeMemoryAccess(MemoryAccess *MA, bool opt);