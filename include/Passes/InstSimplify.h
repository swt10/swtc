#include "IRhead.h"
#include "ConstantFolding.h"
#include "Dominate.h"

using namespace SWTC;

Value* SimplifyInst(Instruction *inst,Dominate *dt);
static Value *SimplifyBin(Instruction::Ops opcode,Value *l,Value *r,int dep,Dominate *dt);
static Value *SimplifyAdd(Value *op1,Value *op2,int dep,Dominate *dt);
static Value *SimplifySub(Value *op1,Value *op2,int dep,Dominate *dt);
static Value *SimplifyMul(Value *op1,Value *op2,int dep,Dominate* dt);
static Value *SimplifySDiv(Value *op1,Value *op2,int dep,Dominate* dt);
static Value *SimplifyUDiv(Value *op1,Value *op2,int dep,Dominate *dt);
static Value *SimplifySRem(Value *op1,Value *op2,int dep,Dominate *dt);
static Value *SimplifyURem(Value *op1,Value *op2,int dep,Dominate *dt);
static Value *SimplifyShl(Value *op1,Value *op2,int dep,Dominate *dt);
static Value *SimplifyLShr(Value *op1,Value *op2,int dep,Dominate *dt);
static Value *SimplifyAShr(Value *op1,Value *op2,int dep,Dominate *dt);
static Value *SimplifyAnd(Value *op1,Value *op2,int dep,Dominate *dt);
static Value *SimplifyOr(Value *op1,Value *op2,int dep,Dominate *dt);
static Value *SimplifyICmp(ICmpInst::Predicate Pred, Value *LHS, Value *RHS, int dep,Dominate *dt);
bool touchinst(Value *inst,Instruction::Ops opcode,Value *&op1,Value *&op2);

