#pragma once
#include "IRhead.h"
#include "GVN.h"
#include <vector>
using namespace SWTC;

struct Varinfo{
    std::set<BasicBlock*> Alivebbs;//活跃的bb
    std::vector<Instruction*> Kills;//这个变量最后使用的inst

    //在bb中寻找被Kill的那条指令
    Instruction *findLast(BasicBlock *bb)
    {
        for(auto x:Kills)
            if(x->getParent()==bb)
            return x;
        return nullptr;
    }
    
    bool removeUse(Instruction *inst)
    {
        for(std::vector<Instruction *>::iterator it=Kills.begin();
            it!=Kills.end();it++)
            {
                if(*it==inst)
                {
                    Kills.erase(it);
                    return true;
                }
            }
        return false;
    }

    //若v在bb活跃，返回true.如果v只被bb里的phi节点use,那么也不活跃
    bool isLiveIn(BasicBlock *bb,Value *v){
        if(Alivebbs.count(bb))return true;
        Instruction *inst=dyn_cast<Instruction>(v);
        if(inst&&inst->getParent()==bb)
            return false;
        return findLast(bb);
    }
};




Varinfo &getVarInfo(Value*v);

//
bool isclobberforValue(Value *a,Value *b);

void LiveVariables(Function *fun);