#include "IRhead.h"
#include "GVN.h"

#include <vector>

using namespace SWTC;
Constant* Const_Eval(Instruction *inst);
Constant *ConstFoldBin(Instruction::Ops opcode,Value *a,Value *b);
Constant *ConstFoldICmp(ICmpInst::Predicate pred, Value *a, Value *b) ;
bool ConstantFoldTerminator(BasicBlock *bb,bool deleteDeadCond);
bool isinstructionnormaldead(Instruction *inst);
Constant *ConstantFoldConstant(Constant *c);
bool RecursivelyDeleteInstructions(Value *I);
