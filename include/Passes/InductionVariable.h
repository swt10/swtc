#pragma once

#include <algorithm>
#include <iostream>
#include <map>
#include <ostream>
#include <set>
#include <unordered_set>
#include <unordered_map>
#include <vector>

#include "IRhead.h"
#include "Loop.h"

using namespace SWTC;

extern LoopInfo *li;

class IV{   //InductionVariable  
public:
    IV *tiv;
    BasicBlock *BB;
    int fctr;
    Value *biv;
    int diff;
};

void runInductionVariable(Function *F);
void findIVs();
void IVPattern();
void MulIV();
void AddIV();
void isLoopConst();
void isAssignBetween();
void noReachDefs();
