#pragma once

#include <algorithm>
#include <iostream>
#include <map>
#include <ostream>
#include <set>
#include <unordered_set>
#include <unordered_map>
#include <vector>

#include "Loop.h"
#include "IRhead.h"
using namespace SWTC;

class FCGnode{
public:
    Function *F = nullptr; //自身（F）
    bool isRecursive = false;
    bool isVisited = false;
    std::unordered_map<FCGnode *, unsigned int> callees;
    FCGnode(Function *f) : F(f), isRecursive(false), isVisited(false){}
    unsigned int getNumInsts(){
        if(F->getEntryBlock() != nullptr){
            int i = 0;
            for(auto BB = F->getEntryBlock(); BB; BB = BB->next){
                for(auto inst = BB->getInstList().head; inst; inst = inst->next){
                    i++;
                }
            }
            return i;
        } else {
            return 0;
        }
    }
};

void runFunctionInline(Module *M);
void init(Module *M);
void inlineFunc(Function *F); //内联F里面可内联的函数
void splitBB(Instruction *inst);
void handlecallee(CallInst *call, Function *callee);
bool isTailRursive(Function *F);
void tailRecursive2Loop(Function *F);



