#pragma once

#include <algorithm>
#include <iostream>
#include <map>
#include <ostream>
#include <set>
#include <unordered_set>
#include <unordered_map>
#include <vector>

#include "IRhead.h"
using namespace SWTC;

class CFGnode{
public:
    bool traversed = false;
    bool isLoopHeader = false;
    int DFSP_pos = 0;
    BasicBlock *BB = nullptr;
    CFGnode *iloop_header = nullptr;
    std::unordered_set<CFGnode *> succ;
    std::unordered_set<CFGnode *> pred;
    CFGnode (BasicBlock *bb) : traversed(false), isLoopHeader(false), DFSP_pos(0), BB(bb), iloop_header(nullptr){}
};

class Loop{
public:
    Loop *parent = nullptr;
    std::set<Loop *> subLoops;
    std::set<BasicBlock *> LoopNodes; 
    BasicBlock *head = nullptr;
    unsigned int getDepth(){
        unsigned int depth = 1;
        auto x = this;
        while(x->parent){
            depth++;
            x = x->parent;
        }
        return depth;
    }
};


class LoopInfo{
public:
    //存放BB->CFGnode的对应关系
    std::unordered_map<BasicBlock *, CFGnode *> map;
    //循环
    std::set<Loop *> loops;
    //回边
    std::set<std::pair<BasicBlock *, BasicBlock *>> backEdge;
    //BB所处的最深的循环
    std::unordered_map<BasicBlock *, Loop *> BB_deepest_loop;
    ~LoopInfo()=default;

    void identify_loops(Function *F);    
    void buildCFG(Function *F, std::unordered_map<BasicBlock *, CFGnode *> &map);
    CFGnode *trav_loops_dfs(CFGnode *b0, int DFSP_pos);
    void tag_lhead(CFGnode *b, CFGnode *h);

    unsigned int getBBDepth(BasicBlock *BB);
    Loop *getLoop(BasicBlock *BB);
    bool isBBInLoop(BasicBlock *BB, Loop *loop);
};
