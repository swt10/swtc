#include "IRhead.h"
#include "Dominate.h"

#include <unordered_map>
#include <unordered_set>
using namespace SWTC;
extern Dominate *dt;
class CSENode
{
    public:
    CSENode(BasicBlock*B,int cur,std::map<Instruction*,Value*>&availValue,std::map<Value*,unsigned>&availInvari,std::map<Value*,std::pair<Instruction*,unsigned>> &AvailableLoads,std::map<Value*,std::pair<Instruction*,unsigned>> &AvailableCalls):
    bb(B),cur(cur),child(cur),availValue(availValue),availInvari(availInvari),AvailableCalls(AvailableCalls),AvailableLoads(AvailableLoads),
    childit(dt->DTSuccBlocks[B].begin()),
    end(dt->DTSuccBlocks[B].end()){}
    BasicBlock *bb;
    int cur,child;
    bool isrun=false;
    std::unordered_set<BasicBlock *>::iterator childit;
    std::unordered_set<BasicBlock *>::iterator end;
    std::map<Instruction*,Value*>availValue;
    std::map<Value*,unsigned>availInvari;
    std::map<Value*,std::pair<Instruction*,unsigned>> AvailableLoads;
    std::map<Value*,std::pair<Instruction*,unsigned>> AvailableCalls;
};

void CSE(Function *func);