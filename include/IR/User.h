#ifndef SWTC_USER_H
#define SWTC_USER_H

#include <vector>

#include "Casting.h"
#include "Use.h"
#include "Value.h"

namespace SWTC {

/// customizes
template <class>
struct OperandTraits;

class User : public Value {
protected:
    ~User() = default;
    //op表
    Use *OperandList=nullptr;
    unsigned NumOperands=0;

public:
    User(Type *Ty, unsigned vty, Use *OpList, unsigned NumOps)
        : Value(Ty, vty), OperandList(OpList), NumOperands(NumOps) {}

    Value *getOperand(unsigned i) const {
        return OperandList[i];
    }

    void setOperand(unsigned i, Value *Val) {
        OperandList[i] = Val;
    }



    unsigned getNumOperands() const { return NumOperands; }

    //little define
    typedef Use *op_iterator;
    typedef const Use *const_op_iterator;

    op_iterator op_begin() { return OperandList; }
    const_op_iterator op_begin() const { return OperandList; }
    op_iterator op_end() { return OperandList + NumOperands; }
    const_op_iterator op_end() const { return OperandList + NumOperands; }
    //置空所有op
    void dropAllReferences() {
        Use *OL = OperandList;
        for (unsigned i = 0, e = NumOperands; i != e; ++i)
            OL[i].set(nullptr);
    }
    //改变所有use的Value从From到to
    void replaceUsesOfWith(Value *From, Value *To)
    {
        for(int i=0,e=getNumOperands();i<e;i++)
        {
            if(getOperand(i)==From){
                setOperand(i,To);
            }
        }
    }

    static bool classof(const User *) { return true; }
    static bool classof(const Value *V) {
        return isa<Instruction>(V) || isa<Constant>(V);
    }
};

}  // namespace SWTC

#endif  // !SWTC_USER_H
