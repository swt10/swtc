#ifndef SWTC_CONSTANT_H
#define SWTC_CONSTANT_H

#include <map>
#include <set>
#include <utility>

#include "Casting.h"
#include "DerivedTypes.h"
#include "Type.h"
#include "User.h"
#include "Value.h"


namespace SWTC {

class Constant : public User {
protected:
    Constant(Type *ty, ValueTy vty, Use *Ops, unsigned NumOps)
        : User(ty, vty, Ops, NumOps){};

    ~Constant() = default;
public:
    static inline bool classof(const Constant *) { return true; }
    static inline bool classof(const GlobalValue *) { return true; }
    static inline bool classof(const Value *V) {
        return V->getValueID() >= ConstantFirstVal &&
           V->getValueID() <= ConstantLastVal;
  }
};

class ConstantInt final : public Constant {
    static ConstantInt *TheTrueVal, *TheFalseVal, *TheZeroVal;
    ConstantInt(IntegerType *Ty, const i32 &V) : Constant(Ty, ConstantIntVal, nullptr, 0), Val(V){};
    i32 Val=0;

public:
    inline const i32 &getValue() const {
        return Val;
    }
    unsigned getBitWidth() const {
        if (this == TheTrueVal || this == TheFalseVal)
            return 1;
        else
            return 32;
    }

    bool equalsInt(i32 V) const {
        return Val == V;
    }

    static ConstantInt *CreateZeroVal() {
        return get(0);
    }

    static ConstantInt *getZeroVal() {
        if (TheZeroVal != nullptr)
            return TheZeroVal;
        return CreateZeroVal();
    }

    static ConstantInt *CreateTrueFalseVals(bool WhichOne) {
        TheTrueVal = new ConstantInt(Type::Int1Ty, 1);
        TheFalseVal = new ConstantInt(Type::Int1Ty, 0);
        return WhichOne ? TheTrueVal : TheFalseVal;
    };

    static inline ConstantInt *getTrue() {
        if (TheTrueVal)
            return TheTrueVal;
        return TheTrueVal = new ConstantInt(Type::Int1Ty, 1);
    }

    static inline ConstantInt *getFalse() {
        if (TheFalseVal)
            return TheFalseVal;
        return TheFalseVal = new ConstantInt(Type::Int1Ty, 0);;
    }


    static std::map<i32, ConstantInt *> Map;

    static ConstantInt *get(const Type *Ty, i32 V, bool isSigned = false) {
        return get(V);
    };
    static ConstantInt *get(const i32 &V) {
        if(V==0)return getFalse();
        if(V==1)return getTrue();
        if (Map.find(V) == Map.end()) {
            Map[V] = new ConstantInt(Type::Int32Ty, V);
            // std::cerr<<"MAp";
            return Map[V];
        }
        return Map[V];
    };

    inline const IntegerType *getType() const {
        return reinterpret_cast<const IntegerType *>(Value::getType());
    }

    bool isNullValue() const {
        return Val == 0;
    }

    bool isZero() const {
        return Val == 0;
    }

    bool isOne() const {
        return Val == 1;
    }

    static bool classof(const Value *V) {
        if(!V)return false;
        return V->getValueID() == ConstantIntVal;
    }
};

class ConstantArray : public Constant {
    friend class Constant;
    std::vector<Constant *> const_array;

protected:
    ConstantArray(ArrayType *ty, const std::vector<Constant *> &Val)
        : Constant(ty, ConstantArrayVal, new Use[Val.size()], Val.size()) {
        Use *OL = OperandList;
        for (std::vector<Constant *>::const_iterator I = Val.begin(), E = Val.end();
             I != E; ++I, ++OL) {
            Constant *C = *I;
            OL->init(C, this);
        }
        this->const_array.assign(Val.begin(), Val.end());
    };
    ~ConstantArray() {
        delete[] OperandList;
    };

public:
    static Constant *get(ArrayType *T, const std::vector<Constant *> &V) {
        return new ConstantArray(T, V);
    };
    static Constant *get(ArrayType *T,
                         Constant *const *Vals, unsigned NumVals) {
        // FIXME: make this the primary ctor method.
        return get(T, std::vector<Constant *>(Vals, Vals + NumVals));
    }
    Constant *getElementValue(int index) {
        return this->const_array[index];
    }
    int getElementSize() {
        return this->const_array.size();
    }
    static inline bool classof(const ConstantArray *) { return true; }

    static bool classof(const Value *V) {
        return V->getValueID() == ConstantArrayVal;
    }
};

class UndefValue: public Constant {
    UndefValue(const UndefValue &);
protected:
    explicit UndefValue(Type *T):Constant(T,UndefValueVal,nullptr,0){}
public: 
    static UndefValue *get(Type *T){
        return new UndefValue(T);
    }
    static bool classof(const Value *V) {
    return V->getValueID() == UndefValueVal ;
  }
};

}  // namespace SWTC

#endif  // !SWTC_CONSTANT_H
