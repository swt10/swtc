#pragma once

#include <cassert>
#include <cstdint>
#include <type_traits>
#include <variant>

#define COLOR_RESET "\033[0m"
#define COLOR_BLACK "\033[30m"              /* Black */
#define COLOR_RED "\033[31m"                /* Red */
#define COLOR_GREEN "\033[32m"              /* Green */
#define COLOR_YELLOW "\033[33m"             /* Yellow */
#define COLOR_BLUE "\033[34m"               /* Blue */
#define COLOR_MAGENTA "\033[35m"            /* Magenta */
#define COLOR_CYAN "\033[36m"               /* Cyan */
#define COLOR_WHITE "\033[37m"              /* White */
#define COLOR_BOLDBLACK "\033[1m\033[30m"   /* Bold Black */
#define COLOR_BOLDRED "\033[1m\033[31m"     /* Bold Red */
#define COLOR_BOLDGREEN "\033[1m\033[32m"   /* Bold Green */
#define COLOR_BOLDYELLOW "\033[1m\033[33m"  /* Bold Yellow */
#define COLOR_BOLDBLUE "\033[1m\033[34m"    /* Bold Blue */
#define COLOR_BOLDMAGENTA "\033[1m\033[35m" /* Bold Magenta */
#define COLOR_BOLDCYAN "\033[1m\033[36m"    /* Bold Cyan */
#define COLOR_BOLDWHITE "\033[1m\033[37m"   /* Bold White */

using i32 = int32_t;

template <typename D, typename B>
bool isa(const B *b) {
    if constexpr (std::is_base_of_v<D, B>) {
        return true;
    } else {
        static_assert(std::is_base_of_v<B, D>);
        return D::classof(b);
    }
}

template <typename D, typename B>
const D *dyn_cast(const B *b) {
    return isa<D>(b) ? static_cast<const D *>(b) : nullptr;
}

template <typename D, typename B>
D *dyn_cast(B *b) {
    return isa<D>(b) ? static_cast<D *>(b) : nullptr;
}

template <typename D, typename B>
const D *dyn_cast_nullable(const B *b) {
    return b && isa<D>(b) ? static_cast<const D *>(b) : nullptr;
}

template <typename D, typename B>
D *dyn_cast_nullable(B *b) {
    return b && isa<D>(b) ? static_cast<D *>(b) : nullptr;
}

template <typename... T>
struct overload : T... {
    using T::operator()...;
};

template <typename... T>
overload(T...) -> overload<T...>;

#define TEST_TYPE(QQQ) std::visit(overload{                                                                          \
                                      [](FunctionType *t) { std::cerr << COLOR_BOLDWHITE << "FuncitonType\n"; },     \
                                      [](PointerType *t) { std::cerr << COLOR_BOLDWHITE << "PointerType\n"; },       \
                                      [](IntegerType *t) { std::cerr << COLOR_BOLDWHITE << "IntegerType\n"; },       \
                                      [](ArrayType *t) { std::cerr << COLOR_BOLDWHITE << "ArrayType\n"; },           \
                                      [](ConstantInt *t) { std::cerr << COLOR_BOLDWHITE << "ConstantInt\n"; },       \
                                      [](GlobalVariable *t) { std::cerr << COLOR_BOLDWHITE << "GlobalVariable\n"; }, \
                                      [](ConstantArray *t) { std::cerr << COLOR_BOLDWHITE << "ConstantArray\n"; },   \
                                      [](Type *t) { std::cerr << COLOR_BOLDWHITE << "NormalType\n"; },               \
                                  },                                                                                 \
                                  std::variant<Type *, FunctionType *, PointerType *, IntegerType *, ArrayType *, ConstantInt *, GlobalVariable *, ConstantArray *>(QQQ));
