#pragma once

#include "Function.h"
#include "Value.h"

namespace SWTC {
class Function;
class Argument final : public Value {
    Function *Parent=nullptr;

    void setParent(Function *parent);

public:
    Argument *prev=nullptr, *next=nullptr;  

    explicit Argument(Type *Ty,
                      const std::string &Name = "",
                      Function *F = nullptr) : Value(Ty, Value::ArgumentVal), Parent(F) {
        setName(Name);
    };

    inline const Function *getParent() const { return Parent; }
    inline Function *getParent() { return Parent; }

    Argument *getNext() { return next; }
    const Argument *getNext() const { return next; }
    Argument *getPrev() { return prev; }
    const Argument *getPrev() const { return prev; }
    static inline bool classof(const Argument *) { return true; }
    static inline bool classof(const Value *V) {
        return V->getValueID() == ArgumentVal;
    }
};

}  // namespace SWTC