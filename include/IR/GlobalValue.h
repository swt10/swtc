#pragma once
#ifndef SWTC_GLOBALVALUE_H
#define SWTC_GLOBALVALUE_H

#include "Constant.h"

namespace SWTC {

class PointerType;
class Module;

class GlobalValue : public Constant {
protected:
    GlobalValue(Type *Ty, ValueTy vty, Use *Ops, unsigned NumOps,
                const std::string &name = "")
        : Constant(PointerType::get(Ty), vty, Ops, NumOps), ValueType(Ty), Parent(nullptr) {
        if (!name.empty())
            setName(name);
    }
    Module *Parent=nullptr;
    static const unsigned GlobalValueSubClassDataBits = 16;
    Type *ValueType=nullptr;

private:
    unsigned SubClassData : GlobalValueSubClassDataBits;

public:
    friend class Constant;
    ~GlobalValue() {
    }

    unsigned getGlobalValueSubClassData() const {
        return SubClassData;
    }
    void setGlobalValueSubClassData(unsigned V) {
        SubClassData = V;
    }
    // Used by SymbolTableListTraits.
    void setParent(Module *parent) {
        Parent = parent;
    }
    GlobalValue(const GlobalValue &) = delete;

    /// Global values are always pointers.
    PointerType *getType() const { return dyn_cast<PointerType>(User::getType()); }

    Type *getValueType() const { return ValueType; }

    bool isDefinitionExact() const {
        return true;
    }

    /// This method unlinks 'this' from the containing module, but does not delete
    /// it.
    void removeFromParent();

    /// This method unlinks 'this' from the containing module and deletes it.
    void eraseFromParent();

    /// Get the module that this global value is contained inside of...
    Module *getParent() { return Parent; }
    const Module *getParent() const { return Parent; }

    static bool classof(const Value *V) {
        return V->getValueID() == Value::FunctionVal ||
               V->getValueID() == Value::GlobalVariableVal;
    }
};

}  // namespace SWTC

#endif  // !SWTC_GLOBALVALUE_H