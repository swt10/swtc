#ifndef SWTC_VALUE_H
#define SWTC_VALUE_H

#include <array>
#include <cassert>
#include <iostream>
#include <string>
#include <vector>

#include "Casting.h"
#include "Use.h"
#include "ilist.h"

namespace SWTC {

class Constant;
class BasicBlock;
class Instruction;
class GlobalValue;
class GlobalVariable;
class Function;
class Type;

class Value {
private:
    Type *Vty=nullptr;
    ilist<Use> UseList;
    std::string name;
    const unsigned char SubclassID;

protected:
    unsigned SubclassData;

public:
    Value(Type *Ty, unsigned scid)
        : SubclassID(scid), SubclassData(0), Vty(Ty) {}

    //Donot use it

    ~Value() = default;

    void print(std::ostream &O) const;

    unsigned getValueID() const {
        return SubclassID;
    }

    inline Type *getType() const {
        // std::cerr<<"cmpinst\n";
        return Vty;
    }

    // value's name
    std::string getName() const { return name; }
    //change name
    void setName(const std::string &_name) {
        name = _name;
    }
    //transfer name from V to this
    void takeName(Value *V){
        name=V->getName();
    }
    //Change all uses of this Value to V
    void replaceAllUsesWith(Value *V) {
        if(V==this)return ;
        while (UseList.head)
            UseList.head->set(V);
    }
    bool use_empty() 
    {
        return UseList.size()==0;
    }
    Use *use_begin() 
    {
        return UseList.head;
    }
    ilist<Use>& getUseList(){
        return UseList;
    }
    //这里用的ilist所以end是最后一个而不是空的
    Use *use_end() 
    {
        return UseList.tail;
    }

    void addUse(Use *U) { UseList.insertAtEnd(U); }

    void killUse(Use *U) { UseList.remove(U); }

    enum ValueTy {
        ArgumentVal,               // This is an instance of Argument
        BasicBlockVal,             // This is an instance of BasicBlock
        FunctionVal,               // This is an instance of Function
        GlobalVariableVal,         // This is an instance of GlobalVariable
        UndefValueVal,             // This is an instance of UndefValue
        ConstantExprVal,           // This is an instance of ConstantExpr
        ConstantAggregateZeroVal,  // This is an instance of ConstantAggregateNull
        ConstantIntVal,            // This is an instance of ConstantInt
        ConstantArrayVal,          // This is an instance of ConstantArray
        InstructionVal,            // This is an instance of Instruction
        MemoryUseVal,                 // Use in memSSA analysis
        MemoryDefVal,
        MemoryPhiVal,

        // Markers:
        ConstantFirstVal = FunctionVal,
        ConstantLastVal = ConstantArrayVal
    };
    static inline bool classof(const Value *) {
        return true;  // Values are always values.
    }
};

}  // namespace SWTC

#endif