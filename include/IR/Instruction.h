#ifndef SWTC_INSTRUCTION_H
#define SWTC_INSTRUCTION_H

#include <algorithm>
#include <cassert>
#include <cstdint>
#include <list>
#include <utility>

#include "Casting.h"
#include "Module.h"
#include "User.h"
#include "Value.h"

namespace SWTC {
class Module;
class Instruction : public User {
    BasicBlock *Parent=nullptr;

public:
    Instruction *prev=nullptr, *next=nullptr;
    mutable unsigned Order = 0;
    enum Ops {
#include "Instruction.def"
    };
    void setParent(BasicBlock *parent) { Parent = parent; }
    inline BasicBlock *getParent() const { return Parent; }
    inline BasicBlock *getParent() { return Parent; }

    const Module *getModule() const;
    /// Return the function this instruction belongs to.
    Function *getFunction() const;
    Function *getFunciton() {
        return const_cast<Function *>(
            static_cast<const Instruction *>(this)->getFunction());
    }

    virtual Instruction *clone() const = 0;

    bool comesBefore(Instruction *other) const;

    /// removeFromParent - This method unlinks 'this' from the containing basic
    /// block, but does not delete it.
    ///

    /// Return true if this instruction may modify memory.
    bool mayWriteToMemory();

    /// Return true if this instruction may read memory.
    bool mayReadFromMemory();

    void removeFromParent();

    /// eraseFromParent - This method unlinks 'this' from the containing basic
    /// block and deletes it.
    ///
    void eraseFromParent();

    /// moveBefore - Unlink this instruction from its current basic block and
    /// insert it into the basic block that MovePos lives in, right before
    /// MovePos.
    void moveBefore(Instruction *MovePos);

    bool isIdenticalto(Instruction *I);

    bool isSameOperation(Instruction *I);

    unsigned getOpcode() const { return getValueID() - InstructionVal; }

    static std::string getOpcodeName(unsigned OpCode);
    bool isTerminator() const { return isTerminator(getOpcode()); }
    bool isUnaryOp() const { return isUnaryOp(getOpcode()); }
    bool isBinaryOp() const { return isBinaryOp(getOpcode()); }
    bool isIntDivRem() const { return isIntDivRem(getOpcode()); }
    bool isShift() const { return isShift(getOpcode()); }
    //bool isCast() const { return isCast(getOpcode()); }

    static inline bool isTerminator(unsigned OpCode) {
        return OpCode >= Ret && OpCode <= Br;
    }

    static inline bool isUnaryOp(unsigned Opcode) {
        return Opcode >= 0 && Opcode < 0;
    }
    static inline bool isBinaryOp(unsigned Opcode) {
        return Opcode >= Add && Opcode < SRem;
    }

    static inline bool isIntDivRem(unsigned Opcode) {
        return Opcode == UDiv || Opcode == SDiv || Opcode == URem || Opcode == SRem;
    }

    /// Determine if the Opcode is one of the shift instructions.
    static inline bool isShift(unsigned Opcode) {
        return Opcode >= Shl && Opcode <= AShr;
    }

    /// Return true if this is a logical shift left or a logical shift right.
    inline bool isLogicalShift() const {
        return getOpcode() == Shl || getOpcode() == LShr;
    }

    /// Return true if this is an arithmetic shift right.
    inline bool isArithmeticShift() const {
        return getOpcode() == AShr;
    }

    /// Determine if the Opcode is and/or/xor.
    static inline bool isBitwiseLogicOp(unsigned Opcode) {
        return Opcode == And || Opcode == Or || Opcode == Xor;
    }

    /// Return true if this is and/or/xor.
    inline bool isBitwiseLogicOp() const {
        return isBitwiseLogicOp(getOpcode());
    }

    /// Return true if the instruction is commutative:
    ///
    ///   Commutative operators satisfy: (x op y) === (y op x)
    ///
    static bool isCommutative(unsigned Opcode) {
        switch (Opcode) {
            case Add:
            case Mul:
            case And:
            case Or:
            case Xor:
                return true;
            default:
                return false;
        }
    }

    /// Return true if the instruction is idempotent:
    ///
    ///   Idempotent operators satisfy:  x op x === x

    bool isIdempotent() const { return isIdempotent(getOpcode()); }
    static bool isIdempotent(unsigned Opcode) {
        return Opcode == And || Opcode == Or;
    }

    /// Return true if the instruction is nilpotent:
    ///
    ///   Nilpotent operators satisfy:  x op x === Id,
    ///
    ///   where Id is the identity for the operator, i.e. a constant such that
    ///     x op Id === x and Id op x === x for all x.
    bool isNilpotent() const { return isNilpotent(getOpcode()); }
    static bool isNilpotent(unsigned Opcode) {
        return Opcode == Xor;
    }

    bool willreturn() const;

    static inline bool classof(const Instruction *) { return true; }
    static inline bool classof(const Value *V) {
        if (!V) return false;
        return V->getValueID() >= Value::InstructionVal;
    }

protected:
    Instruction(Type *Ty, unsigned iType, Use *Ops, unsigned NumOps,
                Instruction *InsertBefore = nullptr);

    Instruction(Type *Ty, unsigned iType, Use *Ops, unsigned NumOps,
                BasicBlock *InsertAtEnd);
};

}  // namespace SWTC

#endif  // !SWTC_INSTRUCTION_H
