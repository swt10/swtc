#ifndef SWTC_FUNCTION_H
#define SWTC_FUNCTION_H

#include <iterator>
#include <list>
#include <vector>

#include "Argument.h"
#include "BasicBlock.h"
#include "Casting.h"
#include "GlobalValue.h"
#include "Value.h"
#include "ilist.h"

namespace SWTC {

class FunctionType;
class Argument;

class Function : public GlobalValue {
public:
    using BasicBlockListType = ilist<BasicBlock>;
    using ArgumentListType = ilist<Argument>;
    using ValueSymbolTable = std::map<const std::string, const Value *>;

private:
    BasicBlockListType BasicBlocks;
    ArgumentListType ArgumentList;
    ValueSymbolTable SymbolTable;
    bool isBuiltin;

    void setParent(Module *parent) {
        Parent = parent;
    };

    Function *getNext() { return next; }
    const Function *getNext() const { return next; }
    Function *getPrev() { return prev; }
    const Function *getPrev() const { return prev; }

public:
    Function *prev=nullptr, *next=nullptr;
    Function(FunctionType *Ty, const std::string &N = "", Module *M = nullptr, bool isbuild = 0);
    bool writeMemory=true,readMemory=true;
    ~Function();

    bool isBuildin() const {
        return isBuiltin;
    };
    const Type *getReturnType() const {
        return getFunctionType()->getReturnType();
    };
    // Return the type of the ret val
    FunctionType *getFunctionType() const {
        return dyn_cast<FunctionType>(getType()->getElementType());
    };

    bool isVarArg() const {
        return getFunctionType()->isVarArg();
    };

    /// getCallingConv()/setCallingConv(uint) - These method get and set the
    /// calling convention of this function.  The enum values for the known
    /// calling conventions are defined in CallingConv.h.
    unsigned getCallingConv() const { return SubclassData; }
    void setCallingConv(unsigned CC) { SubclassData = CC; }

    /// removeFromParent - This method unlinks 'this' from the containing module,
    /// but does not delete it.
    ///=erase in my design
    void removeFromParent();

    
    void dropAllReferences() {
        while (auto I = BasicBlocks.head)
        {
            I->eraseFromParent();
        }
    }
    /// eraseFromParent - This method unlinks 'this' from the containing module
    /// and deletes it.
    ///
    void eraseFromParent() {
        getParent()->getFunctionList().remove(this);
        dropAllReferences();
    };

    bool doesNotAccessMemory()
    {
        return !readMemory&&!writeMemory;
    }
    ArgumentListType &getArgumentList() { return ArgumentList; }

    BasicBlockListType &getBasicBlockList() { return BasicBlocks; }

    BasicBlock *getEntryBlock() { return BasicBlocks.head; }

    Argument *arg_begin() { return ArgumentList.head; }
    Argument *arg_end() { return ArgumentList.tail; }
    static inline bool classof(const Function *) { return true; }
    static inline bool classof(const Value *V) {
        return V->getValueID() == Value::FunctionVal;
    }
};

}  // namespace SWTC

#endif  // !SWTC_FUNCTION_H
