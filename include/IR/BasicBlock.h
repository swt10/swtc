#ifndef SWTC_BASICBLOCK_H
#define SWTC_BASICBLOCK_H

#include <list>
#include <string>
#include <vector>
#include <unordered_set>

#include "Casting.h"
#include "Instruction.h"
#include "Module.h"
#include "Value.h"
#include "ilist.h"

namespace SWTC {

class CallInst;
class Function;
class Module;
class PHINode;

class BasicBlock final : public Value {
public:
    using InstListType = ilist<Instruction>;

    bool isVisit;
    std::vector<BasicBlock *> pred;  //基本块前驱
    std::vector<BasicBlock *> succ;  //基本块后继
    std::unordered_set<BasicBlock *> dominate_by;  // 支配它的节点集
    std::vector<BasicBlock *> dominates;  //被它支配的节点集
    unsigned int dom_level;
    BasicBlock *idom=nullptr;
    

private:
    InstListType InstList;
    Function *Parent=nullptr;

public:
    BasicBlock *prev=nullptr, *next=nullptr;
    const Function *getParent() const {
        return Parent;
    }
    void setParent(Function *parent) {
        Parent = parent;
    };

    void pred_init();

    void succ_init();

    void renumberInstructions(){
        int Order=0;
        for(auto it=InstList.head;it;it=it->next)
        {
            it->Order=Order++;
        }
    }
    explicit BasicBlock(const std::string &Name = "",
                        Function *Parent = nullptr,
                        BasicBlock *InsertBefore = nullptr) : Value(Module::getLabelTy(), Value::BasicBlockVal),
                                                              Parent(nullptr) {
        if (Parent)
            insertInto(Parent, InsertBefore);
        setName(Name);
    };
    ~BasicBlock() {
        dropAllReferences();
        while(InstList.head)
        InstList.remove(InstList.head);
    }
    Function *getParent() { return Parent; }
    const Module *getModule() const;
    Module *getModule() {
        return const_cast<Module *>(
            static_cast<const BasicBlock *>(this)->getModule());
    }
    const Instruction *getTerminator() const {
        if (InstList.head==nullptr || !((InstList.tail)->isTerminator()))
            return nullptr;
        return InstList.tail;
    };
    Instruction *getTerminator() {
        return const_cast<Instruction *>(
            static_cast<const BasicBlock *>(this)->getTerminator());
    }
    const Instruction *getFirstNonPHI() const {
        for (Instruction *I = InstList.head; I ;I = I->next)
            if (!isa<PHINode>(I))
                return I;
        return nullptr;
    };
    Instruction *getFirstNonPHI() {
        return const_cast<Instruction *>(
            static_cast<const BasicBlock *>(this)->getFirstNonPHI());
    }
    void removeFromParent();
    void eraseFromParent();
    void moveBefore(BasicBlock *MovePos);
    void moveAfter(BasicBlock *MovePos);
    void insertInto(Function *Parent, BasicBlock *InsertBefore = nullptr);

    /// Return the successor of this block if it has a single successor.
    /// Otherwise return a null pointer.
    ///
    /// This method is analogous to getSinglePredecessor above.
    const BasicBlock *getSingleSuccessor() const;
    BasicBlock *getSingleSuccessor() {
        return const_cast<BasicBlock *>(
            static_cast<const BasicBlock *>(this)->getSingleSuccessor());
    }
    const InstListType &getInstList() const { return InstList; }
    InstListType &getInstList() { return InstList; }
    void dropAllReferences();

    void resetParent()
    {
        for(auto it=InstList.head;it;it=it->next)it->setParent(this);
    }
    

    /// Update PHI nodes in this BasicBlock before removal of predecessor \p Pred.
    /// Note that this function does not actually remove the predecessor.
    ///
    /// If \p KeepOneInputPHIs is true then don't remove PHIs that are left with
    /// zero or one incoming values, and don't simplify PHIs with all incoming
    /// values the same.
    void removePredecessor(BasicBlock *Pred, bool KeepOneInputPHIs = false);

    BasicBlock *splitBasicBlock(Instruction *I, const std::string &BBName = "",
                                bool Before = false);

    static inline bool classof(const BasicBlock *) { return true; }
    static inline bool classof(const Value *V) {
        return V->getValueID() == Value::BasicBlockVal;
    }
};

}  // namespace SWTC

#endif  // !SWTC_BASICBLOCK_H