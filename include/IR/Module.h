#pragma once

#include <iterator>
#include <list>
#include <map>
#include <string>
#include <vector>

#include "DerivedTypes.h"
#include "Function.h"
#include "GlobalVariable.h"

namespace SWTC {

class FunctionType;

class Module {
    using GlobalList = ilist<GlobalVariable>;
    using FunctionList = ilist<Function>;
    using ValueSymbolTable = std::map<const std::string, const Value *>;

private:
    GlobalList global_list;
    FunctionList function_list;
    std::string module_id;

    IntegerType *int1_ty_=nullptr;
    IntegerType *int32_ty_=nullptr;
    Type *label_ty_=nullptr;
    Type *void_ty_=nullptr;
    PointerType *int32ptr_ty_=nullptr;

    friend class Constant;
    friend class Type;

public:
    friend std::ostream &operator<<(std::ostream &os, const Module *dt);

    explicit Module(const std::string &module_id)
        : module_id(module_id) {}

    ~Module();

    const std::string &getModuleIdentifier() const { return this->module_id; }

    static Type *getVoidTy() {
        return Type::VoidTy;
    }
    static Type *getLabelTy() {
        return Type::LabelTy;
    }
    static IntegerType *getInt1Ty() {
        return Type::Int1Ty;
    };
    static IntegerType *getInt32Ty() {
        return Type::Int32Ty;
    };
    PointerType *getInt32PtrTy() {
        return Type::Int32PtrTy;
    }

    unsigned getInstructionCount();

    void setModuleIdentifier(std::string &ID) { this->module_id = ID; }

    /// Return the global value in the module with the specified name, of
    /// arbitrary type. This method returns null if a global with the specified
    /// name is not found.
    GlobalValue *getNamedValue(std::string Name) const;

    /// Look up the specified function in the module symbol table. Four
    /// possibilities:
    ///   1. If it does not exist, add a prototype for the function and return it.
    ///   2. Otherwise, if the existing function has the correct prototype, return
    ///      the existing function.
    ///   3. Finally, the function exists but has the wrong prototype: return the
    ///      function with a constantexpr cast to the right prototype.
    ///
    /// In all cases, the returned value is a FunctionCallee wrapper around the
    /// 'FunctionType *T' passed in, as well as a 'Value*' either of the Function or
    /// the bitcast to the function.

    FunctionCallee getOrInsertFunction(std::string Name, FunctionType *T);

    /// Look up the specified function in the module symbol table. If it does not
    /// exist, return null.
    Function *getFunction(std::string Name) const;

    GlobalVariable *getOrInsertGlobalVariable(std::string Name, GlobalVariable *T);

    /// getNamedGlobal - Return the first global variable in the module with the
    /// specified name, of arbitrary type.  This method returns null if a global
    /// with the specified name is not found.
    GlobalVariable *getNamedGlobal(const std::string &Name) const;

public:
    const GlobalList &getGlobalList() const { return this->global_list; }
    /// Get the Module's list of global variables.
    GlobalList &getGlobalList() { return this->global_list; }

    FunctionList getFunctions() const { return this->function_list; }

    static GlobalList Module::*getSublistAccess(GlobalVariable *) {
        return &Module::global_list;
    }

    /// Get the Module's list of functions (constant).
    const FunctionList &getFunctionList() const { return this->function_list; }
    /// Get the Module's list of functions.
    FunctionList &getFunctionList() { return this->function_list; }
    static FunctionList Module::*getSublistAccess(Function *) {
        return &Module::function_list;
    }
};

std::ostream &operator<<(std::ostream &os, const Module *dt);

}  // namespace SWTC