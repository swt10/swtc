#pragma once

#include <iterator>
#include <list>

#include "GlobalValue.h"

namespace SWTC {

class Module;
class Constant;
class PointerType;

class GlobalVariable : public GlobalValue {
    void setParent(Module *parent) {
        Parent = parent;
    };

public:
    GlobalVariable *prev=nullptr, *next=nullptr;

    bool isConstantGlobal : 1;
    Use Initializer;

public:
    GlobalVariable(Type *Ty, bool isConstant, Constant *InitVal = nullptr, const std::string &Name = "",
                   Module *Parent = 0);
    GlobalVariable(Type *Ty, bool isConstant, Constant *InitVal, const std::string &Name,
                   GlobalVariable *InsertBefore);

    bool isDeclaration() const { return getNumOperands() == 0; }
    ///
    inline bool hasInitializer() const { return !isDeclaration(); }
    inline Constant *getInitializer() const {
        return reinterpret_cast<Constant *>(Initializer.get());
    }
    inline Constant *getInitializer() {
        return reinterpret_cast<Constant *>(Initializer.get());
    }
    inline void setInitializer(Constant *CPV) {
        if (CPV == 0) {
            if (hasInitializer()) {
                Initializer.set(0);
                NumOperands = 0;
            }
        } else {
            if (!hasInitializer())
                NumOperands = 1;
            Initializer.set(CPV);
        }
    }

    bool isConstant() const { return isConstantGlobal; }
    void setConstant(bool Value) { isConstantGlobal = Value; }
    void removeFromParent();

    void eraseFromParent();

    void replaceUsesOfWithOnConstant(Value *From, Value *To, Use *U) {
        this->setOperand(0, dyn_cast<Constant>(To));
    };
    static inline bool classof(const GlobalVariable *) { return true; }
    static inline bool classof(const Value *V) {
        if(!V)return false;
        return V->getValueID() == Value::GlobalVariableVal;
    }
};

}  // namespace SWTC