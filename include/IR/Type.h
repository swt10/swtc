#ifndef SWTC_TYPE_H
#define SWTC_TYPE_H

#include <cassert>
#include <iostream>

#include "Casting.h"

namespace SWTC {
/// Type类的实例是不会改变的，使用静态方法创建，比较

class IntegerType;
class PointerType;

class Type {
public:
    enum TypeID {
        VoidTyID,   ///<0
        LabelTyID,  ///<1
        ///派生类型
        IntegerTyID,   ///<2
        FunctionTyID,  ///<3
        PointerTyID,   ///<4
        ArrayTyID      ///<5
    };
    friend class ASMBuilder;

private:
    TypeID ID : 8;
    unsigned SubclassData : 24;

protected:
    explicit Type(TypeID id) : ID(id), SubclassData(0) {}

    ~Type() = default;

    unsigned getSubclassData() const { return SubclassData; }

    void setSubClassData(unsigned val) {
        SubclassData = val;
        assert(getSubclassData() == val && "Subclass data too large for field");
    }

public:
    Type(const Type *t) : ID(t->ID), SubclassData(t->SubclassData) {}
    void print(std::ostream &O) const;
    void print(std::ostream *O) const {
        if (O)
            print(*O);
    }

    void dump() const;

    /// getTypeID
    TypeID getTypeID() const { return ID; }
    Type *getElementType();

    int size(bool origin = true);

    ///
    bool isVoidTy() const { return getTypeID() == VoidTyID; }

    bool isLableTy() const { return getTypeID() == LabelTyID; }

    /// True if this is an instance of IntegerType.
    bool isIntegerTy() const { return getTypeID() == IntegerTyID; }
    bool isFunctionTy() const { return getTypeID() == FunctionTyID; }
    bool isPointerTy() const { return getTypeID() == PointerTyID; }
    bool isArrayTy() const { return getTypeID() == ArrayTyID; }
    /// Return true if this is an integer type or a pointer type.
    bool isIntOrPtrTy() const { return isIntegerTy() || isPointerTy(); }
    /// Return true if the type is "first class", meaning it is a valid type for a
    /// Value.
    bool isFirstClassType() const {
        return getTypeID() != FunctionTyID && getTypeID() != VoidTyID;
    }

    /// Return true if the type is a valid type for a register in codegen. This
    /// includes all first-class types except struct and array types.
    bool isSingleValueType() const {
        return isIntegerTy() || isPointerTy();
    }

    bool isAggregateType() const {
        return getTypeID() == ArrayTyID;
    }

    uint64_t getArrayNumElements() const;
    bool operator==(Type rhs);

    static Type *VoidTy, *LabelTy;
    static IntegerType *Int1Ty, *Int32Ty;
    static PointerType *Int32PtrTy;
};


/// getTypeID
}  // namespace SWTC

#endif  // !SWTC_TYPE_H
