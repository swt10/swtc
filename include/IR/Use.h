#ifndef SWTC_USE_H
#define SWTC_USE_H

#include "Casting.h"
namespace SWTC {
class User;
class Value;

class Use {
public:
    void init(Value *V, User *U);

    Use(const Use &U) { init(U.Val, U.Parent); };

    Use() : Val(nullptr),Parent(nullptr) {}
    ~Use();
    Use *next = nullptr;
    Use *prev = nullptr;

private:
    Use(User *Parent) : Parent(Parent) {}
    Value *Val = nullptr;

    User *Parent = nullptr;

public:
    friend class Value;
    friend class User;

    Use(Value *V, User *U) { init(V, U); }

    operator Value *() const { return Val; }
    Value *get() const { return Val; }

    //fast swap
    void swap(Use &RHS) {
        if (Val == RHS.Val)
            return;

        std::swap(Val, RHS.Val);
        std::swap(next, RHS.next);
        std::swap(prev, RHS.prev);

        prev = this;
        if (next)
            next->prev = next;

        *RHS.prev = RHS;
        if (RHS.next)
            RHS.next->prev = RHS.next;
    }

    User *getUser() const { return Parent; }

    void set(Value *V);
    Value *operator=(Value *RHS) {
        set(RHS);
        return RHS;
    }
    const Use &operator=(const Use &RHS) {
        set(RHS.Val);
        return *this;
    };

    Value *operator->() { return Val; }
    const Value *operator->() const { return Val; }

    Use *getNext() const { return next; }
};

}  // namespace SWTC

#endif  // !SWTC_USE_H
