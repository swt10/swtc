#ifndef SWTC_DERIVEDTYPES_H
#define SWTC_DERIVEDTYPES_H

#include <cstdint>
#include <vector>

#include "Casting.h"
#include "Type.h"
#include "Value.h"


namespace SWTC {

class Value;

//
class IntegerType : public Type {
public:
    explicit IntegerType(unsigned NumBits) : Type(IntegerTyID) {
        setSubClassData(NumBits);
    }

public:
    static const IntegerType *get(unsigned NumBits) {
        if (NumBits == 1)
            return Type::Int1Ty;
        if (NumBits == 32)
            return Type::Int32Ty;
        return nullptr;
    };

    static bool classof(const Type *T) {
        return T->getTypeID() == IntegerTyID;
    }
};

//Function class
class FunctionType : public Type {
    std::vector<Type *> ContainedTys;  //[0] is the result type

public:
    FunctionType(Type *Result, std::vector<Type *> Params, bool isVarArg)
        : Type(Type::FunctionTyID) {
        setSubClassData(isVarArg);

        ContainedTys.push_back(Result);

        for (auto p : Params) {
            ContainedTys.push_back(p);
        }
    };

    FunctionType(const FunctionType &) = delete;
    FunctionType &operator=(const FunctionType &) = delete;

    static FunctionType *get(Type *Result, std::vector<Type *> Params, bool isVarArg) {
        return new FunctionType(Result, Params, isVarArg);
    };

    static FunctionType *get(Type *Result, bool isVarArg) {
        return new FunctionType(Result, std::vector<Type *>{}, isVarArg);
    }

    bool isVarArg() const { return getSubclassData() != 0; }

    Type *getReturnType() const {
        return ContainedTys[0];
    };

    unsigned getNumParams() const { return ContainedTys.size() - 1; }

    Type *getParamType(unsigned i) const { return ContainedTys[i + 1]; }

    static bool classof(const Type *T) {
        return T->getTypeID() == FunctionTyID;
    }
};

/// Array type class
class ArrayType : public Type {
    /// The element type of the array.
    Type *ContainedType=nullptr;
    /// Number of elements in the array.
    uint64_t NumElements;

public:
    ArrayType(Type *ElType, uint64_t NumE1)
        : Type(Type::ArrayTyID), NumElements(NumE1) {
        ContainedType = ElType;
    };
    ArrayType(const ArrayType &) = delete;
    ArrayType &operator=(const ArrayType &) = delete;

    uint64_t getNumElements() const { return NumElements; }
    Type *getElementType() const { return ContainedType; }

    /// This static method is the primary way to construct an ArrayType
    static ArrayType *get(Type *ElementType, uint64_t NumElement) {
        return new ArrayType(ElementType, NumElement);
    };

    /// Return true if the specified type is valid as a element type.
    static bool isValidElementType(Type *ElemTy) {
        return ElemTy->isIntegerTy() || ElemTy->isArrayTy();
    };

    /// Methods for support type inquiry through isa, cast, and dyn_cast.
    static bool classof(const Type *T) {
        return T->getTypeID() == ArrayTyID;
    }
};

class PointerType : public Type {
    Type *PointeeTy=nullptr;

public:
    explicit PointerType(Type *ElType)
        : Type(Type::PointerTyID), PointeeTy(ElType){};
    PointerType(const PointerType &) = delete;
    PointerType &operator=(const PointerType &) = delete;

    /// This constructs a pointer to an object of the specified type in a numbered
    /// address space.
    static PointerType *get(Type *ElementType) {
        return new PointerType(ElementType);
    };

    /// This constructs a pointer to an object of the specified type in the
    /// generic address space (address space zero).
    static PointerType *getUnqual(Type *ElementType) {
        return PointerType::get(ElementType);
    }

    Type *getElementType() const { return PointeeTy; }

    /// Return true if the specified type is valid as a element type.
    static bool isValidElementType(Type *ElemTy);

    /// Return true if we can load or store from a pointer to this type.
    static bool isLoadableOrStorableType(Type *ElemTy);

    /// Return the address space of the Pointer type.
    inline unsigned getAddressSpace() const { return getSubclassData(); }

    /// Implement support type inquiry through isa, cast, and dyn_cast.
    static bool classof(const Type *T) {
        return T->getTypeID() == PointerTyID;
    }
};

class FunctionCallee {
public:
    template <typename T, typename U = decltype(&T::getFunctionType)>
    FunctionCallee(T *Fn)
        : FnTy(Fn ? Fn->getFunctionType() : nullptr), Callee(Fn) {}

    FunctionCallee(FunctionType *FnTy, Value *Callee)
        : FnTy(FnTy), Callee(Callee) {
        assert((FnTy == nullptr) == (Callee == nullptr));
    }

    FunctionCallee(std::nullptr_t) {}

    FunctionCallee() = default;

    FunctionType *getFunctionType() { return FnTy; }

    Value *getCallee() { return Callee; }

    explicit operator bool() { return Callee; }

private:
    FunctionType *FnTy = nullptr;
    Value *Callee = nullptr;
};


}  // namespace SWTC

#endif  // SWTC_DERIVEDTYPES_H
