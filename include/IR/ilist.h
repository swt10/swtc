#pragma once
#ifndef SWTC_SUPPORT_ILIST
#define SWTC_SUPPORT_ILIST

namespace SWTC {

template <class Node>
struct ilist {
    Node *head;
    Node *tail;

    ilist() { head = tail = nullptr; }

    // get the last node
    Node *getLast() {
        return tail;
    }

    // insert newNode before insertBefore
    void insertBefore(Node *newNode, Node *insertBefore) {
        newNode->prev = insertBefore->prev;
        newNode->next = insertBefore;
        if (insertBefore->prev) {
            insertBefore->prev->next = newNode;
        }
        insertBefore->prev = newNode;

        if (head == insertBefore) {
            head = newNode;
        }
    }

    // insert newNode after insertAfter
    void insertAfter(Node *newNode, Node *insertAfter) {
        newNode->prev = insertAfter;
        newNode->next = insertAfter->next;
        if (insertAfter->next) {
            insertAfter->next->prev = newNode;
        }
        insertAfter->next = newNode;

        if (tail == insertAfter) {
            tail = newNode;
        }
    }

    // insert newNode at the end of ilist
    void insertAtEnd(Node *newNode) {
        newNode->prev = tail;
        newNode->next = nullptr;

        if (tail == nullptr) {
            head = tail = newNode;
        } else {
            tail->next = newNode;
            tail = newNode;
        }
    }

    // insert newNode at the begin of ilist
    void insertAtBegin(Node *newNode) {
        newNode->prev = nullptr;
        newNode->next = head;

        if (head == nullptr) {
            head = tail = newNode;
        } else {
            head->prev = newNode;
            head = newNode;
        }
    }

    // remove node from ilist
    void remove(Node *node) {
        if (node->prev != nullptr) {
            node->prev->next = node->next;
        } else {
            head = node->next;
        }

        if (node->next != nullptr) {
            node->next->prev = node->prev;
        } else {
            tail = node->prev;
        }
        node->next=node->prev=nullptr;
    }

    void insertilistAtEnd(ilist<Node>&l)
    {
        if(l.size())
        {
            if(head==nullptr)
            {
                head=l.head;
                tail=l.tail;
            }
            else 
            {
                tail->next=l.head;
                l.head->prev=tail;
                tail=l.tail;
            }
        }
        l.head=l.tail=nullptr;
    }
    

    void transfer(Node *pos,ilist<Node> &L2,Node *first,Node *last)
    {
        if(pos==last||first==last)return ;
        Node *Final = last->prev;
        if(first->prev)
        first->prev->next=last;
        last->prev=first->prev;
        Node *P=pos->prev;
        Final->next=pos;
        first->prev=P;
        if(P)
        P->next=first;
        pos->prev=Final;
        if(head==pos)head=first;
        if(L2.head==first)L2.head=last;
    }
    //move bst-...-bed  after Aend;
    void splice(Node *Aend,ilist<Node> &Blist,Node *Bst,Node *Bed)
    {
        if(Bst!=Bed)transfer(Aend,Blist,Bst,Bed);
    }
    void splice(Node *pos,ilist<Node> &Blist)
    {
        if(Blist.head==nullptr)return ;
        Blist.head->prev=pos->prev;
        if(pos->prev)pos->prev->next=Blist.head;
        pos->prev=Blist.tail;
        Blist.tail->next=pos;
        if(Blist.head->prev==nullptr)head=Blist.head;
        Blist.head=Blist.tail=nullptr;

    }
    int size() {
        int counter = 0;
        for (auto current = this->head; current ; current = current->next, counter += 1) {}
        return counter;
    }

    std::vector<Node*> copy()
    {
        std::vector<Node*>t;
        for(auto it=head;it;it=it->next)
        t.push_back(it);
        return t;
    }
    // 想用 for(auto x: list)
    class iterator {
		private: 
		Node * m_ptr;
		public: 

		iterator(Node* p = nullptr): m_ptr(p){}

		iterator& operator ++ (){
			m_ptr = m_ptr->next;
			return *this;
		}

		Node* operator * (){
			return m_ptr;
		}

		Node* operator->(){
			return m_ptr;
		}

		bool operator==(const iterator &arg) const {
			return arg.m_ptr == this->m_ptr;
		}
  
		bool operator!=(const iterator &arg) const {
			return arg.m_ptr != this->m_ptr;
		}
	};

	iterator begin() const {
		return iterator(head);
	}

	iterator end() const {
		return iterator();
	}
};
}  // namespace SWTC

#endif  // !SWTC_SUPPORT_ILIST
