#ifndef SWTC_INSTRUCTIONS_H
#define SWTC_INSTRUCTIONS_H

#include <cassert>

#include "Constant.h"
#include "Instruction.h"

namespace SWTC {

class Instruction;

class UnaryInstruction : public Instruction {
    Use Op;

protected:
    UnaryInstruction(Type *Ty, unsigned iType, Value *V, Instruction *IB = nullptr)
        : Instruction(Ty, iType, &Op, (unsigned)1, IB), Op(V, this) {
    }
    UnaryInstruction(Type *Ty, unsigned iType, Value *V, BasicBlock *IAE)
        : Instruction(Ty, iType, &Op, 1, IAE), Op(V, this) {
    }

public:
    ~UnaryInstruction(){};

    // Transparently provide more efficient getOperand methods.
    Value *getOperand(unsigned i) const {
        assert(i == 0 && "getOperand() out of range!");
        return Op;
    }
    void setOperand(unsigned i, Value *Val) {
        assert(i == 0 && "setOperand() out of range!");
        Op = Val;
    }
    unsigned getNumOperands() const { return 1; }
};

class BinaryOperator : public Instruction {
    Use Ops[2];

public:
    void init(Instruction::Ops iType) {
        Value *LHS = getOperand(0), *RHS = getOperand(1);
    };
    BinaryOperator(Instruction::Ops iType, Value *S1, Value *S2, Type *Ty,
                   const std::string &Name, Instruction *InsertBefore)
        : Instruction(Ty, iType, Ops, 2, InsertBefore) {
        //std::cerr << "In gettype\n";
        Ops[0].init(S1, this);
        Ops[1].init(S2, this);
        init(iType);
        setName(Name);
    };
    BinaryOperator(Instruction::Ops iType, Value *S1, Value *S2, Type *Ty,
                   const std::string &Name, BasicBlock *InsertAtEnd)
        : Instruction(Ty, iType, Ops, 2, InsertAtEnd) {
        Ops[0].init(S1, this);
        Ops[1].init(S2, this);
        init(iType);
        setName(Name);
    };

public:
    /// Transparently provide more efficient getOperand methods.
    Value *getOperand(unsigned i) const {
        assert(i < 2 && "getOperand() out of range!");
        return Ops[i];
    }
    void setOperand(unsigned i, Value *Val) {
        assert(i < 2 && "setOperand() out of range!");
        Ops[i] = Val;
    }
    unsigned getNumOperands() const { return 2; }

    /// create() - Construct a binary instruction, given the opcode and the two
    /// operands.  Optionally (if InstBefore is specified) insert the instruction
    /// into a BasicBlock right before the specified instruction.  The specified
    /// Instruction is allowed to be a dereferenced end iterator.
    ///
    static BinaryOperator *create(Instruction::Ops Op, Value *S1, Value *S2,
                                  const std::string &Name = "",
                                  Instruction *InsertBefore = nullptr) {
        return new BinaryOperator(Op, S1, S2, S1->getType(), Name, InsertBefore);
    };

    /// create() - Construct a binary instruction, given the opcode and the two
    /// operands.  Also automatically insert this instruction to the end of the
    /// BasicBlock specified.
    ///
    static BinaryOperator *create(Instruction::Ops Op, Value *S1, Value *S2,
                                  const std::string &Name,
                                  BasicBlock *InsertAtEnd) {
        return new BinaryOperator(Op, S1, S2, S1->getType(), Name, InsertAtEnd);
    };

    /// Helper functions to construct and inspect unary operations (NEG and NOT)
    /// via binary operators SUB and XOR:
    ///
    /// createNeg, createNot - Create the NEG and NOT
    ///     instructions out of SUB and XOR instructions.
    ///
    static BinaryOperator *createNeg(Value *Op, const std::string &Name = "",
                                     Instruction *InsertBefore = 0) {
        Value *zero = ConstantInt::getZeroVal();
        return new BinaryOperator(Instruction::Sub,
                                  zero, Op, Op->getType(), Name, InsertBefore);
    };
    static BinaryOperator *createNeg(Value *Op, const std::string &Name,
                                     BasicBlock *InsertAtEnd) {
        Value *zero = ConstantInt::getZeroVal();
        return new BinaryOperator(Instruction::Sub,
                                  zero, Op, Op->getType(), Name, InsertAtEnd);
    };

    /// isNeg, isNot - Check if the given Value is a NEG or NOT instruction.
    ///
    static bool isNeg(const Value *V) {
        if (const BinaryOperator *Bop = dyn_cast<BinaryOperator>(V))
            if (Bop->getOpcode() == Instruction::Sub)
                return Bop->getOperand(0) ==
                       ConstantInt::getZeroVal();
        return false;
    };
    static bool isNot(const Value *V) {
        if (const BinaryOperator *Bop = dyn_cast<BinaryOperator>(V))
            if (Bop->getOpcode() == Instruction::Xor)
                return Bop->getOperand(0) ==
                       ConstantInt::getTrue();
        return false;
    };

    /// getNegArgument, getNotArgument - Helper functions to extract the
    ///     unary argument of a NEG or NOT operation implemented via Sub or Xor.
    ///
    static const Value *getNegArgument(const Value *BinOp);
    static Value *getNegArgument(Value *BinOp);
    static const Value *getNotArgument(const Value *BinOp);
    static Value *getNotArgument(Value *BinOp);

    Instruction::Ops getOpcode() const {
        return static_cast<Instruction::Ops>(Instruction::getOpcode());
    }

    BinaryOperator *clone() const {
        return create(getOpcode(), Ops[0], Ops[1]);
    };

    /// swapOperands - Exchange the two operands to this instruction.
    /// This instruction is safe to use on any binary instruction and
    /// does not modify the semantics of the instruction.  If the
    /// instruction is order dependent (SetLT f.e.) the opcode is
    /// changed.  If the instruction cannot be reversed (ie, it's a Div),
    /// then return true.
    ///
    bool swapOperands() {
        if (!isCommutative(getOpcode()))
            return true;  // Can't commute operands
        std::swap(Ops[0], Ops[1]);
        return false;
    };

    // Methods for support type inquiry through isa, dyn_cast, and dyn_cast:
    static inline bool classof(const BinaryOperator *) { return true; }
    static inline bool classof(const Instruction *I) {
        return I->getOpcode() >= Add && I->getOpcode() <= AShr;
    }
    static inline bool classof(const Value *V) {
        return isa<Instruction>(V) && classof(dyn_cast<Instruction>(V));
    }
};

class CmpInst : public Instruction {
    CmpInst();  //do not implement
protected:
    CmpInst(Instruction::Ops op, unsigned short pred, Value *LHS, Value *RHS,
            const std::string &Name = "", Instruction *InsertBefore = nullptr)
        : Instruction(Type::Int1Ty, op, Ops, 2, InsertBefore) {
        // std::cerr<<"cmpinst\n";
        Ops[0].init(LHS, this);
        Ops[1].init(RHS, this);
        // std::cerr<<"cmpinstend\n";
        SubclassData = pred;
        setName(Name);
        // if (op == Instruction::ICmp)
        // {
        //     std::cerr<<"cmpinstend\n";
        //     const Type *Op0Ty = getOperand(0)->getType();
        //     std::cerr<<"cmpinstend0\n";
        //     const Type *Op1Ty = getOperand(1)->getType();
        //     assert(Op0Ty == Op1Ty &&
        //            "Both operands to ICmp instruction are not of the same type!");
        //     // Check that the operands are the right type
        //     std::cerr<<"cmpinstend1\n";
        //     return;
        // }
    };

    CmpInst(Instruction::Ops op, unsigned short pred, Value *LHS, Value *RHS,
            const std::string &Name, BasicBlock *InsertAtEnd)
        : Instruction(Type::Int1Ty, op, Ops, 2, InsertAtEnd) {
        Ops[0].init(LHS, this);
        Ops[1].init(RHS, this);
        SubclassData = pred;
        setName(Name);
        return;
    };

public:
    Use Ops[2];  // CmpInst instructions always have 2 operands, optimize

    static CmpInst *create(Instruction::Ops Op, unsigned short predicate, Value *S1, Value *S2,
                           const std::string &Name = "", Instruction *InsertBefore = 0);
    static CmpInst *create(Instruction::Ops Op, unsigned short predicate, Value *S1, Value *S2,
                           const std::string &Name, BasicBlock *InsertAtEnd);

    /// @brief Get the opcode casted to the right type
    Instruction::Ops getOpcode() const {
        return static_cast<Instruction::Ops>(Instruction::getOpcode());
    }
    /// The predicate for CmpInst is defined by the subclasses but stored in
    /// the SubclassData field (see Value.h).  We allow it to be fetched here
    /// as the predicate but there is no enum type for it, just the raw unsigned
    /// short. This facilitates comparison of CmpInst instances without delving
    /// into the subclasses since predicate values are distinct between the
    /// CmpInst subclasses.
    /// @brief Return the predicate for this instruction.
    unsigned short getPredicate() const {
        return SubclassData;  //TODO
    }
    virtual CmpInst *clone() const;
    /// @brief Provide more efficient getOperand methods.
    Value *getOperand(unsigned i) const {
        assert(i < 2 && "getOperand() out of range!");
        //std::cerr << "getoperand\n";
        return Ops[i];
    }
    void setOperand(unsigned i, Value *Val) {
        Ops[i] = Val;
    }

    /// @brief CmpInst instructions always have 2 operands.
    unsigned getNumOperands() const { return 2; }

    /// This is just a convenience that dispatches to the subclasses.
    /// @brief Swap the operands and adjust predicate accordingly to retain
    /// the same comparison.
    void swapOperands();

    static inline bool classof(const CmpInst *) { return true; }
    static inline bool classof(const Instruction *I) {
        return I->getOpcode() == Instruction::ICmp;
    }
    static inline bool classof(const Value *V) {
        return isa<Instruction>(V) && classof(dyn_cast<Instruction>(V));
    }
};

class AllocaInst : public UnaryInstruction {
    Type *AllocatedType=nullptr;
    AllocaInst(const AllocaInst &AI)
        : AllocaInst(dyn_cast<PointerType>(AI.getType())->getElementType(),
                     (Value *)AI.getOperand(0)) {
    }

protected:
    friend class Instruction;

    AllocaInst *clone() const {
        return new AllocaInst(*this);
    };

public:
    static Value *getAISize(Value *Amt) {
        if (!Amt)
            Amt = ConstantInt::get(Type::Int32Ty, (i32)1);
        return Amt;
    }

    explicit AllocaInst(Type *Ty, Value *ArraySize, const std::string &Name, Instruction *InsertBefore = nullptr)
        : UnaryInstruction(PointerType::get(Ty), Alloca, getAISize(ArraySize), InsertBefore) {
        setName(Name);
        setAllocatedType(Ty);
    };

    AllocaInst(Type *Ty, const std::string &Name, Instruction *InsertBefore)
        : AllocaInst(Ty, nullptr, Name, InsertBefore){};

    AllocaInst(Type *Ty, Value *ArraySize, const std::string &Name, BasicBlock *InsertAtEnd)
        : UnaryInstruction(PointerType::get(Ty), Alloca, getAISize(ArraySize), InsertAtEnd) {
        setName(Name);
        setAllocatedType(Ty);
    };

    AllocaInst(Type *Ty, const std::string &Name, BasicBlock *InsertAtEnd)
        : AllocaInst(Ty, nullptr, Name, InsertAtEnd){};
    AllocaInst(Type *Ty, Value *ArraySize)
        : AllocaInst(Ty, ArraySize, std::string(""), (BasicBlock *)nullptr){};

    Type *getAllocatedType() const { return AllocatedType; }

    void setAllocatedType(Type *Ty) { AllocatedType = Ty; }

    static inline bool classof(const AllocaInst *) { return true; }

    static bool classof(const Instruction *I) {
        return (I->getOpcode() == Instruction::Alloca);
    }
    static bool classof(const Value *V) {
        return isa<Instruction>(V) && classof(dyn_cast<Instruction>(V));
    }
};

class LoadInst : public UnaryInstruction {
    LoadInst(const LoadInst &LI)
        : UnaryInstruction(LI.getType(), Load, LI.getOperand(0)) {
    }
    bool isinvariant = false;  //如果所指向的内存位置存储的值始终不变，那么这个值为true;

public:
    LoadInst(Value *Ptr, const std::string &Name, Instruction *InsertBefore)
        : UnaryInstruction(dyn_cast<PointerType>(Ptr->getType())->getElementType(),
                           Load, Ptr, InsertBefore) {
        setName(Name);
    };
    LoadInst(Value *Ptr, const std::string &Name, BasicBlock *InsertAtEnd)
        : UnaryInstruction(dyn_cast<PointerType>(Ptr->getType())->getElementType(),
                           Load, Ptr, InsertAtEnd) {
        setName(Name);
    };
    LoadInst *clone() const {
        return new LoadInst(*this);
    };
    void setInvariant() {
        isinvariant = true;
    }
    bool getInvariant() const {
        return isinvariant;
    }
    Value *getPointerOperand() { return getOperand(0); }
    const Value *getPointerOperand() const { return getOperand(0); }
    static unsigned getPointerOperandIndex() { return 0U; }

    static inline bool classof(const LoadInst *) { return true; }
    static inline bool classof(const Instruction *I) {
        return I->getOpcode() == Instruction::Load;
    }
    static inline bool classof(const Value *V) {
        return isa<Instruction>(V) && classof(dyn_cast<Instruction>(V));
    }
};

class StoreInst : public Instruction {
    Use Ops[2];
    StoreInst(const StoreInst &SI) : Instruction(SI.getType(), Store, Ops, 2) {
        setName("store");
        Ops[0].init(SI.Ops[0], this);
        Ops[1].init(SI.Ops[1], this);
    }

public:
    StoreInst(Value *Val, Value *Ptr, Instruction *InsertBefore)
        : Instruction(Type::VoidTy, Store, Ops, 2, InsertBefore) {
        setName("store");
        Ops[0].init(Val, this);
        Ops[1].init(Ptr, this);
    }
    StoreInst(Value *Val, Value *Ptr, BasicBlock *InsertAtEnd)
        : Instruction(Type::VoidTy, Store, Ops, 2, InsertAtEnd) {
        setName("store");
        Ops[0].init(Val, this);
        Ops[1].init(Ptr, this);
    }
    virtual StoreInst *clone() const;
    Value *getOperand(unsigned i) const {
        return Ops[i];
    }
    void setOperand(unsigned i, Value *Val) {
        Ops[i] = Val;
    }
    unsigned getNumOperands() const { return 2; }
    Value *getPointerOperand() { return getOperand(1); }
    Value *getPointerOperand() const { return getOperand(1); }
    static unsigned getPointerOperandIndex() { return 1U; }

    static inline bool classof(const StoreInst *) { return true; }
    static inline bool classof(const Instruction *I) {
        return I->getOpcode() == Instruction::Store;
    }
    static inline bool classof(const Value *V) {
        return isa<Instruction>(V) && classof(dyn_cast<Instruction>(V));
    }
};

class GetElementPtrInst : public Instruction {
public:
    GetElementPtrInst(const GetElementPtrInst &GEPI)
        : Instruction(reinterpret_cast<Type *>(GEPI.getType()), GetElementPtr,
                      0, GEPI.getNumOperands()) {
        Use *OL = OperandList = new Use[NumOperands];
        Use *GEPIOL = GEPI.OperandList;
        for (unsigned i = 0, E = NumOperands; i != E; ++i)
            OL[i].init(GEPIOL[i], this);
    }

    Type *ResultElementType;
    GetElementPtrInst(Value *ptr, std::vector<Value *> idxs,
                      const std::string &Name = "", Instruction *InsertBefore = nullptr)
        : Instruction(PointerType::get(getIndexedType(ptr, idxs)), GetElementPtr,
                      nullptr, 0, InsertBefore) {
        init(ptr, idxs, Name);
    };
    GetElementPtrInst(Value *ptr, std::vector<Value *> idxs,
                      const std::string &Name = "", BasicBlock *InsertAtEnd = nullptr)
        : Instruction(PointerType::get(getIndexedType(ptr, idxs)), GetElementPtr,
                      nullptr, 0, InsertAtEnd) {
        init(ptr, idxs, Name);
    };

    void init(Value *Ptr, std::vector<Value *> IdxList, const std::string &NameStr) {
        NumOperands = 1 + IdxList.size();
        Use *OL = OperandList = new Use[NumOperands];
        OL[0].init(Ptr, this);
        for (unsigned i = 0; i < IdxList.size(); i++)
            OL[i + 1].init(IdxList[i], this);
        setName(NameStr);
    };
    ~GetElementPtrInst() {
        delete[] OperandList;
    }

public:
    static GetElementPtrInst *Create(Type *PointeeType, Value *Ptr,
                                     std::vector<Value *> IdxList,
                                     const std::string &NameStr = "",
                                     Instruction *InsertBefore = nullptr);
    static GetElementPtrInst *Create(Type *PointeeType, Value *Ptr,
                                     std::vector<Value *> IdxList,
                                     const std::string &NameStr,
                                     BasicBlock *InsertAtEnd);

    static Type *getIndexedType(Value *Ptr, std::vector<Value *> IdxList) {
        Type *ty = dyn_cast<PointerType>(Ptr->getType())->getElementType();

        if (auto *Array = dyn_cast<ArrayType>(ty)) {
            for (int i = 1; i < IdxList.size(); i++) {
                ty = Array->getElementType();
                if (ty->isArrayTy()) {
                    Array = dyn_cast<ArrayType>(ty);
                }
            }
        }
        return ty;
    }
    GetElementPtrInst *clone() const {
        return new GetElementPtrInst(*this);
    };
    Type *getSourceElementType() const {
        return dyn_cast<PointerType>(getType())->getElementType();
    }
    inline unsigned getNumIndices() const {  // Note: always non-negative
        return getNumOperands() - 1;
    }

    inline bool hasIndices() const {
        return getNumOperands() > 1;
    }
    static inline bool classof(const GetElementPtrInst *) { return true; }
    static inline bool classof(const Instruction *I) {
        return (I->getOpcode() == Instruction::GetElementPtr);
    }
    static inline bool classof(const Value *V) {
        return isa<Instruction>(V) && classof(dyn_cast<Instruction>(V));
    }
};

class ICmpInst : public CmpInst {
public:
    enum Predicate {
        ICMP_EQ = 32,   ///< equal
        ICMP_NE = 33,   ///< not equal
        ICMP_UGT = 34,  ///< unsigned greater than
        ICMP_UGE = 35,  ///< unsigned greater or equal
        ICMP_ULT = 36,  ///< unsigned less than
        ICMP_ULE = 37,  ///< unsigned less or equal
        ICMP_SGT = 38,  ///< signed greater than
        ICMP_SGE = 39,  ///< signed greater or equal
        ICMP_SLT = 40,  ///< signed less than
        ICMP_SLE = 41,  ///< signed less or equal
        FIRST_ICMP_PREDICATE = ICMP_EQ,
        LAST_ICMP_PREDICATE = ICMP_SLE,
        BAD_ICMP_PREDICATE = ICMP_SLE + 1
    };

    /// @brief Constructor with insert-before-instruction semantics.
    ICmpInst(
        Predicate pred,                      ///< The predicate to use for the comparison
        Value *LHS,                          ///< The left-hand-side of the expression
        Value *RHS,                          ///< The right-hand-side of the expression
        const std::string &Name = "",        ///< Name of the instruction
        Instruction *InsertBefore = nullptr  ///< Where to insert
        ) : CmpInst(Instruction::ICmp, pred, LHS, RHS, Name, InsertBefore) {
        std::cerr << "icmp\n";
    }

    /// @brief Constructor with insert-at-block-end semantics.
    ICmpInst(
        Predicate pred,           ///< The predicate to use for the comparison
        Value *LHS,               ///< The left-hand-side of the expression
        Value *RHS,               ///< The right-hand-side of the expression
        const std::string &Name,  ///< Name of the instruction
        BasicBlock *InsertAtEnd   ///< Block to insert into.
        ) : CmpInst(Instruction::ICmp, pred, LHS, RHS, Name, InsertAtEnd) {
    }

    /// @brief Return the predicate for this instruction.
    Predicate getPredicate() const { return Predicate(SubclassData); }

    /// @brief Set the predicate for this instruction to the specified value.
    void setPredicate(Predicate P) { SubclassData = P; }

    /// For example, EQ -> NE, UGT -> ULE, SLT -> SGE, etc.
    /// @returns the inverse predicate for the instruction's current predicate.
    /// @brief Return the inverse of the instruction's predicate.
    Predicate getInversePredicate() const {
        return getInversePredicate(getPredicate());
    }
    static BinaryOperator *createNot(Value *Op, const std::string &Name,
                                               Instruction *InsertBefore = 0) {
        Value *one = ConstantInt::getTrue();
        Value *zero = ConstantInt::getFalse();
        Value *res = new ICmpInst(ICmpInst::Predicate::ICMP_NE, Op, zero, Name, InsertBefore);
        return new BinaryOperator(Instruction::Xor, res, one, Type::Int1Ty, Name, InsertBefore);
    };
    static BinaryOperator *createNot(Value *Op, const std::string &Name,
                                               BasicBlock *InsertAtEnd) {
        Value *one = ConstantInt::getTrue();
        Value *zero = ConstantInt::getFalse();
        Value *res = new ICmpInst(ICmpInst::Predicate::ICMP_NE, Op, zero, Name, InsertAtEnd);
        return new BinaryOperator(Instruction::Xor, res, one, Type::Int1Ty, Name, InsertAtEnd);
    };

    /// For example, EQ -> NE, UGT -> ULE, SLT -> SGE, etc.
    /// @returns the inverse predicate for predicate provided in \p pred.
    /// @brief Return the inverse of a given predicate
    static Predicate getInversePredicate(Predicate pred) {
        switch (pred) {
            default:
                assert(!"icmp error");
            case ICMP_EQ:
                return ICMP_NE;
            case ICMP_NE:
                return ICMP_EQ;
            case ICMP_UGT:
                return ICMP_ULE;
            case ICMP_ULT:
                return ICMP_UGE;
            case ICMP_UGE:
                return ICMP_ULT;
            case ICMP_ULE:
                return ICMP_UGT;
            case ICMP_SGT:
                return ICMP_SLE;
            case ICMP_SLT:
                return ICMP_SGE;
            case ICMP_SGE:
                return ICMP_SLT;
            case ICMP_SLE:
                return ICMP_SGT;
        }
    };

    static bool isTrueWhenEqual(Predicate predicate) {
        switch (predicate) {
            default: return false;
            case ICMP_EQ:
            case ICMP_UGE:
            case ICMP_ULE:
            case ICMP_SGE:
            case ICMP_SLE:
                return true;
        }
    }

    static bool isFalseWhenEqual(Predicate predicate) {
        switch (predicate) {
            case ICMP_NE:
            case ICMP_UGT:
            case ICMP_ULT:
            case ICMP_SGT:
            case ICMP_SLT:
                return true;
            default: return false;
        }
    }

    static bool isImpliedTrueByMatchingCmp(Predicate Pred1, Predicate Pred2) {
        // If the predicates match, then we know the first condition implies the
        // second is true.
        if (Pred1 == Pred2)
            return true;

        switch (Pred1) {
            default:
                break;
            case ICMP_EQ:
                // A == B implies A >=u B, A <=u B, A >=s B, and A <=s B are true.
                return Pred2 == ICMP_UGE || Pred2 == ICMP_ULE || Pred2 == ICMP_SGE ||
                       Pred2 == ICMP_SLE;
            case ICMP_UGT:  // A >u B implies A != B and A >=u B are true.
                return Pred2 == ICMP_NE || Pred2 == ICMP_UGE;
            case ICMP_ULT:  // A <u B implies A != B and A <=u B are true.
                return Pred2 == ICMP_NE || Pred2 == ICMP_ULE;
            case ICMP_SGT:  // A >s B implies A != B and A >=s B are true.
                return Pred2 == ICMP_NE || Pred2 == ICMP_SGE;
            case ICMP_SLT:  // A <s B implies A != B and A <=s B are true.
                return Pred2 == ICMP_NE || Pred2 == ICMP_SLE;
        }
        return false;
    }

    /// For example, EQ->EQ, SLE->SGE, ULT->UGT, etc.
    /// @returns the predicate that would be the result of exchanging the two
    /// operands of the ICmpInst instruction without changing the result
    /// produced.
    /// @brief Return the predicate as if the operands were swapped
    Predicate getSwappedPredicate() const {
        return getSwappedPredicate(getPredicate());
    }

    /// This is a static version that you can use without an instruction
    /// available.
    /// @brief Return the predicate as if the operands were swapped.
    static Predicate getSwappedPredicate(Predicate pred) {
        switch (pred) {
            default:
                assert(!"Unknown icmp predicate!");
            case ICMP_EQ:
            case ICMP_NE:
                return pred;
            case ICMP_SGT:
                return ICMP_SLT;
            case ICMP_SLT:
                return ICMP_SGT;
            case ICMP_SGE:
                return ICMP_SLE;
            case ICMP_SLE:
                return ICMP_SGE;
            case ICMP_UGT:
                return ICMP_ULT;
            case ICMP_ULT:
                return ICMP_UGT;
            case ICMP_UGE:
                return ICMP_ULE;
            case ICMP_ULE:
                return ICMP_UGE;
        }
    }

    /// For example, EQ->EQ, SLE->SLE, UGT->SGT, etc.
    /// @returns the predicate that would be the result if the operand were
    /// regarded as signed.
    /// @brief Return the signed version of the predicate
    Predicate getSignedPredicate() const {
        return getSignedPredicate(getPredicate());
    }

    /// This is a static version that you can use without an instruction.
    /// @brief Return the signed version of the predicate.
    static Predicate getSignedPredicate(Predicate pred) {
        switch (pred) {
            default:
                assert(!"Unknown icmp predicate!");
            case ICMP_EQ:
            case ICMP_NE:
            case ICMP_SGT:
            case ICMP_SLT:
            case ICMP_SGE:
            case ICMP_SLE:
                return pred;
            case ICMP_UGT:
                return ICMP_SGT;
            case ICMP_ULT:
                return ICMP_SLT;
            case ICMP_UGE:
                return ICMP_SGE;
            case ICMP_ULE:
                return ICMP_SLE;
        }
    }
    static bool isEquality(Predicate P) {
        return P == ICMP_EQ || P == ICMP_NE;
    }

    /// This also tests for commutativity. If isEquality() returns true then
    /// the predicate is also commutative.
    /// @returns true if the predicate of this instruction is EQ or NE.
    /// @brief Determine if this is an equality predicate.
    bool isEquality() const {
        return SubclassData == ICMP_EQ || SubclassData == ICMP_NE;
    }
    /// Exchange the two operands to this instruction in such a way that it does
    /// not modify the semantics of the instruction. The predicate value may be
    /// changed to retain the same result if the predicate is order dependent
    /// (e.g. ult).
    /// @brief Swap operands and adjust predicate.

    /// @returns true if the predicate of this ICmpInst is commutative
    /// @brief Determine if this relation is commutative.
    bool isCommutative() const { return isEquality(); }

    /// @returns true if the predicate is relational (not EQ or NE).
    /// @brief Determine if this a relational predicate.
    bool isRelational() const {
        return !isEquality();
    }

    /// @returns true if the predicate of this ICmpInst is signed, false otherwise
    /// @brief Determine if this instruction's predicate is signed.
    bool isSignedPredicate() { return isSignedPredicate(getPredicate()); }

    /// @returns true if the predicate provided is signed, false otherwise
    /// @brief Determine if the predicate is signed.
    static bool isSignedPredicate(Predicate pred) {
        switch (pred) {
            default:
                assert(!"Unknown icmp predicate!");
            case ICMP_SGT:
            case ICMP_SLT:
            case ICMP_SGE:
            case ICMP_SLE:
                return true;
            case ICMP_EQ:
            case ICMP_NE:
            case ICMP_UGT:
            case ICMP_ULT:
            case ICMP_UGE:
            case ICMP_ULE:
                return false;
        }
    }

    void swapOperands() {
        SubclassData = getSwappedPredicate();
        std::swap(Ops[0], Ops[1]);
    }
    /// Return true if the predicate is SGT or UGT.
    ///
    static bool isGT(Predicate P) {
        return P == ICMP_SGT || P == ICMP_UGT;
    }

    /// Return true if the predicate is SLT or ULT.
    ///
    static bool isLT(Predicate P) {
        return P == ICMP_SLT || P == ICMP_ULT;
    }

    /// Return true if the predicate is SGE or UGE.
    ///
    static bool isGE(Predicate P) {
        return P == ICMP_SGE || P == ICMP_UGE;
    }

    /// Return true if the predicate is SLE or ULE.
    ///
    static bool isLE(Predicate P) {
        return P == ICMP_SLE || P == ICMP_ULE;
    }

    static inline bool classof(const ICmpInst *) { return true; }
    static inline bool classof(const Instruction *I) {
        return I->getOpcode() == Instruction::ICmp;
    }
    static inline bool classof(const Value *V) {
        return isa<Instruction>(V) && classof(dyn_cast<Instruction>(V));
    }
};

//===----------------------------------------------------------------------===//
/// This class represents a function call, abstracting a target
/// machine's calling convention.  This class uses low bit of the SubClassData
/// field to indicate whether or not this is a tail call.  The rest of the bits
/// hold the calling convention of the call.
///
class CallInst : public Instruction {
    CallInst(const CallInst &CI)
        : Instruction(CI.getType(), Instruction::Call, new Use[CI.getNumOperands()],
                      CI.getNumOperands()) {
        SubclassData = CI.SubclassData;
        Use *OL = OperandList;
        Use *InOL = CI.OperandList;
        for (unsigned i = 0, e = CI.getNumOperands(); i != e; ++i)
            OL[i].init(InOL[i], this);
    }
    ~CallInst() {
        delete[] OperandList;
    }
    void init(Value *Func, std::vector<Value *> Args, const std::string &Name) {
        unsigned NumParams = Args.size();
        NumOperands = NumParams + 1;
        Use *OL = OperandList = new Use[NumParams + 1];
        OL[0].init(Func, this);

        const FunctionType *FTy =
            dyn_cast<FunctionType>(dyn_cast<PointerType>(Func->getType())->getElementType());
        for (int i = 0; i < NumParams; i++) {
            OL[i + 1].init(Args[i], this);
        }

        setName(Name);
    }

public:
    /// Construct a CallInst given a range of arguments.
    /// Construct a CallInst from a range of arguments
    inline CallInst(FunctionType *Ty, Value *Func, std::vector<Value *> Args,
                    const std::string &NameStr,
                    Instruction *InsertBefore)
        : Instruction(Ty->getReturnType(), Instruction::Call, nullptr, 0, InsertBefore) {
        init(Func, Args, NameStr);
    };
    CallInst(FunctionType *ty, Value *F, std::vector<Value *> Args, const std::string &NameStr,
             BasicBlock *InsertAtEnd)
        : Instruction(ty->getReturnType(), Instruction::Call, nullptr, 0, InsertAtEnd) {
        init(F, Args, NameStr);
    };

    CallInst *clone() const {
        return new CallInst(*this);
    };
    bool isTailCall() const { return SubclassData & 1; }
    void setTailCall(bool isTailCall = true) {
        SubclassData = (SubclassData & ~1) | unsigned(isTailCall);
    }

    /// getCallingConv/setCallingConv - Get or set the calling convention of this
    /// function call.
    unsigned getCallingConv() const { return SubclassData >> 1; }
    void setCallingConv(unsigned CC) {
        SubclassData = (SubclassData & 1) | (CC << 1);
    }

    /// getCalledFunction - Return the function being called by this instruction
    /// if it is a direct call.  If it is a call through a function pointer,
    /// return null.
    Function *getCalledFunction() const {
        return static_cast<Function *>(dyn_cast<Function>(getOperand(0)));
    }

    bool doesNotAccessMemory() {
        return getCalledFunction()->doesNotAccessMemory();
    }

    /// getCalledValue - Get a pointer to the function that is invoked by this
    /// instruction
    inline const Value *getCalledValue() const { return getOperand(0); }
    inline Value *getCalledValue() { return getOperand(0); }

    static inline bool classof(const CallInst *) { return true; }
    static inline bool classof(const Instruction *I) {
        return I->getOpcode() == Instruction::Call;
    }
    static inline bool classof(const Value *V) {
        return isa<Instruction>(V) && classof(dyn_cast<Instruction>(V));
    }
};

class PHINode : public Instruction {
    /// ReservedSpace - The number of operands actually allocated.  NumOperands is
    /// the number actually in use.
    unsigned ReservedSpace=0;

public:
    PHINode(const PHINode &PN)
        : Instruction(PN.getType(), Instruction::PHI,
                      new Use[PN.getNumOperands()], PN.getNumOperands()),
          ReservedSpace(PN.getNumOperands()) {
        Use *OL = OperandList;
        for (unsigned i = 0, e = PN.getNumOperands(); i != e; i += 2) {
            OL[i].init(PN.getOperand(i), this);
            OL[i + 1].init(PN.getOperand(i + 1), this);
        }
    }

    explicit PHINode(Type *Ty, const std::string &Name = "",
                     Instruction *InsertBefore = nullptr)
        : Instruction(Ty, Instruction::PHI, 0, 0, InsertBefore),
          ReservedSpace(0) {
        setName(Name);
    }

    PHINode(Type *Ty, const std::string &Name, BasicBlock *InsertAtEnd)
        : Instruction(Ty, Instruction::PHI, 0, 0, InsertAtEnd),
          ReservedSpace(0) {
        setName(Name);
    }

    ~PHINode() {
        delete[] OperandList;
    }

    /// reserveOperandSpace - This method can be used to avoid repeated
    /// reallocation of PHI operand lists by reserving space for the correct
    /// number of operands before adding them.  Unlike normal vector reserves,
    /// this method can also be used to trim the operand space.
    void reserveOperandSpace(unsigned NumValues) {
        resizeOperands(NumValues * 2);
    }
    PHINode *clone() const {
        return new PHINode(*this);
    };

    /// getNumIncomingValues - Return the number of incoming edges
    ///
    unsigned getNumIncomingValues() const { return getNumOperands() / 2; }

    /// getIncomingValue - Return incoming value number x
    ///
    Value *getIncomingValue(unsigned i) const {
        return getOperand(i * 2);
    }
    void setIncomingValue(unsigned i, Value *V) {
        setOperand(i * 2, V);
    }
    unsigned getOperandNumForIncomingValue(unsigned i) {
        return i * 2;
    }

    /// getIncomingBlock - Return incoming basic block number x
    ///
    BasicBlock *getIncomingBlock(unsigned i) const {
        return reinterpret_cast<BasicBlock *>(getOperand(i * 2 + 1));
    }
    BasicBlock *getIncomingBlock(Use *u) const {
        Use *OL = OperandList;
        for (int i = 0; i < NumOperands; i++)
            if (OL[i].get() == u->get())
                return getIncomingBlock(i >> 1);
        return nullptr;
    }
    void setIncomingBlock(unsigned i, BasicBlock *BB) {
        setOperand(i * 2 + 1, reinterpret_cast<Value *>(BB));
    }
    unsigned getOperandNumForIncomingBlock(unsigned i) {
        return i * 2 + 1;
    }

    /// addIncoming - Add an incoming value to the end of the PHI list
    ///
    void addIncoming(Value *V, BasicBlock *BB) {
        unsigned OpNo = NumOperands;
        if (OpNo + 2 > ReservedSpace)
            resizeOperands(0);  // Get more space!
        // Initialize some new operands.
        NumOperands = OpNo + 2;
        OperandList[OpNo].init(V, this);
        OperandList[OpNo + 1].init(reinterpret_cast<Value *>(BB), this);
    }

    /// removeIncomingValue - Remove an incoming value.  This is useful if a
    /// predecessor basic block is deleted.  The value removed is returned.
    ///
    /// If the last incoming value for a PHI node is removed (and DeletePHIIfEmpty
    /// is true), the PHI node is destroyed and any uses of it are replaced with
    /// dummy values.  The only time there should be zero incoming values to a PHI
    /// node is when the block is dead, so this strategy is sound.
    ///
    Value *removeIncomingValue(unsigned Idx, bool DeletePHIIfEmpty = true);

    Value *removeIncomingValue(const BasicBlock *BB, bool DeletePHIIfEmpty = true) {
        int Idx = getBasicBlockIndex(BB);
        return removeIncomingValue(Idx, DeletePHIIfEmpty);
    }

    /// getBasicBlockIndex - Return the first index of the specified basic
    /// block in the value list for this PHI.  Returns -1 if no instance.
    ///
    int getBasicBlockIndex(const BasicBlock *BB) const {
        Use *OL = OperandList;
        for (unsigned i = 0, e = getNumOperands(); i != e; i += 2)
            if (OL[i + 1] == reinterpret_cast<const Value *>(BB))
                return i / 2;
        return -1;
    }

    Value *getIncomingValueForBlock(const BasicBlock *BB) const {
        return getIncomingValue(getBasicBlockIndex(BB));
    }

    /// hasConstantValue - If the specified PHI node always merges together the
    /// same value, return the value, otherwise return null.
    ///
    Value *hasConstantValue() const;

    /// Methods for support type inquiry through isa, dyn_cast, and dyn_cast:
    static inline bool classof(const PHINode *) { return true; }
    static inline bool classof(const Instruction *I) {
        return I->getOpcode() == Instruction::PHI;
    }
    static inline bool classof(const Value *V) {
        return isa<Instruction>(V) && classof(dyn_cast<Instruction>(V));
    }

private:
    void resizeOperands(unsigned NumofOp) {
        if (NumofOp == 0) {
            NumofOp = (getNumOperands()) * 3 / 2;
            if (NumofOp < 4) NumofOp = 4;
        } else if (NumofOp * 2 > NumOperands) {
            if (ReservedSpace >= NumofOp) return;
        } else if (NumofOp == NumOperands) {
            if (ReservedSpace == NumofOp) return;
        } else {
            return;
        }

        ReservedSpace = NumofOp;
        Use *NewOps = new Use[NumofOp];
        Use *OldOps = OperandList;
        for (int i = 0, e = getNumOperands(); i < e; i++) {
            NewOps[i].init(OldOps[i], this);
            OldOps[i].set(nullptr);
        }
        delete[] OldOps;
        OperandList = NewOps;
    }
};

class ReturnInst : public Instruction {
    Use RetVal;  // Return Value: null if 'void'.
    ReturnInst(const ReturnInst &RI)
        : Instruction(Module::getVoidTy(), Instruction::Ret,
                      &RetVal, RI.getNumOperands()) {
        setName("return");
        if (RI.getNumOperands())
            RetVal.init(RI.RetVal, this);
    }

    void init(Value *retVal) {
        if (retVal && retVal->getType() != Module::getVoidTy()) {
            NumOperands = 1;
            RetVal.init(retVal, this);
        }
    }

public:
    // ReturnInst constructors:
    // ReturnInst()                  - 'ret void' instruction
    // ReturnInst(    null)          - 'ret void' instruction
    // ReturnInst(Value* X)          - 'ret X'    instruction
    // ReturnInst(    null, Inst *)  - 'ret void' instruction, insert before I
    // ReturnInst(Value* X, Inst *I) - 'ret X'    instruction, insert before I
    // ReturnInst(    null, BB *B)   - 'ret void' instruction, insert @ end of BB
    // ReturnInst(Value* X, BB *B)   - 'ret X'    instruction, insert @ end of BB
    //
    // NOTE: If the Value* passed is of type void then the constructor behaves as
    // if it was passed NULL.
    explicit ReturnInst(Value *retVal = nullptr, Instruction *InsertBefore = nullptr)
        : Instruction(Module::getVoidTy(), Instruction::Ret, &RetVal, 0, InsertBefore) {
        setName("return");
        init(retVal);
    }
    ReturnInst(Value *retVal, BasicBlock *InsertAtEnd)
        : Instruction(Module::getVoidTy(), Instruction::Ret, &RetVal, 0, InsertAtEnd) {
        setName("return");
        init(retVal);
    }
    explicit ReturnInst(BasicBlock *InsertAtEnd)
        : Instruction(Module::getVoidTy(), Instruction::Ret, &RetVal, 0, InsertAtEnd) {
        setName("return");
    }

    Value *getOperand(unsigned i) const {
        return RetVal;
    }
    void setOperand(unsigned i, Value *Val) {
        RetVal = Val;
    }
    virtual ReturnInst *clone() const;

    /// Convenience accessor. Returns null if there is no return value.
    Value *getReturnValue() const {
        return getNumOperands() != 0 ? getOperand(0) : nullptr;
    }

    unsigned getNumSuccessors() const { return 0; }

    // Methods for support type inquiry through isa, dyn_cast, and dyn_cast:
    static bool classof(const Instruction *I) {
        return (I->getOpcode() == Instruction::Ret);
    }
    static bool classof(const Value *V) {
        return isa<Instruction>(V) && classof(dyn_cast<Instruction>(V));
    }
};

class BranchInst : public Instruction {
    /// Ops list - Branches are strange.  The operands are ordered:
    ///  TrueDest, FalseDest, Cond.  This makes some accessors faster because
    /// they don't have to check for cond/uncond branchness.
    Use Ops[3];
    BranchInst(const BranchInst &BI)
        : Instruction(Type::VoidTy, Instruction::Br, Ops, BI.getNumOperands()) {
        setName("br");
        OperandList[0].init(BI.getOperand(0), this);
        if (BI.getNumOperands() != 1) {
            assert(BI.getNumOperands() == 3 && "BR can have 1 or 3 operands!");
            OperandList[1].init(BI.getOperand(1), this);
            OperandList[2].init(BI.getOperand(2), this);
        }
    }

public:
    // BranchInst constructors (where {B, T, F} are blocks, and C is a condition):
    // BranchInst(BB *B)                           - 'br B'
    // BranchInst(BB* T, BB *F, Value *C)          - 'br C, T, F'
    // BranchInst(BB* B, Inst *I)                  - 'br B'        insert before I
    // BranchInst(BB* T, BB *F, Value *C, Inst *I) - 'br C, T, F', insert before I
    // BranchInst(BB* B, BB *I)                    - 'br B'        insert at end
    // BranchInst(BB* T, BB *F, Value *C, BB *I)   - 'br C, T, F', insert at end

    explicit BranchInst(BasicBlock *IfTrue, Instruction *InsertBefore = nullptr)
        : Instruction(Type::VoidTy, Instruction::Br, Ops, 1, InsertBefore) {
        setName("br");
        Ops[0].init(reinterpret_cast<Value *>(IfTrue), this);
    }
    BranchInst(BasicBlock *IfTrue, BasicBlock *IfFalse, Value *Cond,
               Instruction *InsertBefore = nullptr)
        : Instruction(Type::VoidTy, Instruction::Br, Ops, 3, InsertBefore) {
        setName("br");
        Ops[0].init(reinterpret_cast<Value *>(IfTrue), this);
        Ops[1].init(reinterpret_cast<Value *>(IfFalse), this);
        Ops[2].init(Cond, this);
    }
    BranchInst(BasicBlock *IfTrue, BasicBlock *InsertAtEnd)
        : Instruction(Type::VoidTy, Instruction::Br, Ops, 1, InsertAtEnd) {
        setName("br");
        Ops[0].init(reinterpret_cast<Value *>(IfTrue), this);
    }
    BranchInst(BasicBlock *IfTrue, BasicBlock *IfFalse, Value *Cond,
               BasicBlock *InsertAtEnd)
        : Instruction(Type::VoidTy, Instruction::Br, Ops, 3, InsertAtEnd) {
        setName("br");
        Ops[0].init(reinterpret_cast<Value *>(IfTrue), this);
        Ops[1].init(reinterpret_cast<Value *>(IfFalse), this);
        Ops[2].init(Cond, this);
    }

    /// Transparently provide more efficient getOperand methods.
    Value *getOperand(unsigned i) const {
        return Ops[i];
    }
    void setOperand(unsigned i, Value *Val) {
        Ops[i] = Val;
    }

    BranchInst *clone() const {
        return new BranchInst(*this);
    }

    inline bool isUnconditional() const { return getNumOperands() == 1; }
    inline bool isConditional() const { return getNumOperands() == 3; }

    inline Value *getCondition() const {
        return getOperand(2);
    }

    void setCondition(Value *V) {
        setOperand(2, V);
    }

    // setUnconditionalDest - Change the current branch to an unconditional branch
    // targeting the specified block.
    // FIXME: Eliminate this ugly method.
    void setUnconditionalDest(BasicBlock *Dest) {
        if (isConditional()) {  // Convert this to an uncond branch.
            NumOperands = 1;
            Ops[1].set(0);
            Ops[2].set(0);
        }
        setOperand(0, reinterpret_cast<Value *>(Dest));
    }

    unsigned getNumSuccessors() const { return 1 + isConditional(); }

    BasicBlock *getSuccessor(unsigned i) const {
        return (i == 0) ? dyn_cast<BasicBlock>(getOperand(0)) : dyn_cast<BasicBlock>(getOperand(1));
    }

    void setSuccessor(unsigned idx, BasicBlock *NewSucc) {
        setOperand(idx, reinterpret_cast<Value *>(NewSucc));
    }

    // Methods for support type inquiry through isa, dyn_cast, and dyn_cast:
    static inline bool classof(const BranchInst *) { return true; }
    static inline bool classof(const Instruction *I) {
        return (I->getOpcode() == Instruction::Br);
    }
    static inline bool classof(const Value *V) {
        return isa<Instruction>(V) && classof(dyn_cast<Instruction>(V));
    }
};

class MoveInst : public Instruction {
public:
    Value *Dst=nullptr;
    Use Src;
    MoveInst(const MoveInst &MI)
        : Instruction(Module::getVoidTy(), Instruction::Move, &Src, 1) {
        setName("move");
        Src.init(MI.getOperand(1), this);
        Dst = MI.getOperand(0);
    }

    void init(Value *Va, Value *Vb) {
        NumOperands = 1;
        Src.init(Vb, this);
        Dst = Va;
    }

public:
    explicit MoveInst(Value *Va = nullptr, Value *Vb = nullptr, Instruction *InsertBefore = nullptr)
        : Instruction(Module::getVoidTy(), Instruction::Move, &Src, 1, InsertBefore) {
        setName("move");
        init(Va, Vb);
    }
    MoveInst(Value *Va, Value *Vb, BasicBlock *InsertAtEnd)
        : Instruction(Module::getVoidTy(), Instruction::Move, &Src, 1, InsertAtEnd) {
        setName("move");
        init(Va, Vb);
    }

    MoveInst *clone() const {
        return new MoveInst(*this);
    }

    Value *getOperand(unsigned i) const {
        if (i == 0)
            return Dst;
        else
            return Src;
    }
    void setOperand(unsigned i, Value *Val) {
        if (i == 0)
            Dst = Val;
        else
            Src = Val;
    }

    // Methods for support type inquiry through isa, dyn_cast, and dyn_cast:
    static bool classof(const Instruction *I) {
        return (I->getOpcode() == Instruction::Move);
    }
    static bool classof(const Value *V) {
        return isa<Instruction>(V) && classof(dyn_cast<Instruction>(V));
    }
};  // namespace SWTC

class MLAInst : public Instruction {
    Use Ops[3];

protected:
    void init(Instruction::Ops iType) {
        Value *LHS = getOperand(0), *RHS = getOperand(1);
    };
    MLAInst(Instruction::Ops iType, Value *S1, Value *S2, Value *S3, Type *Ty,
            const std::string &Name, Instruction *InsertBefore)
        : Instruction(Ty, iType, Ops, 3, InsertBefore) {
        Ops[0].init(S1, this);
        Ops[1].init(S2, this);
        Ops[2].init(S3, this);
        init(iType);
        setName(Name);
    };
    MLAInst(Instruction::Ops iType, Value *S1, Value *S2, Value *S3, Type *Ty,
            const std::string &Name, BasicBlock *InsertAtEnd)
        : Instruction(Ty, iType, Ops, 3, InsertAtEnd) {
        Ops[0].init(S1, this);
        Ops[1].init(S2, this);
        Ops[3].init(S3, this);
        init(iType);
        setName(Name);
    };

public:
    /// Transparently provide more efficient getOperand methods.
    Value *getOperand(unsigned i) const {
        return Ops[i];
    }
    void setOperand(unsigned i, Value *Val) {
        Ops[i] = Val;
    }
    unsigned getNumOperands() const { return 3; }

    /// create() - Construct a binary instruction, given the opcode and the two
    /// operands.  Optionally (if InstBefore is specified) insert the instruction
    /// into a BasicBlock right before the specified instruction.  The specified
    /// Instruction is allowed to be a dereferenced end iterator.
    ///
    static MLAInst *create(Instruction::Ops Op, Value *S1, Value *S2, Value *S3,
                           const std::string &Name = "",
                           Instruction *InsertBefore = nullptr) {
        return new MLAInst(Op, S1, S2, S3, S1->getType(), Name, InsertBefore);
    };

    /// create() - Construct a binary instruction, given the opcode and the two
    /// operands.  Also automatically insert this instruction to the end of the
    /// BasicBlock specified.
    ///
    static MLAInst *create(Instruction::Ops Op, Value *S1, Value *S2, Value *S3,
                           const std::string &Name,
                           BasicBlock *InsertAtEnd) {
        return new MLAInst(Op, S1, S2, S3, S1->getType(), Name, InsertAtEnd);
    };

    Instruction::Ops getOpcode() const {
        return static_cast<Instruction::Ops>(Instruction::getOpcode());
    }

    MLAInst *clone() const {
        return create(getOpcode(), Ops[0], Ops[1], Ops[2]);
    };

    // Methods for support type inquiry through isa, dyn_cast, and dyn_cast:
    static inline bool classof(const Instruction *I) {
        return I->getOpcode() == MLA;
    }
    static inline bool classof(const Value *V) {
        return isa<Instruction>(V) && classof(dyn_cast<Instruction>(V));
    }
};

}  // namespace SWTC

#endif  // !SWTC_INSTRUCTIONS_H
